wow = new WOW(
    {
        boxClass: 'wow',      // default
        animateClass: 'animated', // default
        offset: 0,          // default
        mobile: true,       // default
        live: true        // default
    }
)
wow.init();

$(document).ready(function () {
    if($('.flexslider').length > 0) {
        $('.flexslider').flexslider({
            animation: "slide"
        });
    }
});

$(document).ready(function () {
    $('.gallery-link').on('click', function () {
        $(this).find('.gallery').magnificPopup('open');
    });

    $('.gallery').each(function () {
        //alert($(this).attr('id'));
        $(this).magnificPopup({
            delegate: '.gallery-image-link',
            type: 'image',
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function
            }
        });
    });
});

$(document).ready(function() {
    $('#Carousel').carousel({
        interval: 5000
    });
});

