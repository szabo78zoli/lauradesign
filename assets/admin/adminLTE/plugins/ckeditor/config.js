/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    config.removeDialogTabs = 'image:Upload;link:Upload';

    config.filebrowserBrowseUrl = 'assets/admin/adminLTE/plugins/filemanager/dialog.php?type=2&editor=ckeditor&fldr=';
    config.filebrowserUploadUrl = 'assets/admin/adminLTE/plugins/filemanager/dialog.php?type=2&editor=ckeditor&fldr=';
    config.filebrowserImageBrowseUrl = 'assets/admin/adminLTE/plugins/filemanager/dialog.php?type=1&editor=ckeditor&fldr=';

    config.entities_latin = false;

};