/*********************************************************/
/* LOGIN CHECKBOX 										 */
/*********************************************************/
/*$(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
});*/



/*********************************************************/
/* LOGIN CHECKBOX 										 */
/*********************************************************/
$(function() {
	$('.icp-auto').iconpicker();
});
/*********************************************************/
/* CHECKBOX MIND KILELÖLÉSE								 */
/*********************************************************/
$(".checkall").click(function () {
	$(".checkbox").prop('checked', $(this).prop('checked'));
});
/*********************************************************/
/* JOGOSULTSÁG BETÖLTÉS AJAX-AL 						 */
/*********************************************************/

//if($('[name="RightGroupId"]').val() != 0) {
    $('[name="AdminRightGroupSiteType"]').change(function () {
        $.post('ajax_right/' + $('[name="AdminRightGroupSiteType"]').val() + "/" + $('[name="RightGroupId"]').val(),
            function (data) {
                $('.rights').html(data);
            }
        );
    });
/*}
else {
    $('[name="AdminRightGroupSiteType"]').change(function () {
        alert("11");
        $.post('ajax_right/' + $('[name="AdminRightGroupSiteType"]').val(),
            function (data) {
                $('.rights').html(data);
            }
        );
    });
}*/

$(document).ready(function() {
	$('.converted-charachters-source').blur(function(){
		$.post('ajax_convert_accented_characters/'+$('.converted-charachters-source').val(),
				function(data) {
					$('.converted-charachters-destination').val(data.toLowerCase());
				}
		);
	});


	$('.converted-charachters-source2').blur(function(){
		$.post('ajax_convert_accented_characters/'+$(this).val(),
			function(data) {
				$('.converted-charachters-destination2').val(data.toLowerCase());
			}
		);
	});
});


	$('.start_date').daterangepicker({
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
		timePickerIncrement: 5,
		timePickerSeconds: true,
		autoUpdateInput: false,
		locale: {
			format: 'YYYY-MM-DD HH:mm:ss',
			"applyLabel": "Alkalmaz",
			"cancelLabel": "Mégsem",
			"fromLabel": "Tól",
			"toLabel": "Ig",
			"customRangeLabel": "Egyéni",
			"daysOfWeek": ["V",	"H", "K", "Sz",	"Cs", "P", "Sz"],
			"monthNames": ["Január", "Február",	"Március", "Április", "Május", "Június", "Július", "Augusztus",	"Szeptember", "Október", "November", "December"],
			"firstDay": 1
		}
	}, function(chosen_date) {
		$('.start_date').val(chosen_date.format('YYYY-MM-DD HH:mm:ss'));
	});

	$('.end_date').daterangepicker({
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
		timePickerIncrement: 5,
		timePickerSeconds: true,
		autoUpdateInput: false,
		locale: {
			format: 'YYYY-MM-DD HH:mm:ss',
			"applyLabel": "Alkalmaz",
			"cancelLabel": "Mégsem",
			"fromLabel": "Tól",
			"toLabel": "Ig",
			"customRangeLabel": "Egyéni",
			"daysOfWeek": ["V",	"H", "K", "Sz",	"Cs", "P", "Sz"],
			"monthNames": ["Január", "Február",	"Március", "Április", "Május", "Június", "Július", "Augusztus",	"Szeptember", "Október", "November", "December"],
			"firstDay": 1
		}
	}, function(chosen_date) {
		$('.end_date').val(chosen_date.format('YYYY-MM-DD HH:mm:ss'));
	});


	$('.date').daterangepicker({
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
		timePickerIncrement: 5,
		timePickerSeconds: true,
		autoUpdateInput: false,
		locale: {
			format: 'YYYY-MM-DD HH:mm:ss',
			"applyLabel": "Alkalmaz",
			"cancelLabel": "Mégsem",
			"fromLabel": "Tól",
			"toLabel": "Ig",
			"customRangeLabel": "Egyéni",
			"daysOfWeek": ["V",	"H", "K", "Sz",	"Cs", "P", "Sz"],
			"monthNames": ["Január", "Február",	"Március", "Április", "Május", "Június", "Július", "Augusztus",	"Szeptember", "Október", "November", "December"],
			"firstDay": 1
		}
	}, function(chosen_date) {
		$('.date').val(chosen_date.format('YYYY-MM-DD HH:mm:ss'));
	});


$(function () {
	if($('#ContentShortDescription').length > 0){
		CKEDITOR.replace('ContentShortDescription')
	}
	if($('#ContentDescription').length > 0){
		CKEDITOR.replace('ContentDescription')
	}
	if($('#ContentContent').length > 0){
		CKEDITOR.replace('ContentContent');
	}
    if($('#ProjectDescription').length > 0){
        CKEDITOR.replace('ProjectDescription');
    }
    if($('#SliderDescription').length > 0){
        CKEDITOR.replace('SliderDescription');
    }
    if($('#ProductCategoryDescription').length > 0){
        CKEDITOR.replace('ProductCategoryDescription');
    }
    if($('#ProductCategoryContent').length > 0){
        CKEDITOR.replace('ProductCategoryContent');
    }
	if($('#OfferDescription').length > 0){
		CKEDITOR.replace('OfferDescription')
	}
	if($('#OfferContent').length > 0){
		CKEDITOR.replace('OfferContent');
	}
});
/*********************************************************/
/* tartalom módosítások elrejtése, felfedése			 */
/*********************************************************/
$(".modified_button").click(function() {
	$(".modified").toggle("slow");
});

/*PROJECT KÉP FELTÖLTŐ*/
$(document).ready(function() {
    $("#UserSiteProjectGallery").on('change', function() {
        //Check File API support
        if (window.File && window.FileList && window.FileReader) {

            var files = event.target.files; //FileList object
            var output = document.getElementById("imageResult");

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                //Only pics
                if (!file.type.match('image')) continue;

                var picReader = new FileReader();
                picReader.addEventListener("load", function (event) {
                    var picFile = event.target;
                    var div = document.createElement("div");
                    $(div).css("float", "left");
                    $(div).css("margin-bottom", "10px");
                    div.innerHTML = "<img style='height: 155px; width: 155px; object-fit: cover; border: 1px solid #dddddd; padding: 4px; margin-bottom: 0px;' class='thumbnail loadedimagegallerylistimage' src='" + picFile.result + "'" + "title='" + picFile.name + "'/><!--label>Leírás</label><br/><input type='text' class='Projectgalleryimagesignature"+i+"' name='ProjectGalleryImageSignature[]' /-->";
                    output.insertBefore(div, null);
                });
                //Read the image
                picReader.readAsDataURL(file);
            }
        } else {
            console.log("A böngésző nem támogatja a file feltöltést!");
        }
    });
});
$(document).ready(function() {
    $(document).on("click", ".imageDelete", function(e) {
        $.post('ajax_imagegallerydelete/'+$(this).val(),
            function(data) {
                $('.imagegallery').html(data);
            });
    });
});

/*KÉP FELTÖLTŐ*/
$(document).ready(function() {
    $("#GalleryImage").on('change', function() {
        //Check File API support
        if (window.File && window.FileList && window.FileReader) {

            var files = event.target.files; //FileList object
            var output = document.getElementById("imageResult");

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                //Only pics
                if (!file.type.match('image')) continue;

                var picReader = new FileReader();
                picReader.addEventListener("load", function (event) {
                    var picFile = event.target;
                    var div = document.createElement("div");
                    $(div).css("float", "left");
                    div.innerHTML = "<img style='margin-bottom: 0px; height: 100px;' class='thumbnail' src='" + picFile.result + "'" + "title='" + picFile.name + "'/><label>Leírás</label><br/><input type='text' class='ProductGalleryImageSignature"+i+"' name='ProductGalleryImageSignature[]' /><br/><!--a class='imageDelete'>Törlés</a-->";
                    output.insertBefore(div, null);
                });
                //Read the image
                picReader.readAsDataURL(file);
            }
        } else {
            console.log("A böngésző nem támogatja a file feltöltést!");
        }
    });
});

/*KÉP TÖRLÉSE*/
$(document).ready(function() {
    $(document).on("click", ".imageDelete", function(e) {
        $.post('ajax_imagegallerydelete/'+$(this).val(),
            function(data) {
                $('.imagegallery').html(data);
            });
    });
});

/*PROJECT KÉP TÖRLÉSE*/
$(document).ready(function() {
    $(document).on("click", ".projectImageDelete", function(e) {
        $this = $(this);
        $.post('ajax_projectimagegallerydelete/'+$(this).val(),
            function(data) {
                $this.parent().remove();
            });
    });
});

$(document).ready(function() {
    $(document).on("change", "[name='UserSiteProjectSelect']", function(e) {
        $this = $(this);
        $.post('ajax_select_version/'+$(this).val(),
            function(data) {
				$this.parent().parent().next().children().children().html(data);
            });
    });
});

$(document).ready(function () {
	$('.toggleBtn').on('click', function(){
		$(this).closest('tr').next().toggle();
	});
});