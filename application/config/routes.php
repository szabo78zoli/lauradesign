<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/*SITE*/
$route['(:any)/changelang/(:any)/(:any)'] = 'start/site/site_start_show';


$route['fooldal'] = 'start/site/site_start_show';
$route['home'] = 'start/site/site_start_show';
$route['tartalom/(:any)'] = 'content/site/content_show/show/$1';
$route['galeria'] = 'gallery/site/gallery_category_show/show';
$route['galeria/(:any)'] = 'gallery/site/gallery_show/show/$1';
$route['panorama'] = 'panorama/site/panorama_show/show';
$route['panorama/(:any)'] = 'panorama/site/panorama_show/show/$1';
$route['projekt'] = 'user/site/project_show/show';
$route['csomagajanlat/(:any)'] = 'offers/site/offer_show/show/$1';

$route['ajanlatkero'] = 'contract/site/contract_show/index';
$route['kapcsolat'] = 'contact/site/contact_show/index';
$route['ajanlatkeres'] = 'priceoffer/site/priceoffer_show/index';
$route['ajandekkartya'] = 'giftcard/site/giftcard_show/index';

$route['bejelentkezes'] = 'loginout/site/site_loginout_edit';
$route['kijelentkezes'] = 'loginout/site/site_loginout_edit/logout';

/*SITE END*/
/*ADMIN*/

$route['admin'] = 'loginout/admin/admin_loginout_edit';
$route['admin/changelang/(:any)'] = 'loginout/admin/admin_loginout_edit';
$route['admin/login'] = 'loginout/admin/admin_loginout_edit';
$route['admin/logout'] = 'loginout/admin/admin_loginout_edit/logout';

$route['admin/adminmenuedit'] = 'menu/admin/admin_menu_edit';
$route['admin/adminmenuedit/(:num)'] = 'menu/admin/admin_menu_edit/editorLoad/$1';
$route['admin/adminmenulist'] = 'menu/admin/admin_menu_list';
$route['admin/adminmenulist/(:num)'] = 'menu/admin/admin_menu_list/index/$1';

$route['admin/sitemenuedit'] = 'menu/admin/site_menu_edit';
$route['admin/sitemenuedit/(:num)'] = 'menu/admin/site_menu_edit/editorLoad/$1';
$route['admin/sitemenulist'] = 'menu/admin/site_menu_list';
$route['admin/sitemenulist/(:num)'] = 'menu/admin/site_menu_list/index/$1';

$route['admin/contentlangededit'] = 'content/admin/contentlanged_edit';
$route['admin/contentlangededit/(:num)'] = 'content/admin/contentlanged_edit/editorLoad/$1';
$route['admin/contentlangededit/(:num)/(:num)'] = 'content/admin/contentlanged_edit/editorLoad/$1/$2';
$route['admin/contentlangedlist'] = 'content/admin/contentlanged_list';
$route['admin/contentlangedlist/(:num)'] = 'content/admin/contentlanged_list/index/$1';

$route['admin/contentedit'] = 'content/admin/content2_edit';
$route['admin/contentedit/(:num)'] = 'content/admin/content2_edit/editorLoad/$1';
$route['admin/contentedit/(:num)/(:num)'] = 'content/admin/content2_edit/editorLoad/$1/$2';
$route['admin/contentlist'] = 'content/admin/content2_list';
$route['admin/contentlist/(:num)'] = 'content/admin/content2_list/index/$1';

$route['admin/contentcategoryedit'] = 'content/admin/content_category_edit';
$route['admin/contentcategoryedit/(:num)'] = 'content/admin/content_category_edit/editorLoad/$1';
$route['admin/contentcategorylist'] = 'content/admin/content_category_list';
$route['admin/contentcategorylist/(:num)'] = 'content/admin/content_category_list/index/$1';

$route['admin/contentlayoutedit'] = 'content/admin/content_layout_edit';
$route['admin/contentlayoutedit/(:num)'] = 'content/admin/content_layout_edit/editorLoad/$1';
$route['admin/contentlayoutlist'] = 'content/admin/content_layout_list';
$route['admin/contentlayoutlist/(:num)'] = 'content/admin/content_layout_list/index/$1';

$route['admin/rightedit'] = 'right/admin/right_edit';
$route['admin/rightedit/(:num)'] = 'right/admin/right_edit/editorLoad/$1';
$route['admin/rightlist'] = 'right/admin/right_list';
$route['admin/rightlist/(:num)'] = 'right/admin/right_list/index/$1';

$route['admin/rightgroupedit'] = 'right/admin/right_group_edit';
$route['admin/rightgroupedit/(:num)'] = 'right/admin/right_group_edit/editorLoad/$1';
$route['admin/rightgrouplist'] = 'right/admin/right_group_list';
$route['admin/rightgrouplist/(:num)'] = 'right/admin/right_group_list/index/$1';

$route['admin/optionsedit'] = 'options/admin/options_edit';
$route['admin/optionsedit/(:num)'] = 'options/admin/options_edit/editorLoad/$1';
$route['admin/optionslist'] = 'options/admin/options_list';
$route['admin/optionslist/(:num)'] = 'options/admin/options_list/index/$1';

$route['admin/useradminedit'] = 'user/admin/user_admin_edit';
$route['admin/useradminedit/(:num)'] = 'user/admin/user_admin_edit/editorLoad/$1';
$route['admin/useradminlist'] = 'user/admin/user_admin_list';
$route['admin/useradminlist/(:num)'] = 'user/admin/user_admin_list/index/$1';

$route['admin/usersiteedit'] = 'user/admin/user_site_edit';
$route['admin/usersiteedit/(:num)'] = 'user/admin/user_site_edit/editorLoad/$1';
$route['admin/usersitelist'] = 'user/admin/user_site_list';
$route['admin/usersitelist/(:num)'] = 'user/admin/user_site_list/index/$1';

$route['admin/langedit'] = 'lang/admin/lang_edit';
$route['admin/langedit/(:num)'] = 'lang/admin/lang_edit/editorLoad/$1';
$route['admin/langlist'] = 'lang/admin/lang_list';
$route['admin/langlist/(:num)'] = 'lang/admin/lang_list/index/$1';

$route['admin/eventloglist'] = 'event_log/admin/event_log_list';
$route['admin/eventloglist/(:num)'] = 'event_log/admin/event_log_list/index/$1';

$route['admin/slideredit'] = 'slider/admin/slider_edit';
$route['admin/slideredit/(:num)'] = 'slider/admin/slider_edit/editorLoad/$1';
$route['admin/sliderlist'] = 'slider/admin/slider_list';
$route['admin/sliderlist/(:num)'] = 'slider/admin/slider_list/index/$1';

$route['admin/partneredit'] = 'partner/admin/partner_edit';
$route['admin/partneredit/(:num)'] = 'partner/admin/partner_edit/editorLoad/$1';
$route['admin/partnerlist'] = 'partner/admin/partner_list';
$route['admin/partnerlist/(:num)'] = 'partner/admin/partner_list/index/$1';

$route['admin/gallerycategoryedit'] = 'gallery/admin/gallery_category_edit';
$route['admin/gallerycategoryedit/(:num)'] = 'gallery/admin/gallery_category_edit/editorLoad/$1';
$route['admin/gallerycategorylist'] = 'gallery/admin/gallery_category_list';
$route['admin/gallerycategorylist/(:num)'] = 'gallery/admin/gallery_category_list/index/$1';

$route['admin/galleryedit'] = 'gallery/admin/gallery_edit';
$route['admin/galleryedit/(:num)'] = 'gallery/admin/gallery_edit/editorLoad/$1';
$route['admin/gallerylist'] = 'gallery/admin/gallery_list';
$route['admin/gallerylist/(:num)'] = 'gallery/admin/gallery_list/index/$1';

$route['admin/offersedit'] = 'offers/admin/offers_edit';
$route['admin/offersedit/(:num)'] = 'offers/admin/offers_edit/editorLoad/$1';
$route['admin/offerslist'] = 'offers/admin/offers_list';
$route['admin/offerslist/(:num)'] = 'offers/admin/offers_list/index/$1';

$route['admin/priceofferlist'] = 'priceoffer/admin/priceoffer_list';
$route['admin/priceofferlist/(:num)'] = 'priceoffer/admin/priceoffer_list/index/$1';

$route['admin/giftcardlist'] = 'giftcard/admin/giftcard_list';
$route['admin/giftcardlist/(:num)'] = 'giftcard/admin/giftcard_list/index/$1';

$route['admin/panoramaedit'] = 'panorama/admin/panorama_edit';
$route['admin/panoramaedit/(:num)'] = 'panorama/admin/panorama_edit/editorLoad/$1';
$route['admin/panoramalist'] = 'panorama/admin/panorama_list';
$route['admin/panoramalist/(:num)'] = 'panorama/admin/panorama_list/index/$1';

$route['admin/installedit'] = 'install/admin/install_edit';

/*ADMIN END*/

/*AJAX*/

$route['ajax_right/(:any)/(:any)'] = 'ajax/admin/ajax_right/right/$1/$2';
//$route['ajax_rights_load/(:num)/(:num)'] = 'ajax/admin/ajax_right/rightsLoad/$1/$2';
$route['ajax_convert_accented_characters/(:any)'] = 'ajax/admin/ajax_convert_accented_characters/convert/$1';
$route['ajax_imagegallerydelete/(:num)'] = 'ajax/admin/ajax_imagegallery_delete/deleteImage/$1';
$route['ajax_projectimagegallerydelete/(:num)'] = 'ajax/admin/ajax_imagegallery_delete/deleteProjectImage/$1';
$route['ajax_connect'] = 'ajax/site/ajax_connect/connect';
$route['ajax_contract'] = 'ajax/site/ajax_contract/contract';



$route['ajax_select_version/(:num)'] = 'ajax/admin/ajax_select_version/getVersion/$1';



$route['ajax_select_category_edit/(:num)'] = 'ajax/admin/ajax_select_category/getCategoryEdit/$1';
$route['ajax_select_category_load/(:num)/(:any)'] = 'ajax/admin/ajax_select_category/getCategoryLoad/$1/$2';
$route['ajax_select_order/(:num)'] = 'ajax/admin/ajax_select_category/getOrder/$1';
$route['ajax_select_order_load/(:num)/(:any)'] = 'ajax/admin/ajax_select_category/getOrderLoad/$1/$2';

$route['ajax_select_subcategory/(:num)'] = 'ajax/admin/ajax_select_category/subCategory/$1';
$route['ajax_select_subcategory_value/(:num)'] = 'ajax/admin/ajax_select_category/selectSubCategory/$1';
$route['ajax_select_category/(:num)'] = 'ajax/admin/ajax_select_category/category/$1';
$route['ajax_select_category_value/(:num)'] = 'ajax/admin/ajax_select_category/selectCategory/$1';

/*AJAX END*/

/*OTHER*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['migratie'] = "Migratie/index";
/*OTHER END*/