<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Gallery_Edit extends Admin_Base_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("gallery/admin/Gallery_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Termék geléria kezelés");

        $galleryCategory = $this->Gallery_edit_model->getSelectValues('gallery_category');
        $this->smarty_tpl->assign("Category", $galleryCategory);
        $this->addValue("Category", 0, "Kérem, válasszon!");
        $this->setBasicValue($this->Gallery_edit_model->form_name, "Category", 0);

        $this->load->helper("text");
    }

    public function index() {
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Gallery_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Gallery_edit_model->form_name."Alias", "alias", "required");
        $this->form_validation->set_rules($this->Gallery_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Gallery_edit_model->form_elements, $this->Gallery_edit_model->form_name, "Az ürlapadatok hibásak!", "A projekt geléria adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Gallery_edit_model->form_name, $this->Gallery_edit_model->form_elements, $this->Gallery_edit_model->form_view, $this->Gallery_edit_model->db_table, $this->Gallery_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Gallery_edit_model->form_name, $this->Gallery_edit_model->form_elements, $this->Gallery_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id){
        parent::editorLoad($id);

        $this->smarty_tpl->assign("ImageGallery", $this->Gallery_edit_model->editorImageGalleryLoad($id));

        $this->smarty_tpl->assign("Modify", 1);
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Gallery_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Gallery_edit_model->form_name."Alias", "alias", "required");
        $this->form_validation->set_rules($this->Gallery_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Gallery_edit_model->form_elements, $this->Gallery_edit_model->form_name, "Az ürlapadatok hibásak!", "A projekt geléria mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Gallery_edit_model->form_name, $this->Gallery_edit_model->form_elements, $this->Gallery_edit_model->form_view, $id, $this->Gallery_edit_model->db_table, $this->Gallery_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->Gallery_edit_model->form_name, $this->Gallery_edit_model->form_elements, $this->Gallery_edit_model->form_view, $id, $this->Gallery_edit_model->db_table, $this->Gallery_edit_model->db_loaded_fields, "id = ".$id, $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Gallery_edit_model->form_name, $this->Gallery_edit_model->form_elements, $this->Gallery_edit_model->form_view, $id, $this->Gallery_edit_model->db_table, $this->Gallery_edit_model->db_loaded_fields, "id = ".$id, $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }
        $this->Gallery_edit_model->editorInsert($table, $fields);

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új termék geléria hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);

        if (isset($_FILES["GalleryImage"]["name"]) && !empty($_FILES["GalleryImage"]["name"][0])) {

            foreach ($_FILES["GalleryImage"]["name"] as $key => $value) {
                $imageName = convert_accented_characters($value);

                $_FILES["GalleryImageUpload"]["name"] = $_FILES["GalleryImage"]["name"][$key];;
                $_FILES["GalleryImageUpload"]["type"] = $_FILES["GalleryImage"]["type"][$key];
                $_FILES["GalleryImageUpload"]["tmp_name"] = $_FILES["GalleryImage"]["tmp_name"][$key];
                $_FILES["GalleryImageUpload"]["error"] = $_FILES["GalleryImage"]["error"][$key];
                $_FILES["GalleryImageUpload"]["size"] = $_FILES["GalleryImage"]["size"][$key];

                $uploadedFile = $this->files;

                $position = strpos($uploadedFile["data"]["file_name"], ".");
                $name = substr($uploadedFile["data"]["file_name"], 0, $position);

                $imageFields["name"] = $name;
                $imageFields["image"] = $uploadedFile["data"]["file_name"];

                $this->Gallery_edit_model->imageGalleryInsert($imageFields, $id);

            }
        }
        else{
            //unset($fields["image"]);
        }

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        if (isset($_FILES["GalleryImage"]["name"]) && !empty($_FILES["GalleryImage"]["name"][0])) {

            foreach ($_FILES["GalleryImage"]["name"] as $key => $value) {

                $imageName = convert_accented_characters($_FILES["GalleryImage"]["name"][$key]);

                $_FILES["GalleryImageUpload"]["name"] = $_FILES["GalleryImage"]["name"][$key];
                $_FILES["GalleryImageUpload"]["type"] = $_FILES["GalleryImage"]["type"][$key];
                $_FILES["GalleryImageUpload"]["tmp_name"] = $_FILES["GalleryImage"]["tmp_name"][$key];
                $_FILES["GalleryImageUpload"]["error"] = $_FILES["GalleryImage"]["error"][$key];
                $_FILES["GalleryImageUpload"]["size"] = $_FILES["GalleryImage"]["size"][$key];

                $uploadedFile = $this->files;

                $position = strpos($uploadedFile["data"]["file_name"], ".");
                $name = substr($uploadedFile["data"]["file_name"], 0, $position);

                $imageFields["name"] = $name;
                $imageFields["image"] = $uploadedFile["data"]["file_name"];
                $signature = $this->input->post("GalleryImageSignature");
                $imageFields["signature"] = $signature[$key];                

                $this->Gallery_edit_model->imageGalleryInsert($imageFields, $id);

            }
        }
        else{
            //unset($fields["image"]);
        }

        $this->Gallery_edit_model->editorUpdate($table, $fields, $where, $id);

        $galleryimagesignatureid = $this->input->post("GalleryImageSignatureId");
        $galleryimagesignature = $this->input->post("GalleryImageSignature");

        if(!empty($galleryimagesignatureid)){
            foreach($galleryimagesignatureid as $key => $row){

                $imageFields = array("signature" => $galleryimagesignature[$key]);

                $this->Gallery_edit_model->imageGalleryUpdate($imageFields, $key);
                
            }
        }

        $this->smarty_tpl->assign("ImageGallery", $this->Gallery_edit_model->editorImageGalleryLoad($id));

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Projekt geléria módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
        parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->Gallery_edit_model->editorLoad($table, $this->fields, $where, $id);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }
        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){

        if(!empty($_FILES["GalleryImage"]["name"][0])) {
            foreach($_FILES["GalleryImage"]["name"] as $key => $row){

                $_FILES["GalleryImageUpload"]["name"] = $_FILES["GalleryImage"]["name"][$key];
                $_FILES["GalleryImageUpload"]["type"] = $_FILES["GalleryImage"]["type"][$key];
                $_FILES["GalleryImageUpload"]["tmp_name"] = $_FILES["GalleryImage"]["tmp_name"][$key];
                $_FILES["GalleryImageUpload"]["error"] = $_FILES["GalleryImage"]["error"][$key];
                $_FILES["GalleryImageUpload"]["size"] = $_FILES["GalleryImage"]["size"][$key];

                $this->files = $this->fileUpload(800, 800, "GalleryImageUpload");

                if (isset($this->files["error"])) {
                    $this->form_validation->_field_data["GalleryImage"]["field"] = "Gallery";
                    $this->form_validation->_field_data["GalleryImage"]["label"] = "kép";
                    $this->form_validation->_field_data["GalleryImage"]["rules"] = "";
                    $this->form_validation->_field_data["GalleryImage"]["errors"] = array();
                    $this->form_validation->_field_data["GalleryImage"]["is_array"] = "";
                    $this->form_validation->_field_data["GalleryImage"]["keys"] = array();
                    $this->form_validation->_field_data["GalleryImage"]["postdata"] = "";
                    if ($this->input->post("SaveBtn")) {
                        $this->form_validation->_field_data["GalleryImage"]["error"] = $this->files["error"];
                    }
                }
            }
        }

        $validation = parent::createFormValidation($form_elements, $form_name, $error_message, $success_message);
        if(($validation == FALSE) || (isset($this->form_validation->_field_data["GalleryImage"]["error"]))){

            foreach ($form_elements as $key => $form_element) {
                if(isset($this->form_validation->_field_data[$form_name.$form_element])){
                    $this->smarty_tpl->assign($form_name.$form_element, $this->form_validation->_field_data[$form_name.$form_element]);
                }
            }

            $this->smarty_tpl->clearAssign("success_message");
            if(isset($_REQUEST["SaveBtn"]) && !empty($_REQUEST["SaveBtn"])){
                $this->smarty_tpl->assign("error_message", $error_message);
            }
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function fileUpload($width, $height, $field){

        if(isset($_FILES[$field]["name"])){
            $file_name = convert_accented_characters($_FILES[$field]["name"]);
        }
        else{
            $file_name = null;
        }

        $file['upload_path'] = "application/upload/gallery/";
        $file['allowed_types'] = "gif|jpg|png";
        $file['max_size']	= "1024000";
        $file['max_width']  = "8192";
        $file['max_height']  = "6144";
        $file['file_name'] = $file_name;
        $this->load->library("upload", $file);

        if ( ! $this->upload->do_upload($field) ){
            return $error = array("error" => $this->upload->display_errors());
        }
        else{
            $data = array("data" => $this->upload->data());
            $this->imageResize($data["data"]["file_name"], $width, $height);

            return $data;
        }
    }

    public function imageResize($file, $width, $height){
        $image['source_image'] = "application/upload/gallery/".$file;
        $image['maintain_ratio'] = true;
        $image['width'] = $width;
        $image['height'] = $height;

        $this->load->library('image_lib', $image);

        $this->image_lib->clear();
        $this->image_lib->initialize($image);
        if(!$this->image_lib->resize()){
            $error = array("error" => $this->upload->display_errors());
//$this->printR($error);
        }
    }
}
?>