<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');
class Gallery_Category_Show extends Site_Base_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        parent::index();

    }
    public function show(){
        $this->index();
        $this->load->model("gallery/site/Gallery_category_show_model");
        $result = $this->Gallery_category_show_model->galleryCategoryLoad();

        $this->smarty_tpl->assign("galleryCategories", $result);
        $tartalom = $this->smarty_tpl->fetch($this->Gallery_category_show_model->form_view);

        $this->smarty_tpl->assign("content", $tartalom);
        $this->smarty_tpl->display("base/site/site_base_view.tpl");
    }
}

?>
