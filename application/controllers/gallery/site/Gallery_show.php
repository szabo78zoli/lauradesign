<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');
class Gallery_Show extends Site_Base_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        parent::index();

    }
    public function show($id){
        $this->index();
        $this->load->model("gallery/site/Gallery_show_model");

        $galleryCategoryNamed = $this->Gallery_show_model->getGalleryCategoryNameId($id);
        $gallery = $this->Gallery_show_model->getGallery($galleryCategoryNamed["id"]);
        $this->smarty_tpl->assign("galleryName", $galleryCategoryNamed["name"]);
        $this->smarty_tpl->assign("gallery", $gallery);
        $tartalom = $this->smarty_tpl->fetch($this->Gallery_show_model->form_view);

        $this->smarty_tpl->assign("content", $tartalom);
        $this->smarty_tpl->display("base/site/site_base_view.tpl");
    }
}

?>
