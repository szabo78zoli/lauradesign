<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Admin_Menu_List extends Admin_Base_Controller{
    public function __construct () {
        parent::__construct();
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."--Admin menü kezelés");
        $this->load->model("menu/admin/Admin_menu_list_model");
    }

    public function index() {
        parent::index();
        $this->modifyListForm($this->Admin_menu_list_model->db_table);
        $this->modifyOrderForm();
        $this->smarty_tpl->assign("table_head", $this->Admin_menu_list_model->table_head);
        $this->smarty_tpl->assign(  "content",
            $this->generateListForm($this->generateFilterForm($this->Admin_menu_list_model->form_name, $this->Admin_menu_list_model->form_elements, "menu/admin/admin_menu_filter_view.tpl"),
                $this->Admin_menu_list_model->form_name,
                $this->Admin_menu_list_model->order_form_name,
                $this->Admin_menu_list_model->db_table,
                $this->Admin_menu_list_model->db_list_fields,
                $this->Admin_menu_list_model->form_view,
                $this->createListWhere($this->Admin_menu_list_model->db_table, $this->Admin_menu_list_model->form_name, $this->Admin_menu_list_model->db_where),
                $this->createListOrder($this->Admin_menu_list_model->order_form_name, $this->Admin_menu_list_model->db_order_by),
                $this->Admin_menu_list_model->db_group_by,
                $this->Admin_menu_list_model->db_list_limit,
                "A lista nem tartalmaz elemeket!"));


        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateListForm($filter_form, $form_name, $order_form_name, $table, $list_fields, $form_view, $where, $order_by, $group_by, $limit, $warning = ""){

        $list = $this->Admin_menu_list_model->listLoad($table, $list_fields, $where, $order_by, $group_by, $limit, $this->uri->segment(3));
        $this->smarty_tpl->assign("FilterForm", $filter_form);
        $this->smarty_tpl->assign("FormName", $form_name);
        $this->smarty_tpl->assign("OrderFormName", $order_form_name);
        $this->smarty_tpl->assign("list", $list);
        $this->smarty_tpl->assign("pagination", $this->createPagination($table, $where, $limit));
        if($list == false){
            $this->smarty_tpl->assign("error_message", $warning);
        }
        return $this->smarty_tpl->fetch($form_view);
    }

    public function modifyOrderForm(){
        if($this->input->post("UpBtn")){
            $this->Admin_menu_list_model->menuOrderUp($this->input->post("UpBtn"));
            header( "location:" . $_SERVER['REQUEST_URI']);
        }
        if($this->input->post("DownBtn")){
            $this->Admin_menu_list_model->menuOrderDown($this->input->post("DownBtn"));
            header( "location:" . $_SERVER['REQUEST_URI']);
        }
    }

    public function modifyListForm($table){
        parent::modifyListForm($table);
        if($this->input->post("DeleteBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Admin menü törlése", "id = ".$this->input->post("DeleteBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("AktivBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Admin menü inaktiválása", "id = ".$this->input->post("AktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("InAktivBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Admin menü aktiválása", "id = ".$this->input->post("InAktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
    }
}
?>