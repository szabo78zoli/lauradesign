<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Panorama_Edit extends Admin_Base_Controller{

    public $maxImageWidth = 3840;
    public $maxImageHeight = 2400;

    public function __construct (){
        parent::__construct();
        $this->load->model("panorama/admin/Panorama_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- 3D panoráma kezelés");
        $this->smarty_tpl->assign("Lang", $this->Panorama_edit_model->getSelectValues("lang"));

        $this->smarty_tpl->assign("MaxImageWidth", $this->maxImageWidth);
        $this->smarty_tpl->assign("MaxImageHeight", $this->maxImageHeight);

        $this->load->helper("text");
    }

    public function index() {
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Panorama_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Panorama_edit_model->form_name."Alias", "alias", "required");
        if (empty($_FILES['GalleryCategoryImage']['name'])) {
            $this->form_validation->set_rules($this->Panorama_edit_model->form_name."Image", 'kép', 'required');
        }
        $this->form_validation->set_rules($this->Panorama_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Panorama_edit_model->form_elements, $this->Panorama_edit_model->form_name, "Az ürlapadatok hibásak!", "A 3D panoráma adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Panorama_edit_model->form_name, $this->Panorama_edit_model->form_elements, $this->Panorama_edit_model->form_view, $this->Panorama_edit_model->db_table, $this->Panorama_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Panorama_edit_model->form_name, $this->Panorama_edit_model->form_elements, $this->Panorama_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id){
        parent::editorLoad($id);

        $this->smarty_tpl->assign("Image", $this->Panorama_edit_model->editorImageLoad($id));
        $this->smarty_tpl->assign("Modify", 1);
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Panorama_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Panorama_edit_model->form_name."Alias", "alias", "required");
        $this->form_validation->set_rules($this->Panorama_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Panorama_edit_model->form_elements, $this->Panorama_edit_model->form_name, "Az ürlapadatok hibásak!", "A galéria kategória mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Panorama_edit_model->form_name, $this->Panorama_edit_model->form_elements, $this->Panorama_edit_model->form_view, $id, $this->Panorama_edit_model->db_table, $this->Panorama_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->Panorama_edit_model->form_name, $this->Panorama_edit_model->form_elements, $this->Panorama_edit_model->form_view, $id, $this->Panorama_edit_model->db_table, $this->Panorama_edit_model->db_loaded_fields, "id = ".$id, $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Panorama_edit_model->form_name, $this->Panorama_edit_model->form_elements, $this->Panorama_edit_model->form_view, $id, $this->Panorama_edit_model->db_table, $this->Panorama_edit_model->db_loaded_fields, "id = ".$id, $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        $fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);

        $this->Panorama_edit_model->editorInsert($table, $fields);

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új 3D panoráma hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        if (isset($this->file["data"]["file_name"])) {
            $fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);
        }
        else{
            unset($fields["image"]);
        }

        $this->Panorama_edit_model->editorUpdate($table, $fields, $where, $id);

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "3D panoráma módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign("Image", $this->Panorama_edit_model->editorImageLoad($id));

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
        parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->Panorama_edit_model->editorLoad($table, $this->fields, $where, $id);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }
        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){
        if(!empty($_FILES["PanoramaImage"]["name"])) {
            $this->file = $this->fileUpload();
        }

        if (isset($this->file["error"])) {
            $this->form_validation->_field_data["PanoramaImage"]["field"] = "PanoramaImage";
            $this->form_validation->_field_data["PanoramaImage"]["label"] = "kép";
            $this->form_validation->_field_data["PanoramaImage"]["rules"] = "";
            $this->form_validation->_field_data["PanoramaImage"]["errors"] = array();
            $this->form_validation->_field_data["PanoramaImage"]["is_array"] = "";
            $this->form_validation->_field_data["PanoramaImage"]["keys"] = array();
            $this->form_validation->_field_data["PanoramaImage"]["postdata"] = "";
            if ($this->input->post("SaveBtn") && $this->uri->segment(3)) {
                $this->form_validation->_field_data["PanoramaImage"]["error"] = $this->file["error"];
            }
        }

        $validation = parent::createFormValidation($form_elements, $form_name, $error_message = "", $success_message = "");
        if(($validation == FALSE) || (isset($this->form_validation->_field_data["PanoramaImage"]["error"]))){
            $this->smarty_tpl->clearAssign("success_message");
            if($this->input->post("SaveBtn")){
                $this->smarty_tpl->assign("error_message", $error_message);
            }
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function fileUpload(){
        if(isset($_FILES["PanoramaImage"]["name"])){
            $file_name = convert_accented_characters($_FILES["PanoramaImage"]["name"]);
        }
        else{
            $file_name = null;
        }
        $file['upload_path'] = "application/upload/panorama/";
        $file['allowed_types'] = "gif|jpg|png";
        $file['max_size']	= "20480";
        $file['max_width']  = $this->maxImageWidth;
        $file['max_height']  = $this->maxImageHeight;
        $file['file_name'] = $file_name;
        $this->load->library("upload", $file);

        if ( ! $this->upload->do_upload("PanoramaImage") ){
            return $error = array("error" => $this->upload->display_errors());
        }
        else{
            $data = array("data" => $this->upload->data());
            $this->imageResize($data["data"]["file_name"]);
            return $data;
        }
    }

    public function imageResize($file){
        $image['source_image']	= "application/upload/panorama/".$file;
        $image['maintain_ratio'] = TRUE;
        $image['width']	 = 1920;
        $image['height'] = 1080;

        $this->load->library('image_lib', $image);

        if(!$this->image_lib->resize()){
//            echo $this->image_lib->display_errors();
        }
    }
}
?>