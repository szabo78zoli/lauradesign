<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Panorama_List extends Admin_Base_Controller{
    public function __construct () {
        parent::__construct();
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- 3D panoráma kezelés");
        $this->load->model("panorama/admin/Panorama_list_model");
        $this->smarty_tpl->assign("Lang", $this->Panorama_list_model->getSelectValues("lang"));
    }

    public function index() {
        parent::index();
        $this->modifyListForm($this->Panorama_list_model->db_table);
        $this->smarty_tpl->assign("table_head", $this->Panorama_list_model->table_head);
        $this->smarty_tpl->assign(  "content",
            $this->generateListForm($this->generateFilterForm($this->Panorama_list_model->form_name, $this->Panorama_list_model->form_elements, "panorama/admin/panorama_filter_view.tpl"),
                $this->Panorama_list_model->form_name,
                $this->Panorama_list_model->order_form_name,
                $this->Panorama_list_model->db_table,
                $this->Panorama_list_model->db_list_fields,
                $this->Panorama_list_model->form_view,
                $this->createListWhere($this->Panorama_list_model->db_table, $this->Panorama_list_model->form_name, $this->Panorama_list_model->db_where),
                //$this->Panorama_list_model->db_order_by,
                $this->createListOrder($this->Panorama_list_model->order_form_name, $this->Panorama_list_model->db_order_by),
                $this->Panorama_list_model->db_group_by,
                $this->Panorama_list_model->db_list_limit,
                "A lista nem tartalmaz elemeket!"));

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function modifyListForm($table){
        parent::modifyListForm($table);
        if($this->input->post("DeleteBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "3D panoráma törlése", "id = ".$this->input->post("DeleteBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("AktivBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "3D panoráma inaktiválása", "id = ".$this->input->post("AktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("InAktivBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Galéria kategória aktiválása", "id = ".$this->input->post("InAktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
    }

    public function createListWhere($table, $form, $db_where){

        if($this->input->post("SearchBtn")){
            if($this->input->post($form."Filter")){
                $db_where .= " AND name LIKE '%".$this->input->post($form."Filter")."%'";
            }
            if($this->input->post($form."Lang")){
                $db_where .= " AND lang = ".$this->input->post($form."Lang")."";
            }
        }
        else{
            if($this->session->userdata($form."Filter")){
                $db_where .= " AND name LIKE '%".$this->session->userdata($form."Filter")."%'";
            }
            if($this->session->userdata($form."Lang")){
                $db_where .= " AND lang = ".$this->session->userdata($form."Lang")."";
            }
            /*else{
                $db_where .= " AND lang = 1";
            }*/
        }

        return $db_where;

    }
}
?>