<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');
class Panorama_Show extends Site_Base_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        parent::index();

    }
    public function show($id = 0){
        $this->index();
        $this->load->model("panorama/site/Panorama_show_model");

        $panorama = $this->Panorama_show_model->getPanorama();

        $this->smarty_tpl->assign("panorama", $panorama);
        $tartalom = $this->smarty_tpl->fetch($this->Panorama_show_model->form_view);

        $this->smarty_tpl->assign("content", $tartalom);
        $this->smarty_tpl->display("base/site/site_base_view.tpl");
    }
}

?>
