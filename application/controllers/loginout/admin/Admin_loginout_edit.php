<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Admin_Loginout_Edit extends Admin_Base_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("loginout/admin/Admin_loginout_edit_model");
        $this->smarty_tpl->assign("pageTitle", "Admin Bejelentkezés");
        $this->smarty_tpl->assign("FormName", "adminlogin");
    }

    public function index(){
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Admin_loginout_edit_model->form_name."Email", "E-mail", "required");
        $this->form_validation->set_rules($this->Admin_loginout_edit_model->form_name."Password", "Jelszó", "required");
        $validation = $this->createFormValidation($this->Admin_loginout_edit_model->form_elements, $this->Admin_loginout_edit_model->form_name, "Rossz e-mail cím vagy jelszó!");

        if (($this->admin_user_data == "") && ($validation == TRUE)){
            $login = $this->Admin_loginout_edit_model->login($this->input->post($this->Admin_loginout_edit_model->form_name."Email"), $this->input->post($this->Admin_loginout_edit_model->form_name."Password"));
            if($login){
                $this->session->set_userdata("Admin_User_Id", $login["id"]);
                $this->session->set_userdata("Admin_User_Image", $login["image"]);
                $this->session->set_userdata("Admin_User_Name", $login["name"]);
                $this->session->set_userdata("Admin_User_Rigths_Group", $login["group"]);
                $this->session->set_userdata("Admin_User_Last_Login", $login["last_login"]);
                $this->Admin_loginout_edit_model->updateLastLoginDat($login["id"]);

                $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Bejelentkezés", "Bejelentkezés", $_SERVER["REMOTE_ADDR"]);

                $this->isLogged();
            }
            else {
                $this->smarty_tpl->assign("loginForm", $this->generateForm($this->Admin_loginout_edit_model->form_name, $this->Admin_loginout_edit_model->form_elements, $this->Admin_loginout_edit_model->form_view, "Rossz felhasználónév vagy jelszó!"));
                $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
            }
        }
        elseif(($this->admin_user_data == "") || ($validation != TRUE)){
            $this->smarty_tpl->assign("loginForm", $this->generateForm($this->Admin_loginout_edit_model->form_name, $this->Admin_loginout_edit_model->form_elements, $this->Admin_loginout_edit_model->form_view));
            $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
        }
    }

    public function logout(){
        $this->session->unset_userdata("Admin_User_Id");
        $this->session->unset_userdata("Admin_User_Utolso_Bejelentkezes_Datum");
        $this->session->unset_userdata("Admin_User_Nev");
        $this->session->unset_userdata("Admin_User_Nyelv");

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Kijelentkezés", "Kijelentkezés", $_SERVER["REMOTE_ADDR"]);

        header("location:".$this->config->item("base_url")."index.php/admin/login/");
    }
}