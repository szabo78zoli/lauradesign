<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');

class Site_Loginout_Edit extends Site_Base_Controller{

    public $site_user_data = "";

    public function __construct (){
        parent::__construct();

        $this->load->model("loginout/site/Site_loginout_edit_model");
        $this->smarty_tpl->assign("pageTitle", "Site Bejelentkezés");
        $this->smarty_tpl->assign("FormName", "sitelogin");

        $this->smarty_tpl->assign('request_uri', $_SERVER['REQUEST_URI']);

        $breadcrumb_items = array(array("name" => "Főoldal", "href" => "fooldal"), array("name" => "Bejelentkezés", "href" => "bejelentkezes"));

        $this->smarty_tpl->assign('breadcrumb_items', $breadcrumb_items);
    }

    public function index(){
        parent::index();

        if($this->uri->segment(1) == "bejelentkezes_regisztracio_utan"){
            $this->smarty_tpl->assign("login_message", $this->Site_base_model->getContentElement('bejelentkezes-regisztracio-utan'));
        }


        if(!$this->session->userdata("Site_User_Id")) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules($this->Site_loginout_edit_model->form_name . "Email", "E-mail", "required");
            $this->form_validation->set_rules($this->Site_loginout_edit_model->form_name . "Password", "Jelszó", "required");
            $validation = $this->createFormValidation($this->Site_loginout_edit_model->form_elements, $this->Site_loginout_edit_model->form_name, "Rossz e-mail cím vagy jelszó!");

            if (($this->site_user_data == "") && ($validation == TRUE)) {
                $login = $this->Site_loginout_edit_model->login($this->input->post($this->Site_loginout_edit_model->form_name . "Email"), $this->input->post($this->Site_loginout_edit_model->form_name . "Password"), $this->session->userdata("SessionShopId"));
                if ($login) {
                    $this->session->set_userdata("Site_User_Id", $login["id"]);
                    $this->session->set_userdata("Site_User_First_name", $login["first_name"]);
                    $this->session->set_userdata("Site_User_Last_name", $login["last_name"]);
                    $this->Site_loginout_edit_model->updateLastLoginDat($login["id"]);
                    $this->Site_base_model->eventLogSave($this->session->userdata("Site_User_First_Name")." ".$this->session->userdata("Site_User_Last_Name"), "Site", "1", "Bejelentkezés", "Bejelentkezés", $_SERVER["REMOTE_ADDR"]);

                    $this->isLogged();
                    $this->session->set_userdata("login_after_registration", 1);
                    header("location:" . $this->config->item("base_url")."projekt");

                } else {
                    $this->smarty_tpl->assign("content", $this->generateForm($this->Site_loginout_edit_model->form_name, $this->Site_loginout_edit_model->form_elements, $this->Site_loginout_edit_model->form_view, "Rossz felhasználónév vagy jelszó!", ""));
                    $this->smarty_tpl->display("base/site/site_base_view.tpl");
                }
            } elseif (($this->site_user_data == "") || ($validation != TRUE)) {
                $this->smarty_tpl->assign("content", $this->generateForm($this->Site_loginout_edit_model->form_name, $this->Site_loginout_edit_model->form_elements, $this->Site_loginout_edit_model->form_view));
                $this->smarty_tpl->display("base/site/site_base_view.tpl");
            }
        }
        else{
            $this->session->set_userdata("login_after_registration", 1);
            if($this->uri->segment(1) == "bejelentkezes_regisztracio_utan" && !$this->session->userdata('Cart')){
                header("location:" . $this->config->item("base_url"));
            }
            elseif($this->uri->segment(1) == "bejelentkezes_regisztracio_utan" && $this->session->userdata('Cart')){
                header("location:" . $this->config->item("base_url") . "penztar");
            }
            else {
                header("location:" . $this->config->item("base_url"));
            }
        }
    }

    public function login_from_admin($id){

        $userData = $this->Site_loginout_edit_model->getuserData2($id);

        if($userData){
            $this->session->set_userdata("Site_User_Id", $userData["id"]);
            $this->session->set_userdata("Site_User_First_name", $userData["first_name"]);
            $this->session->set_userdata("Site_User_Last_name", $userData["last_name"]);
            $this->session->set_userdata("Site_User_Registration_From", $userData["registration_from"]);
            $this->session->set_userdata("Site_User_Login_From_Admin", 1);
            $this->Site_loginout_edit_model->updateLastLoginDat($userData["id"]);
//echo $userData["web_address"]. "/regisztracio/".$userData["id"]; die();
//echo "location:https://" .$userData["web_address"]. "/bejelentkezes_adminrol/".$userData["id"]; die();
            header("location:https://" .$userData["web_address"]. "/regisztracio/".$userData["id"]);
        }

    }

    public function login_from_teszvesz($id){

        $userData = $this->Site_loginout_edit_model->getuserData($id);

        if($userData){
            $this->session->set_userdata("Site_User_Id", $userData["id"]);
            $this->session->set_userdata("Site_User_First_name", $userData["first_name"]);
            $this->session->set_userdata("Site_User_Last_name", $userData["last_name"]);
            $this->session->set_userdata("Site_User_Registration_From", $userData["registration_from"]);
            $this->session->set_userdata("Teszvesz_Price", 12);
            $this->session->set_userdata("Site_User_Login_From_Admin", 1);
            $this->Site_loginout_edit_model->updateLastLoginDat($userData["id"]);

            header("location:" . $this->config->item("base_url") . "regisztracio/".$userData["id"]);
        }
    }

    public function login_from_skymarket($id){

        $userData = $this->Site_loginout_edit_model->getuserData($id);

        if($userData){
            $this->session->set_userdata("Site_User_Id", $userData["id"]);
            $this->session->set_userdata("Site_User_First_name", $userData["first_name"]);
            $this->session->set_userdata("Site_User_Last_name", $userData["last_name"]);
            $this->session->set_userdata("Site_User_Registration_From", $userData["registration_from"]);
            $this->session->set_userdata("Skymarket_Price", 17);
            $this->session->set_userdata("Site_User_Login_From_Admin", 1);
            $this->Site_loginout_edit_model->updateLastLoginDat($userData["id"]);

            header("location:" . $this->config->item("base_url") . "regisztracio/".$userData["id"]);
        }
    }

    public function login_from_lealkudtuk($id){

        $userData = $this->Site_loginout_edit_model->getuserData($id);

        if($userData){
            $this->session->set_userdata("Site_User_Id", $userData["id"]);
            $this->session->set_userdata("Site_User_First_name", $userData["first_name"]);
            $this->session->set_userdata("Site_User_Last_name", $userData["last_name"]);
            $this->session->set_userdata("Site_User_Registration_From", $userData["registration_from"]);
            $this->session->set_userdata("Lealkudtuk_Price", 18);
            $this->session->set_userdata("Site_User_Login_From_Admin", 1);
            $this->Site_loginout_edit_model->updateLastLoginDat($userData["id"]);

            header("location:" . $this->config->item("base_url") . "regisztracio/".$userData["id"]);
        }
    }

    public function login_from_emag($id){

        $userData = $this->Site_loginout_edit_model->getuserData($id);

        if($userData){
            $this->session->set_userdata("Site_User_Id", $userData["id"]);
            $this->session->set_userdata("Site_User_First_name", $userData["first_name"]);
            $this->session->set_userdata("Site_User_Last_name", $userData["last_name"]);
            $this->session->set_userdata("Site_User_Registration_From", $userData["registration_from"]);
            $this->session->set_userdata("Emag_Price", 11);
            $this->session->set_userdata("Site_User_Login_From_Admin", 1);
            $this->Site_loginout_edit_model->updateLastLoginDat($userData["id"]);

            header("location:" . $this->config->item("base_url") . "regisztracio/".$userData["id"]);
        }
    }


    public function logout(){
        $this->session->unset_userdata("Site_User_Id");
        $this->session->unset_userdata("Site_User_First_name");
        $this->session->unset_userdata("Site_User_Last_name");

        $this->Site_base_model->eventLogSave($this->session->userdata("Site_User_First_Name")." ".$this->session->userdata("Site_User_Last_Name"),"Site", "1", "Kijelentkezés", "Kijelentkezés", $_SERVER["REMOTE_ADDR"]);

        header("location:".$this->config->item("base_url")."fooldal");
    }

    public function generateForm($form_name, $form_elements, $form_view, $error_message = "", $error_login_message = "") {
        $element = array();

        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            $element[$form_name.$value]["field"] = $form_name.$value;
            $element[$form_name.$value]["postdata"] = "";
            $element[$form_name.$value]["error"] = "";
            $this->smarty_tpl->assign($element, $form_name.$value);
        }

        if(!empty($error_message)){
            $this->smarty_tpl->assign("error_message", $error_message);
        }


        return $this->smarty_tpl->fetch($form_view);
    }
}