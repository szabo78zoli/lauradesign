<?php
class Site_Base_Controller extends CI_Controller {

    public $site_lang;
    public $site_lang_azon;

    public function __construct(){
        parent::__construct();

        $this->load->model("base/site/Site_base_model");

        $this->site_lang = $this->session->userdata("Site_Lang");
        $this->site_lang_azon = $this->session->userdata("Site_Lang_Azon");
        $this->site_user_data = $this->session->userdata("Site_User_Id");

        if(empty($this->site_lang)){
            $this->changeLang("hu", 1);
        }

        if ($this->uri->segment(2) == "changelang"){
            $this->changeLang($this->uri->segment(3), $this->uri->segment(4));
        }

        $this->smarty_tpl->assign("site_lang_azon", $this->site_lang_azon);
        $this->smarty_tpl->assign('charset', $this->config->item('charset'));
        $this->smarty_tpl->assign('base_url', $this->config->item('base_url'));
    }

    public function index() {

        if ($this->site_user_data != "") {
            $this->isLogged();
        }

		$this->smarty_tpl->assign('site_menu', $this->Site_base_model->getMenuTree("", $this->site_lang));
        $this->smarty_tpl->assign('site_mmenu', $this->Site_base_model->getMmenuTree("", $this->site_lang));
    }


    public function isLogged() {
        $this->smarty_tpl->assign("SiteUserId", $this->session->userdata("Site_User_Id"));
        $this->smarty_tpl->assign("SiteUserNev", $this->session->userdata("Site_User_First_name")." ".$this->session->userdata("Site_User_Last_name"));
        $this->site_user_lang = $this->session->userdata("Site_User_Nyelv");
        $this->smarty_tpl->assign("SiteUserNyelv", $this->site_user_lang);
        $LoginForm = $this->smarty_tpl->fetch("loginout/site/site_loginout_view.tpl");
        $this->smarty_tpl->assign("LoginForm", $LoginForm);
    }

    public function langList(){
        return $this->site_base_model->langList();
    }

    public function changeLang($lang_azon, $lang_id){

        $this->session->set_userdata("Site_Lang", $lang_id);
        $this->session->set_userdata("Site_Lang_Azon", $lang_azon);
        $this->site_lang = $this->session->userdata("Site_Lang");
        $this->site_lang_azon = $this->session->userdata("Site_Lang_Azon");
        $this->smarty_tpl->assign("site_url", $this->uri->uri_string());
        $this->smarty_tpl->assign("site_lang_azon", $this->site_lang_azon);

        $this->changeLanguageStatic("site");
    }

    public function changeLanguageStatic($lang_file){
        $this->lang->load($lang_file, $this->site_lang_azon);
        foreach ($this->lang->language as $key => $value) {
            $this->smarty_tpl->assign($key, $value);
        }
    }

    public function getContentElement($alias){
        return $this->Site_base_model->getContentElement($alias);
    }

    public function addValue($element, $value, $option){
        $baseArray = $this->smarty_tpl->tpl_vars[$element]->value;
        $addableArray = array($value => $option);
        $fullArray = $addableArray+$baseArray;
        $this->smarty_tpl->tpl_vars[$element]->value = $fullArray;
    }

    public function setBasicValue($form_name, $element, $value){
        $tomb = array();
        $basicValue = array($form_name.$element=>array("postdata"=>$value));
        $this->smarty_tpl->assign($basicValue, $value);
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){
        if (($this->input->post("SaveBtn")) && ($this->input->post("SaveBtn")!="")){
            if($this->form_validation->run() == FALSE){
                foreach ($form_elements as $key => $form_element) {
                    if(isset($this->form_validation->_field_data[$form_name.$form_element])){
                        $this->smarty_tpl->assign($form_name.$form_element, $this->form_validation->_field_data[$form_name.$form_element]);
                    }
                }
                $this->smarty_tpl->assign("error_message", $error_message);
            }
            else {
                $this->smarty_tpl->assign("success_message", $success_message);
                return TRUE;
            }
        }
    }

    public function printR($variable){
        echo "<pre>";
        print_r($variable);
        echo "</pre>";
    }

    public function varDump($variable){
        echo "<pre>";
        var_dump($variable);
        echo "</pre>";
    }
}

?>