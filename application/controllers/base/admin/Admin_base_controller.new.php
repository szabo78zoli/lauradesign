<?php
class Admin_Base_Controller extends CI_Controller {

    public $baseData = array();
    public $admin_user_data;
    public $file;

    public function __construct(){
        parent::__construct();
        $this->load->model("base/admin/admin_base_model");

        $this->smarty_tpl->assign('charset', $this->config->item('charset'));
        $this->smarty_tpl->assign('base_url', $this->config->item('base_url'));

        $this->admin_user_data = $this->session->userdata("Admin_User_Id");
        $this->session->set_userdata("listLenght", $this->admin_base_model->listLenghtLoad());
        $listLenght = $this->session->userdata("listLenght");
        $this->smarty_tpl->assign('listLenght', $listLenght);
    }
    public function index(){

        if (!empty($this->admin_user_data)) {
            $this->isLogged();
        } else {
            $this->isNotLogged();
        }
    }

    public function editorLoad($id){
        if ($this->admin_user_data != "") {
            $this->isLogged();
        } else {
            $this->isNotLogged();
        }
    }

    public function isLogged(){
        $this->smarty_tpl->assign("menu", $this->admin_base_model->getMenuTree());
        $this->session->userdata("Admin_User_Image");
        $this->smarty_tpl->assign("userId", $this->session->userdata("Admin_User_Id"));
        $this->smarty_tpl->assign("userImage", $this->session->userdata("Admin_User_Image"));
        $this->smarty_tpl->assign("userName", $this->session->userdata("Admin_User_Name"));
        $this->smarty_tpl->assign("userRigthsGroup", $this->session->userdata("Admin_User_Rigths_Group"));
        $this->smarty_tpl->assign("userLastLogin", $this->session->userdata("Admin_User_Last_Login"));
        $this->smarty_tpl->assign("content", " ");
        if((!$this->uri->uri_string()) || $this->uri->uri_string() == "admin/login" || $this->uri->uri_string() == "admin"){
            $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
            exit();
        }
    }

    public function isNotLogged(){
        if(!isset($_SERVER["PATH_INFO"]) && ($this->uri->uri_string != "admin/login")){
            header("location:".$this->config->item("base_url")."admin/login");
        }
    }

    public function generateForm($form_name, $form_elements, $form_view, $error_message = "") {
        $element = array();

        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            $element[$form_name.$value]["field"] = $form_name.$value;
            $element[$form_name.$value]["postdata"] = "";
            $element[$form_name.$value]["error"] = "";
            $this->smarty_tpl->assign($element, $form_name.$value);
        }

        if(!empty($error_message)){
            $this->smarty_tpl->assign("error_message", $error_message);
        }

        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateEditForm($form_name, $form_elements, $form_view) {
        $element = array();

        foreach($form_elements as $key => $value){
            if(is_array($value)){
                foreach ($value as $keyArray => $valueArray) {
                    $element[$form_name.$valueArray.$key]["field"] = $form_name.$valueArray.$key;
                    $getElementValues = $this->smarty_tpl->getTemplateVars($form_name.$valueArray.$key);
                    if(!isset($getElementValues["postdata"])){
                        $element[$form_name.$valueArray.$key]["postdata"] = $this->input->post($form_name.$valueArray.$key);
                    }
                    else{
                        $element[$form_name.$valueArray.$key]["postdata"] = $getElementValues["postdata"];
                    }

                    if(isset($this->form_validation->_field_data[$form_name.$valueArray.$key]["error"])){
                        $element[$form_name.$valueArray.$key]["error"] = $this->form_validation->_field_data[$form_name.$valueArray.$key]["error"];
                    }
                    else{
                        $element[$form_name.$valueArray.$key]["error"] = "";
                    }
                }
            }
            else{
                $element[$form_name.$value]["field"] = $form_name.$value;
                $getElementValues = $this->smarty_tpl->getTemplateVars($form_name.$value);
                if(!isset($getElementValues["postdata"])){
                    $element[$form_name.$value]["postdata"] = $this->input->post($form_name.$value);
                }
                else{
                    $element[$form_name.$value]["postdata"] = $getElementValues["postdata"];
                }

                if(isset($this->form_validation->_field_data[$form_name.$value]["error"])){
                    $element[$form_name.$value]["error"] = $this->form_validation->_field_data[$form_name.$value]["error"];
                }
                else{
                    $element[$form_name.$value]["error"] = "";
                }
            }
//$this->printR($element);
        }

        if(is_array($value)){
            foreach ($value as $keyArray => $valueArray) {
                $this->smarty_tpl->assign("FormName", $form_name);
                $this->smarty_tpl->assign($element, $form_name . $valueArray);
            }
        }
        else{
            $this->smarty_tpl->assign("FormName", $form_name);
            $this->smarty_tpl->assign($element, $form_name.$value);
        }

        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where){
        $this->element = array();
        $this->fields = "";
        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            $this->element[$form_name.$value]["field"] = $form_name.$value;
            $this->element[$form_name.$value]["postdata"] = "";

            if(isset($this->form_validation->_field_data[$form_name.$value]["error"])){
                $this->element[$form_name.$value]["error"] = $this->form_validation->_field_data[$form_name.$value]["error"];
            }
            else{
                $this->element[$form_name.$value]["error"] = "";
            }

            $this->smarty_tpl->assign($this->element, $form_name.$value);
        }
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        $this->element = array();
        $fields = "";
        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            $this->element[$form_name.$value]["field"] = $form_name.$value;
            $this->element[$form_name.$value]["postdata"] = $this->input->post($form_name.$value);
            $this->element[$form_name.$value]["error"] = "";
            $this->smarty_tpl->assign($this->element, $form_name.$value);
        }
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        $this->element = array();
        $fields = "";
        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            if(is_array($value)){
                foreach ($value as $keyArray => $valueArray) {
                    $this->element[$form_name.$valueArray]["field"] = $form_name.$valueArray;
                    $this->element[$form_name.$valueArray]["postdata"] = "";
                    $this->element[$form_name.$valueArray]["error"] = "";
                    $this->smarty_tpl->assign($this->element, $form_name.$valueArray);
                }
            }
            else{
                $this->element[$form_name.$value]["field"] = $form_name.$value;
                $this->element[$form_name.$value]["postdata"] = "";
                $this->element[$form_name.$value]["error"] = "";
                $this->smarty_tpl->assign($this->element, $form_name.$value);
            }
        }
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){
        if (($this->input->post("SaveBtn")) && ($this->input->post("SaveBtn")!="")){
            if($this->form_validation->run() == FALSE){
                foreach ($form_elements as $key => $form_element) {
                    if(is_array($form_element)){
                        foreach($form_element as $form_elementKey => $form_elementRow){
                            if(isset($this->form_validation->_field_data[$form_name.$form_elementRow.$key])){
                                $this->smarty_tpl->assign($form_name.$form_elementRow.$key, $this->form_validation->_field_data[$form_name.$form_elementRow.$key]);
                            }
                        }
                    }
                    else{
                        if(isset($this->form_validation->_field_data[$form_name.$form_element])){
                            $this->smarty_tpl->assign($form_name.$form_element, $this->form_validation->_field_data[$form_name.$form_element]);
                        }
                    }
/*$this->printR($form_elementRow.$key);
die();*/
                }

                $this->smarty_tpl->assign("error_message", $error_message);
            }
            else {
                $this->smarty_tpl->assign("success_message", $success_message);
                return TRUE;
            }
        }
    }

    public function modifyListForm($table){
        if($this->input->post("DeleteBtn")){
            $this->admin_base_model->listStatusModify($table, array("deleted" => "1"), "id", $this->input->post("DeleteBtn"));
        }
        if($this->input->post("AktivBtn")){
            $this->admin_base_model->listStatusModify($table, array("active" => "0"), "id", $this->input->post("AktivBtn"));
        }
        if($this->input->post("InAktivBtn")){
            $this->admin_base_model->listStatusModify($table, array("active" => "1"), "id", $this->input->post("InAktivBtn"));
        }
    }

    public function createListWhere($table, $form, $db_where){

        if(($this->input->post("SearchBtn") && $this->input->post($form."Filter")!= "")){

            return $db_where .= " AND name LIKE '%".$this->input->post($form."Filter")."%'";
        }
        elseif($this->session->userdata($form."Filter")){
            return $db_where .= " AND name LIKE '%".$this->session->userdata($form."Filter")."%'";
        }
        else{
            return $db_where;
        }
    }

    public function addValue($element, $value, $option){
        $this->smarty_tpl->tpl_vars[$element]->value[$value] = $option;
        ksort($this->smarty_tpl->tpl_vars[$element]->value);
    }

    public function setBasicValue($form_name, $element, $value){
        $tomb = array();
        $basicValue = array($form_name.$element=>array("postdata"=>$value));
        $this->smarty_tpl->assign($basicValue, $value);
    }

    public function generateListForm($filter_form, $form_name, $table, $list_fields, $form_view, $where, $order_by, $group_by, $limit, $warning = ""){

        $list = $this->admin_base_model->listLoad($table, $list_fields, $where, $order_by, $group_by, $limit, $this->uri->segment(3));
        $this->smarty_tpl->assign("FilterForm", $filter_form);
        $this->smarty_tpl->assign("FormName", $form_name);
        $this->smarty_tpl->assign("list", $list);
        $this->smarty_tpl->assign("pagination", $this->createPagination($table, $where, $limit));
        if($list == false){
            $this->smarty_tpl->assign("error_message", $warning);
        }
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateFilterForm($form_name, $form_elements, $form_view){
        $element = array();

        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            $element[$form_name.$value]["field"] = $form_name.$value;

            if($this->input->post("BasicBtn")){
                $element[$form_name.$value]["postdata"] = "";
                $this->session->unset_userdata($form_name.$value, $this->input->post($form_name.$value));
                header("location:".$this->config->item("base_url").$this->uri->segment(1)."/".$this->uri->segment(2));
            }
            elseif($this->input->post($form_name.$value)){
                $element[$form_name.$value]["postdata"] = $this->input->post($form_name.$value);
                $this->session->set_userdata($form_name.$value, $this->input->post($form_name.$value));
            }
            elseif($this->session->userdata($form_name.$value)){
                $element[$form_name.$value]["postdata"] = $this->session->userdata($form_name.$value);
            }
            else{
                $element[$form_name.$value]["postdata"] = "";
            }

            $this->smarty_tpl->assign($element, $form_name.$value);
        }

        return $this->smarty_tpl->fetch($form_view);
    }

    public function createPagination($table, $where, $limit){
        $this->load->library('pagination');

        $page = array();

        $page["base_url"] = $this->config->item("base_url")."admin/".$this->uri->segment(2);
        $page["total_rows"] = $this->admin_base_model->listLoadCount($table, $where);
        $page["per_page"] = $limit;
        $page["num_links"] = 3;
        $page["uri_segment"] = 3;
        $page["num_tag_open"] = "<li class='paginate_button'>";
        $page["num_tag_close"] = "</li>";
        $page["first_link"] = "Első";
        $page["first_tag_open"] = "<li class='paginate_button previous'>";
        $page["first_tag_close"] = "</li>";
        $page["last_link"] = "Utolsó";
        $page["last_tag_open"] = "<li class='paginate_button'>";
        $page["last_tag_close"] = "</li>";
        $page["next_link"] = "Következő";
        $page["next_tag_open"] = "<li class='paginate_button'>";
        $page["next_tag_close"] = "</li>";
        $page["prev_link"] = "Előző";
        $page["prev_tag_open"] = "<li class='paginate_button'>";
        $page["prev_tag_close"] ="</li>";
        $page["cur_page"] = 0;
        $page["cur_tag_open"] = "<li class='paginate_button active'><a>";
        $page["cur_tag_close"] = "</a></li>";

        $this->pagination->initialize($page);
        return $pagination_links = $this->pagination->create_links();
    }

    public function printR($variable){
        echo "<pre>";
        print_r($variable);
        echo "</pre>";
    }

    public function varDump($variable){
        echo "<pre>";
        var_dump($variable);
        echo "</pre>";
    }

    public function queriesPrint(){
        echo "<pre>";
        print_r($this->db->queries);
        echo "</pre>";
    }
}
?>