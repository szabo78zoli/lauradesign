<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Lang_Edit extends Admin_Base_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("lang/admin/Lang_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Nyelv kezelés");

        $this->load->helper("text");
    }

    public function index() {
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Lang_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Lang_edit_model->form_name."Abbreviation", "rövidítés", "required");
        if (!isset($_FILES["AdminLangImage"]["name"])) {
            $this->form_validation->set_rules($this->Lang_edit_model->form_name . "Image", "kép", "required");
        }
        $this->form_validation->set_rules($this->Lang_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Lang_edit_model->form_elements, $this->Lang_edit_model->form_name, "Az ürlapadatok hibásak!", "A nyelv adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Lang_edit_model->form_name, $this->Lang_edit_model->form_elements, $this->Lang_edit_model->form_view, $this->Lang_edit_model->db_table, $this->Lang_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Lang_edit_model->form_name, $this->Lang_edit_model->form_elements, $this->Lang_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id){
        parent::editorLoad($id);
        $this->smarty_tpl->assign("Modify", 1);
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Lang_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Lang_edit_model->form_name."Abbreviation", "rövidítés", "required");
        if (!isset($_FILES["AdminLangImage"]["name"])) {
            $this->form_validation->set_rules($this->Lang_edit_model->form_name . "Image", "kép", "required");
        }
        $this->form_validation->set_rules($this->Lang_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Lang_edit_model->form_elements, $this->Lang_edit_model->form_name, "Az ürlapadatok hibásak!", "A nyelv mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Lang_edit_model->form_name, $this->Lang_edit_model->form_elements, $this->Lang_edit_model->form_view, $id, $this->Lang_edit_model->db_table, $this->Lang_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->Lang_edit_model->form_name, $this->Lang_edit_model->form_elements, $this->Lang_edit_model->form_view, $id, $this->Lang_edit_model->db_table, $this->Lang_edit_model->db_loaded_fields, "id = ".$id, $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Lang_edit_model->form_name, $this->Lang_edit_model->form_elements, $this->Lang_edit_model->form_view, $id, $this->Lang_edit_model->db_table, $this->Lang_edit_model->db_loaded_fields, "id = ".$id, $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        $fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);

        $this->Lang_edit_model->editorInsert($table, $fields);

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új nyelv hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        if (isset($this->file["data"]["file_name"])) {
            $fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);
        }
        else{
            unset($fields["image"]);
        }

        $this->Lang_edit_model->editorUpdate($table, $fields, $where, $id);

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Nyelv módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
        parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->Lang_edit_model->editorLoad($table, $this->fields, $where, $id);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }
        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){

        if(!empty($_FILES["AdminLangImage"]["name"])) {
            $this->file = $this->imageUpload(160, 80, "AdminLangImage");
        }

        if (isset($this->file["error"])) {
            $this->form_validation->_field_data["AdminLangImage"]["field"] = "AdminLangImage";
            $this->form_validation->_field_data["AdminLangImage"]["label"] = "kép";
            $this->form_validation->_field_data["AdminLangImage"]["rules"] = "";
            $this->form_validation->_field_data["AdminLangImage"]["errors"] = array();
            $this->form_validation->_field_data["AdminLangImage"]["is_array"] = "";
            $this->form_validation->_field_data["AdminLangImage"]["keys"] = array();
            $this->form_validation->_field_data["AdminLangImage"]["postdata"] = "";
            if ($this->input->post("SaveBtn")) {
                $this->form_validation->_field_data["AdminLangImage"]["error"] = $this->file["error"];
            }
        }

        $validation = parent::createFormValidation($form_elements, $form_name, $error_message, $success_message);
        if(($validation == FALSE) || (isset($this->form_validation->_field_data["AdminLangImage"]["error"]))){

            foreach ($form_elements as $key => $form_element) {
                if(isset($this->form_validation->_field_data[$form_name.$form_element])){
                    $this->smarty_tpl->assign($form_name.$form_element, $this->form_validation->_field_data[$form_name.$form_element]);
                }
            }

            $this->smarty_tpl->clearAssign("success_message");
            if(isset($_REQUEST["SaveBtn"]) && !empty($_REQUEST["SaveBtn"])){
                $this->smarty_tpl->assign("error_message", $error_message);
            }
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function imageUpload($width, $height, $field){

        if(isset($_FILES[$field]["name"])){
            $file_name = convert_accented_characters($_FILES[$field]["name"]);
        }
        else{
            $file_name = null;
        }
        $file['upload_path'] = "application/upload/lang/";
        $file['allowed_types'] = "gif|jpg|png";
        $file['max_size']	= "20480";
        $file['max_width']  = "4096";
        $file['max_height']  = "3072";
        $file['file_name'] = $file_name;
        $this->load->library("upload", $file);

        if ( ! $this->upload->do_upload($field) ){
            return $error = array("error" => $this->upload->display_errors());
        }
        else{
            $data = array("data" => $this->upload->data());
            $this->imageResize($data["data"]["file_name"], $width, $height);
            return $data;
        }
    }

    public function imageResize($file, $width, $height){
        $image['source_image'] = "application/upload/lang/".$file;
        //$image['create_thumb'] = TRUE;
        $image['maintain_ratio'] = true;
        $image['width'] = $width;
        $image['height'] = $height;

        $this->load->library('image_lib', $image);

        $this->image_lib->clear();
        $this->image_lib->initialize($image);
        if(!$this->image_lib->resize()){
            $error = array("error" => $this->upload->display_errors());
            $this->printR($error);
        }
    }
}
?>