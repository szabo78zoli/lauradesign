<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Right_Group_Edit extends Admin_Base_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("right/admin/Right_group_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."--Jogosultság kezelés");
        $sitetType = array("1" => "Site", "2" => "Admin");
        $this->smarty_tpl->assign("SitetType", $sitetType );
        $this->smarty_tpl->assign("Right", $this->Right_group_edit_model->getSelectValues("rights"));
        $this->addValue("SitetType", 0, "Kérem, válasszon!");
        $this->setBasicValue($this->Right_group_edit_model->form_name, "SitetType", 0);
    }

    public function index() {
        parent::index();

        $this->smarty_tpl->assign("RightGroupIdValue", 0);

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Right_group_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Right_group_edit_model->form_name."SitetType", "oldal típus", "greater_than[0]");
        $this->form_validation->set_rules($this->Right_group_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Right_group_edit_model->form_elements, $this->Right_group_edit_model->form_name, "Az ürlapadatok hibásak!", "A jogosultság csoport adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Right_group_edit_model->form_name, $this->Right_group_edit_model->form_elements, $this->Right_group_edit_model->form_view, $this->Right_group_edit_model->db_table, $this->Right_group_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Right_group_edit_model->form_name, $this->Right_group_edit_model->form_elements, $this->Right_group_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id){
        parent::editorLoad($id);
        $this->smarty_tpl->assign("Modify", 1);
        $this->smarty_tpl->assign("RightGroupIdValue", $id);
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Right_group_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Right_group_edit_model->form_name."SitetType", "oldal típus", "greater_than[0]");
        $this->form_validation->set_rules($this->Right_group_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Right_group_edit_model->form_elements, $this->Right_group_edit_model->form_name, "Az ürlapadatok hibásak!", "A jogosultság csoport adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Right_group_edit_model->form_name, $this->Right_group_edit_model->form_elements, $this->Right_group_edit_model->form_view, $id, $this->Right_group_edit_model->db_table, $this->Right_group_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->Right_group_edit_model->form_name, $this->Right_group_edit_model->form_elements, $this->Right_group_edit_model->form_view, $id, $this->Right_group_edit_model->db_table, $this->Right_group_edit_model->db_loaded_fields, "id = ".$id." AND deleted= 0", $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Right_group_edit_model->form_name, $this->Right_group_edit_model->form_elements, $this->Right_group_edit_model->form_view, $id, $this->Right_group_edit_model->db_table, $this->Right_group_edit_model->db_loaded_fields, "id = ".$id." AND deleted = 0", $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }
        $this->Right_group_edit_model->editorInsert($table, $fields, $this->input->post("AdminRightGroupRight"));

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új jogosultság csoport hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);
        $this->fields = "";
        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }
        $this->Right_group_edit_model->editorUpdate($table, $fields, $where, $id, $this->input->post("AdminRightGroupRight"));

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Jogosultság csoport módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }
        $loadedData = $this->Right_group_edit_model->editorLoad($table, $this->fields, $where, $id);

        $this->smarty_tpl->assign("Right", $this->Right_group_edit_model->getSelectValues("rights", $loadedData["site_type"]));
        $this->element["AdminRightGroupRight"]["postdata"] = $loadedData["rights"];

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
        parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->Right_group_edit_model->editorLoad($table, $this->fields, $where, $id);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }

        $this->smarty_tpl->assign("Right", $this->Right_group_edit_model->getSelectValues("rights", $loadedData["site_type"]));
        $this->element["AdminRightGroupRight"]["postdata"] = $loadedData["rights"];
        $this->smarty_tpl->assign($this->element, $form_name.$value);

        return $this->smarty_tpl->fetch($form_view);
    }
}
?>