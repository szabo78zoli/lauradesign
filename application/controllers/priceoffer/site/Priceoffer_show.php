<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');
class PriceOffer_Show extends Site_Base_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->helper("text");

    }
    public function index() {
        parent::index();
        $this->load->model("priceoffer/site/PriceOffer_show_model");

        $offers = $this->PriceOffer_show_model->getOffers();
        $this->smarty_tpl->assign("Offers", $offers);

        $this->year = array(  0 => 'Kérem, válasszon!',
                        2021 => '2021',
                        2022 => '2022',
                        2023 => '2023',
                        2024 => '2024',
                        2025 => '2025',
                        2026 => '2026',
                        2027 => '2027',
                        2028 => '2028',
                        2029 => '2029',
                        2030 => '2030',
                        2031 => '2031',
                        2032 => '2032',
                        2033 => '2033',
                        2034 => '2034',
                        2035 => '2035');

        $this->month = array( 0 => 'Kérem, válasszon!',
                        1 => 'Január',
                        2 => 'Február',
                        3 => 'Március',
                        4 => 'Április',
                        5 => 'Május',
                        6 => 'Június',
                        7 => 'Július',
                        8 => 'Augusztus',
                        9 => 'Szeptember',
                        10 => 'Október',
                        11 => 'November',
                        12 => 'December');

        $this->smarty_tpl->assign("StartYear", $this->year);
        $this->smarty_tpl->assign("Year", $this->year);
        $this->smarty_tpl->assign("StartYear", $this->year);
        $this->smarty_tpl->assign("Month", $this->month);
        $this->smarty_tpl->assign("StartMonth", $this->month);

        $this->load->library("form_validation");
        $this->form_validation->set_rules("Area", "alapterület", "required");
        $this->form_validation->set_rules("County", "megye", "required");
        $this->form_validation->set_rules("City", "város", "required");
        $this->form_validation->set_rules("Type", "ingatlan típusa", "required");
        if(empty($this->input->post('Year')) && empty($this->input->post('Month'))){
            $this->form_validation->set_rules("StartYear", "év", "required|greater_than[0]", array('greater_than' => 'Az év kiválaszása kötelező'));
            $this->form_validation->set_rules("StartMonth", "hónap", "required|greater_than[0]", array('greater_than' => 'A hónap kiválaszása kötelező'));
        }
        //$this->form_validation->set_rules("Year", "év", "required|greater_than[0]", array('greater_than' => 'Az év kiválaszása kötelező'));
        //$this->form_validation->set_rules("Month", "hónap", "required|greater_than[0]", array('greater_than' => 'A hónap kiválaszása kötelező'));
        $this->form_validation->set_rules("Place", "helyiség", "required");
        $this->form_validation->set_rules("Level", "szint", "required");
        $this->form_validation->set_rules("Name", "név", "required");
        $this->form_validation->set_rules("Email", "e-mail", "required");
        $this->form_validation->set_rules("Phone", "telefonszám", "required");
        $this->form_validation->set_rules("Offer", "csomag", "required|greater_than[0]", array('greater_than' => 'A csomag kiválaszása kötelező'));



        $validation = $this->form_validation->run();

        if ($validation == TRUE){
//echo "11"; die();

            $this->PriceOffer_show_model->save();

            if(!empty($_FILES["File"]["name"][0])) {
                foreach($_FILES["File"]["name"] as $key => $row){
                    $this->files = $this->fileUpload("asd", $row);
                }
            }

            $this->sendPriceOfferNotification();
        }
        elseif($validation != TRUE){
//$this->printR($this->form_validation->_field_data); die();
            if(!empty($this->form_validation->_field_data['Type']['error'])){
                $this->smarty_tpl->assign("CollapseOneError", 1);
                $this->smarty_tpl->assign("TypeError", $this->form_validation->_field_data['Type']['error']);
            }
            if(!empty($this->form_validation->_field_data['Area']['error'])){
                $this->smarty_tpl->assign("CollapseThreeError", 1);
                $this->smarty_tpl->assign("AreaError", $this->form_validation->_field_data['Area']['error']);
            }
            if(!empty($this->form_validation->_field_data['County']['error'])){
                $this->smarty_tpl->assign("CollapseSixError", 1);
                $this->smarty_tpl->assign("CountyError", $this->form_validation->_field_data['County']['error']);
            }
            if(!empty($this->form_validation->_field_data['City']['error'])){
                $this->smarty_tpl->assign("CollapseSixError", 1);
                $this->smarty_tpl->assign("CityError", $this->form_validation->_field_data['City']['error']);
            }

            if(!empty($this->form_validation->_field_data['City']['error'])){
                $this->smarty_tpl->assign("CollapseSixError", 1);
                $this->smarty_tpl->assign("CityError", $this->form_validation->_field_data['City']['error']);
            }

            if(!empty($this->form_validation->_field_data['Place']['error'])){
                $this->smarty_tpl->assign("CollapseFourError", 1);
                $this->smarty_tpl->assign("PlaceError", $this->form_validation->_field_data['Place']['error']);
            }

            if(!empty($this->form_validation->_field_data['StartYear']['error'])){
                $this->smarty_tpl->assign("CollapseSevenError", 1);
                $this->smarty_tpl->assign("StartYearError", $this->form_validation->_field_data['StartYear']['error']);
            }

            if(!empty($this->form_validation->_field_data['StartMonth']['error'])){
                $this->smarty_tpl->assign("CollapseSevenError", 1);
                $this->smarty_tpl->assign("StartMonthError", $this->form_validation->_field_data['StartMonth']['error']);
            }

            if(!empty($this->form_validation->_field_data['Year']['error'])){
                $this->smarty_tpl->assign("CollapseSevenError", 1);
                $this->smarty_tpl->assign("YearError", $this->form_validation->_field_data['Year']['error']);
            }

            if(!empty($this->form_validation->_field_data['Month']['error'])){
                $this->smarty_tpl->assign("CollapseSevenError", 1);
                $this->smarty_tpl->assign("MonthError", $this->form_validation->_field_data['Month']['error']);
            }

            if(!empty($this->form_validation->_field_data['Level']['error'])){
                $this->smarty_tpl->assign("CollapseTwoError", 1);
                $this->smarty_tpl->assign("LevelError", $this->form_validation->_field_data['Level']['error']);
            }

            if(!empty($this->form_validation->_field_data['Name']['error'])){
                $this->smarty_tpl->assign("CollapseNineError", 1);
                $this->smarty_tpl->assign("NameError", $this->form_validation->_field_data['Name']['error']);
            }

            if(!empty($this->form_validation->_field_data['Email']['error'])){
                $this->smarty_tpl->assign("CollapseNineError", 1);
                $this->smarty_tpl->assign("EmailError", $this->form_validation->_field_data['Email']['error']);
            }

            if(!empty($this->form_validation->_field_data['Phone']['error'])){
                $this->smarty_tpl->assign("CollapseNineError", 1);
                $this->smarty_tpl->assign("PhoneError", $this->form_validation->_field_data['Phone']['error']);
            }

            if(!empty($this->form_validation->_field_data['Offer']['error'])){
                $this->smarty_tpl->assign("CollapseFiveError", 1);
                $this->smarty_tpl->assign("OfferError", $this->form_validation->_field_data['Offer']['error']);
            }

            if(!empty($this->form_validation->_field_data['Type']['postdata'])){
                $this->smarty_tpl->assign("TypePostdata", $this->form_validation->_field_data['Type']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Area']['postdata'])){
                $this->smarty_tpl->assign("AreaPostdata", $this->form_validation->_field_data['Area']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['County']['postdata'])){
                $this->smarty_tpl->assign("CountyPostdata", $this->form_validation->_field_data['County']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['City']['postdata'])){
                $this->smarty_tpl->assign("CityPostdata", $this->form_validation->_field_data['City']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Place']['postdata'])){
                $this->smarty_tpl->assign("PlacePostdata", $this->form_validation->_field_data['Place']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['StartYear']['postdata'])){
                $this->smarty_tpl->assign("StartYearPostdata", $this->form_validation->_field_data['StartYear']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['StartMonth']['postdata'])){
                $this->smarty_tpl->assign("StartMonthPostdata", $this->form_validation->_field_data['StartMonth']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Year']['postdata'])){
                $this->smarty_tpl->assign("YearPostdata", $this->form_validation->_field_data['Year']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Month']['postdata'])){
                $this->smarty_tpl->assign("MonthPostdata", $this->form_validation->_field_data['Month']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Level']['postdata'])){
                $this->smarty_tpl->assign("LevelPostdata", $this->form_validation->_field_data['Level']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Name']['postdata'])){
                $this->smarty_tpl->assign("NamePostdata", $this->form_validation->_field_data['Name']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Email']['postdata'])){
                $this->smarty_tpl->assign("EmailPostdata", $this->form_validation->_field_data['Email']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Phone']['postdata'])){
                $this->smarty_tpl->assign("PhonePostdata", $this->form_validation->_field_data['Phone']['postdata']);
            }

            if(!empty($this->form_validation->_field_data['Offer']['postdata'])){
                $this->smarty_tpl->assign("OfferPostdata", $this->form_validation->_field_data['Offer']['postdata']);
            }


            if(!empty($_FILES["UploadFile"]["name"][0])) {
                foreach($_FILES["UploadFile"]["name"] as $key => $row){

                    $_FILES["UploadFileUpload"]["name"] = $_FILES["UploadFile"]["name"][$key];
                    $_FILES["UploadFileUpload"]["type"] = $_FILES["UploadFile"]["type"][$key];
                    $_FILES["UploadFileUpload"]["tmp_name"] = $_FILES["UploadFile"]["tmp_name"][$key];
                    $_FILES["UploadFileUpload"]["error"] = $_FILES["UploadFile"]["error"][$key];
                    $_FILES["UploadFileUpload"]["size"] = $_FILES["UploadFile"]["size"][$key];

                    $file_name = convert_accented_characters($_FILES["UploadFileUpload"]["name"]);

                    $this->files = $this->fileUpload($file_name);
                }
            }
        }


        $this->load->model("priceoffer/site/PriceOffer_show_model");

        $tartalom = $this->smarty_tpl->fetch($this->PriceOffer_show_model->form_view);

        $this->smarty_tpl->assign("content", $tartalom);
        $this->smarty_tpl->display("base/site/site_base_view.tpl");
    }

    public function fileUpload($file_name){

        $file['upload_path'] = "application/upload/document/";
        $file['allowed_types'] = "gif|jpg|png|pdf";
        $file['max_size']	= "1024000";
        $file['max_width']  = "8192";
        $file['max_height']  = "6144";
        $file['file_name'] = $file_name;
        $this->load->library("upload", $file);

        if ( ! $this->upload->do_upload("UploadFileUpload") ){
            echo $this->upload->display_errors();
            return $error = array("error" => $this->upload->display_errors());
        }
        else{
            $data = array("data" => $this->upload->data());

            return $data;
        }
    }

    public function sendPriceOfferNotification(){
        $Offer = $this->PriceOffer_show_model->getOffer($this->input->post('Offer'));

        if ($this->input->post('Type') == 1){
            $Type = 'Családi ház';
        }
        elseif ($this->input->post('Type') == 2){
            $Type = 'Iroda ház, láncház';
        }
        elseif ($this->input->post('Type') == 3){
            $Type = 'Társasházi lakás';
        }


        $this->place = array(   1 => 'Konyha',
                                2 => 'Étkező',
                                3 => 'Nappali',
                                4 => 'Gyerekszoba',
                                5 => 'Hálószoba',
                                6 => 'Dolgozó szoba ',
                                7 => 'Előtér, folyosó',
                                8 => 'Egyéb helyiségek');

        $places = str_split($this->input->post('Place'));
        $placeText = '';

        foreach ($places as $place){
            if(empty($placeText)){
                $placeText .= $this->place[$place];
            }
            else{
                $placeText .= ', '.$this->place[$place];
            }
        }

        $html = "<div style='margin: 0 auto; width: 33%;'>";

        $html .= "<table border='0' cellspacing='0px' cellpadding='5px' style='font-family: Arial; color: #888; width: 470px;  border: none; border-collapse: collapse'>";

        $html .= "<tr>";
        $html .= "<td colspan='4' style='font-weight:bold;text-align:left;'><img src='assets/site/images/logo.png' /></td>";
        $html .= "<td colspan='4' style='text-align:left;'>Laura Design, ahol az ötletek valóra válnak</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='8' style='font-weight:bold;text-align:center; color: #000;'>Új ajánlatkérés érkezett! </td>";
        $html .= "</tr>";
        $html .= "</table>";

        $html .= "<table border='1' cellspacing='0px' cellpadding='5px' style='font-family: Arial; border: 1px solid #B8B8B8;border-collapse: collapse'>";

        $html .= "<tr>";
        $html .= "<td colspan='8' style='font-weight:bold;text-align:left; background-color:#ccc;'>Paraméterek:</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'><b>Ingatlan típusa:</b></td>";
        $html .= "<td colspan='6' style='text-align:right'>".$Type."</br></td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Szintek száma:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('Level')."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Alapterület:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('Area')."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Helységek</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$placeText."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Csomag:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$Offer."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Hol található az ingatlan:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('County').", ".$this->input->post('City')."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Mikor kezdődik az építkezés:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('StartYear')." ".$this->month[$this->input->post('StartMonth')]."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Az építkezés elkezdődött:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('Year')." ".$this->month[$this->input->post('Month')]."</td>";
        $html .= "</tr>";
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Dokumentumok:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('Year')." ".$this->input->post('Month')."</td>";
        $html .= "</tr>";
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        $html .= "<tr>";
        $html .= "<td colspan='8' style='font-weight:bold;text-align:left; background-color:#ccc;'>Elérhetőség:</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Név:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('Name')."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>E-mail cím:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('Email')."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:bold;text-align:left'>Telefonszám:</td>";
        $html .= "<td colspan='6' style='text-align:right'>".$this->input->post('Phone')."</td>";
        $html .= "</tr>";



        $html .= "</table>";
        $html .= "</div>";






        $this->load->library('email');

        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from('info@lauradesign.hu', 'www.lauradesign.hu');
        //$this->email->to($user["email"]);
        //$this->email->bcc($webshop["order_mail"]);
        $this->email->subject('Ajánlatkérés');
        $this->email->message($html);
        //$this->email->send();
$this->printR($html); die();
    }
}
?>
