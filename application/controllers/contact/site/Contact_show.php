<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');
class Contact_Show extends Site_Base_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->model("contact/site/Contact_show_model");
    }

    public function index(){
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Contact_show_model->form_name . "Name", "Név", "required");
        $this->form_validation->set_rules($this->Contact_show_model->form_name . "Email", "E-mail", "required|valid_email");
        $this->form_validation->set_rules($this->Contact_show_model->form_name . "Phone", "Telefonszám", "required");
        $this->form_validation->set_rules($this->Contact_show_model->form_name . "Address", "Cím", "required");
        $this->form_validation->set_rules($this->Contact_show_model->form_name . "Message", "Üzenet", "required");
        $validation = $this->createFormValidation($this->Contact_show_model->form_elements, $this->Contact_show_model->form_name, "Hiba történt az üzenet küldése során!", "Az üzenet küldése sikeres!");

        if (($validation == TRUE)) {
            $this->smarty_tpl->assign("content", $this->generateForm($this->Contact_show_model->form_name, $this->Contact_show_model->form_elements, $this->Contact_show_model->form_view));
            $this->smarty_tpl->display("base/site/site_base_view.tpl");

        } elseif (($validation != TRUE)) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Contact_show_model->form_name, $this->Contact_show_model->form_elements, $this->Contact_show_model->form_view));
            $this->smarty_tpl->display("base/site/site_base_view.tpl");
        }
    }

    public function generateForm($form_name, $form_elements, $form_view, $error_message = "", $error_login_message = "") {
        $element = array();

        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            $element[$form_name.$value]["field"] = $form_name.$value;
            $element[$form_name.$value]["postdata"] = "";
            $element[$form_name.$value]["error"] = "";
            $this->smarty_tpl->assign($element, $form_name.$value);
        }

        if(!empty($error_message)){
            $this->smarty_tpl->assign("error_message", $error_message);
        }


        return $this->smarty_tpl->fetch($form_view);
    }


    public function generateEditForm($form_name, $form_elements, $form_view) {
        $element = array();

        foreach($form_elements as $key => $value){
            $element[$form_name.$value]["field"] = $form_name.$value;
            $getElementValues = $this->smarty_tpl->getTemplateVars($form_name.$value);
            if(!isset($getElementValues["postdata"])){
                $element[$form_name.$value]["postdata"] = $this->input->post($form_name.$value);
            }
            else{
                $element[$form_name.$value]["postdata"] = $getElementValues["postdata"];
            }

            if(isset($this->form_validation->_field_data[$form_name.$value]["error"])){
                $element[$form_name.$value]["error"] = $this->form_validation->_field_data[$form_name.$value]["error"];
            }
            else{
                $element[$form_name.$value]["error"] = "";
            }
        }
        $this->smarty_tpl->assign("FormName", $form_name);
        $this->smarty_tpl->assign($element, $form_name.$value);

        return $this->smarty_tpl->fetch($form_view);
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){

        if (($this->input->post("SaveBtn")) && ($this->input->post("SaveBtn")!="")){
            if($this->form_validation->run() == FALSE){
                foreach ($form_elements as $key => $form_element) {
                    if(isset($this->form_validation->_field_data[$form_name.$form_element])){
                        $this->smarty_tpl->assign($form_name.$form_element, $this->form_validation->_field_data[$form_name.$form_element]);
                    }
                }
                $this->smarty_tpl->assign("error_message", $error_message);
            }
            else {
                $this->smarty_tpl->assign("success_message", $success_message);
                return TRUE;
            }
        }
    }
}

?>
