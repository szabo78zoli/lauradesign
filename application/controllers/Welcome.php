<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	 
	public function __construct(){
		parent::__construct();
		
		$this->smarty_tpl->assign('charset', $this->config->item('charset'));
		$this->smarty_tpl->assign('base_url', $this->config->item('base_url'));
	}
	
	public function index() {
		$this->smarty_tpl->display("base/site/site_base_view.tpl");
	}
}
