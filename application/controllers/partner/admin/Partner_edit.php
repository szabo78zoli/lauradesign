<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Partner_Edit extends Admin_Base_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("partner/admin/Partner_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Partner kezelés");

        $this->load->helper("text");
    }

    public function index() {
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Partner_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Partner_edit_model->form_name."Link", "link", "required");
        if (!isset($_FILES["PartnerImage"]["name"])) {
            $this->form_validation->set_rules($this->Partner_edit_model->form_name . "Image", "kép", "required");
        }
        $this->form_validation->set_rules($this->Partner_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Partner_edit_model->form_elements, $this->Partner_edit_model->form_name, "Az ürlapadatok hibásak!", "A partner adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Partner_edit_model->form_name, $this->Partner_edit_model->form_elements, $this->Partner_edit_model->form_view, $this->Partner_edit_model->db_table, $this->Partner_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Partner_edit_model->form_name, $this->Partner_edit_model->form_elements, $this->Partner_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id){
        parent::editorLoad($id);
        $this->smarty_tpl->assign("Modify", 1);
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Partner_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Partner_edit_model->form_name."Link", "link", "required");
        if (!isset($_FILES["PartnerImage"]["name"])) {
            $this->form_validation->set_rules($this->Partner_edit_model->form_name . "Image", "kép", "required");
        }
        $this->form_validation->set_rules($this->Partner_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Partner_edit_model->form_elements, $this->Partner_edit_model->form_name, "Az ürlapadatok hibásak!", "A partner mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Partner_edit_model->form_name, $this->Partner_edit_model->form_elements, $this->Partner_edit_model->form_view, $id, $this->Partner_edit_model->db_table, $this->Partner_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->Partner_edit_model->form_name, $this->Partner_edit_model->form_elements, $this->Partner_edit_model->form_view, $id, $this->Partner_edit_model->db_table, $this->Partner_edit_model->db_loaded_fields, "id = ".$id, $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Partner_edit_model->form_name, $this->Partner_edit_model->form_elements, $this->Partner_edit_model->form_view, $id, $this->Partner_edit_model->db_table, $this->Partner_edit_model->db_loaded_fields, "id = ".$id, $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        $fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);

        $this->Partner_edit_model->editorInsert($table, $fields);

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új partner hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);


        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        if (isset($this->file["data"]["file_name"])) {
            $fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);
        }
        else{
            unset($fields["image"]);
        }

        $this->Partner_edit_model->editorUpdate($table, $fields, $where, $id);

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Partner módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
        parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->Partner_edit_model->editorLoad($table, $this->fields, $where, $id);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }
        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){

        if(!empty($_FILES["PartnerImage"]["name"])) {
            $this->file = $this->imageUpload(160, 80, "PartnerImage");
        }

        if (isset($this->file["error"])) {
            $this->form_validation->_field_data["PartnerImage"]["field"] = "PartnerImage";
            $this->form_validation->_field_data["PartnerImage"]["label"] = "kép";
            $this->form_validation->_field_data["PartnerImage"]["rules"] = "";
            $this->form_validation->_field_data["PartnerImage"]["errors"] = array();
            $this->form_validation->_field_data["PartnerImage"]["is_array"] = "";
            $this->form_validation->_field_data["PartnerImage"]["keys"] = array();
            $this->form_validation->_field_data["PartnerImage"]["postdata"] = "";
            if ($this->input->post("SaveBtn")) {
                $this->form_validation->_field_data["PartnerImage"]["error"] = $this->file["error"];
            }
        }

        $validation = parent::createFormValidation($form_elements, $form_name, $error_message, $success_message);
        if(($validation == FALSE) || (isset($this->form_validation->_field_data["PartnerImage"]["error"]))){

            foreach ($form_elements as $key => $form_element) {
                if(isset($this->form_validation->_field_data[$form_name.$form_element])){
                    $this->smarty_tpl->assign($form_name.$form_element, $this->form_validation->_field_data[$form_name.$form_element]);
                }
            }

            $this->smarty_tpl->clearAssign("success_message");
            if(isset($_REQUEST["SaveBtn"]) && !empty($_REQUEST["SaveBtn"])){
                $this->smarty_tpl->assign("error_message", $error_message);
            }
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function imageUpload($width, $height, $field){

        if(isset($_FILES[$field]["name"])){
            $file_name = convert_accented_characters($_FILES[$field]["name"]);
        }
        else{
            $file_name = null;
        }
        $file['upload_path'] = "application/upload/partner/";
        $file['allowed_types'] = "gif|jpg|png";
        $file['max_size']	= "20480";
        $file['max_width']  = "4096";
        $file['max_height']  = "3072";
        $file['file_name'] = $file_name;
        $this->load->library("upload", $file);

        if ( ! $this->upload->do_upload($field) ){
            return $error = array("error" => $this->upload->display_errors());
        }
        else{
            $data = array("data" => $this->upload->data());
            $this->imageResize($data["data"]["file_name"], $width, $height);
            return $data;
        }
    }

    public function imageResize($file, $width, $height){
        $image['source_image'] = "application/upload/partner/".$file;
        //$image['create_thumb'] = TRUE;
        $image['maintain_ratio'] = true;
        $image['width'] = $width;
        $image['height'] = $height;

        $this->load->library('image_lib', $image);

        $this->image_lib->clear();
        $this->image_lib->initialize($image);
        if(!$this->image_lib->resize()){
            $error = array("error" => $this->upload->display_errors());
            $this->printR($error);
        }
    }
}
?>