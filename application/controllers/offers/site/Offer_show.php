<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');
class Offer_Show extends Site_Base_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        parent::index();

    }
    public function show($id){
        $this->index();
        $this->load->model("offers/site/Offer_show_model");
        $result = $this->Offer_show_model->contentLoad($this->Offer_show_model->db_table, $this->Offer_show_model->db_loaded_fields, "alias = '".$id."' AND active = 1 AND deleted = 0", "1", "");

        $this->smarty_tpl->assign("tartalom", $result);

        $this->smarty_tpl->assign("tartalom", $result);
        $tartalom = $this->smarty_tpl->fetch($this->Offer_show_model->form_view);

        $this->smarty_tpl->assign("content", $tartalom);
        $this->smarty_tpl->assign("meta_keywords", $result["meta_keyword"]);
        $this->smarty_tpl->display("base/site/site_base_view.tpl");
    }
}

?>
