<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Slider_List extends Admin_Base_Controller{
    public function __construct () {
        parent::__construct();
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Slider kezelés");
        $this->load->model("slider/admin/Slider_list_model");
        $this->smarty_tpl->assign("Lang", $this->Slider_list_model->getSelectValues("lang"));
    }

    public function index() {
        parent::index();
        $this->modifyListForm($this->Slider_list_model->db_table);
        $this->modifyOrderForm();
        $this->smarty_tpl->assign(  "content",
            $this->generateListFormMod($this->generateFilterForm($this->Slider_list_model->form_name, $this->Slider_list_model->form_elements, "slider/admin/slider_filter_view.tpl"),
                $this->Slider_list_model->form_name,
                $this->Slider_list_model->db_table,
                $this->Slider_list_model->db_list_fields,
                $this->Slider_list_model->form_view,
                $this->createListWhere($this->Slider_list_model->db_table, $this->Slider_list_model->form_name, $this->Slider_list_model->db_where),
                $this->Slider_list_model->db_order_by,
                $this->Slider_list_model->db_group_by,
                $this->Slider_list_model->db_list_limit,
                "A lista nem tartalmaz elemeket!"));

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateListFormMod($filter_form, $form_name, $table, $list_fields, $form_view, $where, $order_by, $group_by, $limit, $warning = ""){

        $list = $this->Slider_list_model->listLoad($table, $list_fields, $where, $order_by, $group_by, $limit, $this->uri->segment(3));
        $this->smarty_tpl->assign("FilterForm", $filter_form);
        $this->smarty_tpl->assign("FormName", $form_name);
        $this->smarty_tpl->assign("list", $list);
        $this->smarty_tpl->assign("pagination", $this->createPagination($table, $where, $limit));

        if($list == false){
            $this->smarty_tpl->assign("error_message", $warning);
        }
        return $this->smarty_tpl->fetch($form_view);
    }

    public function modifyOrderForm(){
        if($this->input->post("UpBtn")){
            $this->Slider_list_model->menuOrderUp($this->input->post("UpBtn"));;
            //header( "location:" . $_SERVER['REQUEST_URI']);
        }
        if($this->input->post("DownBtn")){
            $this->Slider_list_model->menuOrderDown($this->input->post("DownBtn"));
            //header( "location:" . $_SERVER['REQUEST_URI']);
        }
    }

    public function modifyListForm($table){
        if($this->input->post("DeleteBtn")){
            $this->Slider_list_model->listStatusModifyDelete($table, array("deleted" => "1", "order_number" => NULL), "id", $this->input->post("DeleteBtn"));
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Slider törlése", "id = ".$this->input->post("DeleteBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("AktivBtn")){
            $this->Admin_base_model->listStatusModify($table, array("active" => "0"), "id", $this->input->post("AktivBtn"));
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Slider inaktiválása", "id = ".$this->input->post("AktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("InAktivBtn")){
            $this->Admin_base_model->listStatusModify($table, array("active" => "1"), "id", $this->input->post("InAktivBtn"));
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Slider aktiválása", "id = ".$this->input->post("InAktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
    }

    public function createListWhere($table, $form, $db_where){

        if($this->input->post("SearchBtn")){
            if($this->input->post($form."Filter")){
                $db_where .= " AND name LIKE '%".$this->input->post($form."Filter")."%'";
            }
            if($this->input->post($form."Lang")){
                $db_where .= " AND lang = ".$this->input->post($form."Lang")."";
            }
        }
        else{
            if($this->session->userdata($form."Filter")){
                $db_where .= " AND name LIKE '%".$this->session->userdata($form."Filter")."%'";
            }
            if($this->session->userdata($form."Lang")){
                $db_where .= " AND lang = ".$this->session->userdata($form."Lang")."";
            }
            else{
                $db_where .= " AND lang = 1";
            }
        }

        return $db_where;

    }
}
?>