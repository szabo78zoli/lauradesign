<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Slider_Edit extends Admin_Base_Controller{

    public $maxImageWidth = 3840;
    public $maxImageHeight = 2400;

    public function __construct (){
        parent::__construct();
        $this->load->model("slider/admin/Slider_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Slider kezelés");

        $getSelectValuesSlider = $this->Slider_edit_model->getSelectValuesSlider();
        $this->smarty_tpl->assign("Slider", $getSelectValuesSlider);
        $this->addValue("Slider", 0, "Kérem, válasszon!");
        $this->setBasicValue($this->Slider_edit_model->form_name, "Slider", 0);

        $this->smarty_tpl->assign("MaxImageWidth", $this->maxImageWidth);
        $this->smarty_tpl->assign("MaxImageHeight", $this->maxImageHeight);

        $this->load->helper("text");
    }

    public function index() {
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Slider_edit_model->form_name."Name", "név", "required");
        if (empty($_FILES['SliderImage']['name'])) {
            $this->form_validation->set_rules($this->Slider_edit_model->form_name."Image", 'kép', 'required');
        }
        $this->form_validation->set_rules($this->Slider_edit_model->form_name."Active", "aktív", "required");

        $validation = $this->createFormValidation($this->Slider_edit_model->form_elements, $this->Slider_edit_model->form_name, "Az ürlapadatok hibásak!", "A slier adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Slider_edit_model->form_name, $this->Slider_edit_model->form_elements, $this->Slider_edit_model->form_view, $this->Slider_edit_model->db_table, $this->Slider_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Slider_edit_model->form_name, $this->Slider_edit_model->form_elements, $this->Slider_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id){
        parent::editorLoad($id);

        $this->smarty_tpl->assign("Image", $this->Slider_edit_model->editorImageLoad($id));
        $this->smarty_tpl->assign("Modify", 1);
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Slider_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Slider_edit_model->form_name."Active", "aktív", "required");

        $validation = $this->createFormValidation($this->Slider_edit_model->form_elements, $this->Slider_edit_model->form_name, "Az ürlapadatok hibásak!", "A slier mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Slider_edit_model->form_name, $this->Slider_edit_model->form_elements, $this->Slider_edit_model->form_view, $id, $this->Slider_edit_model->db_table, $this->Slider_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->Slider_edit_model->form_name, $this->Slider_edit_model->form_elements, $this->Slider_edit_model->form_view, $id, $this->Slider_edit_model->db_table, $this->Slider_edit_model->db_loaded_fields, "id = ".$id, $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Slider_edit_model->form_name, $this->Slider_edit_model->form_elements, $this->Slider_edit_model->form_view, $id, $this->Slider_edit_model->db_table, $this->Slider_edit_model->db_loaded_fields, "id = ".$id, $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateEditForm($form_name, $form_elements, $form_view) {
        $element = array();

        foreach($form_elements as $key => $value){
            $element[$form_name.$value]["field"] = $form_name.$value;
            $getElementValues = $this->smarty_tpl->getTemplateVars($form_name.$value);

            if(!isset($getElementValues["postdata"])){
                $element[$form_name.$value]["postdata"] = $this->input->post($form_name.$value);
            }
            else{
                $element[$form_name.$value]["postdata"] = $getElementValues["postdata"];
            }

            if(isset($this->form_validation->_field_data[$form_name.$value]["error"])){
                $element[$form_name.$value]["error"] = $this->form_validation->_field_data[$form_name.$value]["error"];
            }
            else{
                $element[$form_name.$value]["error"] = "";
            }
        }

        $element[$form_name."Before"]["field"] = "SliderBefore";
        $element[$form_name."Before"]["postdata"] = $this->input->post("SliderBefore");
        $element[$form_name."Before"]["error"] = "";

        $this->smarty_tpl->assign("FormName", $form_name);
        $this->smarty_tpl->assign($element, $form_name.$value);

        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        $fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);
        $this->Slider_edit_model->editorInsert($table, $fields);

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új slier hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);

        $this->element[$form_name."Before"]["field"] = "SliderBefore";
        $this->element[$form_name."Before"]["postdata"] = $this->input->post("SliderBefore");
        $this->element[$form_name."Before"]["error"] = "";

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        if (isset($this->file["data"]["file_name"])) {
            $fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);
        }
        else{
            unset($fields["image"]);
        }

        $this->Slider_edit_model->editorUpdate($table, $fields, $where, $id);

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Slier módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        $this->element[$form_name."Before"]["field"] = "SliderBefore";
        $this->element[$form_name."Before"]["postdata"] = $this->input->post("SliderBefore");
        $this->element[$form_name."Before"]["error"] = "";
        $this->smarty_tpl->assign("Image", $this->Slider_edit_model->editorImageLoad($id));

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
        parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->Slider_edit_model->editorLoad($table, $this->fields, $where, $id);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }

        $this->element[$form_name."Before"]["field"] = "SliderBefore";
        $this->element[$form_name."Before"]["postdata"] = $this->Slider_edit_model->getBefore($id);
        $this->element[$form_name."Before"]["error"] = "";

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){

        if(!empty($_FILES["SliderImage"]["name"])) {
            $this->file = $this->fileUpload();
        }

        if (isset($this->file["error"])) {
            $this->form_validation->_field_data["SliderImage"]["field"] = "SliderImage";
            $this->form_validation->_field_data["SliderImage"]["label"] = "kép";
            $this->form_validation->_field_data["SliderImage"]["rules"] = "";
            $this->form_validation->_field_data["SliderImage"]["errors"] = array();
            $this->form_validation->_field_data["SliderImage"]["is_array"] = "";
            $this->form_validation->_field_data["SliderImage"]["keys"] = array();
            $this->form_validation->_field_data["SliderImage"]["postdata"] = "";
            if ($this->input->post("SaveBtn") && $this->uri->segment(3)) {
                $this->form_validation->_field_data["SliderImage"]["error"] = $this->file["error"];
            }
        }

        $validation = parent::createFormValidation($form_elements, $form_name, $error_message = "", $success_message = "");
        if(($validation == FALSE) || (isset($this->form_validation->_field_data["SliderImage"]["error"]))){
            $this->smarty_tpl->clearAssign("success_message");
            if($this->input->post("SaveBtn")){
                $this->smarty_tpl->assign("error_message", $error_message);
            }
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function fileUpload(){
        if(isset($_FILES["SliderImage"]["name"])){
            $file_name = convert_accented_characters($_FILES["SliderImage"]["name"]);
        }
        else{
            $file_name = null;
        }
        $file['upload_path'] = "application/upload/slider/";
        $file['allowed_types'] = "gif|jpg|png";
        $file['max_size']	= "20480";
        $file['max_width']  = $this->maxImageWidth;
        $file['max_height']  = $this->maxImageHeight;
        $file['file_name'] = $file_name;
        $this->load->library("upload", $file);

        if ( ! $this->upload->do_upload("SliderImage") ){
            return $error = array("error" => $this->upload->display_errors());
        }
        else{
            $data = array("data" => $this->upload->data());
            $this->imageResize($data["data"]["file_name"]);
            return $data;
        }
    }

    public function imageResize($file){
        $image['source_image']	= "application/upload/slider/".$file;
        $image['maintain_ratio'] = TRUE;
        $image['width']	 = 1920;
        $image['height'] = 880;

        $this->load->library('image_lib', $image);

        if(!$this->image_lib->resize()){
//            echo $this->image_lib->display_errors();
        }
    }
}
?>