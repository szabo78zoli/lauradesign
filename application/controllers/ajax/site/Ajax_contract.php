<?php
class Ajax_Contract extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("ajax/site/Ajax_contract_model");
        $this->load->helper('form');
        $this->lang->load("site_lang", $this->session->userdata("Site_Lang_Azon"));

        if($this->session->userdata("Site_Lang_Azon") == "hu") {
            $this->lang->load('form_validation_lang', 'hungarian');
        }
        elseif($this->session->userdata("Site_Lang_Azon") == "en"){
            $this->lang->load('form_validation_lang', 'english');
        }
    }
    public function index() {

    }

    public function contract(){

            $this->load->library("form_validation");
            $this->form_validation->set_rules("ContractName", $this->input->post("ContractNameLabel"), "required");
            $this->form_validation->set_rules("ContractAddress", $this->input->post("ContractAddressLabel"), "required");
            $this->form_validation->set_rules("ContractPhone", $this->input->post("ContractPhoneLabel"), "required");
            $this->form_validation->set_rules("ContractEmail", $this->input->post("ContractEmailLabel"), "required|valid_email");


            if ($this->form_validation->run() == TRUE){

                $this->sendMail(    $this->input->post("ContractName"),
                                    $this->input->post("ContractAddress"),
                                    $this->input->post("ContractPhone"),
                                    $this->input->post("ContractEmail"),
                                    $this->input->post("ContractMaterial"),
                                    $this->input->post("ContractProfile"),
                                    $this->input->post("ContractDoorWindowTypeProfile"),
                                    $this->input->post("ContractOpeningMode"),
                                    $this->input->post("ContractGlass"),
                                    $this->input->post("ContractWith"),
                                    $this->input->post("ContractHeight"),
                                    $this->input->post("ContractProvide"),
                                    $this->input->post("ContractPierce"),
									$this->input->post("ContractComment"));
                echo "1";
                exit();
            }
            elseif($this->form_validation->run() == FALSE){
                $error = "";
                foreach($this->form_validation->_field_data as $key => $row){
                    if(!empty($row["error"])){
                        $error .= $row["error"]."<br/>";
                    }
                }
                echo "<div class='newsletter_error'>".$this->lang->language["message_error_lang"]."</br>".$error."</div>";
                exit();
            }
    }

    public function sendMail($name, $address, $phone, $email, $material, $profile, $doorWindowTypeProfile, $openingMode, $glass, $with, $height, $provide, $pierce, $comment){
        $html = "<div style='font-weight:600;'>Ajánlatkérés a www.ajtoablakprofil.hu oldalól!</div>";
        $html .= "<br/>";

        $html .= "<table border='0' cellspacing='0px' cellpadding='5px' style='border: 0px solid #B8B8B8;border-collapse: collapse'>";
        $html .= "<tr>";
        $html .= "<td style='font-weight:600;'>Név:</td>";
        $html .= "<td>".$name."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td style='font-weight:600;'>Cím:</td>";
        $html .= "<td>".$address."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td style='font-weight:600;'>Telefonszám:</td>";
        $html .= "<td>".$phone."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td style='font-weight:600;'>E-mail cím:</td>";
        $html .= "<td>".$email."</td>";
        $html .= "</tr>";

        $html2 = "<table border='1'>";
        $html2 .= "<tr>";
        $html2 .= "<td></td>";
        $html2 .= "<td>Anyag</td>";
        $html2 .= "<td>Profil</td>";
        $html2 .= "<td>Megnevezés</td>";
        $html2 .= "<td>Nyitásmód</td>";
        $html2 .= "<td>Üvegezés</td>";
        $html2 .= "<td>Szélesség (cm)</td>";
        $html2 .= "<td>Magasság (cm)</td>";
        $html2 .= "<td>Darab</td>";
        $html2 .= "<td>Szolgáltatás</td>";
        $html2 .= "</tr>";

        $materialArray = $this->Ajax_contract_model->getSelectValues('material');
        $profileArray = $this->Ajax_contract_model->getSelectValues('profile');
        $doorWindowTypeArray = $this->Ajax_contract_model->getSelectValues('door_window_type');
        $openingModeArray = $this->Ajax_contract_model->getSelectValues('opening_mode');

        $glassArray = $this->Ajax_contract_model->getSelectValues('glass');

        $provideArray = $this->Ajax_contract_model->getSelectValues('provide');

        parse_str(urldecode($_POST["ContractMaterial"]), $Material);
        $Material = $Material["ContractMaterial"];

        parse_str(urldecode($_POST["ContractProfile"]), $Profile);
        $Profile = $Profile["ContractProfile"];

        parse_str(urldecode($_POST["ContractDoorWindowTypeProfile"]), $ContractDoorWindowTypeProfile);
        $ContractDoorWindowTypeProfile = $ContractDoorWindowTypeProfile["ContractDoorWindowTypeProfile"];

        parse_str(urldecode($_POST["ContractOpeningMode"]), $ContractOpeningMode);
        $ContractOpeningMode = $ContractOpeningMode["ContractOpeningMode"];

        parse_str(urldecode($_POST["ContractGlass"]), $ContractGlass);
        $ContractGlass = $ContractGlass["ContractGlass"];

        parse_str(urldecode($_POST["ContractWith"]), $ContractWith);
        $ContractWith = $ContractWith["ContractWith"];

        parse_str(urldecode($_POST["ContractHeight"]), $ContractHeight);
        $ContractHeight = $ContractHeight["ContractHeight"];

        parse_str(urldecode($_POST["ContractPierce"]), $ContractPierce);
        $ContractPierce = $ContractPierce["ContractPierce"];

        parse_str(urldecode($_POST["ContractProvide"]), $ContractProvide);
        $ContractProvide = $ContractProvide["ContractProvide"];

        parse_str(urldecode($_POST["ContractComment"]), $ContractComment);
        $ContractComment = $ContractComment["ContractComment"];

        foreach($Material as $key => $row){

            $rowNumber = $key+1;
            $html2 .= "<tr>";
            $html2 .= "<td>$rowNumber</td>";
            $html2 .= "<td>".$materialArray[$Material[$key]]."</td>";
            $html2 .= "<td>".$profileArray[$Profile[$key]]."</td>";
            $html2 .= "<td>".$doorWindowTypeArray[$ContractDoorWindowTypeProfile[$key]]."</td>";
            $html2 .= "<td>".$openingModeArray[$ContractOpeningMode[$key]]."</td>";
            $html2 .= "<td>".$glassArray[$ContractGlass[$key]]."</td>";
            $html2 .= "<td>".$ContractWith[$key]."</td>";
            $html2 .= "<td>".$ContractHeight[$key]."</td>";
            $html2 .= "<td>".$ContractPierce[$key]."</td>";
            $html2 .= "<td>".$provideArray[$ContractProvide[$key]]."</td>";
            $html2 .= "</tr>";
        }

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:600;'>Választott termékek, és paramétereik:</td>";
        $html .= "</tr>";

        $html2 .= "</table>";

        $html .= "<tr>";
        $html .= "<td>".$html2."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>Megjegyzés</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td>".$ContractComment."</td>";
        $html .= "</tr>";

        $html .= "</table>";

        $this->load->library('email');

        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($email, 'www.ajtoablakprofil.hu');
        $this->email->to("info@ajtoablakprofil.hu");
		//$this->email->to("szabozli@gmail.com");
        $this->email->cc($email);
        $this->email->bcc("szabozli@gmail.com");
        $this->email->subject('Ajánlatkérés');
        $this->email->message($html);
        $this->email->send();
    }
    
}
?>