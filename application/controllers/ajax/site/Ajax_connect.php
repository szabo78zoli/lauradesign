<?php
class Ajax_Connect extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("ajax/site/Ajax_connect_model");
        $this->load->helper('form');
        $this->lang->load("site_lang", $this->session->userdata("Site_Lang_Azon"));

        if($this->session->userdata("Site_Lang_Azon") == "hu") {
            $this->lang->load('form_validation_lang', 'hungarian');
        }
        elseif($this->session->userdata("Site_Lang_Azon") == "en"){
            $this->lang->load('form_validation_lang', 'english');
        }
    }
    public function index() {

    }

    public function connect(){

            $this->load->library("form_validation");
            $this->form_validation->set_rules("ConnectName", $this->input->post("ConnectNameLabel"), "required");
            $this->form_validation->set_rules("ConnectAddress", $this->input->post("ConnectAddressLabel"), "required");
            $this->form_validation->set_rules("ConnectPhone", $this->input->post("ConnectPhoneLabel"), "required");
            $this->form_validation->set_rules("ConnectEmail", $this->input->post("ConnectEmailLabel"), "required|valid_email");
            $this->form_validation->set_rules("ConnectMessage", $this->input->post("ConnectMessageLabel"), "required");


            if ($this->form_validation->run() == TRUE){
                $this->sendMail(    $this->input->post("ConnectName"),
                                    $this->input->post("ConnectAddress"),
                                    $this->input->post("ConnectPhone"),
                                    $this->input->post("ConnectEmail"),
                                    $this->input->post("ConnectMessage"));

                echo "<div class='newsletter_success'>".$this->lang->language["message_success_lang"]."</div>";
                exit();
            }
            elseif($this->form_validation->run() == FALSE){
                $error = "";
                foreach($this->form_validation->_field_data as $key => $row){
                    if(!empty($row["error"])){
                        $error .= $row["error"]."<br/>";
                    }
                }
                echo "<div class='newsletter_error'>".$this->lang->language["message_error_lang"]."</br>".$error."</div>";
                exit();
            }
    }

    public function sendMail($name, $address, $phone, $email, $message){

        $html = "<div style='font-weight:600;'>Üzenet a www.ajtoablakprofil.hu oldalól!.</div>";
        $html .= "<br/>";

        $html .= "<table border='0' cellspacing='0px' cellpadding='5px' style='border: 0px solid #B8B8B8;border-collapse: collapse'>";
        $html .= "<tr>";
        $html .= "<td style='font-weight:600;'>Név:</td>";
        $html .= "<td>".$name."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td style='font-weight:600;'>Cím:</td>";
        $html .= "<td>".$address."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td style='font-weight:600;'>Telefonszám:</td>";
        $html .= "<td>".$phone."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td style='font-weight:600;'>E-mail cím:</td>";
        $html .= "<td>".$email."</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2' style='font-weight:600;'>Üzenet:</td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<td colspan='2'>".$message."</td>";
        $html .= "</tr>";

        $html .= "</table>";

        $this->load->library('email');

        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        //$this->email->from("noreply@ajtoablakprofil.hu", 'www.ajtoablakprofil.hu');
        $this->email->from($email, 'www.ajtoablakprofil.hu');
        $this->email->to("info@ajtoablakprofil.hu");
        $this->email->cc("szabozli@gmail.com");
        $this->email->cc($email);
        $this->email->subject('Kapcsolat felvétel');
        $this->email->message($html);
        $this->email->send();
    }
}
?>