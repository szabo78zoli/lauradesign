<?php
class Ajax_Imagegallery_Delete extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("ajax/admin/Ajax_imagegallery_delete_model");
    }
    public function index() {

    }
    public function deleteImage($id) {

        $deleteResult = $this->Ajax_imagegallery_delete_model->deleteImage($id);

        $imageResult = $this->Ajax_imagegallery_delete_model->getImage($deleteResult);

        $html = "";

        if(isset($imageResult) && !empty($imageResult)){
            foreach($imageResult as $key => $row){
                $html .= '<div style="float: left; margin: 3px;">';
                $html .= '<img src="'.$this->config->item('base_url').'application/upload/product_gallery/'.$row["image"].'" height="100" />';
                $html .= '<br/>';
                $html .= '<button class="btn btn-danger btn-xs imageDelete" type="button" name="ImageGalleryDeleteBtn" value="'.$row["id"].'" >Törlés</button>';
                $html .= '</div>';
            }
            $html .= '<div style="clear: both;"></div>';
            echo $html;
        }
    }

    public function deleteProjectImage($id) {

        $deleteResult = $this->Ajax_imagegallery_delete_model->deleteProjectImage($id);

        if($deleteResult){
            echo 1;
        }

    }
}
?>