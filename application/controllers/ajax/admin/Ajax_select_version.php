<?php
class Ajax_Select_Version extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("ajax/admin/Ajax_select_version_model");
    }
    public function index() {

    }
    public function getVersion($id) {
        $result = $this->Ajax_select_version_model->getVersion($id);

        $html = '<option value="0" class="form-control option">Kérem, válasszon!</option>';

        foreach ($result as $key => $row) {
            $html .= '<option value="'.$key.'"  class="form-control option">'.$row.'</option>';
        }

        echo $html;
    }
}
?>