<?php
class Ajax_Select_Category extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("ajax/admin/Ajax_select_category_model");
    }
    public function index() {

    }

    public function subCategory($id) {
        $result = $this->Ajax_select_category_model->subCategory($id);

        $html = '<option value="0" class="form-control option">Kérem, válasszon!</option>';

        foreach ($result as $key => $row) {
            $html .= '<option value="'.$key.'"  class="form-control option">'.$row.'</option>';
        }

        echo $html;
    }

    public function category($id) {
        $result = $this->Ajax_select_category_model->category($id);

        $html = '<option value="0" class="form-control option">Kérem, válasszon!</option>';

        foreach ($result as $key => $row) {
            $html .= '<option value="'.$key.'"  class="form-control option">'.$row.'</option>';
        }

        echo $html;
    }

    public function product($MainCategory, $subCategory, $category) {
        $result = $this->Ajax_select_category_model->product($MainCategory, $subCategory, $category);

        $html = '<option value="0" class="form-control option">Kérem, válasszon!</option>';

        foreach ($result as $key => $row) {
            $html .= '<option value="'.$key.'"  class="form-control option">'.$row["name"].' - '.$row["item_number"].'</option>';
        }

        echo $html;
    }

    public function selectSubCategory($id){
        echo $result = $this->Ajax_select_category_model->selectSubCategory($id);
    }
}
?>