<?php
class Ajax_Right extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("ajax/admin/Ajax_right_model");
    }
    public function index() {

    }
    public function right($siteType, $id) {

        $rightsInGroup = array();

        if($id != 0) {
            $rightsInGroup = $this->Ajax_right_model->getRightLoad($id);
        }

        $result = "";
        $checked = "";

        $jog = $this->Ajax_right_model->getRight($siteType);
        if(!$jog == false){
            foreach ($jog as $key => $value) {
                if(!empty($rightsInGroup)) {
                    if (in_array($key, $rightsInGroup)) {
                        $checked = "checked";
                    } else {
                        $checked = "";
                    }
                }
                $result .= '<div class="form-group"><label><input '.$checked.' type="checkbox" name="AdminRightGroupRight[]" value="'.$key.'" class="checkbox rightCheckBox" />'.$value.'</label></div>';
            }
            echo $result;
        }
        else{
            echo "";
        }
    }
    /*public function rightsLoad($id) {
        $result = "";
        $jog = $this->Ajax_right_model->getRightLoad($id);
        if(!$jog == false){
            foreach ($jog as $key => $value) {
print_r($value);
                $result .= '<label><input type="checkbox" name="JogosultsagCsoportJog[]" value="'.$key.'" class="checkbox" />'.$value.'</label>';
            }
            echo $result;
        }
        else{
            echo "";
        }
//        echo "2";
        //echo json_encode($jog);
    }*/
}
?>