<?php
class Ajax_Convert_Accented_Characters extends CI_Controller {
    public function __construct() {       
        parent::__construct();
        
        $this->load->helper('text');
        $this->load->helper('url');
    }
    public function index() {     

    }  
    public function convert($szoveg) {
        echo url_title((convert_accented_characters(urldecode($szoveg))));
    }    
}    
?>

