<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class User_Site_Edit extends Admin_Base_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("user/admin/User_site_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Ügyfél kezelés");

        $this->load->helper("text");

        if($this->input->post("SaveImageBtn") !== NULL){
            $this->saveImageToProject();
        }
    }

    public function index() {
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."Email", "email", "required|valid_email|matches[" . $this->User_site_edit_model->form_name . "EmailAgain]|is_unique[user_site.email]");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."EmailAgain", "email ismét", "required|valid_email|matches[" . $this->User_site_edit_model->form_name . "Email]");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."Password", "jelszó", "required|matches[" . $this->User_site_edit_model->form_name . "PasswordAgain]");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."PasswordAgain", "jelszó újra", "required|matches[" . $this->User_site_edit_model->form_name . "Password]");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."FirstName", "keresztnév", "required");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."LastName", "vezetéknév", "required");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."Phone", "telefonszám", "required");   
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."Active", "aktív felhasználó", "required");

        if($this->input->post("SiteUserRegisterType") == 2){
            $userInvoiceTaxNumber = $this->input->post('SiteUserInvoiceTaxNumber');
            if (!empty($userInvoiceTaxNumber)) {
                $this->form_validation->set_rules($this->User_site_edit_model->form_name . "InvoiceTaxNumber[]", "adószám", "required");
            }
        }

        $validation = $this->createFormValidation($this->User_site_edit_model->form_elements, $this->User_site_edit_model->form_name, "Az ürlapadatok hibásak!", "A felhasználó adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->User_site_edit_model->form_name, $this->User_site_edit_model->form_elements, $this->User_site_edit_model->form_view, $this->User_site_edit_model->db_table, $this->User_site_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->User_site_edit_model->form_name, $this->User_site_edit_model->form_elements, $this->User_site_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id){
        parent::editorLoad($id);

        $getSelectProjectsValues = $this->User_site_edit_model->getSelectValues('user_site_project');

        if(!empty($getSelectProjectsValues)){
            $this->smarty_tpl->assign("SelectProjects", $getSelectProjectsValues);
            $this->addValue("SelectProjects", 0, "Kérem, válasszon!");
            $this->setBasicValue($this->User_site_edit_model->form_name, "SelectProjects", 0);
        }


        $getSelectProjectsValues = array();
        $this->smarty_tpl->assign("SelectVersions", $getSelectProjectsValues);
        $this->addValue("SelectVersions", 0, "Kérem, válasszon!");
        $this->setBasicValue($this->User_site_edit_model->form_name, "SelectVersions", 0);

        $this->smarty_tpl->assign("Modify", 1);

        if($this->input->post("SaveProjectBtn")){
            $this->User_site_edit_model->saveProject($id, $this->input->post("UserSiteProject"), $this->input->post("UserSiteProjectAlias"));
        }

        if($this->input->post("SaveProjectVersionBtn")){
            $this->User_site_edit_model->saveProjectVersion($this->input->post("UserSiteSelectProjects"), $this->input->post("UserSiteProjectVersion"), $this->input->post("UserSiteProjectVersionAlias"));
        }

//$this->printR($this->User_site_edit_model->getProject($id)); //die();
        $this->smarty_tpl->assign("projects", $this->User_site_edit_model->getProject($id));

        //$this->smarty_tpl->assign("projectsVersion", $this->User_site_edit_model->getProjectVersion($id));


        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."Email", "email", "required|valid_email|matches[" . $this->User_site_edit_model->form_name . "EmailAgain]");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."EmailAgain", "email ismét", "required|valid_email|matches[" . $this->User_site_edit_model->form_name . "Email]");


        if($this->input->post($this->User_site_edit_model->form_name."Password")){
            $this->form_validation->set_rules($this->User_site_edit_model->form_name."PasswordAgain", "jelszó újra", "required");
        }
        if($this->input->post($this->User_site_edit_model->form_name."Password") && $this->input->post($this->User_site_edit_model->form_name."PasswordAgain")){
            $this->form_validation->set_rules($this->User_site_edit_model->form_name."Password", "jelszó újra", "required|matches[" . $this->User_site_edit_model->form_name . "PasswordAgain]");
            $this->form_validation->set_rules($this->User_site_edit_model->form_name."PasswordAgain", "jelszó", "required|matches[" . $this->User_site_edit_model->form_name . "Password]");
        }

        $this->form_validation->set_rules($this->User_site_edit_model->form_name."FirstName", "keresztnév", "required");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."LastName", "vezetéknév", "required");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."Phone", "telefonszám", "required");
        $this->form_validation->set_rules($this->User_site_edit_model->form_name."Active", "aktív felhasználó", "required");

        $validation = $this->createFormValidation($this->User_site_edit_model->form_elements, $this->User_site_edit_model->form_name, "Az ürlapadatok hibásak!", "A Ügyfél adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->User_site_edit_model->form_name, $this->User_site_edit_model->form_elements, $this->User_site_edit_model->form_view, $id, $this->User_site_edit_model->db_table, $this->User_site_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->User_site_edit_model->form_name, $this->User_site_edit_model->form_elements, $this->User_site_edit_model->form_view, $id, $this->User_site_edit_model->db_table, $this->User_site_edit_model->db_loaded_fields, "id = ".$id, $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->User_site_edit_model->form_name, $this->User_site_edit_model->form_elements, $this->User_site_edit_model->form_view, $id, $this->User_site_edit_model->db_table, $this->User_site_edit_model->db_loaded_fields, "id = ".$id, $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        $insert_id = $this->User_site_edit_model->editorInsert($table, $fields);

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új Ügyfél hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);


        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }


        $this->User_site_edit_model->editorUpdate($table, $fields, $where, $id);
//$this->printR($this->input->post());
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Ügyfél módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
        parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->User_site_edit_model->editorLoad($table, $this->fields, $where, $id);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){

        if(!empty($_FILES["UserSiteProjectGallery"]["name"][0])) {
            foreach($_FILES["UserSiteProjectGallery"]["name"] as $key => $row){

                $_FILES["UserSiteProjectGalleryUpload"]["name"] = $_FILES["UserSiteProjectGallery"]["name"][$key];
                $_FILES["UserSiteProjectGalleryUpload"]["type"] = $_FILES["UserSiteProjectGallery"]["type"][$key];
                $_FILES["UserSiteProjectGalleryUpload"]["tmp_name"] = $_FILES["UserSiteProjectGallery"]["tmp_name"][$key];
                $_FILES["UserSiteProjectGalleryUpload"]["error"] = $_FILES["UserSiteProjectGallery"]["error"][$key];
                $_FILES["UserSiteProjectGalleryUpload"]["size"] = $_FILES["UserSiteProjectGallery"]["size"][$key];

                //$this->files = $this->fileUpload(800, 800, "UserSiteProjectGalleryUpload");

                if (isset($this->files["error"])) {
                    $this->form_validation->_field_data["UserSiteProjectGallery"]["field"] = "Gallery";
                    $this->form_validation->_field_data["UserSiteProjectGallery"]["label"] = "kép";
                    $this->form_validation->_field_data["UserSiteProjectGallery"]["rules"] = "";
                    $this->form_validation->_field_data["UserSiteProjectGallery"]["errors"] = array();
                    $this->form_validation->_field_data["UserSiteProjectGallery"]["is_array"] = "";
                    $this->form_validation->_field_data["UserSiteProjectGallery"]["keys"] = array();
                    $this->form_validation->_field_data["UserSiteProjectGallery"]["postdata"] = "";
                    if ($this->input->post("SaveBtn")) {
                        $this->form_validation->_field_data["UserSiteProjectGallery"]["error"] = $this->files["error"];
                    }
                }
            }
        }

        $validation = parent::createFormValidation($form_elements, $form_name, $error_message, $success_message);
        if(($validation == FALSE) || (isset($this->form_validation->_field_data["UserSiteProjectGallery"]["error"]))){

            foreach ($form_elements as $key => $form_element) {
                if(isset($this->form_validation->_field_data[$form_name.$form_element])){
                    $this->smarty_tpl->assign($form_name.$form_element, $this->form_validation->_field_data[$form_name.$form_element]);
                }
            }

            $this->smarty_tpl->clearAssign("success_message");
            if(isset($_REQUEST["SaveBtn"]) && !empty($_REQUEST["SaveBtn"])){
                $this->smarty_tpl->assign("error_message", $error_message);
            }
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function saveImageToProject(){
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "projekt kép hozzáadás", "id = ".$this->uri->segment(2)." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        if (isset($_FILES["UserSiteProjectGallery"]["name"]) && !empty($_FILES["UserSiteProjectGallery"]["name"][0])) {

            foreach ($_FILES["UserSiteProjectGallery"]["name"] as $key => $value) {
                $imageName = convert_accented_characters($value);

                $_FILES["UserSiteProjectGalleryUpload"]["name"] = $_FILES["UserSiteProjectGallery"]["name"][$key];;
                $_FILES["UserSiteProjectGalleryUpload"]["type"] = $_FILES["UserSiteProjectGallery"]["type"][$key];
                $_FILES["UserSiteProjectGalleryUpload"]["tmp_name"] = $_FILES["UserSiteProjectGallery"]["tmp_name"][$key];
                $_FILES["UserSiteProjectGalleryUpload"]["error"] = $_FILES["UserSiteProjectGallery"]["error"][$key];
                $_FILES["UserSiteProjectGalleryUpload"]["size"] = $_FILES["UserSiteProjectGallery"]["size"][$key];

                $uploadedFile = $this->file;
                $uploadedFile = $this->file = $this->fileUpload(800, 800, "UserSiteProjectGalleryUpload", $imageName);

                $position = strpos($uploadedFile["data"]["file_name"], ".");
                $name = substr($uploadedFile["data"]["file_name"], 0, $position);

                $imageFields["name"] = $name;
                $imageFields["image"] = $uploadedFile["data"]["file_name"];

                if($this->input->post("SaveImageBtn") !== NULL){
                    $this->User_site_edit_model->saveImageToProject($this->input->post("UserSiteProjectSelect"), $this->input->post("UserSiteVersionSelect"), $imageFields);
                }

            }
        }
    }

    public function fileUpload($width, $height, $field){

        if(isset($_FILES[$field]["name"])){
            $file_name = convert_accented_characters($_FILES[$field]["name"]);
        }
        else{
            $file_name = null;
        }

        $file['upload_path'] = "application/upload/project_gallery/";
        $file['allowed_types'] = "gif|jpg|png";
        $file['max_size']	= "1024000";
        $file['max_width']  = "8192";
        $file['max_height']  = "6144";
        $file['file_name'] = $file_name;
        $this->load->library("upload", $file);

        if ( ! $this->upload->do_upload($field) ){
            return $error = array("error" => $this->upload->display_errors());
        }
        else{
            $data = array("data" => $this->upload->data());
            $this->imageResize($data["data"]["file_name"], $width, $height);

            return $data;
        }
    }

    public function imageResize($file, $width, $height){
        $image['source_image'] = "application/upload/project_gallery/".$file;
        $image['maintain_ratio'] = true;
        $image['width'] = $width;
        $image['height'] = $height;

        $this->load->library('image_lib', $image);

        $this->image_lib->clear();
        $this->image_lib->initialize($image);
        if(!$this->image_lib->resize()){
            $error = array("error" => $this->upload->display_errors());
//$this->printR($error);
        }
    }
}
?>