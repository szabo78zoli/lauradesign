<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class User_Admin_Edit extends Admin_Base_Controller{
	public function __construct (){
		parent::__construct();
		$this->load->model("user/admin/User_admin_edit_model");
		$this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Felhasználó kezelés");

		$getSelectValues = $this->User_admin_edit_model->getSelectValues('rights_group');
		$this->smarty_tpl->assign("RightsGroup", $getSelectValues);
		$this->addValue("RightsGroup", 0, "Kérem, válasszon!");
		$this->setBasicValue($this->User_admin_edit_model->form_name, "RightsGroup", 0);

		$this->load->helper("text");
	}

	public function index() {
		parent::index();

		$this->load->library("form_validation");
		$this->form_validation->set_rules($this->User_admin_edit_model->form_name."Name", "név", "required");
		$this->form_validation->set_rules($this->User_admin_edit_model->form_name."Email", "email cím", "required");
		$this->form_validation->set_rules($this->User_admin_edit_model->form_name."Password", "jelszó", "required|matches[" . $this->User_admin_edit_model->form_name . "PasswordAgain]");
		$this->form_validation->set_rules($this->User_admin_edit_model->form_name."PasswordAgain", "jelszó újra", "required|matches[" . $this->User_admin_edit_model->form_name . "Password]");
        $this->form_validation->set_rules($this->User_admin_edit_model->form_name."RightsGroup", "jogosultság csoport", "required");
		$this->form_validation->set_rules($this->User_admin_edit_model->form_name."Active", "aktív felhasználó", "required");
		$validation = $this->createFormValidation($this->User_admin_edit_model->form_elements, $this->User_admin_edit_model->form_name, "Az ürlapadatok hibásak!", "A felhasználó adatok mentése sikeres.");

		if ($validation == TRUE){
			$this->smarty_tpl->assign("content", $this->generateInsertForm($this->User_admin_edit_model->form_name, $this->User_admin_edit_model->form_elements, $this->User_admin_edit_model->form_view, $this->User_admin_edit_model->db_table, $this->User_admin_edit_model->db_edited_fields));
		}
		elseif($validation != TRUE){
			$this->smarty_tpl->assign("content", $this->generateEditForm($this->User_admin_edit_model->form_name, $this->User_admin_edit_model->form_elements, $this->User_admin_edit_model->form_view));
		}
		$this->smarty_tpl->display("base/admin/admin_base_view.tpl");
	}

	public function editorLoad($id){
		parent::editorLoad($id);

		$this->smarty_tpl->assign("getUserAdminImage", $this->User_admin_edit_model->editorImageLoad($id));

		$this->load->library("form_validation");
		$this->form_validation->set_rules($this->User_admin_edit_model->form_name."Name", "név", "required");
		$this->form_validation->set_rules($this->User_admin_edit_model->form_name."Email", "email cím", "required");
		//$this->form_validation->set_rules($this->User_admin_edit_model->form_name."Password", "jelszó", "required");


		if($this->input->post($this->User_admin_edit_model->form_name."Password")){
			$this->form_validation->set_rules($this->User_admin_edit_model->form_name."PasswordAgain", "jelszó újra", "required");
		}
		if($this->input->post($this->User_admin_edit_model->form_name."Password") && $this->input->post($this->User_admin_edit_model->form_name."PasswordAgain")){
			$this->form_validation->set_rules($this->User_admin_edit_model->form_name."Password", "jelszó újra", "required|matches[" . $this->User_admin_edit_model->form_name . "PasswordAgain]");
			$this->form_validation->set_rules($this->User_admin_edit_model->form_name."PasswordAgain", "jelszó", "required|matches[" . $this->User_admin_edit_model->form_name . "Password]");
		}

		$validation = $this->createFormValidation($this->User_admin_edit_model->form_elements, $this->User_admin_edit_model->form_name, "Az ürlapadatok hibásak!", "A felhasználó adatok mentése sikeres.");

		if ($validation == TRUE){
			$this->smarty_tpl->assign("content", $this->generateUpdateForm($this->User_admin_edit_model->form_name, $this->User_admin_edit_model->form_elements, $this->User_admin_edit_model->form_view, $id, $this->User_admin_edit_model->db_table, $this->User_admin_edit_model->db_edited_fields, "id = ", $id));
		}
		elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
			$this->smarty_tpl->assign("content", $this->generateLoadedForm($this->User_admin_edit_model->form_name, $this->User_admin_edit_model->form_elements, $this->User_admin_edit_model->form_view, $id, $this->User_admin_edit_model->db_table, $this->User_admin_edit_model->db_loaded_fields, "id = ".$id, $id));
		}
		elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
			$this->smarty_tpl->assign("content", $this->generateEditForm($this->User_admin_edit_model->form_name, $this->User_admin_edit_model->form_elements, $this->User_admin_edit_model->form_view, $id, $this->User_admin_edit_model->db_table, $this->User_admin_edit_model->db_loaded_fields, "id = ".$id, $id));
		}

		$this->smarty_tpl->display("base/admin/admin_base_view.tpl");
	}

	public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
		parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

		foreach($db_fields as $key => $value){
			$fields[$key] = $this->input->post($form_name.$value);
		}

		$insert_id = $this->User_admin_edit_model->editorInsert($table, $fields);

		$id = $this->db->insert_id();
		$this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új admin felhasználó hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);

		// kép berakása
		//$this->file = $this->fileUpload($insert_id);
		$fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);

		$this->User_admin_edit_model->editorImageUpdate($table, $fields["image"], $insert_id);

		$this->smarty_tpl->assign($this->element, $form_name.$value);
		return $this->smarty_tpl->fetch($form_view);
	}

	public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
		parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

		foreach($db_fields as $key => $value){
			$fields[$key] = $this->input->post($form_name.$value);
		}

		if (isset($this->file["data"]["file_name"])) {
			$fields["image"] = convert_accented_characters($this->file["data"]["file_name"]);
		}
		else{
			unset($fields["image"]);
		}

		$this->User_admin_edit_model->editorUpdate($table, $fields, $where, $id);

		$this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Admin felhasználó módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

		$this->smarty_tpl->assign($this->element, $form_name.$value);
		return $this->smarty_tpl->fetch($form_view);
	}

	public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
		parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

		foreach ($db_fields as $key => $value){
			$this->fields .= $key.", ";
		}

		$loadedData = $this->User_admin_edit_model->editorLoad($table, $this->fields, $where, $id);
		foreach ($db_fields as $key => $value){
			$this->element[$form_name.$value]["postdata"] = $loadedData[$key];
		}
		$this->smarty_tpl->assign($this->element, $form_name.$value);
		return $this->smarty_tpl->fetch($form_view);
	}

	public function createFormValidation($form_elements, $form_name, $error_message = "", $success_message = ""){

		if(!empty($_FILES["UserAdminImage"]["name"])) {
			$this->file = $this->fileUpload();
		}

		if (isset($this->file["error"])) {
			$this->form_validation->_field_data["UserAdminImage"]["field"] = "UserAdminImage";
			$this->form_validation->_field_data["UserAdminImage"]["label"] = "kép";
			$this->form_validation->_field_data["UserAdminImage"]["rules"] = "";
			$this->form_validation->_field_data["UserAdminImage"]["errors"] = array();
			$this->form_validation->_field_data["UserAdminImage"]["is_array"] = "";
			$this->form_validation->_field_data["UserAdminImage"]["keys"] = array();
			$this->form_validation->_field_data["UserAdminImage"]["postdata"] = "";
			if ($this->input->post("SaveBtn") && $this->uri->segment(3)) {
				$this->form_validation->_field_data["UserAdminImage"]["error"] = $this->file["error"];
			}
		}

		$validation = parent::createFormValidation($form_elements, $form_name, $error_message = "", $success_message = "");
		if(($validation == FALSE) || (isset($this->form_validation->_field_data["ContentImage"]["error"]))){
			$this->smarty_tpl->clearAssign("success_message");
			if($this->input->post("SaveBtn")){
				$this->smarty_tpl->assign("error_message", $error_message);
			}
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	public function fileUpload(){
		if(isset($_FILES["UserAdminImage"]["name"])){
			$file_name = convert_accented_characters($_FILES["UserAdminImage"]["name"]);
		}
		else{
			$file_name = null;
		}
		$file['upload_path'] = "application/upload/user_admin/";
		$file['allowed_types'] = "gif|jpg|png";
		$file['overwrite'] = TRUE;
		$file['max_size'] = "2048";
		$file['max_width'] = "1920";
		$file['max_height'] = "1200";
		$file['file_name'] = $file_name;
		$this->load->library("upload", $file);

		if ( ! $this->upload->do_upload("UserAdminImage") ){
			$error = array("error" => $this->upload->display_errors());
			//$this->printR($error);
			return $error;
		}
		else{
			$data = array("data" => $this->upload->data());
			$this->imageResize($data["data"]["file_name"]);
			$this->thumbIMG($data["data"]);
			return $data;
		}
	}

	public function imageResize($file){
		$image['source_image'] = "application/upload/user_admin/".$file;
		//$image['create_thumb'] = TRUE;
		$image['maintain_ratio'] = true;
		$image['width'] = 500;
		$image['height'] = 500;

		$this->load->library('image_lib', $image);

		$this->image_lib->clear();
		$this->image_lib->initialize($image);
		if(!$this->image_lib->resize()){
			$error = array("error" => $this->upload->display_errors());
			$this->printR($error);
		}
	}

	//**********************************************************************
	// Kis kép készítése
	//**********************************************************************
	public function thumbIMG ($fileData) {

		$fileNameOld = "application/upload/user_admin/".$fileData["file_name"];
		//$fileNameNew = "application/upload/user/tb_".$fileData["file_name"];
		$fileNameNew = "application/upload/user_admin/".$fileData["file_name"];

		$hosszusag = 150;
		$szelesseg = 150;

		$nw = $hosszusag;    //New Width
		$nh = $szelesseg;    //new Height

		$source = $fileNameOld;    //Source file
		$dest = $fileNameNew;    //Destination *Note you can add or change the name of the file here. ie. thumb_test.jpg

		$stype = explode(".", $source);
		$stype = $stype[count($stype)-1];

		$size = getimagesize($source);
		$w = $size[0];    //Images width
		$h = $size[1];    //Images height

		switch($stype) {
			case 'gif':
				$simg = imagecreatefromgif($source);
				break;
			case 'jpg':
				$simg = imagecreatefromjpeg($source);
				break;
			case 'png':
				$simg = imagecreatefrompng($source);
				break;
		}

		$dimg = imagecreatetruecolor($nw, $nh);
		$wm = $w/$nw;
		$hm = $h/$nh;
		$h_height = $nh/2;
		$w_height = $nw/2;

		if($w> $h) {
			$adjusted_width = $w / $hm;
			$half_width = $adjusted_width / 2;
			$int_width = $half_width - $w_height;
			imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
		} elseif(($w <$h) || ($w == $h)) {
			$adjusted_height = $h / $wm;
			$half_height = $adjusted_height / 2;
			$int_height = $half_height - $h_height;

			imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
		} else {
			imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
		}

		imagejpeg($dimg,$dest,100);
	}

}

?>