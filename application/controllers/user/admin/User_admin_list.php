<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class User_Admin_List extends Admin_Base_Controller{
    public function __construct () {
        parent::__construct();
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."--Felhasználó kezelés");
        $this->load->model("user/admin/User_admin_list_model");
    }

    public function index() {
        parent::index();
        $this->modifyListForm($this->User_admin_list_model->db_table);
        $this->smarty_tpl->assign("table_head", $this->User_admin_list_model->table_head);
        $this->smarty_tpl->assign(  "content",
            $this->generateListForm($this->generateFilterForm($this->User_admin_list_model->form_name, $this->User_admin_list_model->form_elements, "user/admin/user_admin_filter_view.tpl"),
                $this->User_admin_list_model->form_name,
                $this->User_admin_list_model->order_form_name,
                $this->User_admin_list_model->db_table,
                $this->User_admin_list_model->db_list_fields,
                $this->User_admin_list_model->form_view,
                $this->createListWhere($this->User_admin_list_model->db_table, $this->User_admin_list_model->form_name, $this->User_admin_list_model->db_where),
                $this->createListOrder($this->User_admin_list_model->order_form_name, $this->User_admin_list_model->db_order_by),
                $this->User_admin_list_model->db_group_by,
                $this->User_admin_list_model->db_list_limit,
                "A lista nem tartalmaz elemeket!"));

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function modifyListForm($table){
        parent::modifyListForm($table);
        if($this->input->post("DeleteBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Admin felhasználó törlése", "id = ".$this->input->post("DeleteBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("AktivBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Admin felhasználó inaktiválása", "id = ".$this->input->post("AktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("InAktivBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Admin felhasználó aktiválása", "id = ".$this->input->post("InAktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
    }

    public function generateListForm($filter_form, $form_name, $order_form_name, $table, $list_fields, $form_view, $where, $order_by, $group_by, $limit, $warning = ""){

        $list = $this->User_admin_list_model->listLoad($table, $list_fields, $where, $order_by, $group_by, $limit, $this->uri->segment(3));
        $this->smarty_tpl->assign("FilterForm", $filter_form);
        $this->smarty_tpl->assign("FormName", $form_name);
        $this->smarty_tpl->assign("OrderFormName", $order_form_name);
        $this->smarty_tpl->assign("list", $list);
        $this->smarty_tpl->assign("pagination", $this->createPagination($table, $where, $limit));
        if($list == false){
            $this->smarty_tpl->assign("error_message", $warning);
        }
        return $this->smarty_tpl->fetch($form_view);
    }

    public function createPagination($table, $where, $limit){
        $this->load->library('pagination');

        $page = array();

        $page["base_url"] = $this->config->item("base_url")."admin/".$this->uri->segment(2);
        $page["total_rows"] = $this->User_admin_list_model->listLoadCount($table, $where);
        $page["per_page"] = $limit;
        $page["num_links"] = 3;
        $page["uri_segment"] = 3;
        $page["num_tag_open"] = "<li class='paginate_button'>";
        $page["num_tag_close"] = "</li>";
        $page["first_link"] = "Első";
        $page["first_tag_open"] = "<li class='paginate_button previous'>";
        $page["first_tag_close"] = "</li>";
        $page["last_link"] = "Utolsó";
        $page["last_tag_open"] = "<li class='paginate_button'>";
        $page["last_tag_close"] = "</li>";
        $page["next_link"] = "Következő";
        $page["next_tag_open"] = "<li class='paginate_button'>";
        $page["next_tag_close"] = "</li>";
        $page["prev_link"] = "Előző";
        $page["prev_tag_open"] = "<li class='paginate_button'>";
        $page["prev_tag_close"] ="</li>";
        $page["cur_page"] = 0;
        $page["cur_tag_open"] = "<li class='paginate_button active'><a>";
        $page["cur_tag_close"] = "</a></li>";

        $this->pagination->initialize($page);
        return $pagination_links = $this->pagination->create_links();
    }
}
?>