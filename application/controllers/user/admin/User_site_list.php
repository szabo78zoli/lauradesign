<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class User_site_List extends Admin_Base_Controller{
    public function __construct () {
        parent::__construct();
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Ügyfél kezelés");
        $this->load->model("user/admin/User_site_list_model");
    }

    public function index() {
        parent::index();
        $this->modifyListForm($this->User_site_list_model->db_table);

        $this->smarty_tpl->assign(  "content",
            $this->generateListForm($this->generateFilterForm($this->User_site_list_model->form_name, $this->User_site_list_model->form_elements, "user/admin/user_site_filter_view.tpl"),
                $this->User_site_list_model->form_name,
                $this->User_site_list_model->order_form_name,
                $this->User_site_list_model->db_table,
                $this->User_site_list_model->db_list_fields,
                $this->User_site_list_model->form_view,
                $this->createListWhere($this->User_site_list_model->db_table, $this->User_site_list_model->form_name, $this->User_site_list_model->db_where),
                $this->User_site_list_model->db_order_by,
                $this->User_site_list_model->db_group_by,
                $this->User_site_list_model->db_list_limit,
                "A lista nem tartalmaz elemeket!"));

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function modifyListForm($table){
        parent::modifyListForm($table);
        if($this->input->post("DeleteBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Ügyfél törlése", "id = ".$this->input->post("DeleteBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("AktivBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Ügyfél inaktiválása", "id = ".$this->input->post("AktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
        if($this->input->post("InAktivBtn")){
            $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Ügyfél aktiválása", "id = ".$this->input->post("InAktivBtn")." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);
        }
    }
}
?>