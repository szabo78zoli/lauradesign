<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');
class Project_Show extends Site_Base_Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        parent::index();

    }
    public function show(){
        $this->index();
        $this->load->model("user/site/Project_show_model");

        if($this->session->userdata("Site_User_Id")) {
            $projects = $this->Project_show_model->getProject($this->session->userdata("Site_User_Id"));

            $this->smarty_tpl->assign("siteUser", $this->session->userdata("Site_User_Id"));
            $this->smarty_tpl->assign("projects", $projects);
        }
        else{
            header("location:" . $this->config->item("base_url")."tartalom/projekt");
        }
        $tartalom = $this->smarty_tpl->fetch($this->Project_show_model->form_view);

        $this->smarty_tpl->assign("content", $tartalom);
        $this->smarty_tpl->display("base/site/site_base_view.tpl");
    }
}

?>
