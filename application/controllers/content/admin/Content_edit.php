<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Content_Edit extends Admin_Base_Controller{
    public function __construct () {
        parent::__construct();
//$this->printR($_POST);
        $this->load->model("content/admin/Content_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)." -- Tartalom kezelés");
        $this->smarty_tpl->assign("lang", $this->Content_edit_model->getSelectLangValues("lang"));
        $this->smarty_tpl->assign("layout", $this->Content_edit_model->getSelectValues("content_layout"));
        $this->addValue("layout", 0, "Kérem, válasszon!");
        $this->setBasicValue($this->Content_edit_model->form_name, "layout", 0);
    }

    public function index() {
        parent::index();

        $this->load->library("form_validation");
        $lang = $this->Content_edit_model->getSelectLangValues("lang");
        foreach($lang as $key => $row){
            if($row["abbreviation"] == "hu"){
                $this->form_validation->set_rules($this->Content_edit_model->form_name."Name".$row["abbreviation"], "név", "required");
                $this->form_validation->set_rules($this->Content_edit_model->form_name."Alias".$row["abbreviation"], "alias", "required");
                $this->form_validation->set_rules($this->Content_edit_model->form_name."ShortDescription".$row["abbreviation"], "bevezető szöveg", "required");
                $this->form_validation->set_rules($this->Content_edit_model->form_name."Description".$row["abbreviation"], "leírás", "required");
                $this->form_validation->set_rules($this->Content_edit_model->form_name."Content".$row["abbreviation"], "tartalom", "required");
            }
            else{
                $name = $this->input->post($this->Content_edit_model->form_name."Name".$row["abbreviation"]);
                if(isset($name) && !empty($name)){
                    $this->form_validation->set_rules($this->Content_edit_model->form_name."Name".$row["abbreviation"], "név", "required");
                    $this->form_validation->set_rules($this->Content_edit_model->form_name."Alias".$row["abbreviation"], "alias", "required");
                    $this->form_validation->set_rules($this->Content_edit_model->form_name."ShortDescription".$row["abbreviation"], "bevezető szöveg", "required");
                    $this->form_validation->set_rules($this->Content_edit_model->form_name."Description".$row["abbreviation"], "leírás", "required");
                    $this->form_validation->set_rules($this->Content_edit_model->form_name."Content".$row["abbreviation"], "tartalom", "required");
                }
            }
        }


        //$this->form_validation->set_rules($this->Content_edit_model->form_name."Alias", "alias", "required");
        /*$this->form_validation->set_rules($this->Content_edit_model->form_name."Cim", "cím", "required|is_unique_insert[tartalom.tartalom_cim.".$this->input->post("AdminTartalomNyelv")."]");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Nyelv", "nyelv", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Link", "link", "required||is_unique_insert[tartalom.tartalom_link.".$this->input->post("AdminTartalomNyelv")."]");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Leiras", "leírás", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Kulcsszo", "kulcsszavak", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Jogcsoport", "jogcsoport", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Tartalom", "tartalom", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Aktiv", "aktív", "required");*/
        $validation = $this->createFormValidation($this->Content_edit_model->form_elements, $this->Content_edit_model->form_name, "Az ürlapadatok hibásak!", "A tartalom adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, $this->Content_edit_model->form_view, $this->Content_edit_model->db_table, $this->Content_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, $this->Content_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");

    }

    public function editorLoad($id, $verzio=""){
        parent::editorLoad($id);
        $this->smarty_tpl->assign("Modify", 1);
        $this->load->library("form_validation");
        $lang = $this->Content_edit_model->getSelectLangValues("lang");
        foreach($lang as $key => $row){
            if($row["abbreviation"] == "hu"){
                $this->form_validation->set_rules($this->Content_edit_model->form_name."Name".$row["abbreviation"], "név", "required");
            }
            else{
                $name = $this->input->post($this->Content_edit_model->form_name."Name".$row["abbreviation"]);
                if(isset($name) && !empty($name)){
                    $this->form_validation->set_rules($this->Content_edit_model->form_name."Alias".$row["abbreviation"], "alias", "required");
                }
            }
        }

        $validation = $this->createFormValidation($this->Content_edit_model->form_elements, $this->Content_edit_model->form_name, "Az ürlapadatok hibásak!", "A tartalom adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, $this->Content_edit_model->form_view, $id, $this->Content_edit_model->db_table, $this->Content_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedContentForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, $this->Content_edit_model->form_view, $id, $verzio, $this->Content_edit_model->db_table, $this->Content_edit_model->db_loaded_fields, "id = ".$id, $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, $this->Content_edit_model->form_view, $id, $this->Content_edit_model->db_table, $this->Content_edit_model->db_loaded_fields, "id = ".$id, $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
/*
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Cim", "cím", "required|is_unique_update[tartalom.tartalom_cim.".$id.".".$this->input->post("AdminTartalomNyelv")."]");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Nyelv", "nyelv", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Link", "link", "required|is_unique_update[tartalom.tartalom_link.".$id.".".$this->input->post("AdminTartalomNyelv")."]");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Leiras", "leírás", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Kulcsszo", "kulcsszavak", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Jogcsoport", "jogcsoport", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Tartalom", "tartalom", "required");
        $this->form_validation->set_rules($this->Content_edit_model->form_name."Aktiv", "aktív", "required");
        $validation = $this->createFormValidation($this->Content_edit_model->form_elements, $this->Content_edit_model->form_name, "Az ürlapadatok hibásak!", "A tartalom adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, $this->Content_edit_model->form_view, $id, $this->Content_edit_model->db_table, $this->Content_edit_model->db_edited_fields, "tartalom_id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, $this->Content_edit_model->form_view, $id, $verzio, $this->Content_edit_model->db_table, $this->Content_edit_model->db_loaded_fields, "tartalom_id = ".$id." AND tartalom_torolt = 0", $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, $this->Content_edit_model->form_view, $id, $this->Content_edit_model->db_table, $this->Content_edit_model->db_loaded_fields, "tartalom_id = ".$id." AND tartalom_torolt = 0", $id));
        }
        $this->smarty_tpl->assign("TartalomAdataiLabel", "Tartalom adatai");
        $tartalomAdataiData = $this->Content_edit_model->showTartalomAdatai($id);
        $tartalomAdatai["id"] = $tartalomAdataiData["tartalom_id"];
        $tartalomAdatai["szerzo"] = $tartalomAdataiData["tartalom_szerzo"];
        $tartalomAdatai["letrehozas_datum"] = $tartalomAdataiData["tartalom_letrehozas_datum"];
        $tartalomAdatai["modosito"] = $tartalomAdataiData["tartalom_modosito"];
        $tartalomAdatai["modositas_datum"] = $tartalomAdataiData["tartalom_modositas_datum"];
        $tartalomAdatai["megtekintve"] = $tartalomAdataiData["tartalom_megtekintve"];
        $tartalomAdatai["javitas_szama"] = $tartalomAdataiData["tartalom_javitas_szama"];

        $tartalomVerzio = $this->Content_edit_model->showTartalomVerzio($id);

        $this->smarty_tpl->assign("TartalomAdataiData", $tartalomAdatai);
        $this->smarty_tpl->assign("TartalomVerzioData", $tartalomVerzio);
        $this->smarty_tpl->display("base/admin_base_view.tpl");
        */
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            if(is_array($value)){
                foreach ($value as $keyArray => $valueArray) {
                    $fields[$key] = $this->input->post($form_name.$valueArray);
                }
            }
            else{
                $fields[$key] = $this->input->post($form_name.$value);
            }
        }
        foreach($fields as $key => $row){
            if(strstr($key, ".")){
                list($field, $lang) = explode('.', $key);
                $fieldsArray[$lang][$field] = $row;
                $langarray[$lang] = $lang;
            }
            else{
                $fieldsArray = $fields;
            }
        }

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        foreach($langarray as $key => $row) {
            $this->db->select("id");
            $this->db->where("abbreviation", $row);
            $query = $this->db->get("lang");

            if ($query->num_rows() > 0){
                foreach ($query->result_array() as $keyQuery => $rowQuery){
                    $resultLang = $rowQuery["id"];
                }
                $resultLang;
            }
            $fieldsArray[$row]["lang"] = $resultLang;
            if(!empty($fieldsArray[$row]["name"])) {
                $this->Content_edit_model->editorInsert($table, $fieldsArray[$row]);
            }
        }

        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }
        $this->smarty_tpl->assign($this->element, $form_name.$value);
        $this->fieldsKapcsolodoTartalmak($this->Content_edit_model->form_name, $this->Content_edit_model->fields_kapcsolodo_tartalmak);

        $this->Content_edit_model->editorUpdate($table, $fields, $where, $id);

        $this->smarty_tpl->assign($this->element, $form_name.$value);

        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedContentForm($form_name, $form_elements, $form_view, $id, $verzio, $table, $db_fields, $where) {
        $this->element = array();
        $this->fields = "";
        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            $this->element[$form_name.$value]["field"] = $form_name.$value;
            $this->element[$form_name.$value]["postdata"] = "";

            if(isset($this->form_validation->_field_data[$form_name.$value]["error"])){
                $this->element[$form_name.$value]["error"] = $this->form_validation->_field_data[$form_name.$value]["error"];
            }
            else{
                $this->element[$form_name.$value]["error"] = "";
            }

            $this->smarty_tpl->assign($this->element, $form_name.$value);
        }

        foreach ($db_fields as $key => $value){

            if(strstr($key, ".")){
                list($field, $lang) = explode('.', $key);
                $langarray[$lang] = $lang;
                $value = substr($value, 0, -2);
                $fieldsArray[$lang][$field] = $value;
            }
            else{
                $fieldsArray = $fields;
            }
            $this->fields .= $key.", ";
        }


        foreach($fieldsArray as $key => $row){
            $editor_fields = "";
            foreach($row as $rowKey => $rowRow){
                $editor_fields .= $rowKey.", ";
            }
            $where2 = " AND lang = '".$key."'";
            $wh = $where.$where2;
            $loadedData = $this->Content_edit_model->editorLoad($table, $editor_fields, $wh, $id, $verzio);
$this->printR($loadedData);
            foreach ($db_fields as $key => $value){
$this->printR($loadedData[$key]);
                $this->element[$form_name.$value]["postdata"] = $loadedData[$key];

            }
        }


        /*if(!empty($verzio)){
            $this->element[$form_name."Leiras"]["postdata"] = $loadedData["tartalom_verzio_leiras"];
            $this->element[$form_name."Kulcsszo"]["postdata"] = $loadedData["tartalom_verzio_meta_kulcsszo"];
            $this->element[$form_name."Tartalom"]["postdata"] = $loadedData["tartalom_verzio_tartalom"];
        }*/

        $this->smarty_tpl->assign($this->element, $form_name.$value);

        return $this->smarty_tpl->fetch($form_view);
    }

    public  function generateEditForm($form_name, $form_elements, $form_view, $id="") {
        parent::generateEditForm($form_name, $form_elements, $form_view);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function fieldsKapcsolodoTartalmak($form_name, $fields_kapcsolodo_tartalmak){
        $element = array();
        foreach ($fields_kapcsolodo_tartalmak as $key => $value){
            $element[$form_name.$value]["field"] = $form_name.$value;
            $element[$form_name.$value]["postdata"] = "";
            $element[$form_name.$value]["error"] = "";
        }
        $this->smarty_tpl->assign($element, $form_name.$value);
    }
}
?>