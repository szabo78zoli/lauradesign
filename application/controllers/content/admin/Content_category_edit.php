<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Content_Category_Edit extends Admin_Base_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("content/admin/Content_category_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Tartalom kategória szerkesztés");
        $this->load->helper('text');
    }

    public function index() {
        parent::index();
        $this->smarty_tpl->assign("categorySelect", $this->Content_category_edit_model->getContentCategoryTreeSelect());
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Content_category_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Content_category_edit_model->form_name."Alias", "alias", "required");
        $this->form_validation->set_rules($this->Content_category_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Content_category_edit_model->form_elements, $this->Content_category_edit_model->form_name, "Az ürlapadatok hibásak!", "A tartalom kategória adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Content_category_edit_model->form_name, $this->Content_category_edit_model->form_elements, $this->Content_category_edit_model->form_view, $this->Content_category_edit_model->db_table, $this->Content_category_edit_model->form_elements));                    }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Content_category_edit_model->form_name, $this->Content_category_edit_model->form_elements, $this->Content_category_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id){
        parent::editorLoad($id);
        $this->smarty_tpl->assign("menuSelect", $this->Content_category_edit_model->getMenuTreeSelect($id));
        $this->smarty_tpl->assign("Modify", 1);
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Content_category_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Content_category_edit_model->form_name."Alias", "alias", "required");
        $this->form_validation->set_rules($this->Content_category_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Content_category_edit_model->form_elements, $this->Content_category_edit_model->form_name, "Az ürlapadatok hibásak!", "A tartalom kategória adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Content_category_edit_model->form_name, $this->Content_category_edit_model->form_elements, $this->Content_category_edit_model->form_view, $id, $this->Content_category_edit_model->db_table, $this->Content_category_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedForm($this->Content_category_edit_model->form_name, $this->Content_category_edit_model->form_elements, $this->Content_category_edit_model->form_view, $id, $this->Content_category_edit_model->db_table, $this->Content_category_edit_model->db_loaded_fields, "id = ".$id." AND deleted= 0", $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Content_category_edit_model->form_name, $this->Content_category_edit_model->form_elements, $this->Content_category_edit_model->form_view, $id, $this->Content_category_edit_model->db_table, $this->Content_category_edit_model->db_loaded_fields, "id = ".$id." AND deleted = 0", $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }    
    
    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }

        $this->Content_category_edit_model->editorInsert($table, $fields);

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új tartalom kategória hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }
        $this->Content_category_edit_model->editorUpdate($table, $fields, $where, $id);

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Tartalom kategória módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where) {
        parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->Content_category_edit_model->editorLoad($table, $this->fields, $where, $id);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }
        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }
}