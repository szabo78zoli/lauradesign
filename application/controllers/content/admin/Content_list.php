<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Content_List extends Admin_Base_Controller{
    public function __construct () {
        parent::__construct();
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."-- Tartalom kezelés");
        $this->load->model("content/admin/Content_list_model");
    }

    public function index() {
        parent::index();
        $this->modifyListForm($this->Content_edit_model->db_table);
        $this->smarty_tpl->assign(  "content",
            $this->generateListForm($this->generateFilterForm($this->Content_edit_model->form_name, $this->Content_edit_model->form_elements, "content/admin/content_filter_view.tpl"),
                $this->Content_edit_model->form_name,
                $this->Content_edit_model->db_table,
                $this->Content_edit_model->db_list_fields,
                $this->Content_edit_model->form_view,
                $this->createListWhere($this->Content_edit_model->db_table, $this->Content_edit_model->form_name, $this->Content_edit_model->db_where),
                $this->Content_edit_model->db_order_by,
                $this->Content_edit_model->db_group_by,
                $this->Content_edit_model->db_list_limit,
                "A lista nem tartalmaz elemeket!"));

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    /*public function generateListLangForm($filter_form, $form_name, $table, $list_fields, $form_view, $where, $order_by, $group_by, $limit, $warning = ""){

        $list = $this->Content_edit_model->listLoadLang($table, $list_fields, $where, $order_by, $group_by, $limit, $this->uri->segment(3));
        $this->smarty_tpl->assign("FilterForm", $filter_form);
        $this->smarty_tpl->assign("FormName", $form_name);
        $this->smarty_tpl->assign("list", $list);
        $this->smarty_tpl->assign("pagination", $this->createPagination($table, $where, $limit));
        if($list == false){
            $this->smarty_tpl->assign("error_message", $warning);
        }
        return $this->smarty_tpl->fetch($form_view);
    }*/
}
?>