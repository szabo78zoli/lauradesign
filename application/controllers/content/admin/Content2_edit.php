<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Content2_Edit extends Admin_Base_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("content/admin/Content2_edit_model");
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)." -- Tartalom kezelés");
        $this->smarty_tpl->assign("lang", $this->Content2_edit_model->getSelectValues("lang"));
        $this->smarty_tpl->assign("layout", $this->Content2_edit_model->getSelectValues("content_layout"));
        $this->addValue("layout", 0, "Kérem, válasszon!");
        $this->setBasicValue($this->Content2_edit_model->form_name, "layout", 0);
    }

    public function index() {
        parent::index();

        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Alias", "alias", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."ShortDescription", "bevezető szöveg", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Description", "leírás", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Content", "tartalom", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Content2_edit_model->form_elements, $this->Content2_edit_model->form_name, "Az ürlapadatok hibásak!", "A tartalom adatok mentése sikeres.");

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateInsertForm($this->Content2_edit_model->form_name, $this->Content2_edit_model->form_elements, $this->Content2_edit_model->form_view, $this->Content2_edit_model->db_table, $this->Content2_edit_model->db_edited_fields));
        }
        elseif($validation != TRUE){
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Content2_edit_model->form_name, $this->Content2_edit_model->form_elements, $this->Content2_edit_model->form_view));
        }
        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function editorLoad($id, $version = ""){
        parent::editorLoad($id);
        $this->smarty_tpl->assign("Modify", 1);
        $this->load->library("form_validation");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Name", "név", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Alias", "alias", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."ShortDescription", "bevezető szöveg", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Description", "leírás", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Content", "tartalom", "required");
        $this->form_validation->set_rules($this->Content2_edit_model->form_name."Active", "aktív", "required");
        $validation = $this->createFormValidation($this->Content2_edit_model->form_elements, $this->Content2_edit_model->form_name, "Az ürlapadatok hibásak!", "A tartalom adatok mentése sikeres.");

        $contentDataData = $this->Content2_edit_model->showContentData($id);
        $modifyNumber = $this->Content2_edit_model->showModifyNumber($id);
        $contentData["id"] = $contentDataData["id"];
        $contentData["name"] = $contentDataData["name"];
        $contentData["create_date"] = $contentDataData["create_date"];
        $contentData["hits"] = $contentDataData["hits"];
        $contentData["modifyNumber"] = $modifyNumber;

        $contentVersion = $this->Content2_edit_model->showContentVersion($id);

        $this->smarty_tpl->assign("ContentData", $contentData);
        $this->smarty_tpl->assign("ContentVersion", $contentVersion);

        if ($validation == TRUE){
            $this->smarty_tpl->assign("content", $this->generateUpdateForm($this->Content2_edit_model->form_name, $this->Content2_edit_model->form_elements, $this->Content2_edit_model->form_view, $id, $this->Content2_edit_model->db_table, $this->Content2_edit_model->db_edited_fields, "id = ", $id));
        }
        elseif(($validation != TRUE) && (!$this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateLoadedContentForm($this->Content2_edit_model->form_name, $this->Content2_edit_model->form_elements, $this->Content2_edit_model->form_view, $id, $version, $this->Content2_edit_model->db_table, $this->Content2_edit_model->db_loaded_fields, "id = ".$id, $id));
        }
        elseif(($validation != TRUE) && ($this->input->post("SaveBtn"))) {
            $this->smarty_tpl->assign("content", $this->generateEditForm($this->Content2_edit_model->form_name, $this->Content2_edit_model->form_elements, $this->Content2_edit_model->form_view, $id, $this->Content2_edit_model->db_table, $this->Content2_edit_model->db_loaded_fields, "id = ".$id, $id));
        }

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function generateInsertForm($form_name, $form_elements, $form_view, $table = "", $db_fields = "") {
        parent::generateInsertForm($form_name, $form_elements, $form_view, $table, $db_fields);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }
        $this->Content2_edit_model->editorInsert($table, $fields);

        $id = $this->db->insert_id();
        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Új artalom hozzáadása", "id = ".$id." Tétel hozzáadása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateUpdateForm($form_name, $form_elements, $form_view, $id = "", $table = "", $db_fields = "", $where = "") {
        parent::generateUpdateForm($form_name, $form_elements, $form_view, $id, $table, $db_fields, $where);

        foreach($db_fields as $key => $value){
            $fields[$key] = $this->input->post($form_name.$value);
        }
        $this->Content2_edit_model->editorUpdate($table, $fields, $where, $id);

        $this->Admin_base_model->eventLogSave($this->session->userdata("Admin_User_Name") ,"Admin", "2", "Tartalom módosítása", "id = ".$id." Tétel módosítása", $_SERVER["REMOTE_ADDR"]);

        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }

    public function generateLoadedContentForm($form_name, $form_elements, $form_view, $id, $version, $table, $db_fields, $where) {
        //parent::generateLoadedForm($form_name, $form_elements, $form_view, $id, $version, $table, $db_fields, $where);

        $this->element = array();
        $this->fields = "";
        $this->smarty_tpl->assign("FormName", $form_name);

        foreach ($form_elements as $key => $value) {
            $this->element[$form_name.$value]["field"] = $form_name.$value;
            $this->element[$form_name.$value]["postdata"] = "";

            if(isset($this->form_validation->_field_data[$form_name.$value]["error"])){
                $this->element[$form_name.$value]["error"] = $this->form_validation->_field_data[$form_name.$value]["error"];
            }
            else{
                $this->element[$form_name.$value]["error"] = "";
            }

            $this->smarty_tpl->assign($this->element, $form_name.$value);
        }

        foreach ($db_fields as $key => $value){
            $this->fields .= $key.", ";
        }

        $loadedData = $this->Content2_edit_model->editorLoad($table, $this->fields, $where, $id, $version);
        foreach ($db_fields as $key => $value){
            $this->element[$form_name.$value]["postdata"] = $loadedData[$key];
        }
        $this->smarty_tpl->assign($this->element, $form_name.$value);
        return $this->smarty_tpl->fetch($form_view);
    }
}
?>