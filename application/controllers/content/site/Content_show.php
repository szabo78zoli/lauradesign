<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');
class Content_Show extends Site_Base_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        parent::index();

    }
    public function show($id){
        $this->index();
        $this->load->model("content/site/Content_show_model");
        $result = $this->Content_show_model->contentLoad($this->Content_show_model->db_table, $this->Content_show_model->db_loaded_fields, "alias = '".$id."' AND lang = ".$this->site_lang." AND active = 1 AND deleted = 0", "1", "");
        if($result == false){
            $result = $this->Content_show_model->contentLoad($this->Content_show_model->db_table, $this->Content_show_model->db_loaded_fields, "alias = '404' AND active = 1 AND deleted = 0", "1", "");
        }
        else{


            $this->Content_show_model->megtekintes($id);
            $this->smarty_tpl->assign("tartalom", $result);
        }

        $this->smarty_tpl->assign("tartalom", $result);
        $tartalom = $this->smarty_tpl->fetch($this->Content_show_model->form_view);

        $this->smarty_tpl->assign("content", $tartalom);
        $this->smarty_tpl->assign("meta_keywords", $result["meta_keyword"]);
        $this->smarty_tpl->display("base/site/site_base_view.tpl");
    }
}

?>
