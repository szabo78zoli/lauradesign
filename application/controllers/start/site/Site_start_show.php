<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/site/Site_base_controller.php');

class Site_Start_Show extends Site_Base_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("start/site/Site_start_show_model");
    }

    public function index() {
        parent::index();

        $this->smarty_tpl->assign('slider', $this->Site_base_model->getSlider());
        $this->smarty_tpl->assign('about_us', $this->Site_base_model->getContentElement("rolunk"));
        $this->smarty_tpl->assign('galleryCategories', $this->Site_base_model->getGalleryCategories());
        $this->smarty_tpl->assign('offers', $this->Site_base_model->getOffers());

        $this->smarty_tpl->assign('popup_window', $this->Site_start_show_model->getContentElementById(76));

		$this->smarty_tpl->display('base/site/site_start_view.tpl');
    }
}

?>