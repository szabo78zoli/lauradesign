<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'controllers/base/admin/Admin_base_controller.php');

class Event_Log_List extends Admin_Base_Controller{
    public function __construct () {
        parent::__construct();
        $this->smarty_tpl->assign("pageTitle", $this->config->item("base_url")."admin/".$this->uri->segment(2)."--Esemény napló kezelés");
        $this->load->model("event_log/admin/Event_log_list_model");

        $eventType = array("1" => "Site", "2" => "Admin");
        $this->smarty_tpl->assign("type", $eventType);
        $this->addValue("type", 0, "Kérem, válasszon!");
        $this->setBasicValue($this->Event_log_list_model->form_name, "type", 0);
    }

    public function index() {
        parent::index();
        $this->modifyListForm($this->Event_log_list_model->db_table);
        $this->smarty_tpl->assign("table_head", $this->Event_log_list_model->table_head);
        $this->smarty_tpl->assign(  "content",
            $this->generateListForm($this->generateFilterForm($this->Event_log_list_model->form_name, $this->Event_log_list_model->form_elements, "event_log/admin/event_log_filter_view.tpl"),
                $this->Event_log_list_model->form_name,
                $this->Event_log_list_model->order_form_name,
                $this->Event_log_list_model->db_table,
                $this->Event_log_list_model->db_list_fields,
                $this->Event_log_list_model->form_view,
                $this->createListWhere($this->Event_log_list_model->db_table, $this->Event_log_list_model->form_name, $this->Event_log_list_model->db_where),
                $this->createListOrder($this->Event_log_list_model->order_form_name, $this->Event_log_list_model->db_order_by),
                $this->Event_log_list_model->db_group_by,
                $this->Event_log_list_model->db_list_limit,
                "A lista nem tartalmaz elemeket!"));

        $this->smarty_tpl->display("base/admin/admin_base_view.tpl");
    }

    public function createListWhere($table, $form, $db_where){

        if($this->input->post("SearchBtn")){
            if($this->input->post($form."Filter")!= ""){
                $db_where .= " AND user LIKE '%".$this->input->post($form."Filter")."%'";
            }
            if($this->input->post($form."Type")!= 0){
                $db_where .= " AND event_type = '".$this->input->post($form."Type")."'";
            }
            if($this->input->post($form."StartDate")!= "" && $this->input->post($form."EndDate")!= ""){
                $db_where .= " AND create_date BETWEEN '".$this->input->post($form."StartDate")."' AND '".$this->input->post($form."EndDate")."'";
            }
            if($this->input->post($form."StartDate")!= "" && $this->input->post($form."EndDate")== ""){
                $db_where .= " AND create_date >= '".$this->input->post($form."StartDate")."'";
            }
            if($this->input->post($form."StartDate")== "" && $this->input->post($form."EndDate")!= ""){
                $db_where .= " AND create_date <= '".$this->input->post($form."EndDate")."'";
            }
        }
        else{
            if($this->session->userdata($form."Filter")){
                $db_where .= " AND type LIKE '%".$this->session->userdata($form."Filter")."%'";
            }
            if($this->session->userdata($form."Type")){
                $db_where .= " AND event_type = '".$this->session->userdata($form."Type")."'";
            }
            if($this->session->userdata($form."StartDate") && $this->session->userdata($form."EndDate")){
                $db_where .= " AND create_date BETWEEN '".$this->session->userdata($form."StartDate")."' AND '".$this->session->userdata($form."EndDate")."'";
            }
            if($this->session->userdata($form."StartDate") && !$this->session->userdata($form."EndDate")){
                $db_where .= " AND create_date >= '".$this->session->userdata($form."StartDate")."'";
            }
            if(!$this->session->userdata($form."StartDate") && $this->session->userdata($form."EndDate")){
                $db_where .= " AND create_date <= '".$this->session->userdata($form."EndDate")."'";
            }
        }

        return $db_where;
    }
}
?>