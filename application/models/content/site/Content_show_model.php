<?php

class Content_Show_Model extends Site_Base_Model
{
    public function __construct() {
        parent::__construct();


        $this->db_table = "content";
        $this->db_loaded_fields = array("id",
                                        "name",
                                        "lang",
                                        "alias",
                                        "description",
                                        "meta_keyword",
                                        "author",
                                        "content");
        $this->form_view = "content/site/content_view.tpl";
    }

    public function contentLoad($table, $list_fields, $where, $section, $limit){
        $this->db->select($list_fields);
        $this->db->where($where);
        $this->db->limit($section, $limit);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function megtekintes($id){
        $this->db->where("alias", $id);
        $this->db->set("hits", "hits+1", FALSE);
        $this->db->update("content");
    }

}
?>
