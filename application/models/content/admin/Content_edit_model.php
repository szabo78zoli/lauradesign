<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Content_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Content";
        $this->db_table = "content";

        $lang = $this->getSelectLangValues("lang");

        foreach($lang as $key => $row){
            //$langFields[$row["abbreviation"]]["name"] = "Name";
            $langFields["name.".$row["abbreviation"]] = "Name".$row["abbreviation"];
            $langFields["alias.".$row["abbreviation"]] = "Alias".$row["abbreviation"];
            $langFields["meta_description.".$row["abbreviation"]] = "MetaDescription".$row["abbreviation"];
            $langFields["meta_keyword.".$row["abbreviation"]] = "MetaKeyword".$row["abbreviation"];
            $langFields["short_description.".$row["abbreviation"]] = "ShortDescription".$row["abbreviation"];
            $langFields["description.".$row["abbreviation"]] = "Description".$row["abbreviation"];
            $langFields["content.".$row["abbreviation"]] = "Content".$row["abbreviation"];
            $langFields["create_date.".$row["abbreviation"]] = "CreateDate".$row["abbreviation"];
            $langFields["mod_date.".$row["abbreviation"]] = "ModDate".$row["abbreviation"];
            $langFields["start_date.".$row["abbreviation"]] = "StartDate".$row["abbreviation"];
            $langFields["end_date.".$row["abbreviation"]] = "EndDate".$row["abbreviation"];
            $langFields["layout.".$row["abbreviation"]] = "Layout".$row["abbreviation"];
            $langFields["active.".$row["abbreviation"]] = "Active".$row["abbreviation"];
        }
//$this->printR($langFields);
        /*$this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = array(   "name" => "Name",
                                                                                            "lang" => "Lang",
                                                                                            "alias" => "Alias",
                                                                                            "meta_description" => "MetaDescription",
                                                                                            "meta_keywords" => "MetaKeywords",
                                                                                            "short_description" => "ShortDescription",
                                                                                            "description" => "Description",
                                                                                            "content" => "Content",
                                                                                            "create_date" => "CreateDate",
                                                                                            "mod_date" => "ModDate",
                                                                                            "start_date" => "StartDate",
                                                                                            "end_date" => "EndDate",
                                                                                            "layout" => "Layout",
                                                                                            "active" => "Active");*/

        $this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = $langFields;

        $this->form_view = "content/admin/content_edit_view.tpl";
    }

    public function editorLoad($table, $editor_fields, $where, $id, $verzio = ""){

        $this->db->select($editor_fields);
        /*$this->db->select("tartalom_verzio.tartalom_verzio_tartalom, tartalom_verzio.tartalom_verzio_leiras, tartalom_verzio.tartalom_verzio_meta_kulcsszo");
        $this->db->join("tartalom_verzio", "tartalom.tartalom_id = tartalom_verzio.tartalom_verzio_tartalom_id");*/
        $this->db->where($where);
        if(!empty($verzio)){
            $this->db->where("tartalom_verzio_id", $verzio);
        }

        $query = $this->db->get($table);
//$this->printR($this->db->queries);
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row)
            {
                $result = $row;
            }

            return $result;
        }
        else{
            return false;
        }
    }

    public function editorUpdate($table, $data, $where, $id){

        $this->db->set("tartalom_javitas_szama", "tartalom_javitas_szama+1", FALSE);
        $this->db->where($where, $id);
        $this->db->update($table, $data);

        $this->db->set("tartalom_verzio_tartalom_id", $id);
        $this->db->set("tartalom_verzio_leiras", $data["tartalom_leiras"]);
        $this->db->set("tartalom_verzio_meta_kulcsszo", $data["tartalom_meta_kulcsszo"]);
        $this->db->set("tartalom_verzio_tartalom_cim", $data["tartalom_cim"]);
        $this->db->set("tartalom_verzio_tartalom",$data["tartalom_tartalom"]);
        $this->db->set("tartalom_verzio_felhasznalo",$this->session->userdata("Admin_User_Nev"));
        $this->db->set("tartalom_verzio_datum_ido", "NOW()", FALSE);
        $this->db->insert("tartalom_verzio");

    }

    public function editorInsert($table, $fields){
//$this->printR($fields);
        $this->db->insert($table, $fields);
//$this->printR($this->db->queries);
        /*$fields["tartalom_szerzo"] = $this->session->userdata("Admin_User_Nev");
        $this->db->set("tartalom_letrehozas_datum", "NOW()", FALSE);
        $this->db->insert($table, $fields);

        $this->db->set("tartalom_verzio_tartalom_id", $this->db->insert_id());
        $this->db->set("tartalom_verzio_leiras", $fields["tartalom_leiras"]);
        $this->db->set("tartalom_verzio_meta_kulcsszo", $fields["tartalom_meta_kulcsszo"]);
        $this->db->set("tartalom_verzio_tartalom_cim",$fields["tartalom_cim"]);
        $this->db->set("tartalom_verzio_tartalom",$fields["tartalom_tartalom"]);
        $this->db->set("tartalom_verzio_felhasznalo",$this->session->userdata("Admin_User_Nev"));
        $this->db->set("tartalom_verzio_datum_ido", "NOW()", FALSE);
        $this->db->insert("tartalom_verzio");*/
    }

    public function showTartalomAdatai($id){
        $this->db->select("tartalom_id, tartalom_szerzo, tartalom_modosito, tartalom_megtekintve, tartalom_javitas_szama, tartalom_letrehozas_datum, tartalom_modositas_datum");
        $this->db->where('tartalom_id', $id);
        $query = $this->db->get("tartalom");
        $result = $query->result_array();

        foreach($result as $value){
            $row = $value;
        }
        return $row;
    }

    public function showTartalomVerzio($id){
        $this->db->select("tartalom_verzio_id, tartalom_verzio_felhasznalo, tartalom_verzio_datum_ido");
        $this->db->where('tartalom_verzio_tartalom_id', $id);
        $query = $this->db->get("tartalom_verzio");
        $result = $query->result_array();

        foreach($result as $value){
            $row[] = $value;
        }

        return $row;
    }

    public function getSelectTartalomValues($table){
        $query = $this->db->get_where($table, array($table."_aktiv" =>"1", $table."_torolt" => "0", $table."_nyelv" => $this->session->userdata("Admin_User_Nyelv")));
        foreach ($query->result_array() as $row)
        {
            $result[$row[$table."_id"]] = $row[$table."_cim"];
        }
        return $result;
    }

    public function getSelectLangValues($table){
        $query = $this->db->get_where($table, array("active" =>"1", "deleted" => "0"));
        foreach ($query->result_array() as $row)
        {
            $result[$row["id"]]["id"] = $row["id"];
            $result[$row["id"]]["name"] = $row["name"];
            $result[$row["id"]]["abbreviation"] = $row["abbreviation"];
        }
        return $result;
    }
}
?>