<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Content2_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Content";

        $this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = array(   "name" => "Name",
                                                                                            "lang" => "Lang",
                                                                                            "alias" => "Alias",
                                                                                            "meta_description" => "MetaDescription",
                                                                                            "meta_keyword" => "MetaKeyword",
                                                                                            "short_description" => "ShortDescription",
                                                                                            "description" => "Description",
                                                                                            "content" => "Content",
                                                                                            "start_date" => "StartDate",
                                                                                            "end_date" => "EndDate",
                                                                                            "layout" => "Layout",
                                                                                            "active" => "Active");
        $this->db_table = "content";
        $this->form_view = "content/admin/content2_edit_view.tpl";
    }

    public function editorLoad($table, $editor_fields, $where, $id, $version = ""){
        if(!empty($version)){
            $this->db->select($editor_fields);
            $this->db->where("id", $version);
            $this->db->where("content_id", $id);
            $query = $this->db->get($table."_version");

            if ($query->num_rows() > 0){
                foreach ($query->result_array() as $key => $row)
                {
                    $result = $row;
                }

                return $result;
            }
            else{
                return false;
            }
        }
        else{
            $this->db->select($editor_fields);
            $this->db->where($where);
            $query = $this->db->get($table);

            if ($query->num_rows() > 0){
                foreach ($query->result_array() as $key => $row)
                {
                    $result = $row;
                }

                return $result;
            }
            else{
                return false;
            }
        }
    }

    public function editorInsert($table, $fields){
        $fields["author"] = $this->session->userdata("Admin_User_Id");
        $this->db->set("create_date", "NOW()", FALSE);
        $this->db->insert($table, $fields);

        $this->db->set("content_id", $this->db->insert_id());
        $this->db->set("meta_description", $fields["meta_description"]);
        $this->db->set("meta_keyword", $fields["meta_keyword"]);
        $this->db->set("short_description",$fields["short_description"]);
        $this->db->set("description",$fields["description"]);
        $this->db->set("content",$fields["content"]);
        //$this->db->set("image",$fields["image"]);
        $this->db->set("start_date",$fields["start_date"]);
        $this->db->set("end_date",$fields["end_date"]);
        $this->db->set("author",$this->session->userdata("Admin_User_Id"));
        $this->db->set("modify_date", "NOW()", FALSE);
        $this->db->insert("content_version");
    }

    public function editorUpdate($table, $data, $where, $id){
        $this->db->where($where, $id);
        $this->db->update($table, $data);

        $this->db->set("content_id", $id);
        $this->db->set("meta_description", $data["meta_description"]);
        $this->db->set("meta_keyword", $data["meta_keyword"]);
        $this->db->set("short_description",$data["short_description"]);
        $this->db->set("description",$data["description"]);
        $this->db->set("content",$data["content"]);
        //$this->db->set("image",$fields["image"]);
        $this->db->set("start_date",$data["start_date"]);
        $this->db->set("end_date",$data["end_date"]);
        $this->db->set("author",$this->session->userdata("Admin_User_Id"));
        $this->db->set("modify_date", "NOW()", FALSE);

        $this->db->insert("content_version");
    }

    public function showContentData($id){
        $this->db->select("content.id, user_admin.name, content.create_date, content.hits");
        $this->db->join('user_admin', 'content.author = user_admin.id');
        $this->db->where('content.id', $id);
        $query = $this->db->get("content");

        if ($query->num_rows() > 0){
            $result = $query->result_array();

            foreach($result as $value){
                $row = $value;
            }
            return $row;
        }
        else{
            return false;
        }
    }

    public function showModifyNumber($id){
        $this->db->where('content_id', $id);
        $num = $this->db->count_all_results('content_version');

        return $num;
    }

    public function showContentVersion($id){
        $this->db->select("content_version.id, content_version.content_id, user_admin.name, content_version.modify_date");
        $this->db->join('user_admin', 'content_version.author = user_admin.id');
        $this->db->where('content_version.content_id', $id);
        $this->db->order_by("content_version.id", "DESC");
        $query = $this->db->get("content_version");
        $result = $query->result_array();

        foreach($result as $value){
            $row[] = $value;
        }

        return $row;
    }
}
?>