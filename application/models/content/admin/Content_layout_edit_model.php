<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Content_Layout_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "ContentLayout";

        $this->form_elements = $this->db_edited_fields = $this->db_loaded_fields = array(   "name" => "Name",
                                                                                            "template" => "Template",
                                                                                            "active" => "Active");
        $this->db_table = "content_layout";
        $this->form_view = "content/admin/content_layout_edit_view.tpl";
    }
}
?>