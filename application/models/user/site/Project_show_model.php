<?php

class Project_Show_Model extends Site_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->db_table = "gallery";
        $this->db_loaded_fields = array("id",
                                        "image");

        $this->form_view = "user/site/project_view.tpl";
    }

    public function getProject($id){

        $this->db->select("id, name, alias");
        $this->db->where("user", $id);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("user_site_project");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row)
            {
                $result[$key] = $row;
                $result[$key]["version"] = $this->getProjectVersion($row["id"]);

                foreach ($result[$key]["version"] as $vKey => $version)
                {
                    $result[$key]["version"][$version["id"]]["images"] = $this->getProjectImages($row["id"], $version["id"]);
                }

            }

            return $result;
        }
        else{
            return false;
        }
    }

    public function getProjectVersion($id){

        $this->db->select("id, name, alias");
        $this->db->where("project", $id);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("user_site_project_version");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row)
            {
                $result[$row["id"]] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function getProjectImages($project, $version){

        $this->db->select("id, image");
        $this->db->where("project", $project);
        $this->db->where("version", $version);
        $query = $this->db->get("user_site_project_image");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row)
            {
                $result[] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }
}
?>
