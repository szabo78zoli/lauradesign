<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class User_Admin_Edit_Model extends Admin_Base_Model
{
	public function __construct() {
		parent::__construct();

		$this->form_name = "UserAdmin";

		$this->form_elements = array("Name", "Email", "Password", "PasswordAgain", "Active", "RightsGroup");

		if(!empty($_REQUEST["UserAdminPassword"])){
			$this->db_edited_fields = array( 	"name" => "Name",
												"email" => "Email",
												"password" => "Password",
												"active" => "Active",
												"rights_group" => "RightsGroup",
												"image" => "Image");
		}
		else{
			$this->db_edited_fields = array( 	"name" => "Name",
												"email" => "Email",
												"active" => "Active",
												"rights_group" => "RightsGroup",
												"image" => "Image");
		}


		$this->db_loaded_fields = array(  	"name" => "Name",
											"email" => "Email",
											"active" => "Active",
											"rights_group" => "RightsGroup",
											"image" => "Image");
		$this->db_table = "user_admin";
		$this->form_view = "user/admin/user_admin_edit_view.tpl";

		$this->load->library('encryption');
	}

	public function editorInsert($table, $fields){

		$fields["password"] = "AjdbEfwlfgekaEe".$fields["password"]."AjdbEfwlfgekaEe";
		$fields["password"] = hash("sha256", $fields["password"]);
		$this->db->insert($table, $fields);
		return $this->db->insert_id();
	}

	public function editorUpdate($table, $data, $where, $id){

		if(isset($data["password"]) && !empty($data["password"])) {
			$data["password"] = "AjdbEfwlfgekaEe".$data["password"]."AjdbEfwlfgekaEe";
			$data["password"] = hash("sha256", $data["password"]);
		}

		$this->db->where($where, $id);
		$this->db->update($table, $data);
	}

	public function editorImageUpdate($table, $field, $id){
		$this->db->where("id", $id);
		$this->db->update($table, array("image" => $field));
	}

	public function editorImageLoad($id){
		$this->db->select("image");
		$this->db->where("id", $id);
		$query = $this->db->get("user_admin");

		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row){
				$result = $row['image'];
			}
			return $result;
		}
		else{
			$result = array();
			return $result;
		}
	}

	public function getSelectZipValues($table){
		$query = $this->db->get_where($table, array("active" =>"1", "deleted" => "0"));
		foreach ($query->result_array() as $row)
		{
			$result[$row["id"]] = $row["zip_code"]." - ".$row["city"];
		}
		return $result;
	}
}
?>