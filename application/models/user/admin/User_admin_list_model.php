<?php

class User_Admin_List_Model extends Admin_Base_Model{
    public function __construct() {
        parent::__construct();

        $this->form_name = "UserAdmin";
        $this->order_form_name = "UserAdminOrder";
        $this->form_elements = array("Filter");
        $this->db_table = "user_admin";
        $this->db_list_fields = "id, name, email, active";
        $this->db_where = " deleted = 0";
        $this->db_order_by = "id ASC";
        $this->db_group_by = "";
        $this->table_head = array("Id" =>"id", "Név" => "name", "Email" => "email", "Szerkesztés" => "","Aktív" => "", "Törlés" => "");

        if($this->input->post($this->form_name ."Lenght")!= ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->input->post($this->form_name."Lenght"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->input->post($this->form_name."Lenght"));
        }
        elseif($this->input->post($this->form_name ."Lenght")== "" && $this->session->userdata($this->form_name."Lenght") == ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->session->userdata("defaultListLength"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }
        else{
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }

        $this->db_list_limit = $this->session->userdata($this->form_name."Lenght");
        $this->form_view = "user/admin/user_admin_list_view.tpl";
    }

    public function listLoad($table, $list_fields, $where, $order_by, $group_by, $section, $limit){

        $this->db->select($list_fields);

        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->order_by($order_by);

        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }

        $this->db->limit($section, $limit);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $row){
                $result[$row["id"]] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function listLoadCount($table, $where){
        if(!empty($where)){
            $this->db->where($where);
        }

        $result = $this->db->count_all_results($table);

        return $result;
    }
}
?>