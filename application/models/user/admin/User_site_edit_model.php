<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class User_Site_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "UserSite";

        $this->form_elements = array( "Company", "Email","EmailAgain","Password","PasswordAgain","FirstName","LastName","Phone","Active",
                                        "ProjectSelect", "VersionSelect");

        if(!empty($_REQUEST["UserSitePassword"])){
            $this->db_edited_fields = array(    "company" => "Company",
                                                "email" => "Email",
                                                "password" => "Password",
                                                "first_name" => "FirstName",
                                                "last_name" => "LastName",
                                                "phone" => "Phone",
                                                "active" => "Active");
        }
        else{
            $this->db_edited_fields = array(    "company" => "Company",
                                                "email" => "Email",
                                                "first_name" => "FirstName",
                                                "last_name" => "LastName",
                                                "phone" => "Phone",
                                                "active" => "Active");
        }

        $this->db_loaded_fields = array(  	"company" => "Company",
                                            "email" => "Email",
                                            "first_name" => "FirstName",
                                            "last_name" => "LastName",
                                            "phone" => "Phone",
                                            "active" => "Active");

        $this->db_table = "user_site";
        $this->form_view = "user/admin/user_site_edit_view.tpl";

        $this->load->library('encryption');
    }


    public function editorInsert($table, $fields){

        $fields["password"] = "AjdbEfwlfgekaEe".$fields["password"]."AjdbEfwlfgekaEe";
        $fields["password"] = hash("sha256", $fields["password"]);
        $this->db->insert($table, $fields);
        return $this->db->insert_id();
    }

    public function editorUpdate($table, $data, $where, $id){

        if(isset($data["password"]) && !empty($data["password"])) {
            $data["password"] = "AjdbEfwlfgekaEe".$data["password"]."AjdbEfwlfgekaEe";
            $data["password"] = hash("sha256", $data["password"]);
        }

        $this->db->where($where, $id);
        $this->db->update($table, $data);
    }

    public function getProject($id){

        $this->db->select("id, name");
        $this->db->where("user", $id);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("user_site_project");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row)
            {
                $result[$key] = $row;
                $result[$key]["version"] = $this->getProjectVersion($row["id"]);
           
                foreach ($result[$key]["version"] as $vKey => $version)
                {
                    $result[$key]["version"][$version["id"]]["images"] = $this->getProjectImages($row["id"], $version["id"]);
                }
                
            }

            return $result;
        }
        else{
            return false;
        }
    }

    public function getProjectVersion($id){

        $this->db->select("id, name");
        $this->db->where("project", $id);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("user_site_project_version");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row)
            {
                $result[$row["id"]] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function getProjectImages($project, $version){

        $this->db->select("id, image");
        $this->db->where("project", $project);
        $this->db->where("version", $version);
        $query = $this->db->get("user_site_project_image");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row)
            {
                $result[] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function saveProject($id, $name, $alias){
        $fields["user"] = $id;
        $fields["name"] = $name;
        $fields["alias"] = $alias;
        $this->db->insert("user_site_project", $fields);
    }

    public function saveProjectVersion($project, $name, $alias){
        $fields["project"] = $project;
        $fields["name"] = $name;
        $fields["alias"] = $alias;
        $this->db->insert("user_site_project_version", $fields);
    }

    public function saveImageToProject($project, $version, $image){
        $fields["project"] = $project;
        $fields["version"] = $version;
        $fields["image"] = $image["image"];
        $this->db->insert("user_site_project_image", $fields);
    }
}
?>