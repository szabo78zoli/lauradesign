<?php

class User_Site_List_Model extends Admin_Base_Model{
    public function __construct() {
        parent::__construct();

        $this->form_name = "UserSiteAdmin";
        $this->order_form_name = "UserOrder";
        $this->form_elements = array("Filter","RegistrationDateFrom","RegistrationDateTo","RegistrationConfirmed","EmailIsset","Email","Webshop","IsRegistered","IsNewsletter");
        $this->db_table = "user_site";
        $this->db_list_fields = "id, company, first_name, last_name, email, phone, last_login, active";
        $this->db_where = " user_site.deleted = 0";
        $this->db_order_by = "user_site.id DESC";
        $this->db_group_by = "";

        if($this->input->post($this->form_name ."Lenght")!= ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->input->post($this->form_name."Lenght"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->input->post($this->form_name."Lenght"));
        }
        elseif($this->input->post($this->form_name ."Lenght")== "" && $this->session->userdata($this->form_name."Lenght") == ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->session->userdata("defaultListLength"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }
        else{
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }

        $this->db_list_limit = $this->session->userdata($this->form_name."Lenght");

        $this->form_view = "user/admin/user_site_list_view.tpl";
    }
}
?>