<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Giftcard_show_Model extends Admin_Base_Model{

    public function __construct(){
        parent::__construct();

        $this->form_name = "Giftcard";
        $this->db_table = "giftcard";

        $this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = array(   "first_name" => "FirstName",
                                                                                            "last_name" => "LastName",
                                                                                            "phone" => "Phone",
                                                                                            "email" => "Email",
                                                                                            "cost" => "Cost",
                                                                                            "pcs" => "Pcs",
                                                                                            "invoice_address" => "InvoiceAddress",
                                                                                            "delivery_address" => "DeliveryAddress");
        $this->form_view = "giftcard/site/giftcard_show_view.tpl";
    }
}