<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Admin_Loginout_Edit_Model extends Admin_Base_Model{

    public function __construct(){
        parent::__construct();

        $this->form_name = "AdminLogin";
        $this->db_table = "user";

        $this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = array(   "email" => "Email",
            "password" => "Password",
            "active" => "Active");
        $this->form_view = "loginout/admin/admin_loginout_view.tpl";
    }

    public function login($email, $password) {

        $password = "AjdbEfwlfgekaEe".$password."AjdbEfwlfgekaEe";
        $password = hash("sha256", $password);

        $sql = "    SELECT  ".$this->db->dbprefix."user_admin.id, ".$this->db->dbprefix."user_admin.name, ".$this->db->dbprefix."user_admin.image, ".$this->db->dbprefix."user_admin.last_login, ".$this->db->dbprefix."rights_group.name AS `group`
                    FROM ".$this->db->dbprefix."user_admin
                    INNER JOIN ".$this->db->dbprefix."rights_group ON ".$this->db->dbprefix."user_admin.rights_group = ".$this->db->dbprefix."rights_group.id
                    WHERE BINARY email LIKE ".$this->db->escape($email)."
                    AND BINARY password LIKE ".$this->db->escape($password)."
                    AND ".$this->db->dbprefix."user_admin.active = 1
                    AND ".$this->db->dbprefix."user_admin.deleted = 0
                    LIMIT 1";
        $query = $this->db->query($sql);


        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function updateLastLoginDat($id){

        $sql = "UPDATE ".$this->db->dbprefix."user_admin
                SET last_login = now()
                WHERE id =  ".$id."";

        $this->db->query($sql);
    }
}