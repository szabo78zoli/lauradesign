<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Site_Loginout_Edit_Model extends Admin_Base_Model{

    public function __construct(){
        parent::__construct();

        $this->form_name = "SiteLogin";
        $this->db_table = "user_site";

        $this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = array(   "email" => "Email",
                                                                                            "password" => "Password",
                                                                                            "active" => "Active");
        $this->form_view = "loginout/site/site_loginout_view.tpl";
    }

    public function login($email, $password, $shop) {

        $password = "AjdbEfwlfgekaEe".$password."AjdbEfwlfgekaEe";
        $password = hash("sha256", $password);

        $sql = "    SELECT  ".$this->db->dbprefix."user_site.id, ".$this->db->dbprefix."user_site.first_name, ".$this->db->dbprefix."user_site.last_name, ".$this->db->dbprefix."user_site.last_login
                    FROM ".$this->db->dbprefix."user_site
                    WHERE BINARY email LIKE ".$this->db->escape($email)."
                    AND BINARY password LIKE ".$this->db->escape($password)."
                    AND ".$this->db->dbprefix."user_site.active = 1                    
                    AND ".$this->db->dbprefix."user_site.deleted = 0
                    LIMIT 1";
        $query = $this->db->query($sql);


        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function updateLastLoginDat($id){

        $sql = "UPDATE ".$this->db->dbprefix."user_site
                SET last_login = now()
                WHERE id =  ".$id."";

        $this->db->query($sql);
    }

    public function getuserData($id) {

        $this->db->select("id, first_name, last_name, registration_from");
        $this->db->where("id", $id);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $this->db->limit(1);
        $query = $this->db->get("user_site");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function getuserData2($id) {

        $this->db->select("user_site.id, first_name, last_name, registration_from, web_address");
        $this->db->join("shop_shop", "user_site.registration_from = shop_shop.id");
        $this->db->where("user_site.id", $id);
        $this->db->where("user_site.active", 1);
        $this->db->where("user_site.deleted", 0);
        $this->db->limit(1);
        $query = $this->db->get("user_site");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }
}