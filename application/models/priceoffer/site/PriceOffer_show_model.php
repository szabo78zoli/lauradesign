<?php

class PriceOffer_Show_Model extends Site_Base_Model
{
    public function __construct() {
        parent::__construct();


        $this->db_table = "priceoffer";
        $this->db_loaded_fields = array("id",
                                        "image");
        $this->form_view = "priceoffer/site/priceoffer_view.tpl";
    }

    public function getOffers(){
        $this->db->select("id, name");
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("offer");
        $result[0] = 'Kérem, válasszon!';
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row) {
                $result[$row['id']] = $row['name'];
            }

            return $result;
        }
        else{
            return false;
        }
    }

    public function getOffer($id){
        $this->db->select("name");
        $this->db->where("id", $id);
        $query = $this->db->get("offer");
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row) {
                $result = $row['name'];
            }

            return $result;
        }
        else{
            return false;
        }
    }

    public function save(){
        $this->db->set("name", $this->input->post('Name'));
        $this->db->set("email", $this->input->post('Email'));
        $this->db->set("phone", $this->input->post('Phone'));
        $this->db->set("type", $this->input->post('Type'));
        $this->db->set("level", $this->input->post('Level'));
        $this->db->set("area", $this->input->post('Area'));
        $this->db->set("place", $this->input->post('Place'));
        $this->db->set("offer", $this->input->post('Offer'));
        $this->db->set("start_year", $this->input->post('StartYear'));
        $this->db->set("start_month", $this->input->post('StartMonth'));
        $this->db->set("year", $this->input->post('Year'));
        $this->db->set("month", $this->input->post('Month'));
        $this->db->set("note", $this->input->post('Note'));

        $this->db->insert("price_offer");

        return $this->db->insert_id();
    }

    public function saveFile(){
        $this->db->set("name", $this->input->post('Name'));
        $this->db->set("email", $this->input->post('Email'));
        $this->db->set("phone", $this->input->post('Phone'));
        $this->db->set("type", $this->input->post('Type'));
        $this->db->set("level", $this->input->post('Level'));
        $this->db->set("area", $this->input->post('Area'));
        $this->db->set("place", $this->input->post('Place'));
        $this->db->set("offer", $this->input->post('Offer'));
        $this->db->set("start_year", $this->input->post('StartYear'));
        $this->db->set("start_month", $this->input->post('StartMonth'));
        $this->db->set("year", $this->input->post('Year'));
        $this->db->set("month", $this->input->post('Month'));
        $this->db->set("note", $this->input->post('Note'));

        $this->db->insert("price_offer");

        $id = $this->db->insert_id();

        $this->db->insert("price_offer_file");
    }
}
?>
