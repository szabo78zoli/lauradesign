<?php

class priceoffer_List_Model extends Admin_Base_Model{
    public function __construct() {
        parent::__construct();

        $this->form_name = "PriceOffer";
        $this->form_elements = array("Filter");
        $this->db_table = "price_offer";
        $this->db_list_fields = "price_offer.id, price_offer.name, email, phone, type, level, area, place, offer, county, city, start_year, start_month, year, month, note, price_offer.active";
        $this->db_where = "price_offer.deleted = 0";
        $this->db_order_by = "price_offer.id ASC";
        $this->db_group_by = " price_offer.id";
        $this->month = array(   1 => 'január',
                                2 => 'február',
                                3 => 'március',
                                4 => 'április',
                                5 => 'május',
                                6 => 'június',
                                7 => 'július',
                                8 => 'augusztus',
                                9 => 'szeptember',
                                10 => 'október',
                                11 => 'november',
                                12 => 'december');

        $this->place = array(   1 => 'Konyha',
                                2 => 'Étkező',
                                3 => 'Nappali',
                                4 => 'Gyerekszoba',
                                5 => 'Hálószoba',
                                6 => 'Dolgozó szoba ',
                                7 => 'Előtér, folyosó',
                                8 => 'Egyéb helyiségek');


        if($this->input->post($this->form_name ."Lenght")!= ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->input->post($this->form_name."Lenght"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->input->post($this->form_name."Lenght"));
        }
        else{
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }

        $this->db_list_limit = $this->session->userdata($this->form_name."Lenght");
        $this->form_view = "priceoffer/admin/priceoffer_list_view.tpl";
    }

    public function listLoad($table, $list_fields, $where, $order_by, $group_by, $section, $limit){
        $this->db->select($list_fields);
        $this->db->select("offer.name AS offername");
        $this->db->join("offer", "price_offer.offer = offer.id");

        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->order_by($order_by);

        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }

        $this->db->limit($section, $limit);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $row){
                $result[$row["id"]] = $row;
                $result[$row["id"]]['start_month'] = $this->month[$row['start_month']];
                $result[$row["id"]]['month'] = $this->month[$row['start_month']];

                $places = str_split($row['place']);
                $placeText = '';

                foreach ($places as $place){
                    if(empty($placeText)){
                        $placeText .= $this->place[$place];
                    }
                    else{
                        $placeText .= ', '.$this->place[$place];
                    }
                }
                $result[$row["id"]]['place'] = $placeText;
            }

            return $result;
        }
        else{
            return false;
        }
    }
}
?>