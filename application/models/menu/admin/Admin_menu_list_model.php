<?php

class Admin_Menu_List_Model extends Admin_Base_Model{
    public function __construct() {
        parent::__construct();

        $this->form_name = "AdminMenu";
        $this->order_form_name = "AdminMenuOrder";
        $this->form_elements = array("Filter");
        $this->db_table = "admin_menu";
        $this->db_list_fields = "id, parent_id, name, order_number, level, alias, active";
        $this->db_where = "deleted = 0";
        $this->db_order_by = "id ASC, order_number ASC";
        $this->db_group_by = "";
        $this->table_head = array("Id" =>"id", "Név" => "name", "Link" => "link", "Sorrend" => "", "Szerkesztés" => "","Aktív" => "", "Törlés" => "");

        if($this->input->post($this->form_name ."Lenght")!= ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->input->post($this->form_name."Lenght"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->input->post($this->form_name."Lenght"));
        }
        elseif($this->input->post($this->form_name ."Lenght")== "" && $this->session->userdata($this->form_name."Lenght") == ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->session->userdata("defaultListLength"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }
        else{
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }
        $this->db_list_limit = "";
        $this->form_view = "menu/admin/admin_menu_list_view.tpl";
    }

    public function listLoad($table, $list_fields, $where, $order_by, $group_by, $section, $limit){

        $printedMenu = $this->array_flatten($this->getMenuTreeSelect("", $where));

        if (count($printedMenu) > 0){
            $resultArray = $printedMenu;
            foreach ($resultArray as $key => $row){

                if(isset($resultArray[$key-1]) && $resultArray[$key-1]["parent_id"] != $row["parent_id"] && $resultArray[$key-1]["level"] < $row["level"]){
                    $row["first"] = true;
                }
                elseif(!isset($resultArray[$key-1])){
                    $row["first"] = true;
                }
                else{
                    $row["first"] = false;
                }

                if(isset($resultArray[$key+1]) && $resultArray[$key+1]["parent_id"] != $row["parent_id"] && $resultArray[$key+1]["level"] < $row["level"]){
                    $row["last"] = true;
                }
                elseif(!isset($resultArray[$key+1])){
                    $row["last"] = true;
                }
                else{
                    $row["last"] = false;
                }

                $result[$row["id"]] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function listLoadCount($table, $where){
        if(!empty($where)){
            $this->db->where($where);
        }
        $result = $this->db->count_all_results($table);

        return $result;
    }

    public function menuOrderUp($id){
        $this->db->select('id, parent_id, order_number');
        $this->db->where("id", $id);
        $this->db->where("active", "1");
        $this->db->where("deleted", "0");
        $query = $this->db->get("admin_menu");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $order_number = $row["order_number"];
                $parent_id = $row["parent_id"];
                $id_up = $row["id"];
            }
        }

        $this->db->select('id, order_number');
        $this->db->where("order_number", $order_number-1);
        $this->db->where("parent_id", $parent_id);
        $this->db->where("active", "1");
        $this->db->where("deleted", "0");
        $query = $this->db->get("admin_menu");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $id_down = $row["id"];
            }
        }

        $updateData = array("order_number" => $order_number-1);
        $this->db->where("id", $id_up);
        $this->db->update("admin_menu", $updateData);

        $updateData = array("order_number" => $order_number);
        $this->db->where("id", $id_down);
        $this->db->update("admin_menu", $updateData);
    }

    public function menuOrderDown($id){
        $this->db->select('id, parent_id, order_number');
        $this->db->where("id", $id);
        $this->db->where("active", "1");
        $this->db->where("deleted", "0");
        $query = $this->db->get("admin_menu");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $order_number = $row["order_number"];
                $parent_id = $row["parent_id"];
                $id_up = $row["id"];
            }
        }

        $this->db->select('id, order_number');
        $this->db->where("order_number", $order_number+1);
        $this->db->where("parent_id", $parent_id);
        $this->db->where("active", "1");
        $this->db->where("deleted", "0");
        $query = $this->db->get("admin_menu");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $id_down = $row["id"];
            }
        }

        $updateData = array("order_number" => $order_number+1);
        $this->db->where("id", $id_up);
        $this->db->update("admin_menu", $updateData);

        $updateData = array("order_number" => $order_number);
        $this->db->where("id", $id_down);
        $this->db->update("admin_menu", $updateData);
    }

    function getMenuTreeSelect($menuId = 0, $where){
        $this->db->select('id,parent_id,name,alias,level,active');
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->order_by('parent_id','asc');
        $this->db->order_by('order_number','asc');
        $query = $this->db->get("admin_menu");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                //$result[] = $row;
                $category['categories'][$row['id']] = $row;
                $category['parent_cats'][$row['parent_id']][] = $row['id'];
            }
        }
        else{
            $result = array();
        }

        return $data['category'] = $this->getCategoryTreeSelect(0, $category, $menuId);
    }

    function getCategoryTreeSelect($parent, $category, $menuId = 0){
        $html = array();
        if (isset($category['parent_cats'][$parent])){
            $p = 1;

            foreach ($category['parent_cats'][$parent] as $cat_id){

                $this->db->select('parent_id');
                $this->db->where('id',$menuId);
                $query = $this->db->get("admin_menu");

                if ($query->num_rows() > 0){
                    foreach ($query->result_array() as $row){
                        $parentResult = $row["parent_id"];
                    }
                }
                    $prefix = "";
                    for($i = 1; $i <= $category['categories'][$cat_id]['level']; $i++){
                        $prefix = $prefix."---";
                    }
                    $html[$p][] = $category['categories'][$cat_id]['id'];
                    $html[$p][] = $prefix.$category['categories'][$cat_id]['name'];
                    $html[$p][] = $category['categories'][$cat_id]['alias'];
                    $html[$p][] = $category['categories'][$cat_id]['level'];
                    $html[$p][] = $category['categories'][$cat_id]['active'];
                    $html[$p][] = $category['categories'][$cat_id]['parent_id'];
                    $html[$p][] = $this->getCategoryTreeSelect($cat_id, $category, $menuId);
                    $p++;
            }
        }
        return $html;
    }

    function array_flatten($array) {
        if (!is_array($array)) {
            return false;
        }
        $result = array();
        $i=0;
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->array_flatten($value));
            } else {
                $result[$array["0"]]["id"] = $array["0"];
                $result[$array["0"]]["name"] = $array["1"];
                $result[$array["0"]]["alias"] = $array["2"];
                $result[$array["0"]]["level"] = $array["3"];
                $result[$array["0"]]["active"] = $array["4"];
                $result[$array["0"]]["parent_id"] = $array["5"];
            }
            $i++;
        }
        return $result;
    }
}
?>