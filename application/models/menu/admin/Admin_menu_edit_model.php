<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Admin_Menu_Edit_Model extends Admin_Base_Model{

    public function __construct(){
        parent::__construct();

        $this->form_name = "AdminMenu";
        $this->db_table = "admin_menu";

        $this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = array(   "parent_id" => "Parent",
                                                                                            "name" => "Name",
                                                                                            "alias" => "Alias",
                                                                                            "icon" => "Icon",
                                                                                            "active" => "Active");
        $this->form_view = "menu/admin/admin_menu_view.tpl";
    }

    function getMenuTreeSelect($menuId = 0){
        $this->db->select('id,parent_id,name,level');
        $this->db->order_by('parent_id','asc');
        $this->db->order_by('order_number','asc');
        $query = $this->db->get("admin_menu");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                //$result[] = $row;
                $category['categories'][$row['id']] = $row;
                $category['parent_cats'][$row['parent_id']][] = $row['id'];
            }
        }
        else{
            $result = array();
        }

        return $data['category'] = $this->getCategoryTreeSelect(0, $category, $menuId);
    }

    function getCategoryTreeSelect($parent, $category, $menuId = 0){
        $html = "";
        if (isset($category['parent_cats'][$parent])){
            $p = 1;

            foreach ($category['parent_cats'][$parent] as $cat_id){

                $this->db->select('parent_id');
                $this->db->where('id',$menuId);
                $query = $this->db->get("admin_menu");

                if ($query->num_rows() > 0){
                    foreach ($query->result_array() as $row){
                        $parentResult = $row["parent_id"];
                    }
                }

                $prefix = "";
                $selected = "";

                if($menuId != 0){
                    if($category['categories'][$cat_id]['id'] == $parentResult){
                        $selected = "selected";
                    }
                    $prefix = "";
                    for($i = 1; $i <= $category['categories'][$cat_id]['level']; $i++){
                        $prefix = $prefix."---";
                    }
                    $html .= '<option value="'.$category['categories'][$cat_id]['id'].'" '.$selected.' >'.$prefix.$category['categories'][$cat_id]['name'].'</option>';
                    $html .= $this->getCategoryTreeSelect($cat_id, $category, $menuId);
                    $p++;
                }
                else{
                    $prefix = "";
                    for($i = 1; $i <= $category['categories'][$cat_id]['level']; $i++){
                        $prefix = $prefix."---";
                    }
                    $html .= '<option value="'.$category['categories'][$cat_id]['id'].'" >'.$prefix.$category['categories'][$cat_id]['name'].'</option>';
                    $html .= $this->getCategoryTreeSelect($cat_id, $category, $menuId);
                    $p++;
                }
            }
        }
        return $html;
    }

    public function editorInsert($table, $fields){
        if($fields["parent_id"] != 0) {
            $this->db->select("level");
            $this->db->where("id", $fields["parent_id"]);
            $query = $this->db->get($table);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $level = $row["level"];
                }
            }
        }
        else{
            $level = 0;
        }

        $this->db->select_max("order_number");
        $this->db->where("parent_id", $fields["parent_id"]);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $order_number = $row["order_number"];
            }
        }

        $fields["level"] = $level+1;
        $fields["order_number"] = $order_number+1;
        $this->db->insert($table, $fields);
    }
}