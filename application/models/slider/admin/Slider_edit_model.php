<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Slider_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Slider";

        $this->form_elements =  array("Name","Link","Description","Image","Before","Active");

        $this->form_elements = $this->db_edited_fields = $this->db_loaded_fields = array(   "name" => "Name",
                                                                                            "link" => "Link",
                                                                                            "description" => "Description",
                                                                                            "image" => "Image",
                                                                                            "active" => "Active");
        $this->db_table = "slider";
        $this->form_view = "slider/admin/slider_edit_view.tpl";
    }

    public function editorInsert($table, $fields){
// Az új elemet berakja a lista végére
        $this->db->select_max('order_number');
        $this->db->where("deleted", "0");
        $query = $this->db->get("slider");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $result = $row["order_number"];
            }
            $result;
        }

        if(!empty($result)) {
            $fields["order_number"] = $result+1;
        }
        else{
            $fields["order_number"] = 1;
        }

        $this->db->insert($table, $fields);
        $id = $this->db->insert_id();

        $sliderBefore = $this->input->post("SliderBefore");

        if($sliderBefore > 0){
// A szerkesztendő slider elem sorszámának lekérdezése
            $this->db->select('id, order_number');
            $this->db->where("id", $id);
            $query = $this->db->get("slider");

            if ($query->num_rows() > 0){
                foreach ($query->result_array() as $row){
                    $elementOrderNumber = $row["order_number"];
                    $elementSliderId = $row["id"];
                }
            }
// A kiválasztott slider elem sorszámának lekérdezése
            $this->db->select('id, order_number');
            $this->db->where("id", $this->input->post("SliderBefore"));
            $query = $this->db->get("slider");

            if ($query->num_rows() > 0){
                foreach ($query->result_array() as $row){
                    $movableOrderNumber = $row["order_number"];
                    $movableSliderId = $row["id"];
                }
            }
// Előre mozgatás!

            $this->db->where("id", $id);
            $this->db->update("slider", array("order_number" => $movableOrderNumber));

            $this->db->select('id, order_number');
            $this->db->where("order_number >= ".$movableOrderNumber);
            $this->db->where("id != ".$id);
            $this->db->where("deleted", "0");
            $this->db->order_by("order_number", "ASC");
            $query = $this->db->get("slider");

            if ($query->num_rows() > 0){
                $i = 0;
                $orderNumber = 0;
                foreach ($query->result_array() as $key => $row){

                    if($i > 0){
                        $orderNumber++;
                    }
                    else{
                        $orderNumber = $row["order_number"];
                        $orderNumber++;
                    }

                    $this->db->where("id", $row["id"]);
                    $this->db->update("slider", array("order_number" => $orderNumber));

                    $i++;
                }
            }
        }
    }

    public function editorUpdate($table, $data, $where, $id){
// Tartalom update
        $this->db->where($where, $id);
        $this->db->update($table, $data);

        $sliderBefore = $this->input->post("SliderBefore");

        if($sliderBefore > 0) {
// A szerkesztendő slider elem sorszámának lekérdezése
            $this->db->select('id, order_number');
            $this->db->where("id", $id);
            $query = $this->db->get("slider");

            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $elementOrderNumber = $row["order_number"];
                    $elementSliderId = $row["id"];
                }
            }
// A kiválasztott slider elem sorszámának lekérdezése
            $this->db->select('id, order_number');
            $this->db->where("id", $this->input->post("SliderBefore"));
            $query = $this->db->get("slider");

            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $movableOrderNumber = $row["order_number"];
                    $movableSliderId = $row["id"];
                }
            }
// Az aktuális elem elhelyezése a sorban, és az azt követő elemek sorszámának növelése 1-el
            if ($elementOrderNumber > $movableOrderNumber) {
// Előre mozgatás!

                $this->db->where($where, $id);
                $this->db->update("slider", array("order_number" => $movableOrderNumber));

                $this->db->select('id, order_number');
                $this->db->where("order_number >= " . $movableOrderNumber);
                $this->db->where("id != " . $id);
                $this->db->where("deleted", "0");
                $this->db->order_by("order_number", "ASC");
                $query = $this->db->get("slider");

                if ($query->num_rows() > 0) {
                    $i = 0;
                    $orderNumber = 0;
                    foreach ($query->result_array() as $key => $row) {

                        if ($i > 0) {
                            $orderNumber++;
                        } else {
                            $orderNumber = $row["order_number"];
                            $orderNumber++;
                        }

                        $this->db->where($where, $row["id"]);
                        $this->db->update("slider", array("order_number" => $orderNumber));

                        $i++;
                    }
                }
            } elseif ($elementOrderNumber < $movableOrderNumber) {
// Hátra mozgatás!

                $this->db->where($where, $id);
                $this->db->update("slider", array("order_number" => $movableOrderNumber));

                $this->db->select('id, order_number');
                $this->db->where("order_number >= " . $elementOrderNumber);
                $this->db->where("order_number <= " . $movableOrderNumber);
                $this->db->where("id != " . $movableSliderId);
                $this->db->where("deleted", "0");
                $this->db->order_by("order_number", "ASC");
                $query = $this->db->get("slider");

                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $key => $row) {

                        $orderNumber = $row["order_number"];
                        $orderNumber--;

                        $this->db->where($where, $row["id"]);
                        $this->db->update("slider", array("order_number" => $orderNumber));
                    }
                }
            }
        }
    }

    public function getSelectValuesSlider(){
        $this->db->where("deleted", "0");
        $this->db->order_by("order_number");
        $query = $this->db->get("slider");

        foreach ($query->result_array() as $row) {
            $result[$row["id"]] = $row["name"];
        }
        return $result;
    }

    public function getBefore($id){
        $this->db->select("order_number");
        $this->db->where("id", $id);
        $query = $this->db->get("slider");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row){
                $result = $row["order_number"];
            }

            $before = $result+1;

            $this->db->select("id");
            $this->db->where("order_number", $before);
            $query = $this->db->get("slider");

            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row){
                    $resultSelected = $row["id"];
                }

                return $resultSelected;
            }
        }
    }

    public function editorImageLoad($id){
        $this->db->select("image");
        $this->db->where("id", $id);
        $query = $this->db->get("slider");

        foreach ($query->result_array() as $row) {
            $result = $row["image"];
        }
        return $result;
    }
}
?>