<?php

class Slider_List_Model extends Admin_Base_Model{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Slider";
        $this->form_elements = array("Filter", "Lang");
        $this->db_table = "slider";
        $this->db_list_fields = "id, name, image, order_number, active";
        $this->db_where = "deleted = 0";
        $this->db_order_by = "order_number ASC";
        $this->db_group_by = "";

        if($this->input->post($this->form_name ."Lenght")!= ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->input->post($this->form_name."Lenght"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->input->post($this->form_name."Lenght"));
        }
        else{
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }

        $this->db_list_limit = $this->session->userdata($this->form_name."Lenght");
        $this->form_view = "slider/admin/slider_list_view.tpl";
    }

    public function listLoad($table, $list_fields, $where, $order_by, $group_by, $section, $limit){

        $this->db->select($list_fields);
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->order_by($order_by);

        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }

        $this->db->limit($section, $limit);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $key => $row){
                $result[] = $row;
            }

            $count = count($result);
            $result[0]["first"] = 1;
            $result[$count-1]["last"] = 1;

            return $result;
        }
        else{
            return false;
        }
    }

    public function menuOrderUp($id){
        $this->db->select('id, order_number');
        $this->db->where("id", $id);
        $this->db->where("deleted", "0");
        $query = $this->db->get("slider");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $order_number = $row["order_number"];
                $id_up = $row["id"];
            }
        }

        $this->db->select('id, order_number');
        $this->db->where("order_number", $order_number-1);
        $this->db->where("deleted", "0");
        $query = $this->db->get("slider");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $id_down = $row["id"];
            }
        }

        $updateData = array("order_number" => $order_number-1);
        $this->db->where("id", $id_up);
        $this->db->update("slider", $updateData);

        $updateData = array("order_number" => $order_number);
        $this->db->where("id", $id_down);
        $this->db->update("slider", $updateData);
    }

    public function menuOrderDown($id){
        $this->db->select('id, order_number');
        $this->db->where("id", $id);
        $this->db->where("deleted", "0");
        $query = $this->db->get("slider");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $order_number = $row["order_number"];
                $id_up = $row["id"];
            }
        }

        $this->db->select('id, order_number');
        $this->db->where("order_number", $order_number+1);
        $this->db->where("deleted", "0");
        $query = $this->db->get("slider");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $id_down = $row["id"];
            }
        }

        $updateData = array("order_number" => $order_number+1);
        $this->db->where("id", $id_up);
        $this->db->update("slider", $updateData);

        $updateData = array("order_number" => $order_number);
        $this->db->where("id", $id_down);
        $this->db->update("slider", $updateData);
    }

    public function listStatusModifyDelete($table, $data, $where, $id){
        $this->db->where($where, $id);
        $this->db->update($table, $data);

        $this->db->select('id, order_number');
        $this->db->where("id > ".$id);
        $this->db->where("deleted", "0");
        $this->db->order_by("order_number", "ASC");
        $query = $this->db->get("slider");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $orderNumber = $row["order_number"];
                $sliderId = $row["id"];

                $this->db->where($where, $sliderId);
                $this->db->update("slider", array("order_number" => $orderNumber-1));
            }
        }
    }
}
?>