<?php

class Event_Log_List_Model extends Admin_Base_Model{
    public function __construct() {
        parent::__construct();

        $this->form_name = "EventLog";
        $this->order_form_name = "EventLogOrder";
        $this->form_elements = array("Filter", "Type", "StartDate", "EndDate");
        $this->db_table = "event_log";
        $this->db_list_fields = "id, user, type, module, event, ip, create_date";
        $this->db_where = " active = 1";
        $this->db_order_by = "create_date, id";
        $this->db_group_by = "";
        $this->table_head = array("Id" =>"id", "Felhasználó" => "user", "Felület" => "type", "Modul" => "module", "Esemény" => "event", "IP cím" => "ip", "Dátum" => "create_date");

        if($this->input->post($this->form_name ."Lenght")!= ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->input->post($this->form_name."Lenght"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->input->post($this->form_name."Lenght"));
        }
        elseif($this->input->post($this->form_name ."Lenght")== "" && $this->session->userdata($this->form_name."Lenght") == ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->session->userdata("defaultListLength"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }
        else{
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }

        $this->db_list_limit = $this->session->userdata($this->form_name."Lenght");
        $this->form_view = "event_log/admin/event_log_list_view.tpl";
    }
}
?>