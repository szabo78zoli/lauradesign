<?php

class Site_Start_Show_Model extends Site_Base_Model {

    public function __construct(){
        parent::__construct();

        $this->load->model("base/site/site_base_model");

        $this->smarty_tpl->assign('charset', $this->config->item('charset'));
        $this->smarty_tpl->assign('base_url', $this->config->item('base_url'));

    }

    public function index() {

    }

    public function getContentElementById($id){
        $this->db->select("name, alias, description, content");
        $this->db->where("id", $id);
        $this->db->where("lang", $this->site_lang);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("content");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row) {
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }
}

?>