<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Contact_show_Model extends Admin_Base_Model{

    public function __construct(){
        parent::__construct();

        $this->form_name = "Contact";
        $this->db_table = "contact";

        $this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = array(   "name" => "Name",
                                                                                            "addess" => "Address",
                                                                                            "email" => "Email",
                                                                                            "phone" => "Phone",
                                                                                            "message" => "Message");
        $this->form_view = "contact/site/contact_show_view.tpl";
    }

    public function login($email, $password, $shop) {

        $password = "AjdbEfwlfgekaEe".$password."AjdbEfwlfgekaEe";
        $password = hash("sha256", $password);

        $sql = "    SELECT  ".$this->db->dbprefix."user_site.id, ".$this->db->dbprefix."user_site.first_name, ".$this->db->dbprefix."user_site.last_name, ".$this->db->dbprefix."user_site.last_login
                    FROM ".$this->db->dbprefix."user_site
                    WHERE BINARY email LIKE ".$this->db->escape($email)."
                    AND BINARY password LIKE ".$this->db->escape($password)."
                    AND ".$this->db->dbprefix."user_site.active = 1                    
                    AND ".$this->db->dbprefix."user_site.deleted = 0
                    LIMIT 1";
        $query = $this->db->query($sql);


        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function updateLastLoginDat($id){

        $sql = "UPDATE ".$this->db->dbprefix."user_site
                SET last_login = now()
                WHERE id =  ".$id."";

        $this->db->query($sql);
    }
}