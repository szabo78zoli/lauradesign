<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Partner_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Partner";

        $this->form_elements = $this->db_edited_fields = $this->db_loaded_fields = array(   "name" => "Name",
                                                                                            "link" => "Link",
                                                                                            "image" => "Image",
                                                                                            "active" => "Active");
        $this->db_table = "partner";
        $this->form_view = "partner/admin/partner_edit_view.tpl";
    }
}
?>