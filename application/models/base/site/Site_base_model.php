<?php
class Site_Base_Model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    function getMenuTree($parent = 0, $lang) {
        $this->db->select('id, parent_id, order_number, name, alias, icon');
        $this->db->where("lang", $lang);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $this->db->order_by('parent_id','asc');
        $this->db->order_by('order_number','asc');
        $query = $this->db->get("site_menu");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row){
                $category['categories'][$row['id']] = $row;
                $category['parent_cats'][$row['parent_id']][] = $row['id'];
            }
        }
        else{
            $result = array();
        }

        return $data['category'] = $this->getCategoryTree(0, $category);
    }

    function getCategoryTree($parent, $category) {
        $html = "";
        if (isset($category['parent_cats'][$parent])) {
            $html .= '<li>';
            foreach ($category['parent_cats'][$parent] as $cat_id) {

                if (!isset($category['parent_cats'][$cat_id])) {
                    if($this->session->userdata("Site_User_Id") && $category['categories'][$cat_id]['alias'] == "bejelentkezes" ){
                        $html .= '<li><a href="kijelentkezes">Kilépés</a></li>';
                    }
                    else{
                        $html .= '<li><a href="'.$category['categories'][$cat_id]['alias'].'">'.$category['categories'][$cat_id]['name'].'</a></li>';
                    }
                }
                if (isset($category['parent_cats'][$cat_id])) {
                    $html .= '<li class="dropdown"><a href="'.$category['categories'][$cat_id]['alias'].'" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $category['categories'][$cat_id]['name'] . ' <span class="caret"></span>';
                    $html .= '<span class="pull-right-container">';
                    $html .= '';
                    $html .= '</span>';
                    $html .= '</a><ul class="dropdown-menu" role="menu">';
                    $html .= $this->getCategoryTree($cat_id, $category);
                    $html .= "</ul></li> \n";
                }
            }
            $html .= '</li>';
        }
        return $html;
    }

    function getMmenuTree($parent = 0, $lang) {
        $this->db->select('id, parent_id, order_number, name, alias, icon');
        $this->db->where("lang", $lang);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $this->db->order_by('parent_id','asc');
        $this->db->order_by('order_number','asc');
        $query = $this->db->get("site_menu");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row){
                $category['categories'][$row['id']] = $row;
                $category['parent_cats'][$row['parent_id']][] = $row['id'];
            }
        }
        else{
            $result = array();
        }

        return $data['category'] = $this->getMcategoryTree(0, $category);
    }

    function getMcategoryTree($parent, $category) {
        $html = "";
        if (isset($category['parent_cats'][$parent])) {
            $html .= '<li>';
            foreach ($category['parent_cats'][$parent] as $cat_id) {

                if (!isset($category['parent_cats'][$cat_id])) {
                    $html .= '<li><a href="'.$category['categories'][$cat_id]['alias'].'">'.$category['categories'][$cat_id]['name'].'</a></li>';
                }
                if (isset($category['parent_cats'][$cat_id])) {
                    $html .= '<li><span href="">'.$category['categories'][$cat_id]['name'].'</span>';
                    $html .= '<ul>';
                    $html .= $this->getCategoryTree($cat_id, $category);
                    $html .= "</ul></li> \n";
                }
            }
            $html .= '</li>';
        }
        return $html;
    }

    public function getContentElement($alias){
        $this->db->select("name, alias, description, content");
        $this->db->where("alias", $alias);
        $this->db->where("lang", $this->site_lang);
        $query = $this->db->get("content");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row) {
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    function getSlider() {

        $this->db->select("id, link, description, image");
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $this->db->where("active = 1 AND deleted = 0");
        $this->db->order_by("order_number", "ASC");
        $this->db->group_by("id");
        $query = $this->db->get("slider");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row) {
                $result[] = $row;
            }

            return $result;
        }
        else{
            return false;
        }
    }

    public function getGalleryCategories(){
        $this->db->select("id, alias, image, name");
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $this->db->order_by("id", "DESC");
        $this->db->limit(4);
        $query = $this->db->get("gallery_category");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row) {
                $result[$row["id"]] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function eventLogSave($user, $type, $event_type, $module, $event, $ip){
        $fields = array();
        $fields["user"] = $user;
        $fields["type"] = $type;
        $fields["module"] = $module;
        $fields["event"] = $event;
        $fields["event_type"] = $event_type;
        $fields["ip"] = $ip;

        $count = $this->db->count_all_results('event_log');

        if($count >= 10000){
            $this->db->select('MIN(id) AS minId');
            $query = $this->db->get("event_log");
            $minId = $query->row();

            $this->db->where('id', $minId->minId);
            $this->db->delete('event_log');
        }

        $this->db->set("create_date", "NOW()", FALSE);
        $this->db->insert("event_log", $fields);
    }

    public function getSelectValues($table){
        $query = $this->db->get_where($table, array("active" =>"1", "deleted" => "0"));
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $result[$row["id"]] = $row["name"];
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function getOffers(){
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        //$this->db->limit(3);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get("offer");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row) {
                $result[] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function printR($variable){
        echo "<pre>";
        print_r($variable);
        echo "</pre>";
    }

    public function varDump($variable){
        echo "<pre>";
        var_dump($variable);
        echo "</pre>";
    }
}
?>