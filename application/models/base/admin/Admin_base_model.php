<?php
class Admin_Base_Model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function editorInsert($table, $fields){
        $this->db->insert($table, $fields);
    }
    function getMenuTree($parent = 0, $rights) {
        $this->db->select('id, parent_id, order_number, name, alias, icon');
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $this->db->order_by('parent_id','asc');
        $this->db->order_by('order_number','asc');
        $query = $this->db->get("admin_menu");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row){
                $category['categories'][$row['id']] = $row;
                $category['parent_cats'][$row['parent_id']][] = $row['id'];
            }
        }
        else{
            $result = array();
        }

        return $data['category'] = $this->getCategoryTree(0, $category, $rights);
    }

    function getCategoryTree($parent, $category, $rights) {
        $html = "";
        if (isset($category['parent_cats'][$parent])) {
            $html .= '<li class="treeview">';
            foreach ($category['parent_cats'][$parent] as $cat_id) {

                $class = '';
                $class_li = '';
                $active_uri = $this->uri->segment(2);

                if (strpos($active_uri, $category['categories'][$cat_id]['alias']) !== false) {
                    $class = 'active';
                    $class_li = 'class="active"';
                }

                if (!isset($category['parent_cats'][$cat_id]) && in_array($category['categories'][$cat_id]['alias'], $rights)) {
                    $html .= '<li ' . $class_li . '><a href="admin/'.$category['categories'][$cat_id]['alias'].'"><i class="fa '.$category['categories'][$cat_id]['icon'].'"></i> '.$category['categories'][$cat_id]['name'].'</a></li>';
                }
                if (isset($category['parent_cats'][$cat_id]) && in_array($category['categories'][$cat_id]['alias'], $rights)) {
                    $html .= '<li class="' . $class . ' treeview"><a href="#"><i class="fa '.$category['categories'][$cat_id]['icon'].'"></i>' . $category['categories'][$cat_id]['name'];
                    $html .= '<span class="pull-right-container">';
                    $html .= '<i class="fa fa-angle-left pull-right"></i>';
                    $html .= '</span>';
                    $html .= '</a><ul class="treeview-menu">';
                    $html .= $this->getCategoryTree($cat_id, $category, $rights);
                    $html .= "</ul></li> \n";
                }
            }
            $html .= '</li>';
        }
        return $html;
    }

    public function listLoad($table, $list_fields, $where, $order_by, $group_by, $section, $limit){
        $this->db->select($list_fields);
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->order_by($order_by);

        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }

        $this->db->limit($section, $limit);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $row){
                $result[$row["id"]] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function listLoadCount($table, $where){
        if(!empty($where)){
            $this->db->where($where);
        }
        $result = $this->db->count_all_results($table);

        return $result;
    }

    public function listStatusModify($table, $data, $where, $id){
        $this->db->where($where, $id);
        $this->db->update($table, $data);
    }

    public function editorLoad($table, $editor_fields, $where, $id){

        $this->db->select($editor_fields);
        $this->db->where($where);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $key => $row)
            {
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function editorUpdate($table, $data, $where, $id){
        $this->db->where($where, $id);
        //$this->db->where($table."_nyelv", 1);
        $this->db->update($table, $data);
    }

    public function getSelectValues($table){
        $query = $this->db->get_where($table, array("active" =>"1", "deleted" => "0"));
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $result[$row["id"]] = $row["name"];
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function listLenghtLoad(){
        $this->db->select("value");
		$this->db->where ("parameter", "listLenght");
        $query = $this->db->get("options");
        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $row){
                $result = $row["value"];
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function eventLogSave($user, $type, $event_type, $module, $event, $ip){
        $fields = array();
        $fields["user"] = $user;
        $fields["type"] = $type;
        $fields["module"] = $module;
        $fields["event"] = $event;
        $fields["event_type"] = $event_type;
        $fields["ip"] = $ip;

        $count = $this->db->count_all_results('event_log');

        if($count >= 10000){
            $this->db->select('MIN(id) AS minId');
            $query = $this->db->get("event_log");
            $minId = $query->row();

            $this->db->where('id', $minId->minId);
            $this->db->delete('event_log');
        }

        $this->db->set("create_date", "NOW()", FALSE);
        $this->db->insert("event_log", $fields);
    }

    public function loadRights($id){
        $this->db->select("rights_right_group.right_id AS RIGHT, rights.modul_uri AS URI");
        $this->db->join("rights_group", "rights_right_group.right_group_id = rights_group.id");
        $this->db->join("rights", "rights_right_group.right_id = rights.id");
        $this->db->join("user_admin", "rights_group.id = user_admin.rights_group");
        $this->db->where("user_admin.id", $id);
        $query = $this->db->get("rights_right_group");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $value){
                $row[$value["RIGHT"]] = $value["URI"];
            }
            return $row;
        }
        else{
            return false;
        }

    }

    public function loadOption($parameter){
        $this->db->select("value");
        $this->db->where("parameter", $parameter);
        $query = $this->db->get("options");
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $value){
                $row = $value["value"];
            }
            return $row;
        }
        else{
            return false;
        }
    }

    public function printR($variable){
        echo "<pre>";
        print_r($variable);
        echo "</pre>";
    }

    public function varDump($variable){
        echo "<pre>";
        var_dump($variable);
        echo "</pre>";
    }
}
?>