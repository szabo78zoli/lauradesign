<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Options_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "AdminOptions";

        $this->form_elements = $this->db_edited_fields = $this->db_loaded_fields = array(   "parameter" => "Parameter",
                                                                                            "value" => "Value");
        $this->db_table = "options";
        $this->form_view = "options/admin/options_edit_view.tpl";
    }
}
?>