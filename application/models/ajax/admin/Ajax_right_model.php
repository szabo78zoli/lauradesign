<?php

class Ajax_Right_Model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getRight($siteType, $id=0){

        $row = array();
        $this->db->where("site_type", $siteType);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $this->db->select("id, name");
        $query = $this->db->get("rights");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $value){
                $row[$value["id"]] = $value["name"];
            }
            return $row;
        }
        else{
            return false;
        }
    }
    public function getRightLoad($id){
        $this->db->join("rights_right_group", "rights_group.id = rights_right_group.right_group_id");
        $this->db->where("rights_group.id", $id);
        $query = $this->db->get("rights_group");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $key => $row) {
                $result[] = $row["right_id"];
            }
            return $result;
        }
        else{
            return false;
        }
    }
}
?>
