<?php

class Ajax_Select_Version_Model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getVersion($id){
        $this->db->where("project", $id);
        $this->db->where("active", "1");
        $this->db->where("deleted", "0");
        $query = $this->db->get("user_site_project_version");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[$row["id"]] = $row["name"];
            }
            return $result;
        }
        else{
            return false;
        }
    }
}
?>
