<?php

class Ajax_Imagegallery_Delete_Model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function deleteImage($id){
        $this->db->select("gallery, image");
        $this->db->where("id", $id);
        $this->db->limit(1);
        $query = $this->db->get("gallery_image");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $key => $row) {
                $result = $row["gallery"];
				$resultImage = $row["image"];
            }

            $this->db->where("id", $id);
            $this->db->delete("gallery_image");
			
			unlink("application/upload/gallery/".$resultImage);

            return $result;
        }
        else{
            return false;
        }
    }

    public function deleteProjectImage($id){
        $this->db->select("image");
        $this->db->where("id", $id);
        $this->db->limit(1);
        $query = $this->db->get("user_site_project_image");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $key => $row) {
				$resultImage = $row["image"];
            }

            $this->db->where("id", $id);
            $this->db->delete("user_site_project_image");
            
            unlink("application/upload/project_gallery/".$resultImage);

            return $resultImage;
        }
        else{
            return false;
        }
    }

    public function getImage($showId){
        $this->db->select("id, image");
        $this->db->where("gallery", $showId);
        $query = $this->db->get("gallery_image");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $key => $row) {
                $result[] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }
}
?>
