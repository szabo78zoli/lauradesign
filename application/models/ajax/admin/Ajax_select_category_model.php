<?php

class Ajax_Select_Category_Model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function subCategory($id){
        $query = $this->db->get_where("product_category", array("parent_id" => $id, "level" => 2, "active" =>"1", "deleted" => "0"));
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[$row["id"]] = $row["name"];
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function category($id){
        $query = $this->db->get_where("product_category", array("parent_id" => $id, "level" => 3, "active" =>"1", "deleted" => "0"));
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[$row["id"]] = $row["name"];
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function selectSubCategory($id){
        $this->db->select("sub_category");
        $this->db->where("main_category", $id);
        $query = $this->db->get("product_category");
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result = $row["sub_category"];
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function product($MainCategory, $subCategory, $category){
        $this->db->select("id, shop_product.name, shop_product.item_number");
        $this->db->join("shop_product_in_category", "shop_product.id = shop_product_in_category.product");
        $this->db->where("shop_product_in_category.main_category", $MainCategory);
        $this->db->where("shop_product_in_category.sub_category", $subCategory);
        $this->db->where("shop_product_in_category.category", $category);
        $query = $this->db->get("shop_product");
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $key => $row) {
                $result[$key] = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }
}
?>
