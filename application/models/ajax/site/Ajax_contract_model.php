<?php

class Ajax_Contract_Model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getSelectValues($table){
        $query = $this->db->get_where($table, array("active" =>"1", "deleted" => "0"));
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $result[$row["id"]] = $row["name"];
            }
            return $result;
        }
        else{
            return false;
        }
    }
}
?>
