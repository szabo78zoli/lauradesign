<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Lang_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "AdminLang";

        $this->form_elements = $this->db_edited_fields = $this->db_loaded_fields = array(   "name" => "Name",
                                                                                            "abbreviation" => "Abbreviation",
                                                                                            "image" => "Image",
                                                                                            "active" => "Active");
        $this->db_table = "lang";
        $this->form_view = "lang/admin/lang_edit_view.tpl";
    }
}
?>