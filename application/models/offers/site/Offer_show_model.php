<?php

class Offer_Show_Model extends Site_Base_Model
{
    public function __construct() {
        parent::__construct();


        $this->db_table = "offer";
        $this->db_loaded_fields = array("id",
                                        "name",
                                        "alias",
                                        "description",
                                        "meta_keywords",
                                        "content");
        $this->form_view = "offers/site/offer_view.tpl";
    }

    public function contentLoad($table, $list_fields, $where, $section, $limit){
        $this->db->select($list_fields);
        $this->db->where($where);
        $this->db->limit($section, $limit);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $result = $row;
            }
            return $result;
        }
        else{
            return false;
        }
    }
}
?>
