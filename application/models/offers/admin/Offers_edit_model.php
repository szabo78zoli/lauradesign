<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Offers_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Offer";

        $this->form_elements = $this->db_loaded_fields = $this->db_edited_fields = array(   "name" => "Name",
                                                                                            "alias" => "Alias",
                                                                                            "cost" => "Cost",
                                                                                            "meta_description" => "MetaDescription",
                                                                                            "meta_keywords" => "MetaKeywords",
                                                                                            "description" => "Description",
                                                                                            "content" => "Content",
                                                                                            "active" => "Active");
        $this->db_table = "offer";
        $this->form_view = "offers/admin/offers_edit_view.tpl";
    }


}
?>