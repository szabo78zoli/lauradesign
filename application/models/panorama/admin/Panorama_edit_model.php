<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Panorama_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Panorama";

        $this->form_elements = $this->db_edited_fields = $this->db_loaded_fields = array(   "name" => "Name",
                                                                                            "alias" => "Alias",
                                                                                            "image" => "Image",
                                                                                            "active" => "Active");
        $this->db_table = "panorama";
        $this->form_view = "panorama/admin/panorama_edit_view.tpl";
    }

    public function editorImageLoad($id){
        $this->db->select("image");
        $this->db->where("id", $id);
        $query = $this->db->get("panorama");

        foreach ($query->result_array() as $row) {
            $result = $row["image"];
        }
        return $result;
    }
}
?>