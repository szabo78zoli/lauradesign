<?php

class Panorama_Show_Model extends Site_Base_Model
{
    public function __construct() {
        parent::__construct();


        $this->db_table = "gallery";
        $this->db_loaded_fields = array("id",
                                        "image",
                                        "name");
        $this->form_view = "panorama/site/panorama_view.tpl";
    }

    public function getPanorama(){
        $this->db->select("image, name");
        //$this->db->where("category", $id);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("panorama");
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row) {
                $result[] = $row;
            }

            return $result;
        }
        else{
            return false;
        }
    }

    public function getGalleryCategoryNameId($alias){
        $this->db->select("id, name");
        $this->db->where("alias", $alias);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("gallery_category");
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row) {
                $result = $row;
            }

            return $result;
        }
        else{
            return false;
        }
    }
}
?>
