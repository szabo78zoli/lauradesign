<?php

class Gallery_Show_Model extends Site_Base_Model
{
    public function __construct() {
        parent::__construct();


        $this->db_table = "gallery";
        $this->db_loaded_fields = array("id",
                                        "image");
        $this->form_view = "gallery/site/gallery_view.tpl";
    }

    public function getGallery($id){
        $this->db->select("image, gallery_image.signature");
        $this->db->join("gallery_image", "gallery.id = gallery_image.gallery");
        $this->db->where("category", $id);
        $this->db->where("gallery.active", 1);
        $this->db->where("gallery.deleted", 0);
        $query = $this->db->get("gallery");
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row) {
                $result[] = $row;
            }

            return $result;
        }
        else{
            return false;
        }
    }

    public function getGalleryCategoryNameId($alias){
        $this->db->select("id, name");
        $this->db->where("alias", $alias);
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("gallery_category");
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row) {
                $result = $row;
            }

            return $result;
        }
        else{
            return false;
        }
    }
}
?>
