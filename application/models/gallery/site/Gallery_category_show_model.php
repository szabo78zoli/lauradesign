<?php

class Gallery_Category_Show_Model extends Site_Base_Model
{
    public function __construct() {
        parent::__construct();


        $this->db_table = "product_category";
        $this->db_loaded_fields = array("id",
                                        "name",
                                        "alias",
                                        "image");
        $this->form_view = "gallery/site/gallery_category_view.tpl";
    }

    public function galleryCategoryLoad(){
        $this->db->select("alias, image, name");
        $this->db->where("active", 1);
        $this->db->where("deleted", 0);
        $query = $this->db->get("gallery_category");
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row) {
                $result[] = $row;
            }

            return $result;
        }
        else{
            return false;
        }
    }
}
?>
