<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Gallery_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Gallery";

        $this->form_elements = array("Name","Alias","Category","Image","Description","Active");

        $this->db_edited_fields = $this->db_loaded_fields = array(  "name" => "Name",
                                                                    "alias" => "Alias",
                                                                    "category" => "Category",
                                                                    "active" => "Active");
        $this->db_table = "gallery";
        $this->form_view = "gallery/admin/gallery_edit_view.tpl";
    }

    public function imageGalleryInsert($imageFields, $id){
        $this->db->set("gallery", $id);
        $this->db->insert("gallery_image", $imageFields);
    }

    public function imageGalleryUpdate($imageFields, $id){
        $this->db->where("id", $id);
        $this->db->update("gallery_image", $imageFields);
    }

    public function editorImageGalleryLoad($id){
        $this->db->select("id, image, signature");
        $this->db->where("gallery", $id);
        $query = $this->db->get("gallery_image");

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row){
                $result[$row['id']] = $row;
            }
            return $result;
        }
    }  
}
?>