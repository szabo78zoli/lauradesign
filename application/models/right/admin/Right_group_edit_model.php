<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Right_Group_Edit_Model extends Admin_Base_Model{
    public function __construct(){
        parent::__construct();

        $this->form_name = "AdminRightGroup";

        $this->form_elements = array(   "name" => "Name",
                                        "site_type" => "SiteType",
                                        "modul_uri" => "Right",
                                        "active" => "Active");

        $this->db_edited_fields = $this->db_loaded_fields = array(  "name" => "Name",
                                                                    "site_type" => "SiteType",
                                                                    "active" => "Active");

        $this->db_table = "rights_group";
        $this->form_view = "right/admin/right_group_edit_view.tpl";
    }

    public function editorLoad($table, $editor_fields, $where, $id){

        $this->db->select($editor_fields);
        $this->db->where("rights_group.id", $id);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row){
                $result["site_type"] = $row["site_type"];
                $result["name"] = $row["name"];
                $result["active"] = $row["active"];
                $result["rights"] = $this->rightsLoad($id);
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function rightsLoad($id){
        $this->db->select("rights_right_group.right_id");
        $this->db->where("right_group_id", $id);
        $query = $this->db->get("rights_right_group");

        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $key => $row){
                $result[$row["right_id"]] = $row["right_id"];
            }
            return $result;
        }
        else{
            return false;
        }
    }

    public function editorInsert($table, $fields, $jog=""){

        $this->db->insert($table, $fields);
        $id = $this->db->insert_id();
        if(!empty($jog)) {
            foreach ($jog as $key => $value) {
                $this->db->set("right_id", $value);
                $this->db->set("right_group_id", $id);
                $this->db->insert("rights_right_group");
            }
        }
    }

    public function editorUpdate($table, $data, $where, $id, $jog=""){
        $this->db->where($where, $id);
        $this->db->update($table, $data);

        $this->db->where("right_group_id", $id);
        $this->db->delete("rights_right_group");

        if($jog != "") {
            foreach ($jog as $key => $value) {
                $this->db->set("right_id", $value);
                $this->db->set("right_group_id", $id);
                $this->db->insert("rights_right_group");
            }
        }
    }

    public function getSelectValues($table, $tipus = 0){
        $query = $this->db->get_where($table, array("site_type" => $tipus, "active" =>"1", "deleted" => "0"));
        foreach ($query->result_array() as $row) {
            $result[$row["id"]] = $row["name"];
        }
        if(!empty($result)) {
            return $result;
        }
    }
}
?>