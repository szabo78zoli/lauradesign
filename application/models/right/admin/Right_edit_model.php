<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'models/base/admin/Admin_base_model.php');

class Right_Edit_Model extends Admin_Base_Model
{
    public function __construct() {
        parent::__construct();

        $this->form_name = "AdminRight";

        $this->form_elements = $this->db_edited_fields = $this->db_loaded_fields = array(   "name" => "Name",
                                                                                            "site_type" => "SiteType",
                                                                                            "modul_uri" => "ModulUri",
                                                                                            "active" => "Active");
        $this->db_table = "rights";
        $this->form_view = "right/admin/right_edit_view.tpl";
    }

    public function editorUpdate($table, $data, $where, $id){
        $this->db->where($where, $id);
        $this->db->update($table, $data);
    }
}
?>