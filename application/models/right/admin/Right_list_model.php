<?php

class Right_List_Model extends Admin_Base_Model{
    public function __construct() {
        parent::__construct();

        $this->form_name = "Right";
        $this->order_form_name = "RightOrder";
        $this->form_elements = array("Filter");
        $this->db_table = "rights";
        $this->db_list_fields = "id, name, modul_uri, active";
        $this->db_where = "deleted = 0";
        $this->db_order_by = "id ASC";
        $this->db_group_by = "";
        $this->table_head = array("Id" =>"id", "Név" => "name", "Modul URI" => "modul_uri", "Szerkesztés" => "","Aktív" => "", "Törlés" => "");

        if($this->input->post($this->form_name ."Lenght")!= ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->input->post($this->form_name."Lenght"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->input->post($this->form_name."Lenght"));
        }
        elseif($this->input->post($this->form_name ."Lenght")== "" && $this->session->userdata($this->form_name."Lenght") == ""){
            $this->session->set_userdata($this->form_name."Lenght", $this->session->userdata("defaultListLength"));
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }
        else{
            $this->smarty_tpl->assign($this->form_name."LenghtSelected", $this->session->userdata($this->form_name."Lenght"));
        }

        $this->db_list_limit = $this->session->userdata($this->form_name."Lenght");
        $this->form_view = "right/admin/right_list_view.tpl";
    }
}
?>