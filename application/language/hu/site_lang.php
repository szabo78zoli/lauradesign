<?php
$lang['project_lang'] 		        = "Termékeink";
$lang['see_our_work_lang'] 		    = "Tekintse meg eddigi munkáinkat";
$lang['please_contact_us_lang'] 	= "Lépjen kapcsolatba velünk";
$lang['please_contact_us_2_lang'] 	= "GÉPÉSZ 2000 KFT. Papíripari és egyéb célgépek tervezése, gyártása, utólagos automatizálása, felújítása, átalakítása";
$lang['contacts_lang'] 	            = "Elérhetőségek";
$lang['address_lang'] 	            = "Cím";
$lang['street_number_lang'] 	    = "4220 Hajdúböszörmény,<br>Petőfi Sándor utca 16.";
$lang['email_title_lang'] 	        = "Email";
$lang['phone_title_lang'] 	        = "Telefonszám";
$lang['fax_title_lang'] 	        = "Telefax";
$lang['contact_text_lang'] 	        = "A kapcsolat felvételéhez kérjük, töltse ki az űrlapot!";
$lang['company_title_lang'] 	    = "Cég neve";
$lang['your_name_title_lang'] 	    = "Az Ön neve";
$lang['country_title_lang'] 	    = "Ország";
$lang['message_title_lang'] 	    = "Üzenet";
$lang['send_title_lang'] 	        = "Küldés";
$lang['partners_lang'] 	            = "Partnereink";
$lang['read_more_lang'] 	        = "Olvasson tovább";
$lang['message_error_lang'] 	    = "Az üzenet küldése sikertelen!";
$lang['message_success_lang'] 	    = "Az üzenet küldése sikeres!";
$lang['contact_alias_lang'] 	    = "kapcsolat";
$lang['project_alias_lang'] 	    = "termekeink";
$lang['services_alias_lang'] 	    = "szolgaltatasaink";
$lang['back_lang']           	    = "<- Vissza";
?>