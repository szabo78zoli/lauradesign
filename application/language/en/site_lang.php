<?php
$lang['project_lang'] 		        = "Our products";
$lang['see_our_work_lang'] 		    = "See our works";
$lang['please_contact_us_lang'] 	= "Please contact us";
$lang['please_contact_us_2_lang'] 	= "GÉPÉSZ 2000 LTD. Planning paper-making machines, production, post-automation, renovation, conversion";
$lang['contacts_lang'] 	            = "Contacts";
$lang['address_lang'] 	            = "Address";
$lang['street_number_lang'] 	    = "4220 Hajdúböszörmény,<br>Petőfi Sándor street 16.";
$lang['email_title_lang'] 	        = "Email";
$lang['phone_title_lang'] 	        = "Phone";
$lang['fax_title_lang'] 	        = "Fax";
$lang['contact_text_lang'] 	        = "Please fill the form to contact us!";
$lang['company_title_lang'] 	    = "Company name";
$lang['your_name_title_lang'] 	    = "Your name";
$lang['country_title_lang'] 	    = "Country";
$lang['message_title_lang'] 	    = "Message";
$lang['send_title_lang'] 	        = "Send";
$lang['partners_lang'] 	            = "Our partners";
$lang['read_more_lang'] 	        = "Read more";
$lang['message_error_lang'] 	    = "The message send is unsuccessfully!";
$lang['message_success_lang'] 	    = "The message send is successfully!";
$lang['contact_alias_lang'] 	    = "connect";
?>