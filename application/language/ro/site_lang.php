<?php
$lang['konyvajanlo_lang'] 		= "Könyvajánló";
$lang['esemenyek_lang'] 		= "Események";
$lang['felhasznalonev_lang'] 		= "felhasználónév";
$lang['jelszo_lang']                    = "jelszo";
$lang['bejelentkezes_lang'] 		= "Bejelentkezés";
$lang['regisztracio_lang'] 		= "Regisztráció";
$lang['jelszoemlekezteto_lang'] 	= "Jelszóemlékeztető";
$lang['keres_lang']                     = "Keres";
$lang['eler_lang']                      = "Elérhetőség";
$lang['eler_txt_lang']                  = "4564 Nyírmada, Ady Endre út 9.";
$lang['telefon_lang']                   = "Telefonszám";
$lang['email_lang']                     = "E-mail cím";
$lang['nyitva_lang']                    = "Nyitvatartás";
$lang['hetfo_lang']                     = "Hétfő";
$lang['kedd_lang']                      = "Kedd";
$lang['szerda_lang']                    = "Szerda";
$lang['csutortok_lang']                 = "Csütörtök";
$lang['pentek_lang']                    = "Péntek";
$lang['jog_text_lang']                  = "Minden jog fenntartva © Nyírmada Könyvtár 2013";
$lang['nincs_esemeny_lang']             = "Jelenleg nincsenek események!";
$lang['login_udvozlo_lang']             = "Üdvözöljük oldalunkon";
$lang['login_utolso_bejelentekezs_lang']= "Utolsó bejelentkezés";
$lang['kijelentkezes_lang']             = "Kijelentkezés";
$lang['adataim_lang']                   = "Adataim";
$lang['regisztracio_lang']              = "Regisztráció";
$lang['adatmodositas_lang']             = "Adatmódosítás";
$lang['hirek_lang']                     = "Hírek";
$lang['galeria_lang']                   = "Galéria";
$lang['nincs_kep_galeria_lang']         = "Nincs kép a galériában";
$lang['hasznos_link_lang']              = "Hasznos linkek";
$lang['forum_lang']                     = "Fórum";
$lang['forum_hozzaszolas_lang']         = "Fórum hozzászólások";
$lang['uj_forum_hozzaszolas_lang']      = "Új fórum hozzászólás";
$lang['hozzaszolas_lang']               = "Hozzászólás";
$lang['hozzaszolasok_lang']             = "Hozzászólások";
$lang['valasz_a_hozzaszolasra_lang']    = "Válasz hozzászólásra";
$lang['hozzaszolasok_a_temahoz_lang']   = "Hozzászólás a témához";
$lang['forumtema_bekuldes_lang']        = "Új fórumtéma beküldése";
$lang['forum_kategoriak_lang']          = "Fórum kategóriák";
$lang['uj_forum_tema_lang']             = "Új fórumtéma";
$lang['mentes_lang']                    = "Mentés";
$lang['szavazas_lang']                  = "Szavazás";
$lang['mar_szavazott_lang']             = "Ön már szavazott!";
$lang['szavazashoz_jelentkezz_be_lang'] = "A szavazáshoz kérem jelentkezzen be!";
$lang['hiba_lang']                      = "Hiba!";
$lang['sikeres_lang']                   = "Az adatrögzítés sikeres!";
$lang['sikeres_adatmentes_lang']        = "Sikeres adatmentés!";
$lang['felhasznalonev_lang']            = "Felhasználónév";
$lang['felhasznalonev_text_lang']       = "felhasználónév";
$lang['jelszo_lang']                    = "Jelszó";
$lang['jelszo_megegyszer_lang']         = "Jelszó mégegyszer";
$lang['kapcsolat_nyelve_lang']          = "Kapcsolat nyelve";
$lang['vezeteknev_lang']                = "Vezetéknév";
$lang['keresztnev_lang']                = "Keresztnév";
$lang['szuletesi_datum_lang']           = "Születési dátum";
$lang['nem_lang']                       = "Nem";
$lang['orszag_lang']                    = "Ország";
$lang['iranyitoszam_lang']              = "Irányítószám";
$lang['varos_lang']                     = "Város";
$lang['utca_hazszam_lang']              = "Utca, házszám";
$lang['telefon_lang']                   = "Telefon";
$lang['feliratkozas_hirlevelre_lang']   = "Feliratkozás hírlevélre";
$lang['igen_lang']                      = "Igen";
$lang['nem_lang']                       = "Nem";
$lang['ferfi_lang']                     = "Férfi";
$lang['no_lang']                        = "Nő";
$lang['tovabb_lang']                    = "tovább...";
$lang['vissza_lang']                    = "...vissza";
$lang['elso_lang']                      = "Első";
$lang['utolso_lang']                    = "Utolsó";
$lang['kovetkezo_lang']                 = "Következő";
$lang['elozo_lang']                     = "Előző";
$lang['vendegkonyv_lang']               = "Vendégkönyv";
$lang['uj_vendegkonyv_hozzaszolas_lang']= "Új vendégkönyv hozzászólás";
?>