<section class="background-white text-center about content">
    <div class="container">
        <div class="row align-items-center overflow-hidden">
            <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                <h3 class="ls text-uppercase mt-4 mb-5 color-wblack">Ajándékkértya igénylése</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <h4>Elérhetőségek</h4>
                <div class="Giftcard-item"> <h5>Cím</h5>
                    <p>4026 Debrecen,<br>Jókai utca 33.</p>
                </div>
                <div class="Giftcard-item"> <h5>Email</h5>
                    <p>info@lauradesign.hu</p>
                </div>
                <div class="Giftcard-item"> <h5>Telefonszám</h5>
                    <p> +36 30 960-1944</p>
                </div>
            </div>
            <div class="col-md-7">
                <h4>Az ajándékkártya igényléséhez kérjük, töltse ki az űrlapot!</h4>
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}

                <div class="row">
                    <form action="" method="post" style="width: 100%;">
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$GiftcardLastName.field}" name="{$GiftcardLastName.field}" class="contact-form-element" placeholder="Az Ön vezeték neve">
                                {if {$GiftcardLastName.error}}
                                    <div class="help-block text-danger text-left ml-2">{$GiftcardLastName.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$GiftcardFirstName.field}" name="{$GiftcardFirstName.field}" class="contact-form-element" placeholder="Az Ön kereszt neve">
                                {if {$GiftcardFirstName.error}}
                                    <div class="help-block text-danger text-left ml-2">{$GiftcardFirstName.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$GiftcardPhone.field}" name="{$GiftcardPhone.field}" class="contact-form-element" placeholder="Telefonszám">
                                {if {$GiftcardPhone.error}}
                                    <div class="help-block text-danger text-left ml-2">{$GiftcardPhone.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="email" id="{$GiftcardEmail.field}" name="{$GiftcardEmail.field}" class="contact-form-element" placeholder="Email">
                                {if {$GiftcardEmail.error}}
                                    <div class="help-block text-danger text-left ml-2">{$GiftcardEmail.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$GiftcardCost.field}" name="{$GiftcardCost.field}" class="contact-form-element" placeholder="Összeg">
                                {if {$GiftcardCost.error}}
                                    <div class="help-block text-danger text-left ml-2">{$GiftcardCost.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$GiftcardPcs.field}" name="{$GiftcardPcs.field}" class="contact-form-element" placeholder="Darab">
                                {if {$GiftcardPcs.error}}
                                    <div class="help-block text-danger text-left ml-2">{$GiftcardPcs.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$GiftcardInvoiceAddress.field}" name="{$GiftcardInvoiceAddress.field}" class="contact-form-element" placeholder="Szállítási cím">
                                {if {$GiftcardInvoiceAddress.error}}
                                    <div class="help-block text-danger text-left ml-2">{$GiftcardInvoiceAddress.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$GiftcardDeliveryAddress.field}" name="{$GiftcardDeliveryAddress.field}" class="contact-form-element" placeholder="Számlázási cím">
                                {if {$GiftcardDeliveryAddress.error}}
                                    <div class="help-block text-danger text-left ml-2">{$GiftcardDeliveryAddress.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="webshopButton gift_btn login_btn" name="SaveBtn" value="SaveBtn" style="width: 200px;">Üzenet küldése</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>