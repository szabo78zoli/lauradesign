<section class="content-header">
    <h1>Ajándék kártya lista</h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Főoldal</li>
        <li>Ajándék kártya</li>
        <li class="active">Ajándék kártya lista</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="form-inline">
                        <form action="" name="{$FormName}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>
                                        <select name="{$FormName}Lenght" class="form-control input-sm" onchange="this.form.submit()">
                                            {$listLenght = ";"|explode:$listLenght}
                                            {foreach $listLenght as $elem}
                                                <option value="{$elem}"{if $GiftcardLenghtSelected == $elem} selected=selected {/if} >{$elem}</option>
                                            {/foreach}
                                        </select>
                                        találat oldalanként
                                    </label>
                                </div>
                                <div class="col-sm-6 text-right">
                                    {$FilterForm}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <hr>
                                    <table class="table table-bordered table-striped">
                                        {if isset($error_message)}
                                        <div class="alert alert-warning alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-warning"></i> Figyelem!</h4>
                                            {$error_message}
                                        </div>
                                        {else}
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Név</th>
                                            <th>E-mail</th>
                                            <th>Telefonszám</th>
                                            <th>Aktív</th>
                                            <th>Törlés</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {foreach from=$list key=kulcs item=elem}
                                            <tr role="row">
                                                <td>{$elem.id}</a></td>
                                                <td>{$elem.last_name} {$elem.first_name}</a></td>
                                                <td>{$elem.email}</a></td>
                                                <td>{$elem.phone}</a></td>
                                                <td>
                                                    <div class="btn btn-primary btn-xs toggleBtn">Bővebben</div>
                                                </td>
                                                <td>
                                                    {if $elem.active}
                                                        <button type="submit" name="AktivBtn" class="btn btn-block btn-success btn-xs" value="{$elem.id}" title="Inaktiválás">Aktív</button>
                                                    {else}
                                                        <button type="submit" name="InAktivBtn" class="btn btn-block btn-warning  btn-xs" value="{$elem.id}" title="Aktiválás" >Inaktív</button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <button type="submit" name="DeleteBtn" class="btn btn-danger btn-xs" value="{$elem.id}" title="Aktiválás" onclick="return window.confirm('Biztosan törli a(z) {$elem.name} nyelvet?');" >Törlés</button>
                                                </td>
                                            </tr>
                                            <tr class="toggle" style="display: none;">
                                                <td colspan="3">
                                                    <b>Összeg:</b> {$elem.cost} Ft </br>
                                                    <b>Darab:</b> {$elem.pcs}</br>
                                                </td>
                                                <td colspan="3">
                                                    <b>Szállítási cím:</b> {$elem.delivery_address} </br>
                                                    <b>Számlázási cím:</b> {$elem.invoice_address} </br>
                                                </td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Név</th>
                                            <th>Szerkesztés</th>
                                            <th>E-mail</th>
                                            <th>Telefonszám</th>
                                            <th>Aktív</th>
                                            <th>Törlés</th>
                                        </tr>
                                        </tfoot>
                                        {/if}
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <ul class="pagination">
                                            {$pagination}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
