<!DOCTYPE html>
<html dir="ltr" lang="hu">
<head>
    <base href="{$base_url}">
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Lauradesign"/>
    <meta name="keywords" content="Lauradesign"/>

    <!-- Page Title -->
    <title>www.lauradesign.hu</title>
    <!--link rel="shortcut icon" href="{$base_url}assets/site/images/favicon.ico"-->
    <!-- Favicon and Touch Icons -->

    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:300,400,600">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/jquery.mmenu.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/mmenu/extensions/themes/jquery.mmenu.themes.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/mmenu/extensions/shadows/jquery.mmenu.shadows.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/mmenu/extensions/pagedim/jquery.mmenu.pagedim.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/style.css">
    
    <!-- Javascript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" ></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/jquery.min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/jquery.mmenu.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/wow.min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/site.js"></script>

	<style type="text/css">
		#eucookielaw { display:none }
	</style>    
	<script type="text/javascript">
		function SetCookie(c_name,value,expiredays)
		{
			var exdate=new Date()
			exdate.setDate(exdate.getDate()+expiredays)
			document.cookie=c_name+ "=" +escape(value)+";path=/"+((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
		}
	</script>	
</head>
<body data-spy="scroll" data-target=".inner-link" data-offset="60">
<main>
    <div id="menu_area" class="menu-area">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-light navbar-expand-lg mainmenu">

                    <button id="openMenu" class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div>
                        <img class="logo-img" src="assets/site/images/logo.png"/>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            {$site_menu}
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="flexslider flexslider-simple h-full loading parallax">
        <ul class="slides">
        {if isset($slider)}
            {foreach $slider as $elem}
                <li>
                    <section class="py-0 color-white">
                        <div class="background-holder overlay overlay-slider" style="background-image:url({$base_url}application/upload/slider/{$elem.image});"></div>
                        <div class="container">
                            <div class="row justify-content-start align-items-end pt-11 pb-6 h-full">
                                <div class="col pb-lg-0">
                                    <div class="row align-items-end parallax">
                                        <div class="col-lg">
                                            <div class="owerflow-hidden">
                                                {$elem.description}                            
                                            </div>
                                        </div>
                                        {if isset($elem.link) && !empty($elem.link)}
                                        <div class="col text-lg-right">
                                            <div class="overflow-hidden slideInRight">
                                                <a class="btn btn-sm btn-outline-yellow" href="{$elem.link}">Olvasson tovább...</a>
                                            </div>
                                        </div>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </li>
            {/foreach}
        {/if}
        </ul>
    </div>


    <section class="background-white text-center about">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="row align-items-center overflow-hidden">
                        <div class="col-md-6 col-lg-12 text-md-left text-lg-center wow animate__slideInUp" data-wow-duration="3s" data-wow-delay="0s" data-wow-offset="200">
                            <img class="img-responsive" src="application/upload/content/Laura.png"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 mt-4 mt-lg-0">
                    <div class="row align-items-center overflow-hidden">
                        <div class="col-md-6 col-lg-12 text-md-left text-lg-center wow animate__slideInRight" data-wow-duration="3s" data-wow-delay="0s" data-wow-offset="100">
                            <h4 class="ls text-uppercase mt-4 mb-3">
                                {$about_us.description}
                            </h4>
                            {$about_us.content}
                            <div class="col wow animate__slideInLeft" data-wow-duration="3s" data-wow-delay="0s">
                                <a class="btn btn-sm btn-outline-yellow" href="kapcsolat">Kapcsolatfelvétel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="background-white text-center pricing-table">
        <div class="container">
            <div class="row align-items-center overflow-hidden">
                <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                    <h3 class="ls text-uppercase mt-4 mb-5 color-white">Csomagajánlatok</h3>
                    <h4 class="ls mt-4 mb-5 color-white">Az Ön igényei szerint</h4>
                </div>
            </div>
            <div class="row align-items-center overflow-hidden">
                <div id="Carousel" class="carousel slide col-sm-12">
                    <ol class="carousel-indicators">
                        <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#Carousel" data-slide-to="1"></li>
                        <li data-target="#Carousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        {if isset($offers)}
                            {foreach $offers as $key => $elem}
                                {$kulcs = $key+1}
                                {$result4 = $kulcs % 4}
                                {$result3 = $kulcs % 3}
                                {$result7 = $kulcs % 7}
                                {$result6 = $kulcs % 6}
                                {$result8 = $kulcs % 8}
                                {if ($kulcs == 1 || $result4 == 0 || $result7 == 0) && $result8 != 0}
                                    <div class="item {if $kulcs == 1}active{/if} carousel-item">
                                        <div class="row">
                                {/if}
                                <div class="col-sm-12 col-md-4 col-lg-4">
                                    <div class="pricingtable-wrapper table-option m-b30 " data-wow-duration="3s" data-wow-delay="0s">
                                        <div class="pricingtable-inner">
                                            <div class="pricingtable-title">
                                                <h3 style="font-weight: 600;">{$elem.name}</h3>
                                            </div>
                                            <div class="pricingtable-price">
                                                <span class="pricingtable-bx">{$elem.cost}</span>
                                            </div>
                                            {$elem.description}
                                            <div class="pricingtable-footer">
                                                <a class="btn btn-sm btn-outline-yellow" href="csomagajanlat/{$elem.alias}">Megtekintés</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {if $result3 == 0 || $result6 == 0  || $result8 == 0}
                                    </div>
                                </div>
                                {/if}
                            {/foreach}
                        {/if}
                        <!--.item-->
                    </div>
                    <!--.carousel-inner-->
                    <a data-slide="prev" href="#Carousel" class="left carousel-control">&#x2039;</a>
                    <a data-slide="next" href="#Carousel" class="right carousel-control">&#x203A;</a>
                </div>
                <!--.Carousel-->
            </div>
        </div>
    </section>
    <section class="background-white text-center projects">
        <div class="container">
            <div class="row align-items-center overflow-hidden">
                <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                    <h3 class="ls text-uppercase mt-4 mb-5 color-wblack">Munkáim</h3>
                </div>
            </div>
            <div class="row align-items-center overflow-hidden">
            {if isset($galleryCategories)}
                {foreach $galleryCategories as $key => $elem}
                    {math equation = "x % y" x = $key y=2 assign=paired}
                    {if $paired == 0 }
                        <div class="col-lg-6 mt-4 wow animate__slideInRight"  data-wow-duration="3s" data-wow-delay="0s">
                            <a class="projectImgLink" href="galeria/{$elem.alias}">
                                <img class="projectImg img-fluid" src="application/upload/gallery_category/{$elem.image}" />
                                <div class="projectImgDescription">
                                    {$elem.name}
                                </div>
                            </a>
                        </div>
                    {else}
                        <div class="col-lg-6 mt-4 wow animate__slideInLeft"  data-wow-duration="3s" data-wow-delay="0s">
                            <a class="projectImgLink" href="galeria/{$elem.alias}">
                                <img class="projectImg img-fluid" src="application/upload/gallery_category/{$elem.image}" />
                                <div class="projectImgDescription">
                                    {$elem.name}
                                </div>
                            </a>
                        </div>
                    {/if}
                {/foreach}
            {/if}
            </div>
            <div class="row align-items-center overflow-hidden">
                <div class="col-12 mt-5 text-sm-center text-md-center text-lg-center align-items-center wow animate__slideInLeft" data-wow-duration="3s" data-wow-delay="0s">
                    <a class="btn btn-sm btn-outline-yellow" href="galeria">Tovább a galériához</a>
                </div>
            </div>
        </div>
    </section>
    <section class="text-center footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 wow animate__zoomIn" data-wow-duration="1s" data-wow-delay="0s">
                    <img class="logo-img" src="assets/site/images/logo_invert.png"/><br>
                    <a href="https://www.facebook.com/lauradesignshu" class="social facebook" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://www.instagram.com/liszterlaura/" class="social instagram" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://hu.pinterest.com/laurardesigns/_saved/" class="social pinterest" target="_blank">
                        <i class="fa fa-pinterest"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCOr1LPMdoyJHhu2eVGMUGDQ" class="social youtube" target="_blank">
                        <i class="fa fa-youtube"></i>
                    </a>
                </div>
                <div class="col-lg-3 wow animate__zoomIn" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row align-items-center overflow-hidden">
                        <div class="col-md-6 col-lg-12 text-md-left text-lg-left">
                            <h5 class="title text-uppercase">Hívjon</h5>
                            <p>
                                <span class="color-white fs-0 fw-600">Telefon:</span>
                                <span class="color-white fs-0 fw-300">+36 30 960-1944</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 wow animate__zoomIn" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row align-items-center overflow-hidden">
                        <div class="col-md-6 col-lg-12 text-md-left text-lg-left">
                            <h5 class="title text-uppercase">Írjon</h5>
                            <p>
                                <span class="color-white fs-0 fw-600">E-mail:</span>
                                <span class="color-white fs-0 fw-300">info@lauradesign.hu</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 last wow animate__zoomIn" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row align-items-center overflow-hidden">
                        <div class="col-md-6 col-lg-12 text-md-left text-lg-left">
                            <div class="widget">
                                <h5 class="title text-uppercase">Címünk</h5>
                                <p>
                                    <span class="color-white fs-0 fw-300">
                                        4026 Debrecen, Jókai u. 33.
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="background-primary text-center py-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col">
                    <p>Minden jog fenntartva © Laura Design 2020</p>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- MMENU -->
<nav id="my-menu">
    <ul>
        {$site_mmenu}
    </ul>
</nav><!-- /#menu -->
<div id="mm-blocker" class="mm-slideout"></div>
<script>
    $(document).ready(function() {
        $("#my-menu").mmenu({
            // Options
            extensions: ["pagedim-black", "theme-dark", "shadow-page", "shadow-panels"]

        });
        var API = $("#my-menu").data( "mmenu" );

        $("#openMenu").click(function() {
            API.open();
        });
    });
</script>
</body>
</html>