<!DOCTYPE html>
<html dir="ltr" lang="hu">
<head>
    <base href="{$base_url}">
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Lauradesign"/>
    <meta name="keywords" content="Lauradesign"/>

    <!-- Page Title -->
    <title>www.lauradesign.hu</title>
    <!--link rel="shortcut icon" href="{$base_url}assets/site/images/favicon.ico"-->
    <!-- Favicon and Touch Icons -->

    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:300,400,600">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/jquery.mmenu.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/mmenu/extensions/themes/jquery.mmenu.themes.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/mmenu/extensions/shadows/jquery.mmenu.shadows.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/mmenu/extensions/pagedim/jquery.mmenu.pagedim.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}assets/site/css/style.css">

    <!-- Javascript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" ></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/jquery.min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/jquery.mmenu.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/wow.min.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/jquery.magnific-popup.js"></script>
    <script type="text/javascript" src="{$base_url}assets/site/js/site.js"></script>

	<style type="text/css">
		#eucookielaw { display:none }
	</style>    
	<script type="text/javascript">
		function SetCookie(c_name,value,expiredays)
		{
			var exdate=new Date()
			exdate.setDate(exdate.getDate()+expiredays)
			document.cookie=c_name+ "=" +escape(value)+";path=/"+((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
		}
	</script>	
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
<main>
    <div id="menu_area" class="menu-area">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-light navbar-expand-lg mainmenu">

                    <button id="openMenu" class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div>
                        <img class="logo-img" src="assets/site/images/logo.png"/>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            {$site_menu}
                    </div>
                </nav>
            </div>
        </div>
    </div>

    {$content}

    <section class="text-center footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 wow animate__zoomIn" data-wow-duration="1s" data-wow-delay="0s">
                    <img class="logo-img" src="assets/site/images/logo_invert.png"/><br>
                    <a href="https://www.facebook.com/lauradesignshu" class="social facebook" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://www.instagram.com/liszterlaura/" class="social instagram" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://hu.pinterest.com/laurardesigns/_saved/" class="social pinterest" target="_blank">
                        <i class="fa fa-pinterest"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCOr1LPMdoyJHhu2eVGMUGDQ" class="social youtube" target="_blank">
                        <i class="fa fa-youtube"></i>
                    </a>
                </div>
                <div class="col-lg-3 wow animate__zoomIn" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row align-items-center overflow-hidden">
                        <div class="col-md-6 col-lg-12 text-md-left text-lg-left">
                            <h5 class="title text-uppercase">Hívjon</h5>
                            <p>
                                <span class="color-white fs-0 fw-600">Telefon:</span>
                                <span class="color-white fs-0 fw-300">+36 30 960-1944</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 wow animate__zoomIn" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row align-items-center overflow-hidden">
                        <div class="col-md-6 col-lg-12 text-md-left text-lg-left">
                            <h5 class="title text-uppercase">Írjon</h5>
                            <p>
                                <span class="color-white fs-0 fw-600">E-mail:</span>
                                <span class="color-white fs-0 fw-300">info@lauradesign.hu</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 last wow animate__zoomIn" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="row align-items-center overflow-hidden">
                        <div class="col-md-6 col-lg-12 text-md-left text-lg-left">
                            <div class="widget">
                                <h5 class="title text-uppercase">Címünk</h5>
                                <p>
                                    <span class="color-white fs-0 fw-300">
                                        4026 Debrecen, Jókai u. 33.
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="background-primary text-center py-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col">
                    <p>Minden jog fenntartva © Laura Design 2020</p>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- MMENU -->
<nav id="my-menu">
    <ul>
        {$site_mmenu}
    </ul>
</nav><!-- /#menu -->
<div id="mm-blocker" class="mm-slideout"></div>
<script>
    $(document).ready(function() {
        $("#my-menu").mmenu({
            // Options
            extensions: ["pagedim-black", "theme-dark", "shadow-page", "shadow-panels"]

        });
        var API = $("#my-menu").data( "mmenu" );

        $("#openMenu").click(function() {
            API.open();
        });
    });
</script>
</body>
</html>