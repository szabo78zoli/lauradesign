<section class="background-white text-center gallery content">
    <div class="container">
        <div class="row align-items-center overflow-hidden">
            <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                <h3 class="ls text-uppercase mt-4 mb-5 color-wblack">{$galleryName}</h3>
            </div>
        </div>
        <div class="row align-items-center overflow-hidden">
        {if isset($gallery) && !empty($gallery)}
            {foreach $gallery as $key => $elem}
                {math equation = "x % y" x = $key y=2 assign=paired}
                {if $paired == 0 }
                <div class="col-lg-4 mt-4  wow animate__slideInRight" data-wow-duration="3s" data-wow-delay="0s" >
                    <a class="gallery-image-link" href="application/upload/gallery/{$elem.image}">
                        <img class="projectImg gallery-image img-fluid" src="application/upload/gallery/{$elem.image}">
                    </a>
                    <div class="projectImgDescription">
                        {$elem.signature}
                    </div>
                </div>
                {else}
                <div class="col-lg-4 mt-4  wow animate__slideInLeft" data-wow-duration="3s" data-wow-delay="0s" >
                    <a class="gallery-image-link" href="application/upload/gallery/{$elem.image}">
                        <img class="projectImg gallery-image img-fluid" src="application/upload/gallery/{$elem.image}">
                    </a>
                    <div class="projectImgDescription">
                        {$elem.signature}
                    </div>
                </div>
                {/if}
            {/foreach}
        {/if}
        </div>
    </div>
</section>