<section class="content-header">
    <h1>{if isset($Modify)}Galéria kategória módosítása{else}Új galéria kategória{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Galéria kategória</a></li>
        <li class="active">{if isset($Modify)}Galéria kategória módosítása{else}Új galéria kategória{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {if {$GalleryCategoryName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control converted-charachters-source" id="{$GalleryCategoryName.field}" placeholder="Név" name="{$GalleryCategoryName.field}" value="{$GalleryCategoryName.postdata}">
                            {if {$GalleryCategoryName.error}}
                                <span class="help-block">{$GalleryCategoryName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$GalleryCategoryAlias.error}} has-error {/if}">
                            <label>Alias</label>
                            <input type="text" class="form-control converted-charachters-destination" id="{$GalleryCategoryAlias.field}" placeholder="Alias" name="{$GalleryCategoryAlias.field}" value="{$GalleryCategoryAlias.postdata}">
                            {if {$GalleryCategoryAlias.error}}
                                <span class="help-block">{$GalleryCategoryAlias.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$GalleryCategoryImage.error}} has-error {/if}">
                            <label>Kép</label><div style="color: #ff0000;" >Feltöltendő kép maximális szélessége: {$MaxImageWidth} px, magassága: {$MaxImageHeight} px</div>
                            <input class="form-control" type="file" name="{$GalleryCategoryImage.field}" placeholder="Kép" value="{$GalleryCategoryImage.postdata}"/>
                            {if {$GalleryCategoryImage.error}}
                                <span class="help-block">{$GalleryCategoryImage.error}</span>
                            {/if}
                        </div>
                        <div class="form-group">
                            {if !empty($Image)}
                                <img src="application/upload/gallery_category/{$Image}" width="296" />
                            {/if}
                        </div>
                        <div class="form-group {if {$GalleryCategoryActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$GalleryCategoryActive.field}" {if {isset($GalleryCategoryActive.postdata) && $GalleryCategoryActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$GalleryCategoryActive.field}" {if {isset($GalleryCategoryActive.postdata) && $GalleryCategoryActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$GalleryCategoryActive.error}}
                                <span class="help-block">{$GalleryCategoryActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>