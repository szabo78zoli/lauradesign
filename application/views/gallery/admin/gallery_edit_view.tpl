<section class="content-header">
    <h1>{if isset($Modify)}Galéria módosítása{else}Új galéria{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Galria</a></li>
        <li class="active">Új galéria</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {if {$GalleryName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control converted-charachters-source" id="{$GalleryName.field}" placeholder="Név" name="{$GalleryName.field}" value="{$GalleryName.postdata}">
                            {if {$GalleryName.error}}
                                <span class="help-block">{$GalleryName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$GalleryCategory.error}} has-error {/if}">
                            <label>Kategória</label>
                            {html_options class="form-control" id=$GalleryCategory.field name=$GalleryCategory.field options=$Category selected=$GalleryCategory.postdata}
                            {if {$GalleryCategory.error}}
                                <span class="help-block">{$GalleryCategory.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$GalleryAlias.error}} has-error {/if}">
                            <label>Alias</label>
                            <input type="text" class="form-control converted-charachters-destination" id="{$GalleryAlias.field}" placeholder="Alias" name="{$GalleryAlias.field}" value="{$GalleryAlias.postdata}">
                            {if {$GalleryAlias.error}}
                                <span class="help-block">{$GalleryAlias.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$GalleryImage.error}} has-error {/if}">
                            <label>Képgaléria feltöltése</label>
                            <input class="form-control" type="file" multiple id="{$GalleryImage.field}" name="{$GalleryImage.field}[]" value="{$GalleryImage.postdata}"/>
                            {if {$GalleryImage.error}}
                                <span class="help-block">{$GalleryImage.error}</span>
                            {/if}
                            <h4>Feltöltendő képek</h4>
                            <div id="imageResult">

                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <hr>
                        <div class="form-group {if {$GalleryActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$GalleryActive.field}" {if {isset($GalleryActive.postdata) && $GalleryActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$GalleryActive.field}" {if {isset($GalleryActive.postdata) && $GalleryActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$GalleryActive.error}}
                                <span class="help-block">{$GalleryActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="form-group imagegallery">
                    <div class="box-body">
                        <h4>Feltöltött képek</h4>
                        {if !empty($ImageGallery)}
                            <div class="row">
                                {foreach $ImageGallery key=kulcs item=$elem}
                                    <div class="col-md-6" style="padding: 3px; text-align: center;">
                                        <img src="application/upload/gallery/{$elem.image}" height="150" />
                                        <input type="hidden" name="GalleryImageSignatureId[{$kulcs}]" value="{$kulcs}" />
                                        <br/>
                                        <input style="margin-top: 5px;" type="text" name="GalleryImageSignature[{$kulcs}]" value="{$elem.signature}" /><br/>
                                        <button class="btn btn-danger btn-xs imageDelete" type="button" name="ImageGalleryDeleteBtn" value="{$elem.id}" onclick="return window.confirm('Biztosan törli aképet?');" >Törlés</button>
                                    </div>
                                {/foreach}
                            </div>
                            <div style="clear: both;"></div>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>