<section class="content-header">
    <h1>Csomagajánlat lista</h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Főoldal</li>
        <li>Csomagajánlat</li>
        <li class="active">Csomagajánlat lista</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="form-inline">
                        <form action="" name="{$FormName}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>
                                        <select name="{$FormName}Lenght" class="form-control input-sm" onchange="this.form.submit()">
                                            {$listLenght = ";"|explode:$listLenght}
                                            {foreach $listLenght as $elem}
                                                <option value="{$elem}"{if $PriceofferLenghtSelected == $elem} selected=selected {/if} >{$elem}</option>
                                            {/foreach}
                                        </select>
                                        találat oldalanként
                                    </label>
                                </div>
                                <div class="col-sm-6 text-right">
                                    {$FilterForm}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <hr>
                                    <table class="table table-bordered table-striped">
                                        {if isset($error_message)}
                                        <div class="alert alert-warning alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-warning"></i> Figyelem!</h4>
                                            {$error_message}
                                        </div>
                                        {else}
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Név</th>
                                            <th>E-mail</th>
                                            <th>Telefonszám</th>
                                            <th>Aktív</th>
                                            <th>Törlés</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {foreach from=$list key=kulcs item=elem}
                                            <tr role="row">
                                                <td>{$elem.id}</a></td>
                                                <td>{$elem.name}</a></td>
                                                <td>{$elem.email}</a></td>
                                                <td>{$elem.phone}</a></td>
                                                <td>
                                                    <div class="btn btn-primary btn-xs toggleBtn">Bővebben</div>
                                                </td>
                                                <td>
                                                    {if $elem.active}
                                                        <button type="submit" name="AktivBtn" class="btn btn-block btn-success btn-xs" value="{$elem.id}" title="Inaktiválás">Aktív</button>
                                                    {else}
                                                        <button type="submit" name="InAktivBtn" class="btn btn-block btn-warning  btn-xs" value="{$elem.id}" title="Aktiválás" >Inaktív</button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <button type="submit" name="DeleteBtn" class="btn btn-danger btn-xs" value="{$elem.id}" title="Aktiválás" onclick="return window.confirm('Biztosan törli a(z) {$elem.name} nyelvet?');" >Törlés</button>
                                                </td>
                                            </tr>
                                            <tr class="toggle" style="display: none;">
                                                <td colspan="2">
                                                    <b>Ingatlan típusa:</b> {if $elem.type == 1} Családi ház {/if}{if $elem.type == 2} Iroda ház, láncház {/if}{if $elem.type == 3} Társasházi lakás {/if} </br>
                                                    <b>Szintek száma:</b> {$elem.level} </br>
                                                    <b>Alapterület:</b> {$elem.area} <span>m</span><span style="ont-size: 12px; position: relative; top: -4px;">2</span> </br>
                                                    <b>Helyiségek:</b> {$elem.place}
                                                    </br>
                                                </td>
                                                <td colspan="2">
                                                    <b>Csomag:</b> {$elem.offername} </br>
                                                    <b>Hol található az ingatlan:</b> {$elem.county}, {$elem.city} </br>
                                                    <b>Mikor kezdődik az építkezés:</b> {if !empty($elem.start_year) && !empty($elem.start_month)} {$elem.start_year}. {$elem.start_month} {else} {$elem.year}. {$elem.month} {/if}</br>
                                                    <b>Dokumentumok:</b> dokumentumok </br>
                                                </td>
                                                <td colspan="2">
                                                    <b>Megjegyzés:</b> {$elem.note} </br>
                                                </td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">ID</th>
                                            <th rowspan="1" colspan="1">Név</th>
                                            <th rowspan="1" colspan="1">Szerkesztés</th>
                                            <th rowspan="1" colspan="1">Aktív</th>
                                            <th rowspan="1" colspan="1">Törlés</th>
                                        </tr>
                                        </tfoot>
                                        {/if}
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <ul class="pagination">
                                            {$pagination}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
