<script type="text/javascript" src="{$base_url}assets/site/js/jquery.validate.js"></script>

<form method="post" action="" id="ticketForm" enctype="multipart/form-data">
    <section class="background-white text-center gallery content ticketForm">
        <div class="container" id="accordion">
            <div class="row align-items-center overflow-hidden">
                <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                    <h3 class="ls text-uppercase mt-4 mb-5 color-wblack">Ajánlat kérése</h3>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseOne">
                        Ingatlan típusa
                    </a>
                </div>
                <div id="collapseOne" class="collapse show" data-parent="#accordion">
                    <div class="type-chooser-container card-body">
                        <div class="col-md-12">
                            <em>Kérem, válasszon az alábbi lehetőségek közül</em>
                        </div>
                        <div class="type-chooser col-md-2 {if isset($TypePostdata) && $TypePostdata == 1}active{/if}" data-id="1">
                            <img src="{$base_url}assets/site/images/house.png" alt="lapos tető" class="img-fluid">
                            Családi ház
                        </div>
                        <div class="type-chooser col-md-2 {if isset($TypePostdata) && $TypePostdata == 2}active{/if}" data-id="2">
                            <img src="{$base_url}assets/site/images/office.png" alt="ferde tető" class="img-fluid">
                            Irodaház, láncház
                        </div>
                        <div class="type-chooser col-md-2 {if isset($TypePostdata) && $TypePostdata == 3}active{/if}" data-id="3">
                            <img src="{$base_url}assets/site/images/detached_house.png" alt="ferde tető" class="img-fluid">
                            Társasházi lakás
                        </div>
                        <input type="hidden" name="Type" value="{if isset($TypePostdata)}{$TypePostdata}{/if}">
                        {if isset($TypeError)}
                            <div class="error-msg">{$TypeError}</div>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseTwo">
                        Szintek száma
                    </a>
                </div>
                <div id="collapseTwo" class="collapse {if isset($CollapseTwoError)} show{/if}" data-parent="#accordion">
                    <div class="type-chooser-container card-body">
                        <input type="text" name="Level" class="datepicker" value="{if isset($LevelPostdata)}{$LevelPostdata}{/if}">
                        {if isset($LevelError)}
                            <div class="error-msg">{$LevelError}</div>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseThree">
                        Alapterület
                    </a>
                </div>
                <div id="collapseThree" class="collapse {if isset($CollapseThreeError)} show{/if}" data-parent="#accordion">
                    <div class="type-chooser-container card-body">
                        <input type="text" name="Area" class="datepicker" value="{if isset($AreaPostdata)}{$AreaPostdata}{/if}"><span>m</span><span style="ont-size: 12px; position: relative; top: -4px;">2</span>
                        {if isset($AreaError)}
                        <div class="error-msg">{$AreaError}</div>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseFour">
                        Helyiségek
                    </a>
                </div>
                <div id="collapseFour" class="collapse {if isset($CollapseFourError)} show{/if}" data-parent="#accordion">
                    <div class="type-chooser-container card-body">
                        <div class="col-md-12">
                            <em>Kérem, válasszon az alábbi lehetőségek közül</em>
                        </div>
                        <div class="place-chooser col-md-2 {if isset($PlacePostdata) && $PlacePostdata == 1}active{/if}" data-id="1">
                            <img src="{$base_url}assets/site/images/kitchen.png" alt="lapos tető" class="img-fluid">
                            <div>Konyha</div>
                        </div>
                        <div class="place-chooser col-md-2" data-id="2" {if isset($PlacePostdata) && $PlacePostdata == 2}active{/if}>
                            <img src="{$base_url}assets/site/images/dinning-room.png" alt="ferde tető" class="img-fluid">
                            <div>Étkező</div>
                        </div>
                        <div class="place-chooser col-md-2" data-id="3" {if isset($PlacePostdata) && $PlacePostdata == 3}active{/if}>
                            <img src="{$base_url}assets/site/images/living-room.png" alt="ferde tető" class="img-fluid">
                            <div>Nappali</div>
                        </div>
                        <div class="place-chooser col-md-2 {if isset($PlacePostdata) && $PlacePostdata == 4}active{/if}" data-id="4">
                            <img src="{$base_url}assets/site/images/childrens-room.png" alt="lapos tető" class="img-fluid">
                            <div>Gyerekszoba</div>
                        </div>
                        <div class="place-chooser col-md-2 {if isset($PlacePostdata) && $PlacePostdata == 5}active{/if}" data-id="5">
                            <img src="{$base_url}assets/site/images/bedroom.png" alt="ferde tető" class="img-fluid">
                            <div>Hálószoba</div>
                        </div>
                        <div class="place-chooser col-md-2 {if isset($PlacePostdata) && $PlacePostdata == 6}active{/if}" data-id="6">
                            <img src="{$base_url}assets/site/images/working-room.png" alt="ferde tető" class="img-fluid">
                            <div>Dolgozó szoba</div>
                        </div>
                        <div class="place-chooser col-md-2 {if isset($PlacePostdata) && $PlacePostdata == 7}active{/if}" data-id="7">
                            <img src="{$base_url}assets/site/images/corridor.png" alt="lapos tető" class="img-fluid">
                            <div>Előtér, folyosó</div>
                        </div>
                        <div class="place-chooser col-md-2 {if isset($PlacePostdata) && $PlacePostdata == 8}active{/if}" data-id="8">
                            <img src="{$base_url}assets/site/images/other-places.png" alt="ferde tető" class="img-fluid">
                            <div>Egyéb helyiségek</div>
                        </div>
                        <input type="hidden" name="Place" value="{if isset($PlacePostdata)}{$PlacePostdata}{/if}">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseFive">
                        Melyik csomag keltette fel az érdeklődését?
                    </a>
                </div>
                <div id="collapseFive" class="collapse {if isset($CollapseFiveError)} show{/if}" data-parent="#accordion">
                    <div class="type-chooser-container card-body">
                        {html_options class="form-control" name=Offer options=$Offers selected=$OfferPostdata}
                        {if isset($OfferError)}
                            <div class="error-msg">{$OfferError}</div>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseSix">
                        Hol található az ingatlan?
                    </a>
                </div>
                <div id="collapseSix" class="collapse {if isset($CollapseSixError)} show{/if}" data-parent="#accordion">
                    <div class="type-chooser-container card-body row">
                        <div class="col-md-6">
                            <label style="text-align: right; width: 100px;">Megye:</label><input type="text" name="County" value="{if isset($CountyPostdata)}{$CountyPostdata}{/if}">
                            {if isset($CountyError)}
                                <div class="error-msg">{$CountyError}</div>
                            {/if}
                        </div>
                        <div class="col-md-6">
                            <label style="text-align: right; width: 100px;">Város:</label><input type="text" name="City" value="{if isset($CityPostdata)}{$CityPostdata}{/if}">
                            {if isset($CityError)}
                                <div class="error-msg">{$CityError}</div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseSeven">
                        Mikor kezdődik az építkezés?
                    </a>
                </div>
                <div id="collapseSeven" class="collapse {if isset($CollapseSevenError)} show{/if}" data-parent="#accordion">
                    <div class="type-chooser-container card-body row">
                        <div class="col-md-12">
                            <label>Kezdés időpontja</label>
                        </div>
                        <div class="col-md-6">
                            <label style="text-align: right; width: 100px;">Év:</label>
                            {html_options class="form-control" name=StartYear options=$Year selected=$StartYearPostdata}
                            {if isset($StartYearError)}
                                <div class="error-msg">{$StartYearError}</div>
                            {/if}
                        </div>
                        <div class="col-md-6">
                            <label style="text-align: right; width: 100px;">Hónap:</label>
                            {html_options class="form-control" name=StartMonth options=$Month selected=$StartMonthPostdata}
                            {if isset($StartMonthError)}
                                <div class="error-msg">{$StartMonthError}</div>
                            {/if}
                        </div>
                        <div class="col-md-12">
                            <label>Ha már elkezdődött</label>
                        </div>
                        <div class="col-md-6">
                            <label style="text-align: right; width: 100px;">Év:</label>
                            {html_options class="form-control" name=Year options=$Year selected=$YearPostdata}
                            {if isset($YearError)}
                                <div class="error-msg">{$YearError}</div>
                            {/if}
                        </div>
                        <div class="col-md-6">
                            <label style="text-align: right; width: 100px;">Hónap:</label>
                            {html_options class="form-control" name=Month options=$Month selected=$MonthPostdata}
                            {if isset($MonthError)}
                                <div class="error-msg">{$MonthError}</div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseEight">
                        Dokumentumok feltöltése
                    </a>
                </div>
                <div id="collapseEight" class="collapse" data-parent="#accordion">
                    <div class="type-chooser-container card-body">
                        <div class="">
                            <label style="text-align: right; width: 150px;">Csatolandó file-ok:</label><input type="file" multiple name="UploadFile[]" style=" border-radius: 0; display: inline-block;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseNine">
                        Elérhetőség
                    </a>
                </div>
                <div id="collapseNine" class="collapse {if isset($CollapseNineError)} show{/if}" data-parent="#accordion">
                    <div class="type-chooser-container card-body row">
                        <div class="col-md-12">
                            <label style="text-align: right; width: 100px;">Név:</label><input style="width: 500px;" type="text" name="Name" value="{if isset($NamePostdata)}{$NamePostdata}{/if}">
                            {if isset($NameError)}
                                <div class="error-msg">{$NameError}</div>
                            {/if}
                        </div>
                        <div class="col-md-12">
                            <label style="text-align: right; width: 100px;">E-mail cím:</label><input style="width: 500px;" type="text" name="Email"  value="{if isset($EmailPostdata)}{$EmailPostdata}{/if}">
                            {if isset($EmailError)}
                                <div class="error-msg">{$EmailError}</div>
                            {/if}
                        </div>
                        <div class="col-md-12">
                            <label style="text-align: right; width: 100px;">Telefonszám:</label><input style="width: 500px;" type="text" name="Phone"  value="{if isset($PhonePostdata)}{$PhonePostdata}{/if}">
                            {if isset($PhoneError)}
                                <div class="error-msg">{$PhoneError}</div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header btn  btn-outline-yellow">
                    <a class="card-link" data-toggle="collapse" href="#collapseTen">
                        Megjegyzés
                    </a>
                </div>
                <div id="collapseTen" class="collapse" data-parent="#accordion">
                    <div class="type-chooser-container card-body row">
                        <div class="col-md-12">
                            <textarea style="width: 900px; height: auto" type="text" name="Note" rows="10">{if isset($NotePostdata)}{$NotePostdata}{/if}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <button type="submit" class="btn btn-send-priceoffer">Ajánlatkérés elküldése</button>
                </div>
            </div>
        </div>
    </section>
</form>




    <script>
			$(function() {
                $(".type-chooser").click(function(){
                    $(".type-chooser").removeClass("active");
                    $(this).addClass("active");
                    var id = $(this).attr('data-id');
                    $('input[name=Type]').val(id);
                });

                $(".place-chooser").click(function(){

                    var id = $(this).attr('data-id');

                    if($(this).hasClass('active')){
                        $(this).removeClass("active");
                        $('input[name=Place]').val($('input[name=Place]').val().replace(id, ""))
                    }
                    else {
                        $(this).addClass("active");
                        $('input[name=Place]').val($('input[name=Place]').val()+id);
                    }
                });
            });
    </script>

<style>

    .toggleDiv{
        border: 1px solid #aaa;
        border-radius: 0 0 17px 17px;
    }

    .btn-napelem {
        color: #444 !important;
        border: 1px solid #52ae31;
        /*background: #52ae31; */
        background: rgba(82, 174, 49, 0.5);
        white-space:normal;
    }

    .radio-group label {
        overflow: hidden;
    }
    .radio-group input {
        /* This is on purpose for accessibility. Using display: hidden is evil.
        This makes things keyboard friendly right out tha box! */
        height: 1px;
        width: 1px;
        position: absolute;
        top: -20px;
    }
    .radio-group .not-active  {
        color: #3276b1;
        background-color: #fff;
    }


    section {
        padding: 10px;
    }
    section>.error-msg {
        visibility:hidden;
        color: red;
        font-weight: bold;
        padding: 5px;
    }
    section.error {
        background: #FFD7D7;
        outline: 3px solid red;
    }
    section.error>.error-msg {
        visibility:visible;
    }

    .type-chooser-container {
        text-align: center;
        -webkit-perspective: 2000px;
        -moz-perspective: 2000px;
        -o-perspective: 2000px;
        perspective: 2000px;
    }
    .type-chooser img, .place-chooser img {
        width: 60%;
    }
    .type-chooser, .place-chooser{
        padding: 20px 10px;
        /*margin:20px;*/
        border: 10px solid #aaa;
        border-radius: 10px;
        opacity: 0.5;
        text-align: center;
        /*width: 200px;*/
        /*height: 240px;*/
        display: inline-block;
        transition: 0.3s ease all;

        -webkit-transition: -webkit-transform 1s;
        -moz-transition: -moz-transform 1s;
        -o-transition: -o-transform 1s;
        transition: transform 1s;
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        -o-transform-style: preserve-3d;
        transform-style: preserve-3d;
        -webkit-transform-origin: center center;
        -moz-transform-origin: center center;
        -o-transform-origin: center center;
        transform-origin: center center;
    }
    .type-chooser:hover, .type-chooser.active, .place-chooser:hover, .place-chooser.active {
        border: 10px solid #52ae31;
        cursor: pointer;
        opacity: 0.8;
    }
    .type-chooser.active, .place-chooser.active {
        background: rgba(82, 174, 49, 0.5);
        -webkit-transform: rotatey( 30deg ) translate(7px, -20px);
        -moz-transform: rotatey( 30deg ) translate(7px, -20px);
        -o-transform: rotatey( 30deg ) translate(7px, -20px);
        transform: rotatey( 30deg ) translate(7px, -20px);
    }





    .rangeslider__fill {
        background: #52ae31;
    }

    output.companyslider-output, output.slider-output, output.slider-powerplant-output {
        display: block;
        font-size: 30px;
        font-weight: bold;
        text-align: center;
        margin: 30px 0;
    }
    output.companyslider-output.active, output.slider-output.active, output.slider-powerplant-output.active {
        text-shadow: 0 0 5px rgba(255,255,255,0);
        -webkit-animation: motionblur 4s ease-in-out;
        animation: motionblur 0.7s ease-in-out;
    }
    @keyframes motionblur {
        0% {
            left: 0;
            text-shadow: 0 0 0 rgba(0,0,0,0);
        }
        50% {
            text-shadow: -5px 0 5px rgba(0,0,0,0.7);
            transform: skewX(-14deg);
        }
        100% {
            left: 0px;
            text-shadow: 0 0 0 rgba(0,0,0,0);
            transform: skewX(0deg);
        }
    }






    .calc-input.error {
        box-shadow: 0px 0px 13px 5px red inset;
    }
    .calc-input::placeholder {
        color:#999;
    }
    .calc-input {
        margin-bottom: 1px;
        outline: none;
        width: 100%;
        padding: 12px 10px;
        border: none;
        background: #444;
        color: #fff;
        transition: 0.5s ease all;
    }
    .calc-input:hover, .calc-input:focus {
        background: #555;
    }
    .calc-input:-webkit-autofill {
        -webkit-box-shadow: 0 0 0 30px #444 inset;
        -webkit-text-fill-color: #fff !important;
    }
    .calc-input:focus {
        -webkit-box-shadow: 0 0 0 30px #fff inset;
        color: #000;
        -webkit-text-fill-color: #000 !important;
    }
    .checkbox label:after,
    .radio label:after {
        content: '';
        display: table;
        clear: both;
    }

    .checkbox .cr,
    .radio .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        float: left;
        margin-right: .5em;
    }

    .radio .cr {
        border-radius: 50%;
    }

    .checkbox .cr .cr-icon,
    .radio .cr .cr-icon {
        position: absolute;
        font-size: .8em;
        line-height: 0;
        top: 50%;
        left: 20%;
    }

    .radio .cr .cr-icon {
        margin-left: 0.04em;
    }

    .checkbox label input[type="checkbox"],
    .radio label input[type="radio"] {
        display: none;
    }

    .checkbox label input[type="checkbox"] + .cr > .cr-icon,
    .radio label input[type="radio"] + .cr > .cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }

    .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
    .radio label input[type="radio"]:checked + .cr > .cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }

    .checkbox label input[type="checkbox"]:disabled + .cr,
    .radio label input[type="radio"]:disabled + .cr {
        opacity: .5;
    }
    .checkbox label {
        user-select: none;
    }

    .btn-send {
        background: #52ae31;
        opacity: 0.7;
        border: none;
        padding: 10px 20px;
        color: #fff;
        cursor: pointer;
        transition: 0.5s ease all;
        outline: none;
    }
    .btn-send:hover {
        opacity: 1;
    }


    /* Large Devices, Wide Screens */
    @media only screen and (max-width : 1200px) {

    }

    @media only screen and (max-width : 1024px) {
        .radio-group label:first-child {
            border-radius: 5px 5px 0px 0px !important;
            left: -1px;
        }
        .radio-group label:last-child {
            border-radius: 0px 0px 5px 5px !important;
        }
        .radio-group label {
            overflow: hidden;
            float: none !important;
            display: block !important;
        }
        .radio-group input {
            height: 1px;
            width: 1px;
            position: absolute;
            top: -20px;
        }
        .radio-group .not-active  {
            color: #3276b1;
            background-color: #fff;
        }
    }

    /* Medium Devices, Desktops */
    @media only screen and (max-width : 992px) {

    }

    /* Small Devices, Tablets */
    @media only screen and (max-width : 768px) {

    }

    /* Extra Small Devices, Phones */

    .type-chooser-container {
        -webkit-perspective: 2800px;
        -moz-perspective: 2800px;
        -o-perspective: 2800px;
        perspective: 2800px;
    }
    .type-chooser, .place-chooser{
        paddig: 20px;
        margin: 12px 20px;
        border: 10px solid #aaa;
        border-radius: 10px;
        opacity: 0.5;
        text-align: center;
    }

    .error-msg {
        color: red;
        font-weight: bold;
        padding: 5px;
    }
</style>