<div class="login-box">
    <div class="login-logo">
        <a href="admin">www.lauradesign.hu</a>
        {if isset($error_message)}
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                {$error_message}
            </div>
        {/if}
    </div>
    <div class="login-box-body">
        <p class="login-box-msg"><img src="{$base_url}assets/img/site-logo.png" width="100"></p>
        <form action="" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="{$AdminLoginEmail.field}" value="{$AdminLoginEmail.postdata}" class="form-control" placeholder="E-mail cím">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="{$AdminLoginPassword.field}" value="{$AdminLoginPassword.postdata}" class="form-control" placeholder="Jelszó">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Belépés</button>
                </div>
            </div>
        </form>
    </div>
</div>