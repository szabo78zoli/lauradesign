<section class="background-white text-center gallery content">
    <div class="container">
        <div class="row align-items-center overflow-hidden">
            <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                <h3 class="ls text-uppercase mt-4 mb-5 color-wblack">Belépés</h3>
            </div>
        </div>

        <div class="row align-items-center overflow-hidden">
            <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                {if isset($login_message) && !empty($login_message)}{$login_message}{/if}
                {if !isset($SiteUserId) || empty($SiteUserId)}
                <form name="user_login_main" id="user_login_main" method="post" action="">
                    <input type="hidden" id="from" name="from" value="kosar">
                    <div align="center">
                        <table>
                            <tbody>
                                {if isset($error_message)}
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4 style="line-height: 0.1;"><i class="icon fa fa-ban"></i> Hiba!</h4>
                                        {$error_message}
                                    </div>
                                {/if}
                                <tr>
                                    <td class="registration_label">Email:</td>
                                    <td><input type="text" id="{$SiteLoginEmail.field}" name="{$SiteLoginEmail.field}" class="registration_input" value="" ></td>
                                </tr>
                                <tr>
                                    <td class="registration_label">Jelszó:</td>
                                    <td><input type="password" id="{$SiteLoginPassword.field}" name="{$SiteLoginPassword.field}" class="registration_input" value="" ></td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <button type="submit" class="webshopButton gift_btn login_btn" name="SaveBtn" value="SaveBtn" style="width: 200px">Belépés</button>
                        <br><br>
                        <a href="elfelejtett_jelszo" class="webshopButton gift_btn forgotten_password_btn">Elfelejtett jelszó</a>
                        <br>
                        <br>
                    </div>
                </form>
                {else}
                {/if}
            </div>
        </div>
    </div>
</section>