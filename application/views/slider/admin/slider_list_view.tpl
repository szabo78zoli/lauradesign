<section class="content-header">
    <h1>Slider lista</h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Főoldal</li>
        <li>Slider</li>
        <li class="active">Slider lista</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="form-inline">
                        <form action="" name="{$FormName}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>
                                        <select name="{$FormName}Lenght" class="form-control input-sm" onchange="this.form.submit()">
                                            {$listLenght = ";"|explode:$listLenght}
                                            {foreach $listLenght as $elem}
                                                <option value="{$elem}"{if $SliderLenghtSelected == $elem} selected=selected {/if} >{$elem}</option>
                                            {/foreach}
                                        </select>
                                        találat oldalanként
                                    </label>
                                </div>
                                <div class="col-sm-6 text-right">
                                    {$FilterForm}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <hr>
                                    <table class="table table-bordered table-striped">
                                        {if isset($error_message)}
                                        <div class="alert alert-warning alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-warning"></i> Figyelem!</h4>
                                            {$error_message}
                                        </div>
                                        {else}
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Név</th>
                                            <th>Kép</th>
                                            <th>Sorrend</th>
                                            <th>Szerkesztés
                                            <th>Aktív</th>
                                            <th>Törlés</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {foreach from=$list key=kulcs item=elem}
                                            <tr role="row">
                                                <td>{$elem.id}</a></td>
                                                <td>{$elem.name}</a></td>
                                                <td><img src="application/upload/slider/{$elem.image}" style = "height: 50px;" /></td>
                                                <td>
                                                    {if isset($elem.first) && $elem.first == 1}
                                                        <button type="submit" name="DownBtn" class="btn btn-block btn-primary btn-xs btn-move" value="{$elem.id}" title="Le"><i class="fa fa-arrow-down"></i></button>
                                                    {elseif isset($elem.last) && $elem.last == 1}
                                                        <button type="submit" name="UpBtn" class="btn btn-block btn-primary btn-xs btn-move" value="{$elem.id}" title="Fel"><i class="fa fa-arrow-up"></i></button>
                                                    {else}
                                                        <button type="submit" name="UpBtn" class="btn btn-block btn-primary btn-xs btn-move" value="{$elem.id}" title="Fel"><i class="fa fa-arrow-up"></i></button>
                                                        <button type="submit" name="DownBtn" class="btn btn-block btn-primary btn-xs btn-move" value="{$elem.id}" title="Le"><i class="fa fa-arrow-down"></i></button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs" href="admin/slideredit/{$elem.id}">Szerkesztés</a>
                                                </td>
                                                <td>
                                                    {if $elem.active}
                                                        <button type="submit" name="AktivBtn" class="btn btn-block btn-success btn-xs" value="{$elem.id}" title="Inaktiválás">Aktív</button>
                                                    {else}
                                                        <button type="submit" name="InAktivBtn" class="btn btn-block btn-warning  btn-xs" value="{$elem.id}" title="Aktiválás" >Inaktív</button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <button type="submit" name="DeleteBtn" class="btn btn-danger btn-xs" value="{$elem.id}" title="Aktiválás" onclick="return window.confirm('Biztosan törli a(z) {$elem.name} slidert?');" >Törlés</button>
                                                </td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Név</th>
                                            <th>Kép</th>
                                            <th>Sorrend</th>
                                            <th>Szerkesztés</th>
                                            <th>Aktív</th>
                                            <th>Törlés</th>
                                        </tr>
                                        </tfoot>
                                        {/if}
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <ul class="pagination">
                                            {$pagination}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>