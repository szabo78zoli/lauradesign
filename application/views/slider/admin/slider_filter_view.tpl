<div class="input_field">
    <div class="col-sm-4">
        <label>Keresés: </label>
        <input class="form-control input-sm" placeholder="Név" aria-controls="search type="search" name="{$SliderFilter.field}" value="{$SliderFilter.postdata}"/>
    </div>
    <div class="col-sm-4">
        <label>Nyelv: </label>
        {html_options class="form-control" name=$SliderLang.field options=$Lang selected=$SliderLang.postdata}
    </div>
    <div class="col-sm-4">
        <input type="submit" name="SearchBtn" value="Keresés" class="filter_submit btn btn-block btn-primary">
        <input type="submit" name="BasicBtn" value="Alaphelyzet" class="filter_submit btn btn-block btn-primary">
    </div>
</div>