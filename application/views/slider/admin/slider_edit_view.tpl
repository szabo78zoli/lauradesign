<section class="content-header">
    <h1>{if isset($Modify)}Slider módosítása{else}Új slider{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Slider</a></li>
        <li class="active">{if isset($Modify)}Slider módosítása{else}Új slider{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {if {$SliderName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control" id="{$SliderName.field}" placeholder="Név" name="{$SliderName.field}" value="{$SliderName.postdata}">
                            {if {$SliderName.error}}
                                <span class="help-block">{$SliderName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$SliderLink.error}} has-error {/if}">
                            <label>Link</label>
                            <input type="text" class="form-control" id="{$SliderLink.field}" placeholder="link" name="{$SliderLink.field}" value="{$SliderLink.postdata}">
                            {if {$SliderLink.error}}
                                <span class="help-block">{$SliderLink.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$SliderBefore.error}} has-error {/if}">
                            <label>Elhelyezés az alábbi slider előtt{if !isset($Modify)}<br/>(Ha nem válasz ki elemet, akkor automatikusan a lista végére kerül a slider elem!){/if}</label>
                            {html_options class="form-control" name=$SliderBefore.field options=$Slider selected=$SliderBefore.postdata}
                            {if {$SliderBefore.error}}
                                <span class="help-block">{$SliderBefore.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$SliderDescription.error}} has-error {/if}">
                            <label>Leírás</label>
                            <textarea class="form-control" id="{$SliderDescription.field}" placeholder="Leírás" name="{$SliderDescription.field}">{$SliderDescription.postdata}</textarea>
                            {if {$SliderDescription.error}}
                                <span class="help-block">{$SliderDescription.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$SliderImage.error}} has-error {/if}">
                            <label>Kép</label><div style="color: #ff0000;" >Feltöltendő kép maximális szélessége: {$MaxImageWidth} px, magassága: {$MaxImageHeight} px</div>
                            <input class="form-control" type="file" name="{$SliderImage.field}" placeholder="Kép" value="{$SliderImage.postdata}"/>
                            {if {$SliderImage.error}}
                                <span class="help-block">{$SliderImage.error}</span>
                            {/if}
                        </div>
                        <div class="form-group">
                            {if !empty($Image)}
                                <img src="application/upload/slider/{$Image}" width="296" />
                            {/if}
                        </div>
                        <div class="form-group {if {$SliderActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$SliderActive.field}" {if {isset($SliderActive.postdata) && $SliderActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$SliderActive.field}" {if {isset($SliderActive.postdata) && $SliderActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$SliderActive.error}}
                                <span class="help-block">{$SliderActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>