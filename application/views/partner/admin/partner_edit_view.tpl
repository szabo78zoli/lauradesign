<section class="content-header">
    <h1>{if isset($Modify)}Partner módosítása{else}Új partner{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Partner</a></li>
        <li class="active">{if isset($Modify)}Partner módosítása{else}Új partner{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {if {$PartnerName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control" id="{$PartnerName.field}" placeholder="Név" name="{$PartnerName.field}" value="{$PartnerName.postdata}">
                            {if {$PartnerName.error}}
                                <span class="help-block">{$PartnerName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$PartnerLink.error}} has-error {/if}">
                            <label>Link</label>
                            <input type="text" class="form-control" id="{$PartnerLink.field}" placeholder="Link" name="{$PartnerLink.field}" value="{$PartnerLink.postdata}">
                            {if {$PartnerLink.error}}
                                <span class="help-block">{$PartnerLink.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$PartnerImage.error}} has-error {/if}">
                            <label>Profilkép</label>
                            <input class="form-control" type="file" name="{$PartnerImage.field}" placeholder="Kép" value="{$PartnerImage.postdata}"/>
                            {if {$PartnerImage.error}}
                                <span class="help-block">{$PartnerImage.error}</span>
                            {/if}
                        </div>
                        <div class="form-group">
                            {if !empty($PartnerImage.postdata)}
                                <img src="application/upload/partner/{$PartnerImage.postdata}" width="120" />
                            {/if}
                        </div>                        
                        <div class="form-group {if {$PartnerActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$PartnerActive.field}" {if {isset($PartnerActive.postdata) && $PartnerActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$PartnerActive.field}" {if {isset($PartnerActive.postdata) && $PartnerActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$PartnerActive.error}}
                                <span class="help-block">{$PartnerActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>