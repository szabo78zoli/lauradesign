{if isset($error_message)}
    <div class="err">
        <div class="err_icon"></div>
        <a class="close">x</a>
        <div class="desc">
            <span>Hiba!</span>
            <p>{$error_message}</p>
        </div>
    </div>
{/if}
{if isset($success_message)}
    <div class="succes">
        <div class="succes_icon"></div>
        <a class="close">x</a>
        <div class="desc">
            <span>Sikeres adatmentés!</span>
            <p>{$success_message}</p>
        </div>
    </div>
{/if}
<form name="{$FormName}" method=post action="" enctype="multipart/form-data">        
    <div class="admin_edit_title">
        <h2>Tartalom</h2>
    </div>
    <span class="admin_edit_list_img">
        <a href="admin/tartalomlist">
            <img src="{$admin_domain}system/application/img/admin_img/attributes.png" />
        </a>
    </span>      
    <fieldset>        
        <div class="input_field">
            <label>Cím *</label><input class="mediumfield" type="text" name="{$AdminTartalomCim.field}" value="{$AdminTartalomCim.postdata}"/>    
            {if {$AdminTartalomCim.error}}
                <div class="validate_error">{$AdminTartalomCim.error}</div>
            {/if}
        </div>
        <div class="input_field">
            <label>Nyelv *</label>{html_options class="dropdown" name=$AdminTartalomNyelv.field options=$nyelv selected=$AdminTartalomNyelv.postdata}  
            {if {$AdminTartalomNyelv.error}}
                <div class="validate_error">{$AdminTartalomNyelv.error}</div>
            {/if}
        </div>
        <div class="input_field">
            <label>Link *</label><input class="mediumfield" type="text" name="{$AdminTartalomLink.field}" value="{$AdminTartalomLink.postdata}"/>  
            {if {$AdminTartalomLink.error}}
                <div class="validate_error">{$AdminTartalomLink.error}</div> 
            {/if}
        </div>
        <div class="input_field">
            <label>Leírás *</label><textarea class="textarea" cols="20" rows="5" id="{$AdminTartalomLeiras.field}" name="{$AdminTartalomLeiras.field}">{$AdminTartalomLeiras.postdata}</textarea>            
            {if {$AdminTartalomLeiras.error}}
                <div class="validate_error">{$AdminTartalomLeiras.error}</div> 
            {/if}
        </div> 
        <div class="input_field">
            <label>Kulcsszavak *</label><textarea class="textarea" cols="20" rows="5" id="{$AdminTartalomKulcsszo.field}" name="{$AdminTartalomKulcsszo.field}">{$AdminTartalomKulcsszo.postdata}</textarea>            
            {if {$AdminTartalomKulcsszo.error}}
                <div class="validate_error">{$AdminTartalomKulcsszo.error}</div> 
            {/if}
        </div>         
        <div class="input_field">
            <label>Jogcsoport *</label>{html_options class="dropdown" name=$AdminTartalomJogcsoport.field options=$jogosultsag_csoport selected=$AdminTartalomJogcsoport.postdata}  
            {if {$AdminTartalomJogcsoport.error}}
                <div class="validate_error">{$AdminTartalomJogcsoport.error}</div> 
            {/if}
        </div>   
        <div class="input_field">
            <label>Tatrtalom *</label><textarea class="textarea tinymce" cols="20" rows="20" id="{$AdminTartalomTartalom.field}" name="{$AdminTartalomTartalom.field}">{$AdminTartalomTartalom.postdata}</textarea>            
            {if {$AdminTartalomTartalom.error}}
                <div class="validate_error">{$AdminTartalomTartalom.error}</div> 
            {/if}
        </div>  
        <div class="input_field">
            <label>Kapcsolódó tartalmak</label>{html_options style="width *300px;" multiple = "true" class="dropdown" name="AdminTartalomKapcsolodoTartalom[]" options=$tartalmak selected=$AdminTartalomKapcsolodoTartalom.postdata}                                                  
            {if {$AdminTartalomKapcsolodoTartalom.error}}
                <div class="validate_error">{$AdminTartalomKapcsolodoTartalom.error}</div>
            {/if}
        </div> 
        <div class="input_field">
            <label>Kapcsolódó hírek</label>{html_options style="width *300px;" multiple = "true" class="dropdown" name="AdminTartalomKapcsolodoHir[]" options=$hirek selected=$AdminTartalomKapcsolodoHir.postdata}                                                  
            {if {$AdminTartalomKapcsolodoHir.error}}
                <div class="validate_error">{$AdminTartalomKapcsolodoHir.error}</div>
            {/if}
        </div>      
        <div class="input_field">
            <label>Kapcsolódó galériák</label>{html_options style="width *300px;" multiple = "true" class="dropdown" name="AdminTartalomKapcsolodoGaleria[]" options=$galeriak selected=$AdminTartalomKapcsolodoGaleria.postdata}                                                  
            {if {$AdminTartalomKapcsolodoGaleria.error}}
                <div class="validate_error">{$AdminTartalomKapcsolodoGaleria.error}</div>
            {/if}
        </div>        
        <div class="input_field">
            <label>Aktív *</label>
            <div class="column">
                {html_radios class="radio" name=$AdminTartalomAktiv.field options=$aktiv selected=$AdminTartalomAktiv.postdata}
            </div></br></br></br></br>
            {if {$AdminTartalomAktiv.error}}
                <div class="validate_error">{$AdminTartalomAktiv.error}</div>
            {/if} 
        </div>     
        <div class="input_field no_margin_bottom">
            <input type="submit" name="SaveBtn" value="Mentés" class="submit"> 
            <input type="reset" name="ResetBtn" value="Törlés" class="submit"> 
        </div>
    </fieldset>
</form>       