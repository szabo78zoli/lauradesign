<div class="input_field">
    <label>Felhasználó: </label>
    <input class="form-control input-sm" placeholder="Felhasználó" aria-controls="search type="search" name="{$EventLogFilter.field}" value="{$EventLogFilter.postdata}"/>
    <label>Oldal típus:</label>
    {html_options class="form-control" name=$EventLogType.field options=$type selected=$EventLogType.postdata}
    <label>Dátum-tól: </label>
    <input class="form-control input-sm start_date" placeholder="Dátum-tól" aria-controls="search type="search" name="{$EventLogStartDate.field}" value="{$EventLogStartDate.postdata}"/>
    <label>Dátum-ig: </label>
    <input class="form-control input-sm end_date" placeholder="Dátum-ig" aria-controls="search type="search" name="{$EventLogEndDate.field}" value="{$EventLogEndDate.postdata}"/>
    <input type="submit" name="SearchBtn" value="Keresés" class="filter_submit btn btn-block btn-primary">
    <input type="submit" name="BasicBtn" value="Alaphelyzet" class="filter_submit btn btn-block btn-primary">
</div>