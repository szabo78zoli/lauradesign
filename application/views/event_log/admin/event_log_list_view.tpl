<section class="content-header">
    <h1>Esemény napló lista</h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Főoldal</li>
        <li>Esemény napló</li>
        <li class="active">Esemény napló lista</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="form-inline">
                        <form action="" name="{$FormName}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>
                                        <select name="{$FormName}Lenght" class="form-control input-sm" onchange="this.form.submit()">
                                            {$listLenght = ";"|explode:$listLenght}
                                            {foreach $listLenght as $elem}
                                                <option value="{$elem}"{if $EventLogLenghtSelected == $elem} selected=selected {/if} >{$elem}</option>
                                            {/foreach}
                                        </select>
                                        találat oldalanként
                                    </label>
                                </div>
                                <div class="col-sm-10 text-right">
                                    {$FilterForm}
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-sm-12">
                                <hr>
                                <table class="table table-bordered table-striped">
                                    {if isset($error_message)}
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-warning"></i> Figyelem!</h4>
                                        {$error_message}
                                    </div>
                                    {else}
                                    <thead>
                                    <tr>
                                        <form action="" name="{$OrderFormName}" method="post" enctype="multipart/form-data">
                                            {foreach from=$table_head key=kulcs item=elem}
                                                <th>{$kulcs}
                                                    {if !empty($elem)}
                                                        <button class="order_btn" type="submit" name="OrderBtn" value="{$elem}">
                                                            {if isset($EventLogOrderValue) && !empty($EventLogOrderValue)}
                                                                {foreach from=$EventLogOrderValue key=kulcs item=value}
                                                                    {if {$kulcs} == $elem}
                                                                        {if $value == "ASC"}
                                                                            <i class="glyphicon glyphicon-sort-by-attributes-alt"></i>
                                                                        {/if}
                                                                        {if $value == "DESC"}
                                                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                                                        {/if}
                                                                    {else}
                                                                        <i class="glyphicon glyphicon-sort" style="opacity: 0.3;"></i>
                                                                    {/if}
                                                                {/foreach}
                                                            {else}
                                                                <i class="glyphicon glyphicon-sort" style="opacity: 0.3;"></i>
                                                            {/if}
                                                        </button>
                                                    {/if}
                                                </th>
                                            {/foreach}
                                        </form>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$list key=kulcs item=elem}
                                        <tr role="row">
                                            <td>{$elem.id}</a></td>
                                            <td>{$elem.user}</a></td>
                                            <td>{$elem.type}</td>
                                            <td>{$elem.module}</td>
                                            <td>{$elem.event}</td>
                                            <td>{$elem.ip}</td>
                                            <td>{$elem.create_date}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Felhasználó</th>
                                        <th>Felület</th>
                                        <th>Modul</th>
                                        <th>Esemény</th>
                                        <th>IP cím</th>
                                        <th>Dátum</th>
                                    </tr>
                                    </tfoot>
                                    {/if}
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <ul class="pagination">
                                        {$pagination}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
