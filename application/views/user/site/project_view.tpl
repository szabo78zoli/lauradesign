{if isset($siteUser)}
<section class="background-white text-center gallery content">
    <div class="container">
        <div class="row align-items-center overflow-hidden">
            <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                <h3 class="ls text-uppercase mt-4 mb-5 color-wblack">Projektek</h3>
            </div>
        </div>
        <div class="row align-items-center overflow-hidden">
            <div class="col-md-12">
                <ul class="nav nav-tabs background-white" id="projectTab" role="projecttablist">
                    {foreach from=$projects key=kulcs item=elem}
                        <li class="nav-item">
                            <a class="nav-link {if $elem@first} active {/if}" id="{$elem.alias}-tab" data-toggle="tab" href="#{$elem.alias}" role="tab" aria-controls="{$elem.alias}" aria-selected="true">{$elem.name}</a>
                        </li>
                    {/foreach}
                </ul>
            </div>
        </div>
        <div class="tab-content background-white" id="projectTabContent">
            {foreach from=$projects key=kulcs item=elem}
            <div class="tab-pane fade{if $elem@first} show active {/if}" id="{$elem.alias}" role="projecttabpanel" aria-labelledby="{$elem.alias}-tab">
                <ul class="nav nav-tabs" id="versionTab" role="versiontablist">
                    {foreach from=$elem.version key=kulcs2 item=version}
                        <li class="nav-item">
                            <a class="gallery-link nav-link {if $version@first} active {/if}" id="{$version.alias}-tab" data-toggle="tab" href="#{$version.alias}" role="tab" aria-controls="{$version.alias}" aria-selected="true">{$version.name}</a>
                        </li>
                    {/foreach}
                </ul>
                <div class="tab-content" id="versionTabContent">
                    {foreach from=$elem.version key=kulcs2 item=version}
                    <div class="tab-pane gallery background-white fade{if $version@first} show active {/if}" id="{$version.alias}" role="versiontabpanel" aria-labelledby="{$version.alias}-tab">
                        <div class="box-body pad background-white">
                            <div>
                                <div class="row">
                                    {if isset($version.images) && !empty($version.images)}
                                        {foreach from=$version.images key=kulcs3 item=image}
                                            <div class="col-md-4 text-center">
                                                <a class="gallery-image-link" href="application/upload/project_gallery/{$image.image}">
                                                    <img class="img-responsive gallery-image" src="application/upload/project_gallery/{$image.image}"/>
                                                </a>
                                            </div>
                                        {/foreach}
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
            {/foreach}
        </div>
</div>
</section>
{/if}

