<section class="content-header">
    <h1>Felhasználó lista</h1>
    <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Ügyfelek</a></li>
        <li class="active">Felhasználó lista</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="form-inline">
                        <form action="" name="{$FormName}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>
                                        <select name="{$FormName}Lenght" class="form-control input-sm" onchange="this.form.submit()">
                                            {$listLenght = ";"|explode:$listLenght}
                                            {foreach $listLenght as $elem}
                                                <option value="{$elem}"{if $UserSiteAdminLenghtSelected == $elem} selected=selected {/if} >{$elem}</option>
                                            {/foreach}
                                        </select>
                                        találat oldalanként
                                    </label>
                                </div>
                                <div class="col-sm-9 text-right">
                                    {$FilterForm}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr>
                                    {if !empty($pagination)}
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="text-center">
                                                <ul class="pagination">
                                                    {$pagination}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    {/if}
                                    <table style="display: none; background: #ffffff;" class="table table-bordered table-striped allways_on_top">
                                        <thead>
                                            <tr>
                                                <th class="aot-th" id="aot-th-1">ID</th>
                                                <th class="aot-th" id="aot-th-2">Cégnév</th>
                                                <th class="aot-th" id="aot-th-3">Vezetéknév</th>
                                                <th class="aot-th" id="aot-th-4">Keresztnév</th>
                                                <th class="aot-th" id="aot-th-5">Email</th>
                                                <th class="aot-th" id="aot-th-12">Szerkesztés</th>
                                                <th class="aot-th" id="aot-th-13">Aktív</th>
                                                <th class="aot-th" id="aot-th-14">Törlés</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <table class="table table-bordered table-striped">
                                        <thead class="table-visible">
                                            <tr>
                                                <th class="visible-th" id="visible-th-1">ID</th>
                                                <th class="visible-th" id="visible-th-2">Cégnév</th>
                                                <th class="visible-th" id="visible-th-3">Vezetéknév</th>
                                                <th class="visible-th" id="visible-th-4">Keresztnév</th>
                                                <th class="visible-th" id="visible-th-5">Email</th>
                                                <th class="visible-th" id="visible-th-12">Szerkesztés</th>
                                                <th class="visible-th" id="visible-th-13">Aktív</th>
                                                <th class="visible-th" id="visible-th-14">Törlés</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {foreach from=$list key=kulcs item=elem}
                                            <tr role="row">
                                                <td>{$elem.id}</a></td>
                                                <td>{$elem.company}</a></td>
                                                <td>{$elem.first_name}</a></td>
                                                <td>{$elem.last_name}</a></td>
                                                <td>{$elem.email}</td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs" href="admin/usersiteedit/{$elem.id}">Szerkesztés</a>
                                                </td>
                                                <td>
                                                    {if $elem.active}
                                                        <button type="submit" name="AktivBtn" class="btn btn-success btn-xs" value="{$elem.id}" title="Inaktiválás">Aktív</button>
                                                    {else}
                                                        <button type="submit" name="InAktivBtn" class="btn btn-warning btn-xs" value="{$elem.id}" title="Aktiválás" >Inaktív</button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <button type="submit" name="DeleteBtn" class="btn btn-danger btn-xs" value="{$elem.id}" title="Aktiválás" onclick="return window.confirm('Biztosan törli a(z) {$elem.last_name} {$elem.first_name} felhasználót?');" >Törlés</button>
                                                </td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Cégnév</th>
                                            <th>Vezetéknév</th>
                                            <th>Keresztnév</th>
                                            <th>Email</th>
                                            <th>Szerkesztés</th>
                                            <th>Aktív</th>
                                            <th>Törlés</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <ul class="pagination">
                                            {$pagination}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
