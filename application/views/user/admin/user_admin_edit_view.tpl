<section class="content-header">
    <h1>Új admin felhasználó</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Felhasználók</a></li>
        <li class="active">Új admin felhasználó csoport</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {if {$UserAdminName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control" id="{$UserAdminName.field}" placeholder="Név" name="{$UserAdminName.field}" value="{$UserAdminName.postdata}">
                            {if {$UserAdminName.error}}
                                <span class="help-block">{$UserAdminName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$UserAdminEmail.error}} has-error {/if}">
                            <label>Email</label>
                            <input type="text" class="form-control" id="{$UserAdminEmail.field}" placeholder="Email" name="{$UserAdminEmail.field}" value="{$UserAdminEmail.postdata}">
                            {if {$UserAdminEmail.error}}
                                <span class="help-block">{$UserAdminEmail.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$UserAdminPassword.error}} has-error {/if}">
                            <label>Jelszó</label>
                            <input type="password" class="form-control" id="{$UserAdminPassword.field}" placeholder="Jelszó" name="{$UserAdminPassword.field}" value="{$UserAdminPassword.postdata}">
                            {if {$UserAdminPassword.error}}
                                <span class="help-block">{$UserAdminPassword.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$UserAdminPasswordAgain.error}} has-error {/if}">
                            <label>Jelszó újra</label>
                            <input type="password" class="form-control" id="{$UserAdminPasswordAgain.field}" placeholder="Jelszó újra" name="{$UserAdminPasswordAgain.field}" value="{$UserAdminPasswordAgain.postdata}">
                            {if {$UserAdminPasswordAgain.error}}
                                <span class="help-block">{$UserAdminPasswordAgain.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$UserAdminRightsGroup.error}} has-error {/if}">
                            <label>Jogosultság csoport</label>
                            {html_options class="form-control" name=$UserAdminRightsGroup.field options=$RightsGroup selected=$UserAdminRightsGroup.postdata}
                            {if {$UserAdminRightsGroup.error}}
                                <span class="help-block">{$UserAdminRightsGroup.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Profilkép</label>
                                    <input type="file" name="UserAdminImage" value="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                {if isset($getUserAdminImage)}
                                    <div class="form-group">
                                        <img src="application/upload/user_admin/{$getUserAdminImage}">
                                    </div>
                                {/if}
                            </div>
                        </div>
                        <div class="form-group {if {$UserAdminActive.error}} has-error {/if}">
                            <label>Aktív felhasználó: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$UserAdminActive.field}" {if {isset($UserAdminActive.postdata) && $UserAdminActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$UserAdminActive.field}" {if {isset($UserAdminActive.postdata) && $UserAdminActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$UserAdminActive.error}}
                                <span class="help-block">{$UserAdminActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>