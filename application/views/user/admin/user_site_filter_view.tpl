<div class="input_field">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-12">
                <label class="control-label col-sm-3" style="padding-left: 0px; padding-right: 0px; margin-top: 0px; margin-bottom: 15px;">Keresés: </label>
                <div class="col-sm-9" style="padding-left: 0px; padding-right: 0px; margin-bottom: 15px;">
                    <input class="form-control" style="width: 75%; margin: 3px 0;" placeholder="Név" aria-controls="search type="search" name="{$UserSiteAdminFilter.field}" value="{$UserSiteAdminFilter.postdata}"/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
        </div>
    </div>
    <div class="col-sm-12">
        <input type="submit" name="ExportCsvBtn" value="CSV" class="filter_submit btn btn-block btn-primary">
        <input type="submit" name="SearchBtn" value="Keresés" class="filter_submit btn btn-block btn-primary">
        <input type="submit" name="BasicBtn" value="Alaphelyzet" class="filter_submit btn btn-block btn-primary">
    </div>
</div>