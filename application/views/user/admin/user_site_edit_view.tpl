<section class="content-header">
    <h1>Új ügyfél</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Ügyfelek</a></li>
        <li class="active">Új ügyfél</li>
    </ol>
</section>
<section class="content">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
                <a class="product_tab" href="#basicparams" data-toggle="tab">Alapadatok</a>
                <input type="hidden" name="basicparams" value="1"/>
            </li>
            {foreach from=$projects key=kulcs item=elem}
                <li class="">
                    <a class="product_tab" href="#{$elem.id}" data-toggle="tab">{$elem.name}</a>
                    <input type="hidden" name="{$elem.id}" value="1"/>
                </li>
            {/foreach}
        </ul>
        <div class="tab-content">
            <div class="active tab-pane" id="basicparams">
                <div class="box-body pad">
                    <div>
                        <hr>
                        <div class="row">
                            <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                                <div class="col-md-6">
                                    <div class="box box-primary">
                                        {if isset($error_message)}
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                                                {$error_message}
                                            </div>
                                        {/if}
                                        {if isset($success_message)}
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                                                {$success_message}
                                            </div>
                                        {/if}
                                        <div class="box-body">
                                            <div class="form-group {if {$UserSiteFirstName.error}} has-error {/if}">
                                                <label>Vezetéknév</label>
                                                <input type="text" class="form-control" id="{$UserSiteFirstName.field}" placeholder="Keresztnév" name="{$UserSiteFirstName.field}" value="{$UserSiteFirstName.postdata}">
                                                {if {$UserSiteFirstName.error}}
                                                    <span class="help-block">{$UserSiteFirstName.error}</span>
                                                {/if}
                                            </div>
                                            <div class="form-group {if {$UserSiteLastName.error}} has-error {/if}">
                                                <label>Keresztnév</label>
                                                <input type="text" class="form-control" id="{$UserSiteLastName.field}"
                                                       placeholder="Vezetékév" name="{$UserSiteLastName.field}"
                                                       value="{$UserSiteLastName.postdata}">
                                                {if {$UserSiteLastName.error}}
                                                    <span class="help-block">{$UserSiteLastName.error}</span>
                                                {/if}
                                            </div>
                                            <div class="form-group {if {$UserSiteCompany.error}} has-error {/if}">
                                                <label>Cégnév</label>
                                                <input type="text" class="form-control" id="{$UserSiteCompany.field}"
                                                       placeholder="Cégnév" name="{$UserSiteCompany.field}"
                                                       value="{$UserSiteCompany.postdata}">
                                                {if {$UserSiteCompany.error}}
                                                    <span class="help-block">{$UserSiteCompany.error}</span>
                                                {/if}
                                            </div>
                                            <div class="form-group {if {$UserSitePhone.error}} has-error {/if}">
                                                <label>Telefonszám</label>
                                                <input type="text" class="form-control" id="{$UserSitePhone.field}"
                                                       placeholder="Telefonszám" name="{$UserSitePhone.field}"
                                                       value="{$UserSitePhone.postdata}">
                                                {if {$UserSitePhone.error}}
                                                    <span class="help-block">{$UserSitePhone.error}</span>
                                                {/if}
                                            </div>
                                            <div class="form-group {if {$UserSiteEmail.error}} has-error {/if}">
                                                <label>Email</label>
                                                <input type="text" class="form-control" id="{$UserSiteEmail.field}"
                                                       placeholder="Email" name="{$UserSiteEmail.field}"
                                                       value="{$UserSiteEmail.postdata}">
                                                {if {$UserSiteEmail.error}}
                                                    <span class="help-block">{$UserSiteEmail.error}</span>
                                                {/if}
                                            </div>
                                            <div class="form-group {if {$UserSiteEmailAgain.error}} has-error {/if}">
                                                <label>Email ismét</label>
                                                <input type="text" class="form-control" id="{$UserSiteEmailAgain.field}"
                                                       placeholder="Email ismét" name="{$UserSiteEmailAgain.field}"
                                                       value="{$UserSiteEmail.postdata}">
                                                {if {$UserSiteEmailAgain.error}}
                                                    <span class="help-block">{$UserSiteEmailAgain.error}</span>
                                                {/if}
                                            </div>
                                            <div class="form-group {if {$UserSitePassword.error}} has-error {/if}">
                                                <label>Jelszó</label>
                                                <input type="password" class="form-control"
                                                       id="{$UserSitePassword.field}"
                                                       placeholder="Jelszó" name="{$UserSitePassword.field}"
                                                       value="{$UserSitePassword.postdata}">
                                                {if {$UserSitePassword.error}}
                                                    <span class="help-block">{$UserSitePassword.error}</span>
                                                {/if}
                                            </div>
                                            <div class="form-group {if {$UserSitePasswordAgain.error}} has-error {/if}">
                                                <label>Jelszó ismét</label>
                                                <input type="password" class="form-control"
                                                       id="{$UserSitePasswordAgain.field}" placeholder="Jelszó újra"
                                                       name="{$UserSitePasswordAgain.field}"
                                                       value="{$UserSitePasswordAgain.postdata}">
                                                {if {$UserSitePasswordAgain.error}}
                                                    <span class="help-block">{$UserSitePasswordAgain.error}</span>
                                                {/if}
                                            </div>
                                            <div class="form-group {if {$UserSiteActive.error}} has-error {/if}">
                                                <label>Aktív felhasználó: </label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="{$UserSiteActive.field}"
                                                           {if {isset($UserSiteActive.postdata) && $UserSiteActive.postdata == 1}}checked{/if}
                                                           value="1"> igen
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="{$UserSiteActive.field}"
                                                           {if {isset($UserSiteActive.postdata) && $UserSiteActive.postdata == 0}}checked{/if}
                                                           value="0"> nem
                                                </label>
                                                {if {$UserSiteActive.error}}
                                                    <span class="help-block">{$UserSiteActive.error}</span>
                                                {/if}
                                            </div>
                                            <div class="row">
                                                {if isset($Modify)}
                                                    <div class="col-xs-4">
                                                        <button type="button" class="btn btn-primary btn-block btn-flat"
                                                                data-toggle="modal" data-target="#projectModal">Új
                                                            projekt
                                                        </button>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <button type="button" class="btn btn-primary btn-block btn-flat"
                                                                data-toggle="modal" data-target="#versionModal">Új
                                                            verzió
                                                        </button>
                                                    </div>
                                                {/if}
                                                <div class="col-xs-4">
                                                    <button type="submit" class="btn btn-primary btn-block btn-flat"
                                                            name="SaveBtn" value="SaveBtn">Mentés
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                                {if isset($Modify)}
                                    <div class="col-md-6">
                                        <div class="box box-primary">
                                            <div class="box-body">
                                                <div class="form-group{if {$UserSiteProjectSelect.error}} has-error {/if}">
                                                    <label>Projekt</label>
                                                    <div class="form-controll">
                                                        {html_options class="form-control" name=$UserSiteProjectSelect.field options=$SelectProjects selected=$UserSiteProjectSelect.postdata}
                                                        {if {$UserSiteProjectSelect.error}}
                                                            <span class="help-block">{$UserSiteProjectSelect.error}</span>
                                                        {/if}
                                                    </div>
                                                </div>
                                                <div class="form-group{if {$UserSiteVersionSelect.error}} has-error {/if}">
                                                    <label>Verzió</label>
                                                    <div class="form-controll">
                                                        {html_options class="form-control" name=$UserSiteVersionSelect.field options=$SelectVersions selected=$UserSiteVersionSelect.postdata}
                                                        {if {$UserSiteVersionSelect.error}}
                                                            <span class="help-block">{$UserSiteVersionSelect.error}</span>
                                                        {/if}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Képgaléria feltöltése</label>
                                                    <input class="form-control" type="file" multiple
                                                           id="UserSiteProjectGallery" name="UserSiteProjectGallery[]"
                                                           value=""/>

                                                    <h4>Feltöltendő képek</h4>
                                                    <div id="imageResult">

                                                    </div>
                                                    <div style="clear:both;"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <button type="submit" class="btn btn-primary btn-block btn-flat"
                                                                name="SaveImageBtn" value="SaveImageBtn">Kép mentése
                                                            projekthez
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {foreach from=$projects key=kulcs item=elem}
                <div class="tab-pane" id="{$elem.id}">
                    <div class="box-body pad">
                        <div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-primary">
                                        {if isset($elem.version) && !empty($elem.version)}
                                            <ul class="nav nav-tabs">
                                                {foreach from=$elem.version key=kulcs2 item=version}
                                                    <li class=""><a class="product_version_tab"
                                                                    href="#{$elem.id}{$version.id}"
                                                                    data-toggle="tab">{$version.name}</a><input
                                                                type="hidden" name="{$elem.id}{$version.id}"
                                                                value="1"/></li>
                                                {/foreach}
                                            </ul>
                                            <div class="tab-content">
                                                {foreach from=$elem.version key=kulcs2 item=version}
                                                    <div class="tab-pane" id="{$elem.id}{$version.id}">
                                                        <div class="box-body pad">
                                                            <div>
                                                                <hr>
                                                                <div class="row">
                                                                    {if isset($version.images) && !empty($version.images)}
                                                                        {foreach from=$version.images key=kulcs3 item=image}
                                                                            <div class="col-md-3 text-center">
                                                                                <img class="img-responsive"
                                                                                     src="application/upload/project_gallery/{$image.image}"/><br>
                                                                                <button class="btn btn-danger btn-xs projectImageDelete"
                                                                                        type="button"
                                                                                        name="ProjectImageGalleryDeleteBtn"
                                                                                        value="{$image.id}"
                                                                                        onclick="return window.confirm('Biztosan törli aképet?');">
                                                                                    Törlés
                                                                                </button>
                                                                            </div>
                                                                        {/foreach}
                                                                    {/if}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {/foreach}
                                            </div>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</section>

{if isset($Modify)}
    <div class="modal fade" id="projectModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Új project hozzáadása</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Project név</label>
                                    <input type="text" class="form-control converted-charachters-source2" id="UserSiteProject"
                                           placeholder="Project név" name="UserSiteProject">
                                    <input type="hidden" class="converted-charachters-destination2" id="UserSiteProjectAlias"
                                           name="UserSiteProjectAlias">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveProjectBtn"
                                        value="SaveProjectBtn">Mentés
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="versionModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Új verzió hozzáadása</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group {if {$UserSitePassword.error}} has-error {/if}">
                                    <label>Verzió</label>
                                    {html_options class="form-control" name=UserSiteSelectProjects options=$SelectProjects selected=0}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Project név</label>
                                    <input type="text" class="form-control converted-charachters-source2" id="UserSiteProjectVersion"
                                           placeholder="Project verzió" name="UserSiteProjectVersion">
                                    <input type="hidden" class="converted-charachters-destination2" id="UserSiteProjectVersionAlias"
                                           name="UserSiteProjectVersionAlias">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat"
                                        name="SaveProjectVersionBtn" value="SaveProjectVersionBtn">Mentés
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{/if}