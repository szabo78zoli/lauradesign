<section class="content-header">
    <h1>{if isset($Modify)}Nyelv módosítása{else}Új nyelv{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Nyelv</a></li>
        <li class="active">{if isset($Modify)}Nyelv módosítása{else}Új nyelv{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {if {$AdminLangName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control" id="{$AdminLangName.field}" placeholder="Név" name="{$AdminLangName.field}" value="{$AdminLangName.postdata}">
                            {if {$AdminLangName.error}}
                                <span class="help-block">{$AdminLangName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$AdminLangAbbreviation.error}} has-error {/if}">
                            <label>Rövidítés</label>
                            <input type="text" class="form-control" id="{$AdminLangAbbreviation.field}" placeholder="Rövidítés" name="{$AdminLangAbbreviation.field}" value="{$AdminLangAbbreviation.postdata}">
                            {if {$AdminLangAbbreviation.error}}
                                <span class="help-block">{$AdminLangAbbreviation.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$AdminLangImage.error}} has-error {/if}">
                            <label>Kép</label>
                            <input class="form-control" type="file" name="{$AdminLangImage.field}" placeholder="Kép" value="{$AdminLangImage.postdata}"/>
                            {if {$AdminLangImage.error}}
                                <span class="help-block">{$AdminLangImage.error}</span>
                            {/if}
                        </div>
                        <div class="form-group">
                            {if !empty($AdminLangImage.postdata)}
                                <img src="application/upload/lang/{$AdminLangImage.postdata}" width="120" />
                            {/if}
                        </div>
                        <div class="form-group {if {$AdminLangActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$AdminLangActive.field}" {if {isset($AdminLangActive.postdata) && $AdminLangActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$AdminLangActive.field}" {if {isset($AdminLangActive.postdata) && $AdminLangActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$AdminLangActive.error}}
                                <span class="help-block">{$AdminLangActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>