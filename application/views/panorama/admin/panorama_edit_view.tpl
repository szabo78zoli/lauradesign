<section class="content-header">
    <h1>{if isset($Modify)}3D panoráma módosítása{else}Új 3D panoráma{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">3D panoráma</a></li>
        <li class="active">{if isset($Modify)}3D panoráma módosítása{else}Új 3D panoráma{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group {if {$PanoramaName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control converted-charachters-source" id="{$PanoramaName.field}" placeholder="Név" name="{$PanoramaName.field}" value="{$PanoramaName.postdata}">
                            {if {$PanoramaName.error}}
                                <span class="help-block">{$PanoramaName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$PanoramaAlias.error}} has-error {/if}">
                            <label>Alias</label>
                            <input type="text" class="form-control converted-charachters-destination" id="{$PanoramaAlias.field}" placeholder="Alias" name="{$PanoramaAlias.field}" value="{$PanoramaAlias.postdata}">
                            {if {$PanoramaAlias.error}}
                                <span class="help-block">{$PanoramaAlias.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$PanoramaImage.error}} has-error {/if}">
                            <label>Kép</label><div style="color: #ff0000;" >Feltöltendő kép maximális szélessége: {$MaxImageWidth} px, magassága: {$MaxImageHeight} px</div>
                            <input class="form-control" type="file" name="{$PanoramaImage.field}" placeholder="Kép" value="{$PanoramaImage.postdata}"/>
                            {if {$PanoramaImage.error}}
                                <span class="help-block">{$PanoramaImage.error}</span>
                            {/if}
                        </div>
                        <div class="form-group">
                            {if !empty($Image)}
                                <img src="application/upload/panorama/{$Image}" width="296" />
                            {/if}
                        </div>
                        <div class="form-group {if {$PanoramaActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$PanoramaActive.field}" {if {isset($PanoramaActive.postdata) && $PanoramaActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$PanoramaActive.field}" {if {isset($PanoramaActive.postdata) && $PanoramaActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$PanoramaActive.error}}
                                <span class="help-block">{$PanoramaActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>