<div class="input_field">
    <div class="col-sm-6">
        <label>Keresés: </label>
        <input class="form-control input-sm" placeholder="Név" aria-controls="search type="search" name="{$PanoradaFilter.field}" value="{$PanoradaFilter.postdata}"/>
    </div>
    <div class="col-sm-6">
        <input type="submit" name="SearchBtn" value="Keresés" class="filter_submit btn btn-block btn-primary">
        <input type="submit" name="BasicBtn" value="Alaphelyzet" class="filter_submit btn btn-block btn-primary">
    </div>
</div>