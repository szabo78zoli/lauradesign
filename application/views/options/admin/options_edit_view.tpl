<section class="content-header">
    <h1>{if isset($Modify)}Rendszerparaméter módosítása{else}Új rendszerparaméter{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Rendszerparaméter</a></li>
        <li class="active">{if isset($Modify)}Rendszerparaméter módosítása{else}Új rendszerparaméter{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}">
                    <div class="box-body">
                        <div class="form-group {if {$AdminOptionsParameter.error}} has-error {/if}">
                            <label>Paraméter</label>
                            <input type="text" class="form-control" id="{$AdminOptionsParameter.field}" placeholder="Paraméter" name="{$AdminOptionsParameter.field}" value="{$AdminOptionsParameter.postdata}">
                            {if {$AdminOptionsParameter.error}}
                                <span class="help-block">{$AdminOptionsParameter.error}</span>
                            {/if}
                        </div>
                        
                        <div class="form-group {if {$AdminOptionsValue.error}} has-error {/if}">
                            <label>Érték</label>
                            <input type="text" class="form-control" id="{$AdminOptionsValue.field}" placeholder="Érték" name="{$AdminOptionsValue.field}" value="{$AdminOptionsValue.postdata}">
                            {if {$AdminOptionsValue.error}}
                                <span class="help-block">{$AdminOptionsValue.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>