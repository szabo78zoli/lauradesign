<section class="content-header">
    <h1>{if isset($Modify)}Site menü módosítása{else}Új site menü{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Site menü</a></li>
        <li class="active">Új site menü</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Menü</label>
                            <select class="form-control select2 select2-hidden-accessible" name="SiteMenuParent" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                <option value="0" selected >Főmenü</option>
                                {$menuSelect}
                            </select>
                        </div>
                        <div class="form-group {if {$SiteMenuName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control" id="{$SiteMenuName.field}" placeholder="Név" name="{$SiteMenuName.field}" value="{$SiteMenuName.postdata}">
                            {if {$SiteMenuName.error}}
                                <span class="help-block">{$SiteMenuName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$SiteMenuAlias.error}} has-error {/if}">
                            <label>Alias</label>
                            <input type="text" class="form-control" id="{$SiteMenuAlias.field}" placeholder="Alias" name="{$SiteMenuAlias.field}" value="{$SiteMenuAlias.postdata}">
                            {if {$SiteMenuAlias.error}}
                                <span class="help-block">{$SiteMenuAlias.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$SiteMenuLang.error}} has-error {/if}">
                            <label>Nyelv</label>
                            {html_options class="form-control" name=$SiteMenuLang.field options=$lang selected=$SiteMenuLang.postdata}
                            {if {$SiteMenuLang.error}}
                                <span class="help-block">{$SiteMenuLang.error}</span>
                            {/if}
                        </div>
                        <div class="form-group">
                            <label>Ikon</label>
                            <div class="input-group">
                                <input data-placement="bottomRight" class="form-control icp icp-auto" id="{$SiteMenuIcon.field}" name="{$SiteMenuIcon.field}" value="{$SiteMenuIcon.postdata}" type="text" />
                                <span class="input-group-addon"></span>
                            </div>
                        </div>
                        <div class="form-group {if {$SiteMenuActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$SiteMenuActive.field}" {if {isset($SiteMenuActive.postdata) && $SiteMenuActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$SiteMenuActive.field}" {if {isset($SiteMenuActive.postdata) && $SiteMenuActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$SiteMenuActive.error}}
                                <span class="help-block">{$SiteMenuActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>