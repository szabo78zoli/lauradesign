<section class="content-header">
    <h1>{if isset($Modify)}Admin menü módosítása{else}Új admin menü{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Admin menü</a></li>
        <li class="active">Új admin menü</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Menü</label>
                            <select class="form-control select2 select2-hidden-accessible" name="AdminMenuParent" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                <option value="0" selected >Főmenü</option>
                                {$menuSelect}
                            </select>
                        </div>
                        <div class="form-group {if {$AdminMenuName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control" id="{$AdminMenuName.field}" placeholder="Név" name="{$AdminMenuName.field}" value="{$AdminMenuName.postdata}">
                            {if {$AdminMenuName.error}}
                                <span class="help-block">{$AdminMenuName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$AdminMenuAlias.error}} has-error {/if}">
                            <label>Alias</label>
                            <input type="text" class="form-control" id="{$AdminMenuAlias.field}" placeholder="Alias" name="{$AdminMenuAlias.field}" value="{$AdminMenuAlias.postdata}">
                            {if {$AdminMenuAlias.error}}
                                <span class="help-block">{$AdminMenuAlias.error}</span>
                            {/if}
                        </div>
                        <div class="form-group">
                            <label>Ikon</label>
                            <div class="input-group">
                                <input data-placement="bottomRight" class="form-control icp icp-auto" id="{$AdminMenuIcon.field}" name="{$AdminMenuIcon.field}" value="{$AdminMenuIcon.postdata}" type="text" />
                                <span class="input-group-addon"></span>
                            </div>
                        </div>
                        <div class="form-group {if {$AdminMenuActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$AdminMenuActive.field}" {if {isset($AdminMenuActive.postdata) && $AdminMenuActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$AdminMenuActive.field}" {if {isset($AdminMenuActive.postdata) && $AdminMenuActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$AdminMenuActive.error}}
                                <span class="help-block">{$AdminMenuActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>