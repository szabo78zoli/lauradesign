<section class="background-white text-center about content">
    <div class="container">
        <div class="row align-items-center overflow-hidden">
            <div class="col-12 text-sm-center text-md-center text-lg-center align-items-center">
                <h3 class="ls text-uppercase mt-4 mb-5 color-wblack">Lépjen kapcsolatba velünk</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <h4>Elérhetőségek</h4>
                <div class="contact-item"> <h5>Cím</h5>
                    <p>4026 Debrecen,<br>Jókai utca 33.</p>
                </div>
                <div class="contact-item"> <h5>Email</h5>
                    <p>info@lauradesign.hu</p>
                </div>
                <div class="contact-item"> <h5>Telefonszám</h5>
                    <p> +36 30 960-1944</p>
                </div>
            </div>
            <div class="col-md-7">
                <h4>A kapcsolat felvételéhez kérjük, töltse ki az űrlapot!</h4>
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}

                <div class="row">
                    <form action="" method="post" style="width: 100%;">
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$ContactName.field}" name="{$ContactName.field}" class="contact-form-element" placeholder="Az Ön neve">
                                {if {$ContactName.error}}
                                    <div class="help-block text-danger text-left ml-2">{$ContactName.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$ContactAddress.field}" name="{$ContactAddress.field}" class="contact-form-element" placeholder="Cím">
                                {if {$ContactAddress.error}}
                                    <div class="help-block text-danger text-left ml-2">{$ContactAddress.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="email" id="{$ContactEmail.field}" name="{$ContactEmail.field}" class="contact-form-element" placeholder="Email">
                                {if {$ContactEmail.error}}
                                    <div class="help-block text-danger text-left ml-2">{$ContactEmail.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <input type="text" id="{$ContactPhone.field}" name="{$ContactPhone.field}" class="contact-form-element" placeholder="Telefonszám">
                                {if {$ContactPhone.error}}
                                    <div class="help-block text-danger text-left ml-2">{$ContactPhone.error}</div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-form">
                                <textarea name="{$ContactMessage.field}" id="{$ContactMessage.field}" class="contact-form-element" rows="10" placeholder="Üzenet"></textarea>
                                {if {$ContactMessage.error}}
                                    <div class="help-block text-danger text-left ml-2">{$ContactMessage.error}</div>
                                {/if}
                            </div>
                            <button type="submit" class="webshopButton gift_btn login_btn" name="SaveBtn" value="SaveBtn" style="width: 200px;">Üzenet küldése</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>