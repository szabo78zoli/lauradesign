<section class="content-header">
    <h1>Tartalom kategória lista</h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Főoldal</li>
        <li>Tartalom</li>
        <li class="active">Tartalom kategória lista</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="form-inline">
                        <form action="" name="{$FormName}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-6 text-right">
                                    {$FilterForm}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr>
                                    <table class="table table-bordered table-striped">
                                        {if isset($error_message)}
                                            <div class="alert alert-warning alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <h4><i class="icon fa fa-warning"></i> Figyelem!</h4>
                                                {$error_message}
                                            </div>
                                        {else}
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Név</th>
                                            <th>Link</th>
                                            <th>Sorrend</th>
                                            <th>Szerkesztés</th>
                                            <th>Aktív</th>
                                            <th>Törlés</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {foreach from=$list key=kulcs item=elem}
                                            <tr role="row">
                                                <td>{$elem.id}</a></td>
                                                <td>{for $i = 1 to $elem.level}----{/for}{$elem.name}</a></td>
                                                <td>{$elem.alias}</td>
                                                <td>
                                                    {if $elem.first != 1}
                                                    <button type="submit" name="UpBtn" class="btn btn-block btn-primary btn-xs btn-move" value="{$elem.id}" title="Fel"><i class="fa fa-arrow-up"></i></button>
                                                    {/if}
                                                    {if $elem.last != 1}
                                                    <button type="submit" name="DownBtn" class="btn btn-block btn-primary btn-xs btn-move" value="{$elem.id}" title="Le"><i class="fa fa-arrow-down"></i></button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs" href="admin/adminmenuedit/{$elem.id}">Szerkesztés</a>
                                                </td>
                                                <td>
                                                    {if $elem.active}
                                                        <button type="submit" name="AktivBtn" class="btn btn-block btn-success btn-xs" value="{$elem.id}" title="Inaktiválás">Aktív</button>
                                                    {else}
                                                        <button type="submit" name="InAktivBtn" class="btn btn-block btn-warning  btn-xs" value="{$elem.id}" title="Aktiválás" >Inaktív</button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <button type="submit" name="DeleteBtn" class="btn btn-danger btn-xs" value="{$elem.id}" title="Aktiválás" onclick="return window.confirm('Biztosan törli a(z) {$elem.name} admin menüt?');" >Törlés</button>
                                                </td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">ID</th>
                                            <th rowspan="1" colspan="1">Név</th>
                                            <th rowspan="1" colspan="1">Link</th>
                                            <th rowspan="1" colspan="1">Sorrend</th>
                                            <th rowspan="1" colspan="1">Szerkesztés</th>
                                            <th rowspan="1" colspan="1">Aktív</th>
                                            <th rowspan="1" colspan="1">Törlés</th>
                                        </tr>
                                        </tfoot>
                                        {/if}
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <ul class="pagination">
                                            {$pagination}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
