<section class="content-header">
    <h1>{if isset($Modify)}Tartalom módosítása{else} Új tartalom{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Tartalom</a></li>
        <li class="active">{if isset($Modify)}Tartalom módosítása{else} Új tartalom{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
                <div class="box-body pad">
                    <div>
                        <hr>
                        {if isset($error_message)}
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                                {$error_message}
                            </div>
                        {/if}
                        {if isset($success_message)}
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                                {$success_message}
                            </div>
                        {/if}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="box box-primary">
                                    <form role="form" action="" method="post">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {if {$ContentName.error}} has-error {/if}">
                                                    <label>Név</label>
                                                    <input type="text" class="form-control converted-charachters-source" id="{$ContentName.field}" placeholder="Név" name="{$ContentName.field}" value="{$ContentName.postdata}">
                                                    {if {$ContentName.error}}
                                                        <span class="help-block">{$ContentName.error}</span>
                                                    {/if}
                                                </div>
                                                <div class="form-group{if {$ContentAlias.error}} has-error {/if}">
                                                    <label>Alias</label>
                                                    <input type="text" class="form-control converted-charachters-destination" id="{$ContentAlias.field}" placeholder="Alias" name="{$ContentAlias.field}" value="{$ContentAlias.postdata}">
                                                    {if {$ContentAlias.error}}
                                                        <span class="help-block">{$ContentAlias.error}</span>
                                                    {/if}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group{if {$ContentMetaDescription.error}} has-error {/if}">
                                                    <label>Meta leírás</label>
                                                    <textarea class="form-control" id="{$ContentMetaDescription.field}" placeholder="Meta leírás" name="{$ContentMetaDescription.field}">{$ContentMetaDescription.postdata}</textarea>
                                                    {if {$ContentMetaDescription.error}}
                                                        <span class="help-block">{$ContentMetaDescription.error}</span>
                                                    {/if}
                                                </div>
                                                <div class="form-group{if {$ContentMetaKeyword.error}} has-error {/if}">
                                                    <label>Meta kulcsszavak</label>
                                                    <textarea class="form-control" id="{$ContentMetaKeyword.field}" placeholder="Meta kulcsszavak" name="{$ContentMetaKeyword.field}">{$ContentMetaKeyword.postdata}</textarea>
                                                    {if {$ContentMetaKeyword.error}}
                                                        <span class="help-block">{$ContentMetaKeyword.error}</span>
                                                    {/if}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group{if {$ContentShortDescription.error}} has-error {/if}">
                                            <label>Bevezető szöveg</label>
                                            <textarea class="form-control" id="{$ContentShortDescription.field}" placeholder="Bevezető szöveg" name="{$ContentShortDescription.field}">{$ContentShortDescription.postdata}</textarea>
                                            {if {$ContentShortDescription.error}}
                                                <span class="help-block">{$ContentShortDescription.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$ContentDescription.error}} has-error {/if}">
                                            <label>Leírás</label>
                                            <textarea class="form-control" id="{$ContentDescription.field}" placeholder="Leírás" name="{$ContentDescription.field}">{$ContentDescription.postdata}</textarea>
                                            {if {$ContentDescription.error}}
                                                <span class="help-block">{$ContentDescription.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$ContentContent.error}} has-error {/if}">
                                            <label>Tartalom</label>
                                            <textarea class="form-control" id="{$ContentContent.field}" placeholder="Leírás" name="{$ContentContent.field}">{$ContentContent.postdata}</textarea>
                                            {if {$ContentContent.error}}
                                                <span class="help-block">{$ContentContent.error}</span>
                                            {/if}
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8"></div>
                                            <div class="col-xs-4">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box box-primary">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-8"></div>
                                            <div class="col-xs-4">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                                            </div>
                                        </div>
                                        <div class="form-group{if {$ContentLang.error}} has-error {/if}">
                                            <label>Nyelv</label>
                                            {html_options class="form-control" name=$ContentLang.field options=$lang selected=$ContentLang.postdata}
                                            {if {$ContentLang.error}}
                                                <span class="help-block">{$ContentLang.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group {if {$ContentStartDate.error}} has-error {/if}">
                                            <label>Megjelenés kezdeti dátuma</label>
                                            <input type="text" class="form-control start_date" id="{$ContentStartDate.field}" placeholder="Megjelenés kezdeti dátuma" name="{$ContentStartDate.field}" value="{if $ContentStartDate.postdata != '0000-00-00 00:00:00'}{$ContentStartDate.postdata}{/if}">
                                            {if {$ContentStartDate.error}}
                                                <span class="help-block">{$ContentStartDate.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$ContentEndDate.error}} has-error {/if}">
                                            <label>Megjelenés végső dátuma</label>
                                            <input type="text" class="form-control end_date" id="{$ContentEndDate.field}" placeholder="Megjelenés végső dátuma" name="{$ContentEndDate.field}" value="{if $ContentEndDate.postdata != '0000-00-00 00:00:00'}{$ContentEndDate.postdata}{/if}">
                                            {if {$ContentEndDate.error}}
                                                <span class="help-block">{$ContentEndDate.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group">
                                            <label>Profilkép</label>
                                            <input type="file" name="UserImage" value="">
                                        </div>
                                        <div class="form-group{if {$ContentLayout.error}} has-error {/if}">
                                            <label>Elrendezés</label>
                                            {html_options class="form-control" name=$ContentLayout.field options=$layout selected=$ContentLayout.postdata}
                                            {if {$ContentLayout.error}}
                                                <span class="help-block">{$ContentLayout.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group {if {$ContentActive.error}} has-error {/if}">
                                            <label>Aktív: </label><br>
                                            <label class="radio-inline">
                                                <input type="radio" name="{$ContentActive.field}" {if {isset($ContentActive.postdata) && $ContentActive.postdata == 1}}checked{/if} value="1"> igen
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="{$ContentActive.field}" {if {isset($ContentActive.postdata) && $ContentActive.postdata == 0}}checked{/if} value="0"> nem
                                            </label>
                                            {if {$ContentActive.error}}
                                                <span class="help-block">{$ContentActive.error}</span>
                                            {/if}
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8"></div>
                                            <div class="col-xs-4">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            {if isset($ContentData) || isset($ContentVersion)}
                            <div class="col-md-4">
                                <div class="box box-success">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-8"></div>
                                            <div class="col-xs-4">
                                            </div>
                                        </div>
                                        {if isset($ContentData)}
                                        <h4>Tartalom adatai</h4>
                                        <p><label>Szerző: </label> <span class="content-data">{$ContentData.name}</span></p>
                                        <p><label>Létrehozás dátuma: </label> <span class="content-data">{$ContentData.create_date}</span></p>
                                        <p><label>Megtekintések száma: </label> <span class="content-data">{if !empty($ContentData.hits)}{$ContentData.hits}{else}0{/if}</span></p>
                                        <p><label>Módosítások száma: </label> <span class="content-data">{$ContentData.modifyNumber}</span></p>
                                        <hr/>
                                        {/if}
                                        {if isset($ContentVersion)}
                                        <h4>Verziók</h4>
                                        <div class="btn btn-success btn-block btn-flat modified_button">Verziók listája</div>
                                        <div class="modified">
                                            {$i = $ContentVersion|@count}
                                            {foreach from=$ContentVersion key=kulcs item=elem}
                                                {$i}. {$elem.name} {$elem.modify_date}<br/>
                                                <a href="{$base_url}admin/contentedit/{$elem.content_id}/{$elem.id}">Megtekintés</a><br/><br/>
                                                {$i = $i-1}
                                            {/foreach}
                                        </div>
                                        {/if}
                                    </div>
                                    </form>
                                </div>
                            </div>
                            {/if}
                        </div>
                    </div>
                </div>
            <!--/div-->
        </div>
    </div>
</section>