<section class="content-header">
    <h1>{if isset($Modify)}Tartalom kategória módosítása{else}Új tartalom kategória{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Tartalom</a></li>
        <li class="active">Új tartalom kategória</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Menü</label>
                            <select class="form-control select2 select2-hidden-accessible" name="ContentCategoryParent" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                <option value="0" selected >Főkategória</option>
                                {$categorySelect}
                            </select>
                        </div>
                        <div class="form-group {if {$ContentCategoryName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control" id="{$ContentCategoryName.field}" placeholder="Név" name="{$ContentCategoryName.field}" value="{$ContentCategoryName.postdata}">
                            {if {$ContentCategoryName.error}}
                                <span class="help-block">{$ContentCategoryName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$ContentCategoryAlias.error}} has-error {/if}">
                            <label>Alias</label>
                            <input type="text" class="form-control" id="{$ContentCategoryAlias.field}" placeholder="Alias" name="{$ContentCategoryAlias.field}" value="{$ContentCategoryAlias.postdata}">
                            {if {$ContentCategoryAlias.error}}
                                <span class="help-block">{$ContentCategoryAlias.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$ContentCategoryActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$ContentCategoryActive.field}" {if {isset($ContentCategoryActive.postdata) && $ContentCategoryActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$ContentCategoryActive.field}" {if {isset($ContentCategoryActive.postdata) && $ContentCategoryActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$ContentCategoryActive.error}}
                                <span class="help-block">{$ContentCategoryActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>