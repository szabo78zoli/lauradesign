<section class="content-header">
    <h1>{if isset($Modify)}Tartalom módosítása{else} Új tartalom{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Tartalom</a></li>
        <li class="active">{if isset($Modify)}Tartalom módosítása{else} Új tartalom{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Nyelvek</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            {foreach from=$lang key=kulcs item=elem}
                                <li role="presentation" class="{if $elem@first}active{/if}"><a href="#{$elem.abbreviation}" aria-controls="{$elem.abbreviation}" role="tab" data-toggle="tab">{$elem.name}</a></li>
                            {/foreach}
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!--form role="form" action="" method="post"-->
                            {foreach from=$lang key=kulcsForm item=elemForm}
                            <div role="tabpanel" class="tab-pane {if $elemForm@first}active{/if}" id="{$elemForm.abbreviation}">
                                <hr>
                                {if isset($error_message)}
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                                        {$error_message}
                                    </div>
                                {/if}
                                {if isset($success_message)}
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                                        {$success_message}
                                    </div>
                                {/if}
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="box box-primary">
                                            <form role="form" action="" method="post">
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group {if {$ContentName{$elemForm.abbreviation}.error}} has-error {/if}">
                                                                <label>Név</label>
                                                                <input type="text" class="form-control converted-charachters-source" id="{$ContentName{$elemForm.abbreviation}.field}" placeholder="Név" name="{$ContentName{$elemForm.abbreviation}.field}" value="{$ContentName{$elemForm.abbreviation}.postdata}">
                                                                {if {$ContentName{$elemForm.abbreviation}.error}}
                                                                    <span class="help-block">{$ContentName{$elemForm.abbreviation}.error}</span>
                                                                {/if}
                                                            </div>
                                                            <div class="form-group{if {$ContentAlias{$elemForm.abbreviation}.error}} has-error {/if}">
                                                                <label>Alias</label>
                                                                <input type="text" class="form-control converted-charachters-destination" id="{$ContentAlias{$elemForm.abbreviation}.field}" placeholder="Alias" name="{$ContentAlias{$elemForm.abbreviation}.field}" value="{$ContentAlias{$elemForm.abbreviation}.postdata}">
                                                                {if {$ContentAlias{$elemForm.abbreviation}.error}}
                                                                    <span class="help-block">{$ContentAlias{$elemForm.abbreviation}.error}</span>
                                                                {/if}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group{if {$ContentMetaDescription{$elemForm.abbreviation}.error}} has-error {/if}">
                                                                <label>Meta leírás</label>
                                                                <textarea class="form-control" id="{$ContentMetaDescription{$elemForm.abbreviation}.field}" placeholder="Meta leírás" name="{$ContentMetaDescription{$elemForm.abbreviation}.field}">{$ContentMetaDescription{$elemForm.abbreviation}.postdata}</textarea>
                                                                {if {$ContentMetaDescription{$elemForm.abbreviation}.error}}
                                                                    <span class="help-block">{$ContentMetaDescription{$elemForm.abbreviation}.error}</span>
                                                                {/if}
                                                            </div>
                                                            <div class="form-group{if {$ContentMetaKeyword{$elemForm.abbreviation}.error}} has-error {/if}">
                                                                <label>Meta kulcsszavak</label>
                                                                <textarea class="form-control" id="{$ContentMetaKeyword{$elemForm.abbreviation}.field}" placeholder="Meta kulcsszavak" name="{$ContentMetaKeyword{$elemForm.abbreviation}.field}">{$ContentMetaKeyword{$elemForm.abbreviation}.postdata}</textarea>
                                                                {if {$ContentMetaKeyword{$elemForm.abbreviation}.error}}
                                                                    <span class="help-block">{$ContentMetaKeyword{$elemForm.abbreviation}.error}</span>
                                                                {/if}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group{if {$ContentShortDescription{$elemForm.abbreviation}.error}} has-error {/if}">
                                                        <label>Bevezető szöveg</label>
                                                        <textarea class="form-control" id="{$ContentShortDescription{$elemForm.abbreviation}.field}" placeholder="Bevezető szöveg" name="{$ContentShortDescription{$elemForm.abbreviation}.field}">{$ContentShortDescription{$elemForm.abbreviation}.postdata}</textarea>
                                                        {if {$ContentShortDescription{$elemForm.abbreviation}.error}}
                                                            <span class="help-block">{$ContentShortDescription{$elemForm.abbreviation}.error}</span>
                                                        {/if}
                                                    </div>
                                                    <div class="form-group{if {$ContentDescription{$elemForm.abbreviation}.error}} has-error {/if}">
                                                        <label>Leírás</label>
                                                        <textarea class="form-control" id="{$ContentDescription{$elemForm.abbreviation}.field}" placeholder="Leírás" name="{$ContentDescription{$elemForm.abbreviation}.field}">{$ContentDescription{$elemForm.abbreviation}.postdata}</textarea>
                                                        {if {$ContentDescription{$elemForm.abbreviation}.error}}
                                                            <span class="help-block">{$ContentDescription{$elemForm.abbreviation}.error}</span>
                                                        {/if}
                                                    </div>
                                                    <div class="form-group{if {$ContentContent{$elemForm.abbreviation}.error}} has-error {/if}">
                                                        <label>Tartalom</label>
                                                        <textarea class="form-control" id="{$ContentContent{$elemForm.abbreviation}.field}" placeholder="Leírás" name="{$ContentContent{$elemForm.abbreviation}.field}">{$ContentContent{$elemForm.abbreviation}.postdata}</textarea>
                                                        {if {$ContentContent{$elemForm.abbreviation}.error}}
                                                            <span class="help-block">{$ContentContent{$elemForm.abbreviation}.error}</span>
                                                        {/if}
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-8"></div>
                                                        <div class="col-xs-4">
                                                            <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="box box-primary">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-xs-8"></div>
                                                    <div class="col-xs-4">
                                                        <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                                                    </div>
                                                </div>
                                                <div class="form-group {if {$ContentStartDate{$elemForm.abbreviation}.error}} has-error {/if}">
                                                    <label>Megjelenés kezdeti dátuma</label>
                                                    <input type="text" class="form-control start_date" id="{$ContentStartDate{$elemForm.abbreviation}.field}" placeholder="Megjelenés kezdeti dátuma" name="{$ContentStartDate{$elemForm.abbreviation}.field}" value="{$ContentStartDate{$elemForm.abbreviation}.postdata}">
                                                    {if {$ContentStartDate{$elemForm.abbreviation}.error}}
                                                        <span class="help-block">{$ContentStartDate{$elemForm.abbreviation}.error}</span>
                                                    {/if}
                                                </div>
                                                <div class="form-group{if {$ContentEndDate{$elemForm.abbreviation}.error}} has-error {/if}">
                                                    <label>Megjelenés végső dátuma</label>
                                                    <input type="text" class="form-control end_date" id="{$ContentEndDate{$elemForm.abbreviation}.field}" placeholder="Megjelenés végső dátuma" name="{$ContentEndDate{$elemForm.abbreviation}.field}" value="{$ContentEndDate{$elemForm.abbreviation}.postdata}">
                                                    {if {$ContentEndDate{$elemForm.abbreviation}.error}}
                                                        <span class="help-block">{$ContentEndDate{$elemForm.abbreviation}.error}</span>
                                                    {/if}
                                                </div>
                                                <div class="form-group">
                                                    <label>Profilkép</label>
                                                    <input type="file" name="UserImage" value="">
                                                </div>
                                                <div class="form-group{if {$ContentLayout{$elemForm.abbreviation}.error}} has-error {/if}">
                                                    <label>Elrendezés</label>
                                                    {html_options class="form-control" name=$ContentLayout{$elemForm.abbreviation}.field options=$layout selected=$ContentLayout{$elemForm.abbreviation}.postdata}
                                                    {if {$ContentLayout{$elemForm.abbreviation}.error}}
                                                        <span class="help-block">{$ContentLayout{$elemForm.abbreviation}.error}</span>
                                                    {/if}
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-8"></div>
                                                    <div class="col-xs-4">
                                                        <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="{$elemForm.abbreviation}">
                            </div>
                            {/foreach}
                            <!--/form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>