<section class="content-header">
    <h1>{if isset($Modify)}Elrendezés módosítása{else}Új elrendezés{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Tartalom</a></li>
        <li class="active">{if isset($Modify)}Elrendezés módosítása{else}Új elrendezés{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}">
                    <div class="box-body">
                        <div class="form-group {if {$ContentLayoutName.error}} has-error {/if}">
                            <label>Név</label>
                            <input type="text" class="form-control" id="{$ContentLayoutName.field}" placeholder="Név" name="{$ContentLayoutName.field}" value="{$ContentLayoutName.postdata}">
                            {if {$ContentLayoutName.error}}
                                <span class="help-block">{$ContentLayoutName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$ContentLayoutTemplate.error}} has-error {/if}">
                            <label>Template</label>
                            <input type="text" class="form-control" id="{$ContentLayoutTemplate.field}" placeholder="Template" name="{$ContentLayoutTemplate.field}" value="{$ContentLayoutTemplate.postdata}">
                            {if {$ContentLayoutTemplate.error}}
                                <span class="help-block">{$ContentLayoutTemplate.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$ContentLayoutActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$ContentLayoutActive.field}" {if {isset($ContentLayoutActive.postdata) && $ContentLayoutActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$ContentLayoutActive.field}" {if {isset($ContentLayoutActive.postdata) && $ContentLayoutActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$ContentLayoutActive.error}}
                                <span class="help-block">{$ContentLayoutActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>