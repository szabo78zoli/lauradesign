<div class="input_field">
    <div class="col-sm-4">
        <label>Keresés: </label>
        <input class="form-control input-sm" placeholder="Név" aria-controls="search type="search" name="{$ContentFilter.field}" value="{$ContentFilter.postdata}"/>
    </div>
    <div class="col-sm-4">
        <label>Nyelv: </label>
        {html_options class="form-control" name=$ContentLang.field options=$Lang selected=$ContentLang.postdata}
    </div>
    <div class="col-sm-4">
        <input type="submit" name="SearchBtn" value="Keresés" class="filter_submit btn btn-block btn-primary">
        <input type="submit" name="BasicBtn" value="Alaphelyzet" class="filter_submit btn btn-block btn-primary">
    </div>
</div>