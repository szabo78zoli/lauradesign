<section class="content-header">
    <h1>Jogosultság csoport lista</h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Főoldal</li>
        <li>Jogosultság</li>
        <li class="active">Jogosultság csoport lista</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <form action="" name="{$FormName}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_Lenght" id="example1_Lenght">
                                        <label>
                                            <select name="Lenght" aria-controls="example" class="form-control input-sm">
                                                {$listLenght = ";"|explode:$listLenght}
                                                {foreach $listLenght as $elem}
                                                    <option value="{$elem}">{$elem}</option>
                                                {/foreach}
                                            </select>
                                            találat oldalanként
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div id="example1_filter" class="dataTables_filter">
                                        {$FilterForm}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="admin-list" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        {if isset($error_message)}
                                        <div class="alert alert-warning alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-warning"></i> Figyelem!</h4>
                                            {$error_message}
                                        </div>
                                        {else}
                                        <thead>
                                        <tr role="row">
                                            <form action="" name="{$OrderFormName}" method="post" enctype="multipart/form-data">
                                                {foreach from=$table_head key=kulcs item=elem}
                                                    <th>{$kulcs}
                                                        {if !empty($elem)}
                                                            <button class="order_btn" type="submit" name="OrderBtn" value="{$elem}">
                                                                {if isset($RightGroupOrderValue) && !empty($RightGroupOrderValue)}
                                                                    {foreach from=$RightGroupOrderValue key=kulcs item=value}
                                                                        {if {$kulcs} == $elem}
                                                                            {if $value == "ASC"}
                                                                                <i class="glyphicon glyphicon-sort-by-attributes-alt"></i>
                                                                            {/if}
                                                                            {if $value == "DESC"}
                                                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                                                            {/if}
                                                                        {else}
                                                                            <i class="glyphicon glyphicon-sort" style="opacity: 0.3;"></i>
                                                                        {/if}
                                                                    {/foreach}
                                                                {else}
                                                                    <i class="glyphicon glyphicon-sort" style="opacity: 0.3;"></i>
                                                                {/if}
                                                            </button>
                                                        {/if}
                                                    </th>
                                                {/foreach}
                                            </form>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {foreach from=$list key=kulcs item=elem}
                                            <tr role="row">
                                                <td class="sorting_1">{$elem.name}</a></td>
                                                <td>{$elem.site_type}</td>
                                                <td>
                                                    <span class="label label-primary edit-label">
                                                        <a href="admin/rightgroupedit/{$elem.id}">Szerkesztés</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    {if $elem.active}
                                                        <button type="submit" name="AktivBtn" class="btn btn-block btn-success btn-xs" value="{$elem.id}" title="Inaktiválás">Aktív</button>
                                                    {else}
                                                        <button type="submit" name="InAktivBtn" class="btn btn-block btn-warning  btn-xs" value="{$elem.id}" title="Aktiválás" >Inaktív</button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <button type="submit" name="DeleteBtn" class="btn btn-block btn-danger  btn-xs" value="{$elem.id}" title="Aktiválás" onclick="return window.confirm('Biztosan törli a(z) {$elem.name} jogosultságot?');" >Törlés</button>
                                                </td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th rowspan="1" colspan="1">
                                                    Név
                                                </th>
                                                <th rowspan="1" colspan="1">
                                                    Modul URI
                                                </th>
                                                <th rowspan="1" colspan="1">
                                                    Szerkesztés
                                                </th>
                                                <th rowspan="1" colspan="1">
                                                    Aktív
                                                </th>
                                                <th rowspan="1" colspan="1">
                                                    Törlés
                                                </th>
                                            </tr>
                                        </tfoot>
                                        {/if}
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">
                                        Showing 1 to 10 of 57 entries
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                        <ul class="pagination">
                                            {$pagination}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>