<section class="content-header">
    <h1>{if isset($Modify)}Jogosultság csoport módosítása{else}Új jogosultság csoport{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Jogosultság</a></li>
        <li class="active">{if isset($Modify)}Jogosultság csoport módosítása{else}Új jogosultság csoport{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="{$FormName}">
                    <div class="box-body">
                        <div class="form-group {if {$AdminRightGroupName.error}} has-error {/if}">
                            <label for="exampleInputPassword1">Név</label>
                            <input type="text" class="form-control" id="{$AdminRightGroupName.field}" placeholder="Név" name="{$AdminRightGroupName.field}" value="{$AdminRightGroupName.postdata}">
                            {if {$AdminRightGroupName.error}}
                                <span class="help-block">{$AdminRightGroupName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$AdminRightGroupSiteType.error}} has-error {/if}">
                            <label for="exampleInputPassword1">Oldal típus</label>
                            {html_options class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name=$AdminRightGroupSiteType.field options=$SitetType selected=$AdminRightGroupSiteType.postdata}
                            {if {$AdminRightGroupSiteType.error}}
                                <span class="help-block">{$AdminRightGroupSiteType.error}</span>
                            {/if}
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="RightGroupId" class="" value="{$RightGroupIdValue}">
                        </div>
                        <div class="form-group{if {$AdminRightGroupRight.error}} has-error {/if}">
                            <label for="exampleInputPassword1">Mind kijelölése</label>
                                <input type="checkbox" name="checkall" class="checkbox checkall" value="">
                            {if {$AdminRightGroupRight.error}}
                                <div class="validate_error">{$AdminRightGroupRight.error}</div>
                            {/if}
                        </div>
                        <div class="form-group">
                            <label>Jogosultságok</label>
                            <div class="checkbox">
                                <div class="rights">
                                    {html_checkboxes class="checkbox" name=$AdminRightGroupRight.field options=$Right selected=$AdminRightGroupRight.postdata}
                                </div>
                            </div>
                        </div>
                        <div class="form-group {if {$AdminRightGroupActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$AdminRightGroupActive.field}" {if {isset($AdminRightGroupActive.postdata) && $AdminRightGroupActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$AdminRightGroupActive.field}" {if {isset($AdminRightGroupActive.postdata) && $AdminRightGroupActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$AdminRightGroupActive.error}}
                                <span class="help-block">{$AdminRightGroupActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>