<section class="content-header">
    <h1>Jogosultság lista</h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Főoldal</li>
        <li>Jogosultság</li>
        <li class="active">Jogosultság lista</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="form-inline">
                        <form action="" name="{$FormName}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>
                                        <select name="{$FormName}Lenght" class="form-control input-sm" onchange="this.form.submit()">
                                            {$listLenght = ";"|explode:$listLenght}
                                            {foreach $listLenght as $elem}
                                                <option value="{$elem}"{if $RightLenghtSelected == $elem} selected=selected {/if} >{$elem}</option>
                                            {/foreach}
                                        </select>
                                        találat oldalanként
                                    </label>
                                </div>
                                <div class="col-sm-6 text-right">
                                    {$FilterForm}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr>
                                    <table class="table table-bordered table-striped">
                                        {if isset($error_message)}
                                        <div class="alert alert-warning alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-warning"></i> Figyelem!</h4>
                                            {$error_message}
                                        </div>
                                        {else}
                                        <thead>
                                        <tr>
                                            <form action="" name="{$OrderFormName}" method="post" enctype="multipart/form-data">
                                                {foreach from=$table_head key=kulcs item=elem}
                                                    <th>{$kulcs}
                                                        {if !empty($elem)}
                                                            <button class="order_btn" type="submit" name="OrderBtn" value="{$elem}">
                                                                {if isset($RightOrderValue) && !empty($RightOrderValue)}
                                                                    {foreach from=$RightOrderValue key=kulcs item=value}
                                                                        {if {$kulcs} == $elem}
                                                                            {if $value == "ASC"}
                                                                                <i class="glyphicon glyphicon-sort-by-attributes-alt"></i>
                                                                            {/if}
                                                                            {if $value == "DESC"}
                                                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                                                            {/if}
                                                                        {else}
                                                                            <i class="glyphicon glyphicon-sort" style="opacity: 0.3;"></i>
                                                                        {/if}
                                                                    {/foreach}
                                                                {else}
                                                                    <i class="glyphicon glyphicon-sort" style="opacity: 0.3;"></i>
                                                                {/if}
                                                            </button>
                                                        {/if}
                                                    </th>
                                                {/foreach}
                                            </form>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {foreach from=$list key=kulcs item=elem}
                                            <tr role="row">
                                                <td>{$elem.id}</a></td>
                                                <td>{$elem.name}</a></td>
                                                <td>{$elem.modul_uri}</td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs" href="admin/rightedit/{$elem.id}">Szerkesztés</a>
                                                </td>
                                                <td>
                                                    {if $elem.active}
                                                        <button type="submit" name="AktivBtn" class="btn btn-block btn-success btn-xs" value="{$elem.id}" title="Inaktiválás">Aktív</button>
                                                    {else}
                                                        <button type="submit" name="InAktivBtn" class="btn btn-block btn-warning  btn-xs" value="{$elem.id}" title="Aktiválás" >Inaktív</button>
                                                    {/if}
                                                </td>
                                                <td>
                                                    <button type="submit" name="DeleteBtn" class="btn btn-danger btn-xs" value="{$elem.id}" title="Aktiválás" onclick="return window.confirm('Biztosan törli a(z) {$elem.name} jogosultságot?');" >Törlés</button>
                                                </td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">ID</th>
                                            <th rowspan="1" colspan="1">Név</th>
                                            <th rowspan="1" colspan="1">Modul URI</th>
                                            <th rowspan="1" colspan="1">Szerkesztés</th>
                                            <th rowspan="1" colspan="1">Aktív</th>
                                            <th rowspan="1" colspan="1">Törlés</th>
                                        </tr>
                                        </tfoot>
                                        {/if}
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <ul class="pagination">
                                            {$pagination}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
