<section class="content-header">
    <h1>{if isset($Modify)}Jogosultság módosítása{else}Új Jogosultság{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Jogosultság</a></li>
        <li class="active">{if isset($Modify)}Jogosultság módosítása{else}Új Jogosultság{/if}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                {if isset($error_message)}
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                        {$error_message}
                    </div>
                {/if}
                {if isset($success_message)}
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                        {$success_message}
                    </div>
                {/if}
                <form role="form" action="" method="post" name="">
                    <div class="box-body">
                        <div class="form-group {if {$AdminRightName.error}} has-error {/if}">
                            <label for="exampleInputPassword1">Név</label>
                            <input type="text" class="form-control" id="{$AdminRightName.field}" placeholder="Név" name="{$AdminRightName.field}" value="{$AdminRightName.postdata}">
                            {if {$AdminRightName.error}}
                                <span class="help-block">{$AdminRightName.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$AdminRightSiteType.error}} has-error {/if}">
                            <label for="exampleInputPassword1">Oldal típus</label>
                            {html_options class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name=$AdminRightSiteType.field options=$SiteType selected=$AdminRightSiteType.postdata}
                            {if {$AdminRightSiteType.error}}
                                <span class="help-block">{$AdminRightSiteType.error}</span>
                            {/if}
                        </div>
                        <div class="form-group{if {$AdminRightModulUri.error}} has-error {/if}">
                            <label for="exampleInputPassword1">Modul URI</label>
                            <input type="text" class="form-control" id="{$AdminRightModulUri.field}" placeholder="Név" name="{$AdminRightModulUri.field}" value="{$AdminRightModulUri.postdata}">
                            {if {$AdminRightModulUri.error}}
                                <span class="help-block">{$AdminRightModulUri.error}</span>
                            {/if}
                        </div>
                        <div class="form-group {if {$AdminRightActive.error}} has-error {/if}">
                            <label>Aktív: </label><br>
                            <label class="radio-inline">
                                <input type="radio" name="{$AdminRightActive.field}" {if {isset($AdminRightActive.postdata) && $AdminRightActive.postdata == 1}}checked{/if} value="1"> igen
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="{$AdminRightActive.field}" {if {isset($AdminRightActive.postdata) && $AdminRightActive.postdata == 0}}checked{/if} value="0"> nem
                            </label>
                            {if {$AdminRightActive.error}}
                                <span class="help-block">{$AdminRightActive.error}</span>
                            {/if}
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>