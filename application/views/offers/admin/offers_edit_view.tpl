<section class="content-header">
    <h1>{if isset($Modify)}Csomag ajánlat módosítása{else} Új csomag ajánlat{/if}</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Főoldal</a></li>
        <li><a href="#">Tartalom</a></li>
        <li class="active">{if isset($Modify)}Csomag ajánlat módosítása{else} Új csomag ajánlat{/if}</li>
    </ol>
</section>
<section class="Offer">
    <div class="row">
        <div class="col-md-12">
            <div class="box-body pad">
                <div>
                    <hr>
                    {if isset($error_message)}
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                            {$error_message}
                        </div>
                    {/if}
                    {if isset($success_message)}
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Sikeres adatmentés!</h4>
                            {$success_message}
                        </div>
                    {/if}
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box box-primary">
                                <form role="form" action="" method="post">
                                    <div class="box-body">
                                        <div class="form-group {if {$OfferName.error}} has-error {/if}">
                                            <label>Név</label>
                                            <input type="text" class="form-control converted-charachters-source" id="{$OfferName.field}" placeholder="Név" name="{$OfferName.field}" value="{$OfferName.postdata}">
                                            {if {$OfferName.error}}
                                                <span class="help-block">{$OfferName.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$OfferAlias.error}} has-error {/if}">
                                            <label>Alias</label>
                                            <input type="text" class="form-control converted-charachters-destination" id="{$OfferAlias.field}" placeholder="Alias" name="{$OfferAlias.field}" value="{$OfferAlias.postdata}">
                                            {if {$OfferAlias.error}}
                                                <span class="help-block">{$OfferAlias.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$OfferCost.error}} has-error {/if}">
                                            <label>Költség</label>
                                            <input type="text" class="form-control" id="{$OfferCost.field}" placeholder="Költség" name="{$OfferCost.field}" value="{$OfferCost.postdata}">
                                            {if {$OfferCost.error}}
                                                <span class="help-block">{$OfferCost.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$OfferMetaDescription.error}} has-error {/if}">
                                            <label>Meta leírás</label>
                                            <textarea class="form-control" id="{$OfferMetaDescription.field}" placeholder="Meta leírás" name="{$OfferMetaDescription.field}">{$OfferMetaDescription.postdata}</textarea>
                                            {if {$OfferMetaDescription.error}}
                                                <span class="help-block">{$OfferMetaDescription.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$OfferMetaKeywords.error}} has-error {/if}">
                                            <label>Meta kulcsszavak</label>
                                            <textarea class="form-control" id="{$OfferMetaKeywords.field}" placeholder="Meta kulcsszavak" name="{$OfferMetaKeywords.field}">{$OfferMetaKeywords.postdata}</textarea>
                                            {if {$OfferMetaKeywords.error}}
                                                <span class="help-block">{$OfferMetaKeywords.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$OfferDescription.error}} has-error {/if}">
                                            <label>Leírás</label>
                                            <textarea class="form-control" id="{$OfferDescription.field}" placeholder="Leírás" name="{$OfferDescription.field}">{$OfferDescription.postdata}</textarea>
                                            {if {$OfferDescription.error}}
                                                <span class="help-block">{$OfferDescription.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group{if {$OfferContent.error}} has-error {/if}">
                                            <label>Tartalom</label>
                                            <textarea class="form-control" id="{$OfferContent.field}" placeholder="Leírás" name="{$OfferContent.field}">{$OfferContent.postdata}</textarea>
                                            {if {$OfferContent.error}}
                                                <span class="help-block">{$OfferContent.error}</span>
                                            {/if}
                                        </div>
                                        <div class="form-group {if {$OfferActive.error}} has-error {/if}">
                                            <label>Aktív: </label><br>
                                            <label class="radio-inline">
                                                <input type="radio" name="{$OfferActive.field}" {if {isset($OfferActive.postdata) && $OfferActive.postdata == 1}}checked{/if} value="1"> igen
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="{$OfferActive.field}" {if {isset($OfferActive.postdata) && $OfferActive.postdata == 0}}checked{/if} value="0"> nem
                                            </label>
                                            {if {$OfferActive.error}}
                                                <span class="help-block">{$OfferActive.error}</span>
                                            {/if}
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8"></div>
                                            <div class="col-xs-4">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="SaveBtn" value="SaveBtn">Mentés</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>