<section class="background-white text-center pt-8 about">
<div class="container">
    <div class="row">
        <div class="section-title text-center center col-sm-12 wow zoomIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: zoomIn;">
            <h2>{$tartalom.name}</h2>
        </div>
        <div class="col-xs-12 col-sm-12 wow zoomIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: zoomIn;">
            {$tartalom.content}
        </div>
    </div>
</div>
</section>