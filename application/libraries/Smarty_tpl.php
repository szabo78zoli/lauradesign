<?php

if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );

require_once( 'application/third_party/smarty_tpl/libs/Smarty.class.php' );

class smarty_tpl extends Smarty
{
    public function __construct()
    {
        parent::__construct();

        $this->caching = 1;
        $this->setTemplateDir( APPPATH . 'views' );
        $this->setCompileDir( APPPATH . 'third_party/smarty/templates_c' );
        $this->setConfigDir( APPPATH . 'third_party/smarty/configs' );
        $this->setCacheDir( APPPATH . 'cache/smarty' );
        $this->cache_lifetime = 1;
        $this->caching = 0;
    }

    //if specified template is cached then display template and exit, otherwise, do nothing.
    public function useCached( $tpl, $cacheId = null )
    {
        if ( $this->isCached( $tpl, $cacheId ) )
        {
            $this->display( $tpl, $cacheId );
            exit();
        }
    }
}